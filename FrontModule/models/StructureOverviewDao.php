<?php
/**
 * Model zacházající s uloženou procedurou stromové struktury
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;
use Exception;

class StructureOverviewDao extends BaseModel
{

	/** @var string  table name */
	protected $table = 'getdesc_fixedlimit2';

	/** @var string|array  primary key column name */
	protected $primary = 'idu';

	/** skeleton pattern */
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new StructureOverviewDao();
		return $me;
	}

	public $limit = 10;
	private $act = 0;

	public $levels;
	public $idus;
	public $counts;

    public function find($id = null)
	{
		// should be compatibile with parent
		if( !is_numeric($id) )
			throw new Exception('Není zadáno ID od kterého mám procházet strukturu.');

		$r = $this->connection->select('*')->from($this->table.'('.$id.', '.$this->limit.','. $this->act.')')->orderBy('act_idu');
		$tmp = array();
		$tmp2 = array();
		$tmp3 = array();
		$tmp4 = array();
		for( $i=1; $i<=$this->limit; $i++ ) $tmp3[$i]=0;
		foreach( $r as $v )
		{
			$tmp[ $v['act_level'] ][] = $v['act_idu'];
			$tmp2[] = $v['act_idu'];
			$tmp3[ $v['act_level'] ]++;
		}
		$this->levels = $tmp;
		$this->idus = $tmp2;
		$this->counts = $tmp3;

		return $r;
	}



}
