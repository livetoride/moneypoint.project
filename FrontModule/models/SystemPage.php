<?php
/**
 * Model zacházající s tabulkou kariéra
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class SystemPage
{
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new SystemPage();
		return $me;
	}

	public function show( $pagePriznak, $presenter )
	{
		$presenter->forward(':Front:News:show', $pagePriznak);
	}

}
