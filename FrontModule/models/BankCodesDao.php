<?php
/**
 * Model pro manipulaci s objednávkami
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class BankCodesDao extends BaseModel
{

	/** @var string  table name */
	protected $table = 'bank_codes';

	/** @var string|array  primary key column name */
	protected $primary = 'bank_code';

	protected $vals = array(
		'bank_code' => 'int',
		'bank_name' => 'varchar'
	);

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new BankCodesDao();
		return $me;
	}


}
