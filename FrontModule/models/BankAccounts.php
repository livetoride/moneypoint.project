<?php
/**
 * Model zacházající s tabulkou kariéra
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class BankAccounts extends BaseModel
{

	/** @var string  table name */
	protected $table = 'bank_accounts';

	/** @var string|array  primary key column name */
	protected $primary = 'id_account';

	protected $vals = array(
		'id_account' => 'int',
		'idu' => 'integer',
		'account' => 'integer',
		'bank_code' => 'integer',
		'insert_timestamp' => 'timestamp'

	);


	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new BankAccounts();
		return $me;
	}


	public function findByIdu($idu = null)
	{
		if( $idu === null ) throw new \Exception( 'Zadej hledanou hodnotu' );
		return $this->findAll()->where("idu=%i", $idu)->orderBy('insert_timestamp DESC');
	}


}
