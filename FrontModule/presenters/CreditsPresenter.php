<?php
/**
 * Presenter obsluhující požadavky operací s kredity
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace FrontModule;

use AntoninRykalsky\CreditsAccounts;
use dibi;
use Nette\Environment;

class CreditsPresenter extends \Base_MembersPresenter
{
	/** @var \MoneyPoint\PartnerOrderFacade */
	protected $partnerOrderFacade;
	
	/** @var \MoneyPoint\TransactionFacade */
	protected $transactionFacade;
	
	var $idu;
	
	public function inject( 
		\MoneyPoint\PartnerOrderFacade $partnerOrderFacade ,
		\MoneyPoint\TransactionFacade $transactionFacade ) {
		$this->partnerOrderFacade = $partnerOrderFacade;
		$this->transactionFacade = $transactionFacade;
	}

	public function actionAccount( $idu )
	{
		unset($idu); // Strict standards: Declaration should be compatible with parent

		$this->template->title = 'Kreditový účet';
		$user = Environment::getSession('user');
		$this->idu = $user->idu;
	}

	protected function createComponentList()     {
		$ds = Uzivatele::get()->getDataSource();
		$ds->where('idu>0');
		$model = new DataGrid\DataSources\Dibi\DataSource($ds);

		$grid = new DataGrid\DataGrid;
		$renderer = new DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setRenderer($renderer);
		$translator = new \GettextTranslator( APP_DIR . '/locale/datagrid.mo' );
		$grid->setTranslator($translator);

		$grid->itemsPerPage = 10;
		$grid->setDataSource($model);
		$grid->multiOrder = FALSE;

		$operations = array();
//		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, Uzivatele::get()->getPrimary());

		$grid->addNumericColumn('idu', 'ID');
		$grid['idu']->addFilter();
		$grid['jmeno']->addFilter();

		return $grid;
	}

	protected function createComponentCreditsAccount()
    {
		$ds = CreditsAccounts::get()->getDataSource();
		$ds->orderBy('change_timestamp', dibi::DESC);
		$ds->orderBy('id', dibi::DESC);
		$ds->where('idu=%i', $this->idu );
		$model = new \DataGrid\DataSources\Dibi\DataSource($ds);


		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setRenderer($renderer);
		$translator = new \GettextTranslator( APP_DIR . '/locale/datagrid.mo' );
		$grid->setTranslator($translator);

		$grid->itemsPerPage = 10;
		$grid->setDataSource($model);
		$grid->multiOrder = FALSE;

		$operations = array(); // define operations
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, CreditsAccounts::get()->getPrimary()); // allows checkboxes to do operations with more rows


		$grid->addColumn('change_timestamp', 'Datum');
		$grid['change_timestamp']->formatCallback[] = 'FormatingHelpers::stamp2FullDateWithoutSec';
		$grid['change_timestamp']->addDefaultSorting('desc');

		$grid->addColumn('reason', 'Popis');
		$grid->addColumn('credits_change', 'Změna kreditu');
		$grid['credits_change']->formatCallback[] = 'FormatingHelpers::intPlusSign';
		$grid->addColumn('credits_left', 'Zůstatek');


		return $grid;
    }
	
	public function actionDefault()
	{
		$this->template->title = 'Historie kreditu';
	}
	
	// historie kred uctu
	public function createComponentCreditsAccountList()
	{
		$c = new \MoneyPoint\CreditsAccountList( $this->partnerOrderFacade, $this->transactionFacade, $this->commonTranslator );
		$c->idu = \MoneyPoint\LoggedMember::get()->idu;
		return $c;
	}
	// /historie kred uctu
}
