<?php
/**
 * Administrace struktury
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace FrontModule;

class StructurePresenter extends \Base_MembersPresenter
{
	/** @var \MoneyPoint\PosibleRewardsFacade */
	protected $posibleRewardsFacade;
	function inject( \MoneyPoint\PosibleRewardsFacade $posibleRewardsFacade)
    {
        $this->posibleRewardsFacade = $posibleRewardsFacade;
    }

	/** @var \MoneyPoint\FixedCommisionFacade */
	protected $fixedCommisionFacade;
	protected $maxLevel;

	function injectPartner(\MoneyPoint\FixedCommisionFacade $fixedCommisionFacade) {
		$this->fixedCommisionFacade = $fixedCommisionFacade;
		$this->maxLevel = $this->fixedCommisionFacade->getLastLevel();
	}
	
	public function actionPosibleRewards()
	{
		$this->template->title = 'Potenciální odměny';
		$this->template->smallNetworkMenu = 1;
		
		$idu = \MoneyPoint\LoggedMember::get()->idu;
		
		$this->template->rewards = $this->posibleRewardsFacade->getRewards( $idu );
		
		#print_r( $this->template->rewards );exit;
		
		$this->template->firstLineRewards = $this->posibleRewardsFacade->getFirstLineRewards( $idu );
		$this->template->creditsLeft = (int)@\AntoninRykalsky\CreditsAccounts::get()->actualState( $idu )->credits_left;
	}
	
	public function actionFirstLine()
	{
		$this->template->title = '1.linie';
		$this->template->smallNetworkMenu = 1;
		$this->template->maxLevel = $this->maxLevel;
		$this->template->maxSubLevel = $this->maxLevel-1;

		$idu = \MoneyPoint\LoggedMember::get()->idu;
		// prvni lajna
		\AntoninRykalsky\StructureOverviewDao::get()->limit = 1;
		\AntoninRykalsky\StructureOverviewDao::get()->find( $idu );
		$levels = \AntoninRykalsky\StructureOverviewDao::get()->levels;
		

		// dalsi lajny
		// BBB
		\AntoninRykalsky\StructureOverviewDao::get()->limit = 9;

		if( !empty( $levels[1]))
		{
			$u = \DAO\Uzivatele::get()->findAll()->where('idu in %in', @$levels[1] )->fetchAssoc( 'idu' );

			$flDetails = \DAO\MpFirstLine::get()->findAll()->where('idu in %in', @$levels[1] )->fetchAssoc( 'idu' );

			$this->template->firstLineStats = $firstLineStats = $this->myRewardsFirstLine( $idu );

			$details = array();
			$abadons = array();
			foreach( $levels[1] as $idu2 )
			{
				// nastavime uzivatele
				$details[$idu2]['user'] = $u[$idu2];

				if(  $u[$idu2]['idrole'] == 6)
				{
					$abadons[] = \DAO\UzivateleAbadon::get()->findAll()->where('idu=%i', $idu2 )->orderBy('id', 'desc')->fetch();
				}

				// hledame v siti podclenu
				\AntoninRykalsky\StructureOverviewDao::get()->find( $idu2 );
				$counts = \AntoninRykalsky\StructureOverviewDao::get()->counts;

					// pocet v prvni podlinii
					$details[$idu2]['1linecount'] = $counts[1];

					// pocet ve vsech podliniich
					$tc = 0;
					for( $i=1; $i<=9; $i++ ) $tc+=$counts[$i];
					$details[$idu2]['totalcount'] = $tc;

					$m = new \MoneyPoint\Member($idu2);

					$details[$idu2]['creditsTree'] = $flDetails[$idu2]->credits;
					$details[$idu2]['rewardsTree'] = $flDetails[$idu2]->rewards;

				// hledame kdo kam patri
				$iduBelongsTo = array();
				$levels2 = \AntoninRykalsky\StructureOverviewDao::get()->levels;
				foreach( $levels2 as $idu )	$iduBelongsTo[ $idu2 ] = $idu;

			}
		} else {
			$abadons = array();
			$details = array();
		}

		$this->template->abadons = $abadons;
		$this->template->firstLine = $details;


	}

	private function myRewardsFirstLine( $myIdu )
	{
		$firstLine = \DAO\Uzivatele::get()->findAll()->where('idusponzor=%i', $myIdu )->fetchPairs('idu', 'idu');
		$idus = array_keys( $firstLine );

		$details = \DAO\MpStructureOverview::get()->findAll('credits_parent, rewards_parent, idu')->where('idu IN %in', $idus )->fetchAssoc( 'idu' );
		foreach( $details as $k => $v)
		{
			$details[$k]= array();
			$details[$k]['credits_parent'] = $v->credits_parent;
			$details[$k]['rewards_parent'] = $v->rewards_parent;
		}
		return $details;
	}

	private function myRewardsFromLevels( $myIdu )
	{
		\AntoninRykalsky\StructureOverviewDao::get()->find( $myIdu );
		$levels = \AntoninRykalsky\StructureOverviewDao::get()->levels;
		// $counts = \AntoninRykalsky\StructureOverviewDao::get()->counts;

		$details = (array)\DAO\MpStructureOverview::get()->findAll()->where('idu=%i', $myIdu )->fetch();
		// $rewards = \dibi::query("SELECT line, SUM( points_change ) FROM mp_points_accounts WHERE id_type=1 AND idu=$idu GROUP BY line;")->fetchAssoc('line');

		$structureDetails = array();
		foreach( $levels as $level => $idus )
		{
//			$orders = \dibi::query('SELECT SUM( credits_sum ) FROM orders WHERE idu IN ('. implode(',', (array)$idus) .')');
//			$structureDetails[$level]['creditsSum'] = (int)$orders->fetch()->sum;
			$structureDetails[$level]['creditsSum'] = $details['credits_'.$level];
			$structureDetails[$level]['clients'] = $details['users_'.$level];
			$structureDetails[$level]['rewards'] = $details['rewards_'.$level];
//			$structureDetails[$level]['rewards'] = (int)@$rewards[$level]->sum;
		}
		
		if( count( $structureDetails) < $this->maxLevel )
		{
			for( $i=1; $i<=$this->maxLevel; $i++ )
			{
				if( empty( $structureDetails[$i] ))
				$structureDetails[$i]=array( 'creditsSum'=>0, 'clients'=>0, 'rewards'=>0 );
			}
		}

		return $structureDetails;
	}

	public function actionOverview()
	{
		$this->template->title = 'Malá síť';
		$this->template->smallNetworkMenu = 1;

		// zjisti idu
		$idu = \MoneyPoint\LoggedMember::get()->idu;
		$this->template->structure = $this->myRewardsFromLevels( $idu );
	}
}
