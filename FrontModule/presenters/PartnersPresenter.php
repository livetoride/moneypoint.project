<?php
/**
 *
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace FrontModule;

use AntoninRykalsky as AR;
use AntoninRykalsky\CreditsAccounts;
use LogicException;
use MpPartnerDetail;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Environment;

class PartnersPresenter extends \Base_GuestPresenter
{
	/** @var \MoneyPoint\PartnersFacade */
	protected $partnersFacade;
	public function __construct(\MoneyPoint\PartnersFacade $partnersFacade 
	) {
		$this->partnersFacade = $partnersFacade;
	}

	
	protected function startup() {
		$this->template->menukey = 1;
		parent::startup();

	}
	public function renderCentropol( )
	{
		$this->template->title = 'CENTROPOL';
		$idu = \AntoninRykalsky\SystemUser::get()->idu;
		$this->template->userLogged = !empty( $idu );
	}
	public function renderSmartNetwork( )
	{
		$this->template->title = 'SMART NETWORK BUSINESS CENTER';
	}
	public function renderCestovatelskyKlub( )
	{
		$this->template->title = 'CESTOVATELSKÝ KLUB';
	}
	public function renderList( $country )
    {
		
		
		$mapping = array ('cr' => 'ČR', 'sr' => 'SR', 'ostatni' => 'Ostatní' );
		//throw new InvalidStateException('a');
		$title = 'Partneři';
		if( !empty( $mapping[$country] ))
		{
			$title.=' - '.$mapping[$country];
		}
		$this->template->title = $title;
		
		if( $country == 'cr' )
		{
			$this->setView('cr');
			return;
		}
		
		$this->template->detailComponent = $d = $this['mpPartnerDetail'];

		$this->template->logged = \AntoninRykalsky\SystemUser::isLogged();
		$partnerSearch = Environment::getSession('user')->partnerSearch;
		$def = $this->partnersFacade->getPartners( $this->tags, $partnerSearch, $mapping[$country] );
		
		//\Doctrine\Common\Util\Debug::dump($def); exit;
		/* @var $p \MoneyPoint\Entity\Partner */

		foreach( $def as $p )
		{
			if( $p->getImg() === null ) continue;
			$img = $this->loadImg( $p->getId(), $p->getImg() );
			$p->setLocalImg( $img );
		}
		$pn = $this->partnersFacade->getPartnersNames();
		// \Doctrine\Common\Util\Debug::dump($def);exit; 
		$this->template->pn = $pn;
		
		$this->template->partners = $def;
		$this->template->tags = $this->getUsedTags($country);
		
		$this['searchForm']->setDefaults( array( 'tags' => @$partnerSearch['tags']) );
    }
	
	private function loadImg( $id, $img )
	{
		$localName = '/files/partners/'.$id;
		if(file_exists(WWW_DIR.$localName))
			return $localName;

		if( empty($img)) return;
		
		$img = file_get_contents( $img );
		file_put_contents( WWW_DIR. $localName, $img);
		return $localName;
	}

	private	function check_site_existence($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 1); //nevypisovat hlavičky
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //výsledek jako string
		curl_setopt($ch, CURLOPT_URL, $url); //nastavení url
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1); //nasleduj presmerovani
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); //nasleduj presmerovani
		$data = curl_exec($ch);
		curl_close($ch);

		return preg_match("/HTTP\/\d.\d 200 OK/", $data);
	}

	public function actionRecommend()
	{
		$this->template->title = 'Doporučit obchod';
	}

	public function actionRating( $path='' )
	{
		$p = AR\PartnersDao::get()->findAll()->where('path=%s', $path )->fetch();
		if(empty( $p->id_partner ))			throw new BadRequestException('Zadaný partner neexistuje');

		$this->template->title = 'Hodnocení obchodu '.$p->name;


		$idu = Environment::getSession('user')->idu;
		$this->template->hasRating = PartnersRating::get()->hasRating( $idu );
	}

	protected function createComponentRecommend()
	{
        $form = new Form($this, 'recommend');

		$form->addText('website', 'webová adresa doporučovaného obchodu')
		->addRule(Form::URL, 'Adresa musí být zadána v platném tvaru http://neco.cz' );


		$guess = array(
			2000=>'2 000 Kč',
			5000=>'5 000 Kč',
			10000=>'10 000 Kč',
			15000=>'15 000 Kč',
			20000=>'20 000 Kč',
			30000=>'30 000 Kč',
			40000=>'40 000 Kč',
			50000=>'50 000 Kč',
			75000=>'75 000 Kč',
			100000=>'100 000 Kč',
			);
		$my_orders = AR\PartnersRecommend::get()->my_orders;
		$satisfaction = array(
			60=>'60%',
			70=>'70%',
			80=>'80%',
			90=>'90%',
			100=>'100%',
			);


		$form->addSelect('guess_money', 'odhad celkové ceny nákupů ročně', $guess);

		$form->addSelect('my_orders', 'počet zatím realizovaných nákupů', $my_orders);

		$form->addSelect('satisfaction', 'celková spokojenost', $satisfaction);


		$form->addSubmit('send', 'Doporučit')->getControlPrototype()->class('mybutton');

        $form->onSuccess[] = array($this, 'submitedRecommend');

		if( !AR\Configs::get()->byContent('settings.testMode'))
			$form->addProtection('Please submit this form again (security token has expired).');

        return $form;
	}
	

	protected function createComponentSearchForm() {
		$form = new Form();
		$renderer = $form->getRenderer();

		
		$form->addText('tags');
		$form->addSubmit('submit', '');
		$form->onSuccess[] = array($this, 'submitedSearchForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}

	private $name;
	function submitedSearchForm(Form $form) {
		if (isset($form['submit']) && $form['submit']->isSubmittedBy()) {
			$v = (array) $form->getValues();
			
			Environment::getSession('user')->partnerSearch = $v;
			$this->redirect('this');
		}
		
	}
	public function submitedRecommend( Form $form )
    {
		if ($form['send']->isSubmittedBy()) {
            $values = $form->getValues();
			$values['idu'] = Environment::getSession('user')->idu;

//			AR\PartnersRecommend::get()->addRecommend();
			$r = $this->tryCatcher( "AR\PartnersRecommend::get()->addRecommend", $values );
            $this->redirect( "this" );
         }
    }

	protected function createComponentMpPartnerDetail()
	{
		$this->getUsedTags();
        return new MpPartnerDetail();
	}
	
	function getUsedTags($country = null) {
		
		switch ($country) {
			case 'cr': $country = 1;
						break;
			case 'sr': $country = 2;
						break;
			case 'ostatni': $country = 3;		
						break;
			
		}
		if ( $country == null ) {
			$tags[0]='Vše';
			$tags += \DAO\PartnerTags::get()->findAll()->join(\DAO\BTags::get()->getTable())->on('partner_tags.tag_id=b_tags.id')->orderBy('tag')->fetchPairs('id', 'tag');
			return $tags; 
		} else {
			$tags[0]='Vše';
			$tags += \DAO\PartnerTags::get()->findAll()
					->join(\DAO\BTags::get()->getTable())
					->on('partner_tags.tag_id=b_tags.id')
					->join(\DAO\PartnerCountry::get()->getTable())
					->on('partner_tags.partner_id=partner_country.partner_id')
					->join(\DAO\Partners::get()->getTable())
					->on('partner_tags.partner_id=partners.id_partner')
					->where('country_id = %i AND type=2', $country)->orderBy('tag')->fetchPairs('id', 'tag');
					
			return $tags; 
		}
	}
	
	private $tags;
	public function handleRewriteTags( $tag_id )
	{
		$this->tags = $tag_id;
		Environment::getSession('user')->partnerSearch = null;
		$this->invalidateControl();
	}
	
	protected function createComponentTagForm() {
		$form = new Form();
		$renderer = $form->getRenderer();
		$tags = $this->writeAllTags();
		$form->addRadioList('tag', '', $tags);
		
		$form->addSubmit('save', 'Hledat');
		$form->onSuccess[] = array($this, 'submitedtagForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}


	function submitedTagForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = (array) $form->getValues();
		if ($v['tag']!= 0) {
			$def = \DAO\Partners::get()->findAll()->join(\DAO\PartnerTags::get()->getTable())->on('partners.id_partner=partner_tags.partner_id')->where("type=1 AND active=1 AND tag_id = %i", $v)->orderBy('name')->fetchAll();
			
			}	else {
				$def = \DAO\Partners::get()->findAll()->where("type=1 AND active=1")->orderBy('name')->fetchAll();
			}
			$this->template->partners = $def;
		}
	}

	protected function createComponentRating()
	{
        $form = new Form($this, 'rating');

		$items = array(
			null => 'nevím',
			0=>'0%',
			10=>'10%',
			20=>'20%',
			30=>'30%',
			40=>'40%',
			50=>'50%',
			60=>'60%',
			70=>'70%',
			80=>'80%',
			90=>'90%',
			100=>'100%'
			);

		$partners = AR\PartnersDao::get()->findAll()->where('type=1')->fetchPairs('id_partner', 'name');



		$l=  PartnersRating::get()->labels;

		foreach( $l as $key => $label )
		{
			if( $key == 'id_partner' )
			{
				//$form->addSelect($key, $label, $partners); continue;
			}
			elseif(in_array($key, array('insert_timestamp', 'idu')))
			{ null; }
			else
				$form->addSelect($key, $label, $items);
		}


		$form->addSubmit('send', 'Hodnotit')->getControlPrototype()->class('mybutton');

        $form->onSuccess[] = array($this, 'submitedRating');

		if( !AR\Configs::get()->byContent('settings.testMode'))
			$form->addProtection('Please submit this form again (security token has expired).');

        return $form;
	}

	protected function createComponentRatingList()
    {
		$idu = Environment::getSession('user')->idu;

		$r = new PartnersRating();
		return $r->getRatingList( $idu );
    }

	protected function createComponentRecommendList()
    {
		$idu = Environment::getSession('user')->idu;

		$r = new AR\PartnersRecommend();
		return $r->getRecommendList( (int) $idu );
    }

	public function submitedRating( Form $form )
    {
		if ($form['send']->isSubmittedBy()) {
            $values = $form->getValues();
			$values['idu'] = Environment::getSession('user')->idu;

			$p=$this->getParam('path');
			$p=AR\PartnersDao::get()->findAll()->where('path=%s', $p)->fetch();
			if(empty( $p->id_partner ))	throw new LogicException('Neznámé ID partnera');
			$values['id_partner'] = $p->id_partner;

			//PartnersRating::get()->addRating();
			$r = $this->tryCatcher( "PartnersRating::get()->addRating", $values, 1 );
			if( $r == 'fail' )
				$this->redirect('this');
			else
				$this->redirect( "Partners:list" );


         }
    }

	public function handleDetail( $path )
	{
		$path = $this->getParam('path');
		$p = AR\PartnersDao::get()->findAll()->where('[path]=%s', $path )->fetch();
		$this->template->partner = $p;

		$idu = \AntoninRykalsky\SystemUser::get()->getIdu();
		if( !empty(\AntoninRykalsky\CreditsAccounts::get()->actualState( $idu )->credits_left))
				$this->template->credits = \AntoninRykalsky\CreditsAccounts::get()->actualState( $idu )->credits_left;
			else
				$this->template->credits = 0;

		if($this->isAjax())
			$this->invalidateControl ();
	}

	
	public function actionDetail( $path )
    {
		
		$path = $this->getParam('path');
		$p = AR\PartnersDao::get()->findAll()->where('[path]=%s', $path )->fetch();

		// změn [+baseUrl] na $baseUri
		$find = 'http://' . $_SERVER['SERVER_NAME'] . $this->template->basePath;
		$p->img = str_replace("[+baseUri+]", $find, $p->img);

		$s = Environment::getSession('user');
		$k = CreditsAccounts::get()->actualState( $s->idu );
		$this->template->credits = @$k->credits_left;

		$this->template->title = $p->name . ' (ZO = '.$p->provize.'%)';
		$this->template->partner = $p;
    }




}
