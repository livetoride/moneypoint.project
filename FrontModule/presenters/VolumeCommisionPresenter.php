<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace FrontModule;

use MoneyPoint;
use Nette;

class VolumeCommisionPresenter extends \Base_MembersPresenter
{
	/** @var MoneyPoint\DiferentialCommisionFacade */
	protected $direfentialCommisionFacade;

	/** @var MoneyPoint\TransactionFacade */
	protected $transactionFacade;
	
	/** @var MoneyPoint\PartnerOrderFacade */
	protected $partnerOrderFacade;
	
	/** @var MoneyPoint\PeriodFacade */
	protected $periodFacade;
	
	function inject( 
			MoneyPoint\DiferentialCommisionFacade $direfentialCommisionFacade,
			MoneyPoint\TransactionFacade $transactionFacade,
			MoneyPoint\PartnerOrderFacade $partnerOrderFacade,
			MoneyPoint\PeriodFacade $periodFacade 
		) {
		$this->direfentialCommisionFacade = $direfentialCommisionFacade;
		$this->transactionFacade = $transactionFacade;
		$this->partnerOrderFacade = $partnerOrderFacade;
		$this->periodFacade = $periodFacade;
	}

	protected function createComponentMpTransactionDetail()
    {
		return new \MpTransactionDetail( $this->partnerOrderFacade, $this->transactionFacade );
	}
	
	private function checkCorrectResults( $coefA, $item )
	{
		try {
			$coefB = $item->getCarrier()->getPriceCoef();
			$netPoint = $item->getNetworkPointsPeriod();
			$netReward = $item->getParentNetworkReward();
			if( $netReward != $netPoint*($coefA-$coefB) )
				throw new \Exception("Špatně spočteno sitove");

			$diPoint = $item->getDirectPointsPeriod();
			$diReward = $item->getParentTotalReward();
			if( $diReward != $diPoint*($coefA) )
				throw new \Exception("Špatně spočteno prime");
		} catch( \Exception $e )
		{
			$this->flashMessage( $e->getMessage(), \Flashes::$error );
		}
	}
	
	public function actionFixOne()
	{
		$this->direfentialCommisionFacade->recountPeriodNetworkPoints();
		exit;
	}
	public function actionFix( $id )
	{
		$this->direfentialCommisionFacade->fixPeriodNetworkPoints( $id );
		exit;
	}
	
	public function actionDefault()
	{
//		$this->direfentialCommisionFacade->fixRanks();
		
		$idu = \AntoninRykalsky\SystemUser::get()->idu;
				
		$this->template->title = 'Objemové odměny za aktuální kvalifikační období';
		
		$this->direfentialCommisionFacade->installDefaultAccount( $idu );
		$volumeCommision = $this->direfentialCommisionFacade->findWithLazyCounting( $idu );

		$this->template->commision = $volumeCommision;
		
		


		$this->template->details = $d =  $volumeCommision->getChildrenCommision();
	
		$allowTesting =1;
		if( $allowTesting )
		{
			$coefA = $volumeCommision->getCarrier()->getPriceCoef();
			foreach( $d as $item )
			{
				$this->checkCorrectResults($coefA, $item);
			}
		}
		
		$this->template->bigNetworkMenu=1;
		
		$this->template->now = new \DateTime();
	}
	
	public function actionRanks()
	{
		$this->template->title = $this->projectTranslator->translate('Carriers and rates');
		$this->template->ranks = $r = $this->periodFacade->getActualRankTable();
		$this->template->bigNetworkMenu=1;
	}
	
	
	protected function createComponentVolumeCommisionList()     {
		$l = new MoneyPoint\VolumeCommisionList;
		return $l->getList( $this->direfentialCommisionFacade, $this->commonTranslator );
	}
	
	protected $volumeCommisionFormBuilder;
	protected function createComponentVolumeCommisionForm() {
		$this->volumeCommisionFormBuilder = new MoneyPoint\VolumeCommisionForm( $this->direfentialCommisionFacade, $this );
		return $this->volumeCommisionFormBuilder->getForm();
	}

	

}
