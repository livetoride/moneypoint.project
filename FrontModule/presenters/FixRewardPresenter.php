<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace FrontModule;

use MoneyPoint;

class FixRewardPresenter extends \Base_MembersPresenter
{
	private $em;
	public function testOne() {
		$container = \Nette\Environment::getContext(); 
		
	}
	
	/** @var MoneyPoint\FixedCommisionFacade */
	protected $fixedCommisionFacade;

	function injectPartner( 
			MoneyPoint\FixedCommisionFacade $fixedCommisionFacade,
			\AntoninRykalsky\EntityManager $em
		) {
		$this->fixedCommisionFacade = $fixedCommisionFacade;
		$this->em = $em->getEm();
	}	
	public function actionRewardsNEXT()
	{
		$distributionGroup = $this->fixedCommisionFacade->getDistributionGroup();
		$dp = $this->template->rewards = $distributionGroup->getDistributions();
		$this->template->title = 'Potenciální fixní odměny za nákupy kreditu v Malé síti';
		$this->template->smallNetworkMenu=1;
	}
	public function actionRewardsWithBPO()
	{
		$forms = array();
		$lower=null;
		foreach( \DAO\MpCreditsDiscount::get()->findAll('price')->fetchAll() as $p )
		{
			$forms[] = $p->price;
			if( empty($lower ))
				$lower = $p->price;
		}
		$this->template->forms = $forms;
		$this->template->lower = $lower;
		
		$distributionGroup = $this->fixedCommisionFacade->getDistributionGroup();
		$dp = $this->template->rewards = $distributionGroup->getDistributions();
		$this->template->title = 'Potenciální fixní odměny a potenciální BPO';
		$this->template->smallNetworkMenu=1;
		$this->setView('rewardsWithBpo.latte');
	}
	public function actionRewards()
	{
		$lower=null;
		
		/* @var $creditDiscountGroup \MoneyPoint\Entity\CreditDiscountGroup */
		$creditDiscountGroup = $this->em->getRepository("MoneyPoint\Entity\CreditDiscountGroup")->findOneBy(array('active'=>1));
		$creditDiscounts = $this->template->forms =  $creditDiscountGroup->getDiscounts();
		
		$distributionGroup = $this->fixedCommisionFacade->getDistributionGroup();
		$distributions = $this->template->rewards = $distributionGroup->getDistributions();



		$this->template->title = 'Sazby Základních odměn';
		$this->template->smallNetworkMenu=1;
	}
}
