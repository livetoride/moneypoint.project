<?php
namespace FrontModule;
use Nette\Application\UI\Form;

class CreditTransferPresenter extends \Base_MembersPresenter
{
	/** @var \MoneyPoint\PartnerOrderFacade */
	protected $partnerOrderFacade;
	
	/** @var \MoneyPoint\TransactionFacade */
	protected $transactionFacade;
	
	/** @var \MoneyPoint\CreditTransferFacade */
	protected $creditTransferFacade;

	function injectPartner(\MoneyPoint\CreditTransferFacade $creditTransferFacade) {
		$this->creditTransferFacade = $creditTransferFacade;
	}
	
	public function inject( 
		\MoneyPoint\PartnerOrderFacade $partnerOrderFacade ,
		\MoneyPoint\TransactionFacade $transactionFacade ) {
		$this->partnerOrderFacade = $partnerOrderFacade;
		$this->transactionFacade = $transactionFacade;
	}

	public function actionDefault()
	{
		$this->template->title = 'Převod kreditu';
		#echo  $this->creditTransferFacade->canBeSendedTo( 1 );exit;
		
	}
	
	protected function createComponentTransferForm() {
		$form = new Form();
		$renderer = $form->getRenderer();

		$form->addText('credit', "Zadejte hodnotu:");
		$form->addText('userId', "Zadejte ID příjemce:");

		$form->addSubmit('save', 'Odeslat');
		$form->onSuccess[] = array($this, 'submitedTransferForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}


	function submitedTransferForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			$loggedIdu = \AntoninRykalsky\SystemUser::get()->idu;
			
			$this->creditTransferFacade->createTransfer( $loggedIdu, $v );
			$stop = \Flashes::deliver($this);
			if( $stop ){ return; }
			
			$this->flashMessage("Kredit byl odeslán.", \Flashes::$success );
			$this->redirect(":Front:Credits:default");
			
		}
	}
	
	public function createComponentCreditsAccountList()
	{
		$c = new \MoneyPoint\CreditsAccountList( $this->partnerOrderFacade, $this->transactionFacade, $this->commonTranslator );
		$c->idu = \MoneyPoint\LoggedMember::get()->idu;
		return $c;
	}
}
