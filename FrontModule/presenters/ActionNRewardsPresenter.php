<?php
/**
 * Presenter obsluhující požadavky operací s kredity
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace FrontModule;
use Nette\Utils\Html;
use dibi;


class ActionNRewardsPresenter extends \Base_MembersPresenter
{
	/** @var \MoneyPoint\PartnerOrderFacade */
	protected $partnerOrderFacade;
	public function inject( 
		\MoneyPoint\PartnerOrderFacade $partnerOrderFacade 
	) {
		$this->partnerOrderFacade = $partnerOrderFacade;
	}
	
	/** @var TransactionFacade */
	protected $transactionFacade;

	function injectTransactionFacade(\MoneyPoint\TransactionFacade $transactionFacade) {
		$this->transactionFacade = $transactionFacade;
	}
	
	/**
	 *
	 * @var type id uživatele pro vypis
	 */
	var $idu;

	
	/** @var MoneyPoint\DiferentialCommisionFacade */
	protected $direfentialCommisionFacade;
	protected $em;

	
	function injectA( 
			\MoneyPoint\DiferentialCommisionFacade $direfentialCommisionFacade,
			\AntoninRykalsky\EntityManager $em
		) {
		$this->direfentialCommisionFacade = $direfentialCommisionFacade;
		$this->em = $em->getEm();
		
	}

	public function actionAccount( $idu )
	{
		unset($idu); // Strict standards: Declaration should be compatible with parent

		
		$this->template->title = 'Odměny';
		$this->idu = \MoneyPoint\LoggedMember::get()->idu;
		
		// TONIK-TODO: findout better way
		$this->direfentialCommisionFacade->countCommision( null, $this->idu  );
		$this->em->flush();
	}

	protected function createComponentMpFilters()
    {
		$f = new \MpFilters();
		return $f;
	}

	protected function createComponentMpTransactionDetail()
    {
		return new \MpTransactionDetail( $this->partnerOrderFacade, $this->transactionFacade );
	}

	public function creditsAccountDS( $datagrid = null )
	{
		$model = \Moneypoint\DAO\Transactions::get()->getDataSource();
//		echo $model->__toString();exit;
		$model->orderBy('id_transaction', dibi::DESC);
		$model->where('idu=%i', $this->idu );

		$data = \AntoninRykalsky\SystemUser::get()->getData();
		$d=$data->filters;
		
		if( empty( $d['date_since'] ))
		{
			$now = new \DateTime();
			$d['date_since'] = '1.2.2013';
			$d['date_to'] = $now->format('j.n.Y');
		}
		$d['date_since'] = \FormatingHelpers::CzDate2PgDate($d['date_since']);
		$d['date_to'] = date('Y-m-d', strtotime($d['date_to'] . ' + 1 day'));
		
		$model->where("insert_timestamp >= '".$d['date_since']."' AND insert_timestamp <= '".$d['date_to']."'");

		if( !empty($d['id_type']) && $d['id_type']!= 0)
		{
			$model->where("id_type = ".$d['id_type']);
		} else {
			$model->where("id_order IS NULL" );
		}

		$model2 = $model->fetchAll();
		$model3=array();
		$model3[1]=0;
		$model3[2]=0;
		$model3[3]=0;
		$model3[4]=0;

		foreach( $model2 as $m )
		{
			if( empty($model3[ $m['id_type'] ])) $model3[ $m['id_type'] ] = 0;
			
			if( $m['id_type'] == 5 ) 
			{
				$model3[ 1 ] += $m['points_change'];
			} else {
				$model3[ $m['id_type'] ] += $m['points_change'];
			}
		}
		if( $datagrid )
			return $model;

		return $model3;
	}

	public function gridOnCellRendered(Html $cell, $column, $value)
	{
		if ($column === 'points_change') {
			if ($value < 30000) $cell->addClass('money-low');
			elseif ($value >= 100000) $cell->addClass('money-high');
		}
		return $cell;
	}

	protected function createComponentCreditsAccount()
    {
		// zvyrazneni aktualniho stavu
//		$acs = \Moneypoint\DAO\Transactions::get()->actualState($user->idu);
//		if( !empty( $acs ))
//			$user->actual_credits_id = $acs->id;

		$model = $this->creditsAccountDS( 1 );



//		print_r( $model->fetchAll() );exit;
//		print_r( $model->__toString() );exit;
//
		$model = new \DataGrid\DataSources\Dibi\DataSource($model);
		$grid = new \DataGrid\DataGrid;
		$renderer = \MoneyPoint\DatagridRenderer::get()->getRenderer();
		$renderer->onCellRender[] = array($this, 'gridOnCellRendered');
		$translator = new \GettextTranslator( APP_DIR . '/locale/datagrid.mo' );
		$grid->setTranslator($translator);

		$grid->setDataSource( $model );
		$grid->setRenderer($renderer);
		$grid->itemsPerPage = 20;
		$grid->multiOrder = FALSE;



		$operations = array(); // define operations
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, \Moneypoint\DAO\Transactions::get()->getPrimary()); // allows checkboxes to do operations with more rows

		// if no columns are defined, takes all cols from given data source
		$grid->addActionColumn('akce')->getHeaderPrototype()->style('width: 20px;');
		$icon = Html::el('span');
		// <a href="{$detailComponent->link('detail!', array(path => $p->path))}" onclick="showtable()" class="ajax">

		$action = $grid->addAction('Detail', 'mpTransactionDetail:detail!', clone $icon->class('icon mp-info'))->getHtml();
		$action->class('show-table');



		$grid->addColumn('insert_timestamp', 'Datum');
		$grid['insert_timestamp']->formatCallback[] = 'FormatingHelpers::czechDateShort';
		$grid['insert_timestamp']->addDefaultSorting('desc');
		$grid['insert_timestamp']->getCellPrototype()->class('insert_timestamp');

		$grid->addColumn('mp_trans_id', 'Číslo akce');
		$grid['mp_trans_id']->replacement[''] = '-';
		//$grid['variable_symbol']->formatCallback[] = 'FormatingHelpers::MP_id';
		$grid['mp_trans_id']->getCellPrototype()->class('mpid');
		$grid['mp_trans_id']->getCellPrototype()->class('cislo_akce');

		$grid->addColumn('reason', 'Popis');
		$grid['reason']->getCellPrototype()->class('reason');

		$grid->addColumn('objem', 'Objem');
		$grid['objem']->formatCallback[] = 'FormatingHelpers::currency2';
		$grid['objem']->getCellPrototype()->class('objem');

		$grid->addColumn('points_change', 'Odměna');
		$grid['points_change']->formatCallback[] = 'MpHelpers::creditsAccountPointsSign';
		$grid['points_change']->getCellPrototype()->class('points_change');
		$grid['points_change']->getHeaderPrototype()->class('points_change');


		return $grid;
    }

	public function cellStyleRenderer(Html $cell, $column, $value)
	{
		if ($column === 'credits_left' ||
				$column === 'credits_change' ) {
			$cell->addClass('right');

		}
		return $cell;
	}
}
