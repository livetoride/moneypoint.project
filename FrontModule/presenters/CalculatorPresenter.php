<?php
/**
 * Presenter obsluhující požadavky eshopu
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace FrontModule;
use Nette\Application\UI\Form;
use AntoninRykalsky as AR;
use Nette\Utils\Html;


class CalculatorPresenter extends \Base_MlmPresenter
{
	private function recount( $d ) {
		// distribuční tabulka
		$p=Produkty::get()->findAll()->where('active=1')->fetch();
		$cs = ProduktyLevelDistribution::get()->findAll()->where('id_product=%i', $p->id_product)->fetchPairs('level_depth', 'points');
		$this->template->distribution = $cs;

		$mt = MPKariera::get()->findAll()->orderBy('bonus')->fetchPairs('trida', 'bonus');
		$mtcka=array();$i=0;
		foreach( $mt as $t => $b )
		{
			$i++;if( $i==1 )continue;
			$mtcka[$i]['trida']= $t;
			$mtcka[$i]['bonus']= $b+100;
		}
		$this->template->mtcka=$mtcka;

		// max distribution levels
		$distLevels =  AR\Configs::get()->byContent('products.distrubutionLevels');
		$this->template->distLevels = $distLevels;

		$data = array();
		$avgCreditsPrice = array();
		for( $i = 1; $i<= $distLevels; $i++)
		{
			$data[$i]['1-linie']=$i;
			$data[$i]['2-zo']=$cs[$i];
			$data[$i]['3-zoklient']=($d['avgPrice']/200)*$data[$i]['2-zo'];
			if( $i!=1 )
				$data[$i]['4-klient']=$d['avgClints']*$data[$i-1]['4-klient'];
			else
				$data[$i]['4-klient']=($d['directClints']);
			$data[$i]['5-zolks']=$data[$i]['4-klient']*$data[$i]['3-zoklient'];
			$data[$i]['7-zoeo']=2*$data[$i]['5-zolks'];
		}
		// echo $linie[11];
		$this->template->data = $data;
		$this->avgCreditsPrice = $avgCreditsPrice;
	}

	private $linie;
	private $avgCreditsPrice;
	private $param;



	public function actionDefault( $param )
	{
		$this->template->title = 'Orientační kalkulátor odměn za nákupy kreditu';

		$f = $this['calculator'];
		$d = array(
			'directClints' => 5,
			'avgClints'=> 3,
			'avgPrice'=> 5,
		);

		if( !empty($param)) $d=$param;

		$this->recount( $d );

			$this->template->linie = $this->linie;
			$this->template->avgCreditsPrice = $this->avgCreditsPrice;
			$f->setDefaults($d);

			$this->template->f = $f;
			$this->template->v = $d;

	}

	protected function createComponentCalculator()
	{
        $form = new Form($this, 'calculator');

		$directsClients = array(
			1=>1, 2, 3, 4, 5, 6, 7, 8, 9, 10
		);
		$form->addSelect('directClints', 'přímo doporučených Klientů v LKS 1', $directsClients);

		$avgClients = array(
			1=>1,2,3
		);
			$form->addSelect('avgClints', 'průměrně doporučováno Klientů', $avgClients);

		$avgPrice = array(
			200=>'200 Kč',
			400=>'400 Kč',
			600=>'600 Kč',
			1000=>'1 000 Kč',
			2000=>'2 000 Kč',
		);
		$form->addSelect('avgPrice', 'průměrná cena nákupů kreditu (PCNK)', $avgPrice);



		$form->addSubmit('send', 'Přepočítat')->getControlPrototype()->class('mybutton');

        $form->onSuccess[] = array($this, 'submittedCalculator');

		if( !AR\Configs::get()->byContent('settings.testMode'))
			$form->addProtection('Please submit this form again (security token has expired).');

        return $form;
	}


	function submittedCalculator( Form $form )
	{
		if(isset($form['send']) && $form['send']->isSubmittedBy()) {
			$d = $form->getValues();
			$this->redirect( 'default', array('param'=>$d ));
		}
	}

}
