<?php
/**
 * Presenter obsluhující požadavky operací s body
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace FrontModule;

use AntoninRykalsky as AR;
use AntoninRykalsky\CreditsAccounts;
use AntoninRykalsky\CreditsStructure;
use AntoninRykalsky\GetDescendantFixedLevel;
use AntoninRykalsky\MPKoef;
use AntoninRykalsky\PointsAccounts;
use Base_MembersPresenter;
use DAO\Uzivatele;
use DataGrid\DataGrid;
use DataGrid\DataGrid as DataGrid2;
use DataGrid\DataSources\Dibi\DataSource;
use DataGrid\Renderers\Conventional;
use dibi;
use FormatingHelpers;
use Moneypoint\MpWithdrawControl;
use Nette\Application\UI\Form;
use Nette\Environment;
use Nette\Utils\Html;


class PointsPresenter extends Base_MembersPresenter
{
	/**
	 *
	 * @var type id uživatele pro vypis
	 */
	var $idu;

	public function actionAccount( $idu )
	{
		unset($idu); // Strict standards: Declaration should be compatible with parent

		$user = Environment::getSession('user');
		if( $this->getParam('type') != '' )
		$user->points_type = $this->getParam('type');
		$this->idu = \MoneyPoint\LoggedMember::get()->idu;

		$this->template->menukey = 2;

		switch ($user->points_type) {
			case 1:
				$this->template->title = 'Bodový účet - všechny transakce';
				break;
			case 2:
				$this->template->title = 'Bodový účet - odměny za nákupy';
				break;
			case 3:
				$this->template->title = 'Bodový účet - odměny za Klientskou síť';
				break;
			case 4:
				$this->template->title = 'Bodový účet - historie vyplacených odměn';
				break;
		}


	}


	public function actionDefault( $idu )
	{
		echo  get_class( $this );exit;
//		if(isset( $this->idu ))
//			$this->forward('account', 1);
		unset($idu); // Strict standards: Declaration should be compatible with parent

		$this->template->title = 'Bodový účet';
		$this->idu = \MoneyPoint\LoggedMember::get()->idu;

		$d = array('type' => 1 );
		$this['pointsType']->setDefaults($d);
	}



    public function actionStructureRewards() {


		$this->template->title = 'Aktuální stav';

		$s = Environment::getSession('user');

		// strom
		$u = Uzivatele::get()->find( $s->idu )->fetch();
		$this->template->root_popis = $u->jmeno . ' ' . $u->prijmeni;
		$this->template->idu = (int) $s->idu;

		//$d = GetDescendantFixedLevel::get()->findBy( $s->idu, 10 );
		//print_r( $d );exit;

		// / strom

		$k = CreditsAccounts::get()->actualState( $s->idu );
		$b = PointsAccounts::get()->actualState( $s->idu );
		$ks = CreditsStructure::get()->actualState( $s->idu );
		$class = MPKoef::get()->getCoef( $s->idu );

		$this->template->points_structure = (int) @$b->points_structure;
		$this->template->credits_structure = (int) @$ks->credits_structure;
		$this->template->credits_sum = (int) @$k->credits_sum;
		$this->template->class = $class;

		$s = GetDescendantFixedLevel::get()->findBy( $s->idu, 10 );
		$maxLevels = AR\Configs::get()->byContent('products.distrubutionLevels');

		for( $i=1; $i<=$maxLevels; $i++ )
		{
			if(empty($s[$i])) $s[$i] = array();
		}

		$idus = implode(', ', (array)@$s[1] );
		if( empty($idus)) $idus='null';
		$sNames = Uzivatele::get()->findAll();
		$sNames->where('idu IN('.$idus.')' );
		$sNames->fetchAll();

		$tmp = ''; $count=0;
		foreach( $sNames as $n ) {
			$count++;
			$tmp .= $n->jmeno . ' ' . $n->prijmeni . ' (' . $n->variable_symbol .') '.  FormatingHelpers::czechDateShort($n->reg_date).'<br />';
			if( $count != count($sNames)) $tmp .= ', ';
		}
		$sNamesText[1] = $tmp;

		$this->template->usersLevels = $s;
		$this->template->usersLevelsNames = $sNamesText;

    }

	public function createComponentWithdraw() {
		return new MpWithdrawControl();
	}

	public function actionWithdraw()
	{
		$this->template->title = 'Požadavek na vyplacení odměn';
	}



	/** @return DataGrid2 tovarnicka na ordersdetail */
	protected function createComponentPointsType()
    {
		// limity vyberyu
		$points = $this->getUpperLimit();
		$minWithdraw = AR\Configs::get()->byContent('general.minimalWithdraw');

        $form = new Form();
		$form->setMethod(Form::GET );
		$this->addComponent($form, 'pointsType');

		$renderer = $form->getRenderer();

		$items[1] = 'všechny transakce';
		$items[2] = 'odměny za nákupy v obchodech';
		$items[3] = 'odměny za nákupy kreditu v síti';
		$items[4] = 'historie vyplacených odměn';
		$items[5] = 'nová žádost o vyplacení odměn';

		$form->addRadioList('type', '', $items);

		$form->addSubmit('save', 'Zobrazit');
		$form->onSuccess[] = array($this, 'submitedPointsType');
		return $form;
    }

	function submitedPointsType( Form $form )
    {
		if(isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			if($v['type']<5) {
				$this->redirect('Points:account', array('type' => $v['type'] ));
			} else {
				$this->forward('Points:withdraw' );
			}

		}
	}




	protected function createComponentPointsAccount()
    {
		// zvyrazneni aktualniho stavu
		$user = Environment::getSession('user');
		$aps = PointsAccounts::get()->actualState($user->idu);
		if( !empty($aps ))
		$user->actual_points_id = $aps->id;

		$model = PointsAccounts::get()->getDataSource();
		$model->orderBy('change_timestamp', dibi::DESC);
		$model->orderBy('id', dibi::DESC);
		$model->where('idu=%i', $this->idu );

		switch ($this->getParam('type')) {
			case 2:
				$model->where('id_pio_order IS NOT NULL' );
				break;
			case 3:
				$model->where('id_order IS NOT NULL' );
				break;
			case 4:
				$model->where('id_type = 3' );
				break;
		}


		$model = new DataSource($model);
		$grid = new DataGrid;
		$renderer = new Conventional;
		$renderer->paginatorFormat = '%input%'; // customize format of paginator
		$renderer->footerFormat = '%paginator%';
		$renderer->onRowRender[] = array($this, 'ordersGridOnRowRendered');
		$grid->setDataSource( $model );
		$grid->setRenderer($renderer);
		$grid->itemsPerPage = 10;
		$grid->multiOrder = FALSE;


		$operations = array(); // define operations
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, PointsAccounts::get()->getPrimary()); // allows checkboxes to do operations with more rows

		// if no columns are defined, takes all cols from given data source
		// $grid->addColumn('id', 'stav objednavky');
		// $grid->addColumn('idu', 'stav objednavky');
		$grid->addColumn('change_timestamp', 'Datum');
		$grid['change_timestamp']->formatCallback[] = 'FormatingHelpers::czechDateShort';
		$grid['change_timestamp']->getCellPrototype()->class('col-date');
		$grid['change_timestamp']->getHeaderPrototype()->class('col-date');

		if( $this->getParam('type') != 2 )
		if( $this->getParam('type') != 4 )
		{
			$grid->addColumn('variable_symbol', 'ID klienta a LKS');

			$grid['variable_symbol']->replacement[''] = '-';
			$grid['variable_symbol']->getCellPrototype()->class('mpid');
			$grid['variable_symbol']->getHeaderPrototype()->class('mpid');
			//$grid['variable_symbol']->formatCallback[] = 'FormatingHelpers::MP_id';
//			$grid['variable_symbol']->getCellPrototype()->class('mpid');
		}


		$grid->addColumn('reason', 'Popis');
		$grid['reason']->getCellPrototype()->class('col-reason');
		$grid['reason']->getHeaderPrototype()->class('col-reason');

		$grid->addColumn('points_change', 'Změna');
		$grid['points_change']->formatCallback[] = 'FormatingHelpers::intPlusSignGapPoints';
		$grid['points_change']->getCellPrototype()->class('center');
		$grid['points_change']->getHeaderPrototype()->class('center');

		$caption = Html::el('span')->setText('Zůstatek')->class('center');
		$grid->addColumn('points_left', 'Zůstatek');
		$grid['points_left']->formatCallback[] = 'FormatingHelpers::body2';
		$grid['points_left']->getCellPrototype()->class('center');
		$grid['points_left']->getHeaderPrototype()->class('center');



		// výchozí řazení
		//$grid['change_timestamp']->addDefaultSorting('desc');




		#$grid->addActionColumn('Akce')->getHeaderPrototype()->style('width: 98px');
		#$icon = Html::el('span');
		#$grid->addAction('Detail', 'order-detail', clone $icon->class('icon icon-detail'));
		#$grid->addAction('Edit', 'edit', clone $icon->class('icon icon-edit'));
		#$grid->addAction('Delete', 'confirmForm:confirmDeleteProduct!', clone $icon->class('icon icon-del'));
		#$grid->addAction('Nastav kategorie', 'setCategories');

		#if( $archive )
		#$grid->addAction('Aktivuj', 'confirmForm:confirmActivateProduct!');



		return $grid;
    }

	public function ordersGridOnRowRendered($row, $data)
	{
		$user = Environment::getSession('user');

		if( $data['id'] == $user->actual_points_id )
			$row->addClass('actual-state');
		return $row;
	}



}
