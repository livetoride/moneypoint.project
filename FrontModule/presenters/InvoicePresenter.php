<?php
/**
 * Presenter obsluhující požadavky eshopu
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace FrontModule;

use MoneyPoint\OrdersInvoice;
use Base_MlmPresenter;
use DAO\Orders;


class InvoicePresenter extends \Base_GuestPresenter
{
	public $dontSendHeader = 0;
	
	public function renderShow( $id_order, $render = 1 )
	{
		exit;
		
		if( $render )
			OrdersInvoice::$show_only = true;
		
		$o = \DAO\Orders::get()->find( $id_order )->fetch();
		$g= $this['invoice'];
		$g->setObjednavka($o);
		$g->render();
		exit;
	}
	
	public function renderStorno( $id_order, $render = 1 )
	{
		exit;
		if( $render )
			OrdersInvoice::$show_only = true;
		
		$o = \DAO\Orders::get()->find( $id_order )->fetch();
		$g= $this['invoice'];
		$g->setObjednavka($o);
		$g->setStorno();
		$g->render();
		exit;
	}
	
	public function actionDefault( $id_order = 5 )
	{
	}

	public function getMyComponent()
	{
		return $this['invoice'];
	}

	protected function createComponentInvoice() {
		$this->dontSendHeader = 1;
		$g = new OrdersInvoice();
		$g->setPath( $this->template->baseUri );
		$g->setBankAccount( \AntoninRykalsky\Configs::get()->byContent('general.bank_account') );
		$g->setBankCode( \AntoninRykalsky\Configs::get()->byContent('general.bank_code') );
		if( $this->template->baseUri == 'http:' )
			$path = 'http://localhost/beta.moneypoint.cz';
		else
			$path = $this->template->baseUri;
		$g->setPath( $path );
		return $g;
	}

}
