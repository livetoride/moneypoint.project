<?php

/**
 * Presenter obsluhující požadavky operací s body
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */

namespace FrontModule;

use AntoninRykalsky as AR;
use Nette\Application\UI\Form;
use Nette\Utils\Html;

class InvitationPresenter extends \Base_MembersPresenter {

	/** @var \MoneyPoint\PartnerOrderFacade */
	protected $partnerOrderFacade;
	/** @var \MoneyPoint\TransactionFacade */
	protected $transactionFacade;
	
	public function inject( 
		\MoneyPoint\PartnerOrderFacade $partnerOrderFacade,
		\MoneyPoint\TransactionFacade $transactionFacade 
	) {
		$this->partnerOrderFacade = $partnerOrderFacade;
		$this->transactionFacade = $transactionFacade;
	}

	
	public function actionDefault() {
		$this->template->title = 'Moje pozvánky';
		
		if( \MoneyPoint\Invitation::ENABLE_INVITATION_LIMIT )
			$this->template->leftInvitation = \MoneyPoint\LoggedMember::get()->invitation()->leftInvitations();
	}

	public function actionCreate() {
		$this->template->title = 'Odeslat pozvánku';
		
		$d['text'] = AR\Configs::get()->byContent('mailer-invitation.email');
		$this->template->emailBottom =  AR\Configs::get()->byContent('mailer-invitation.email_padding');

		$d['email'] = '@';
		$this['form']->setDefaults( $d );
	}
	
	protected function createComponentMpTransactionDetail()
    {
		return new \MpTransactionDetail( $this->partnerOrderFacade, $this->transactionFacade );
	}
	
	
	protected function createComponentForm() {
		$form = new Form($this, 'form');
		#$form->setMethod('post');

		$form->addText('jmeno', 'Jméno adresáta:');
		$form['jmeno']->getControlPrototype()->tabindex(1);

		$form->addText('prijmeni', 'Přijmení adresáta:');

		$form->addText('email', 'Emailová adresa:')
				->addRule(Form::EMAIL, 'Zadejte platný email');
		
		$form->addTextArea('text', 'Text doporučovacího emailu')->getControlPrototype()->class('invitationck');
		
		if( \MoneyPoint\LoggedMember::get()->invitation()->leftInvitations() == 0 )
		{
			$form->addCheckbox('agree_nonstandard', 'Souhlasím s odečtením 100 kreditu pro vygenerování pozvánky.')
					->addRule(Form::EQUAL, 'Již nemáte žádné volné pozvánky. Novou pozvánku bez odečtení 100 kreditu nelze vygenerovat', TRUE);
		}

		$form->addSubmit('save', 'Odeslat pozvánku')->getControlPrototype()->class('mybutton');
		$form->onSuccess[] = array($this, 'formSubmitted');

//		if( $this->getContext()->configs->allowProtection() )
//			$form->addProtection('Vypršel časový limit, odešlete formulář znovu');
		return $form;
	}

	
	protected function createComponentList()
    {
        $ds = \DAO\UserInvitation::get()->findAll();
		$ds->where('idu=%s', \MoneyPoint\LoggedMember::get()->idu );
		$ds->orderBy('id', 'desc');
		$ds = $ds->fetchAll();
		foreach( $ds as &$v )
		{
			$v['jmeno'].=' '.$v['prijmeni'];
			// $v['generated'] = empty($v['credits_account_id']) ? ' standartní' : ' generovaná';
			if( $v['used'] )
				$v['success'] = 'využitá';
			elseif( strtotime($v['ts_expires']) < time() )
				$v['success'] = 'nevyužitá';
			else{
				$v['success'] = 'odeslaná';
			}
//			$v['success'] .= $v['generated'];
		}
		$model = new \DataGrid\DataSources\PHPArray\PHPArray( $ds );

		$grid = new \DataGrid\DataGrid;
		$renderer = \MoneyPoint\DatagridRenderer::get()->getRenderer();
		$renderer->paginatorFormat = '%input%';
		$grid->setRenderer($renderer);
		$translator = new \GettextTranslator( APP_DIR . '/locale/datagrid.mo' );
		$grid->setTranslator($translator);

		$grid->itemsPerPage = 10;
		$grid->setDataSource($model);
		$grid->multiOrder = FALSE;

		$operations = array();
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, \DAO\UserInvitation::get()->getPrimary());

		// if no columns are defined, takes all cols from given data source
		$akce = $grid->addActionColumn('akce');
		$akce->getHeaderPrototype()->style('width: 20px;');
		$akce->getHeaderPrototype()->class('hide-child');
		
		$icon = Html::el('span');
		// <a href="{$detailComponent->link('detail!', array(path => $p->path))}" onclick="showtable()" class="ajax">

		$action = $grid->addAction('Detail', 'mpTransactionDetail:invitationDetail!', clone $icon->class('icon mp-info'))->getHtml();
		$action->class('ajax');
		$action->class('show-table');
		#$action->onClick('showtable()');
		
		$grid->addColumn('invitation_id', 'číslo');
		$grid->addColumn('jmeno', 'jméno');
		#$grid->addColumn('prijmeni', 'přijmení');
		$grid->addColumn('email', 'email');
		$grid->addColumn('success', 'stav');
		$grid['success']->getCellPrototype()->class('lastcol');
		$grid['success']->getHeaderPrototype()->class('lastcol');
		
		//$grid->addColumn('generated', 'typ');

//		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
//		$icon = Html::el('span');
//		$grid->addAction('Structure:detail', 'Users:detail', clone $icon->class('icon icon-detail'));
		return $grid;
    }
	
	public function formSubmitted(Form $form) {
		if ($form['save']->isSubmittedBy()) {
			$data = (array) $form->getValues();
			$data['idu'] = \MoneyPoint\LoggedMember::get()->idu;
			
			$agree = @$data['agree_nonstandard'];
			try {
				$invitation = \MoneyPoint\Invitation::create($data, (bool)$agree );
			} catch( \LogicException $e )
			{
				if( $e->getCode() == 1)
				{
					$span = Html::el('span');
					$span->setHtml('Nemáte dostatečný zůstatek kreditu pro vygenerování nové pozvánky! Pro vygenerování pozvánky potřebujete 100 kreditu, které Vám budou po vygenerování pozvánky odečteny! Klikněte <a target="_blank" href="'.$this->link(':Front:eshop:credits-order').'">sem</a> pro přechod k nákupu kreditu.');
					$this->flashMessage( $span, \Flashes::$info );
					return;
				} elseif( $e instanceof \AntoninRykalsky\LogicException ) {
					$this->flashMessage($e->getMessage(), 'ERROR');
					return;
				} else { throw $e; }
			}
			
			$invitation->send();

			$this->flashMessage('Pozvánka byla odeslána.', AR\Flashes::$success);
			$this->redirect('this');
		}
	}

}
