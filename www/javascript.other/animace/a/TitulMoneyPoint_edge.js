/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='javascript/animace/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'Foto',
            type:'rect',
            rect:['0','-54','auto','auto','auto','auto']
         },
         {
            id:'Napis_vovyRozmer',
            type:'image',
            rect:['0px','0px','960px','500px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Napis_vovyRozmer.png",'0px','0px']
         },
         {
            id:'Text2',
            type:'text',
            rect:['72px','17px','838px','36px','auto','auto'],
            clip:['rect(0px -18px 36px 0px)'],
            text:"Koupíte zboží u našich obchodních partnerů a my Vám pošleme až 10% z ceny nákupu.",
            align:"left",
            font:['\'Trebuchet MS\', Arial, Helvetica, sans-serif',20,"rgba(138,6,6,1.00)","900","none","normal"]
         }],
         symbolInstances: [
         {
            id:'Foto',
            symbolName:'Foto'
         }
         ]
      },
   states: {
      "Base State": {
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '960px'],
            ["style", "height", '500px'],
            ["style", "overflow", 'hidden']
         ],
         "${_Napis_vovyRozmer}": [
            ["style", "top", '0px'],
            ["subproperty", "filter.blur", '0px'],
            ["style", "left", '884px']
         ],
         "${_Text2}": [
            ["style", "top", '17px'],
            ["style", "clip", [0,-18,36,0], {valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'} ],
            ["style", "width", '838px'],
            ["color", "color", 'rgba(138,6,6,1.00)'],
            ["subproperty", "filter.blur", '0px'],
            ["style", "left", '72px'],
            ["style", "font-size", '20px']
         ],
         "${_Foto}": [
            ["style", "display", 'block']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 7000,
         autoPlay: true,
         timeline: [
            { id: "eid32", tween: [ "style", "${_Text2}", "clip", [0,840,36,0], { valueTemplate: 'rect(@@0@@px @@1@@px @@2@@px @@3@@px)', fromValue: [0,-18,36,0]}], position: 3500, duration: 2000, easing: "easeInCirc" },
            { id: "eid33", tween: [ "style", "${_Foto}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0, easing: "easeInCirc" },
            { id: "eid34", tween: [ "style", "${_Foto}", "display", 'block', { fromValue: 'block'}], position: 7000, duration: 0, easing: "easeInCirc" },
            { id: "eid23", tween: [ "style", "${_Napis_vovyRozmer}", "left", '0px', { fromValue: '884px'}], position: 2000, duration: 1000, easing: "easeInCirc" }         ]
      }
   }
},
"Foto": {
   version: "1.5.0",
   minimumCompatibleVersion: "1.5.0",
   build: "1.5.0.217",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'Pixmac000078595829',
      type: 'image',
      rect: ['0px','0px','960px','573px','auto','auto'],
      fill: ['rgba(0,0,0,0)','javascript/animace/images/Pixmac000078595829.jpg','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '573px'],
            ["style", "width", '960px']
         ],
         "${_Pixmac000078595829}": [
            ["style", "height", '573px'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["style", "width", '960px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-4611446");
