// JavaScript Document

/**
 * Ajax
 *
 * @constructor Ajax
 * @param url
 * @param method - GET, POST
 * @param param - parametry url
 */
function Ajax(url,method,param){
  this.url=encodeURI(url);
  this.method=method;
  if(param)this.param=param;
}

/**
 * send
 *
 * Navaze spojeni
 *
 */
Ajax.prototype.send=function (){
  this.httpRequest=null;
  try{
  // Firefox, Opera 8.0+, Safari
    this.httpRequest=new XMLHttpRequest();
  }
  catch (e){
  // Internet Explorer
    try{
      this.httpRequest=new ActiveXObject("Microsoft.XMLHTTP");

    }
    catch (e){
      this.httpRequest=new ActiveXObject("Msxml2.XMLHTTP");
    }
  }
  if (!this.httpRequest){
    alert("Chyba při vytváření spojení")
    return;
  }
  var listener = this.listener
	var thisAjax = this
	this.httpRequest.onreadystatechange = function(){
		if(listener){
			listener(thisAjax)
		}
	}
	if(this.method == 'get'){
		this.getMethod();
	}
	if(this.method == 'post'){
		this.postMethod();
	}
}

/**
 * getMethod
 *
 * Vykona pozadavek GET
 *
 */
Ajax.prototype.getMethod=function(){
  if(this.url){
    try{
        this.httpRequest.open("GET",this.url,true);
        //this.httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        this.httpRequest.setRequestHeader("Connection", "close");
        this.httpRequest.send(null)
      }
      catch(e){
        alert("Can't connect to server.")
      }
    }else alert("Url is false.")

}
/**
 * getMethod
 *
 * Vykona pozadavek POST
 *
 */
Ajax.prototype.postMethod=function(){
  if(this.url){
    try{
        this.httpRequest.open("POST",this.url,true);
        this.httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
         this.httpRequest.setRequestHeader("Connection", "close");
        this.httpRequest.send(this.param);

      }
      catch(e){
        alert("Can't connect to server.")
      }
    }else alert("Url is false.")

}
/**
 * getData
 *
 * Vraci data
 *
 *@return getData
 *
 */
Ajax.prototype.getData=function(){
  return this.httpRequest.responseText;
}

/**
 * abort
 *
 * Ukonci pozadavek
 *
 *@return this.httpRequest.abort();
 *
 */
Ajax.prototype.abort=function(){
  alert("ano");
  return this.httpRequest.abort();
}

/**
 * state
 *
 * stav pozadavku
 *
 *@return this.httpRequest.readyState;
 *
 */
Ajax.prototype.state = function(){
	return this.httpRequest.readyState;
}

/**
 * status
 *
 * stavovy kod
 *
 *@return  this.httpRequest.status;
 *
 */
Ajax.prototype.status=function(){
  return this.httpRequest.status;
}

/**
 * statusText
 *
 * Odpoved serveru
 *
 *@return this.httpRequest.statusText;
 *
 */
Ajax.prototype.statusText=function(){
  return this.httpRequest.statusText;
}
