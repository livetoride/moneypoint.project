<?php

namespace MoneyPoint;

use DataGrid;

class PeriodList
{
	public function getList( PeriodFacade $periodFacade, $commonTranslator = null )
	{
		$ds = $periodFacade->getDatasource();
		$model = new \DataGrid\DataSources\Doctrine\QueryBuilder($ds);
		$model->mapping['id'] = 'a_id';

		$grid = new DataGrid\DataGrid;
		$grid->setTranslator($commonTranslator);
		$renderer = new DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setRenderer($renderer);

		$grid->itemsPerPage = 10;
		$grid->setDataSource($model);
		$grid->multiOrder = FALSE;
		
		$operations = array();
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, 'id' );

		$grid->addColumn('a_starts', 'počátek');
		$grid->addColumn('a_ends', 'konec');
		$grid->addColumn('a_period', 'účetní období');

		$grid['a_starts']->formatCallback[] = 'FormatingHelpers::czechDateShort';
		$grid['a_ends']->formatCallback[] = 'FormatingHelpers::czechDateShort';
				$grid['a_starts']->addFilter();
		$grid['a_starts']->addDefaultSorting();
		
		

		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
		$icon = \Nette\Utils\Html::el('span');
		$grid->addAction('Edit', 'edit', clone $icon->class('icon icon-edit'));
		return $grid;
	}

}
?>
