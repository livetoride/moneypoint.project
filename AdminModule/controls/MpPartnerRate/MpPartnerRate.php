<?php

/**
 * Nette Framework Extras
 *
 * This source file is subject to the New BSD License.
 *
 * For more information please see http://extras.nettephp.com
 *
 * @copyright  Copyright (c) 2009 David Grudl
 * @license    New BSD License
 * @link       http://extras.nettephp.com
 * @package    Nette Extras
 * @version    $Id: VisualPaginator.php 4 2009-07-14 15:22:02Z david@grudl.com $
 */

namespace MoneyPoint\Control;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use AntoninRykalsky as AR;

class PartnerRate extends Control
{
	/** @var \MoneyPoint\PartnersFacade */
	protected $partnersFacade;
	public function __construct( 
		\MoneyPoint\PartnersFacade $partnersFacade 
	) {
		$this->partnersFacade = $partnersFacade;
	}
	
	protected $showModal = 0;
	
	protected function createComponentRateForm() {
		$form = new Form();
		$renderer = $form->getRenderer();

		$form->addHidden('id');
		$form->addHidden('id_partner');
		$form->addText('rateName', 'Název sazby');
		$items = array( 1 => 'Procento', 2=> 'Bodová odměna' );
		$form->addSelect('feeType', 'Typ sazby', $items );
		$form->addText('fee', 'Sazba');

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedRateForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}


	function submitedRateForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			
			$this->partnersFacade->saveRate( $v );
			
			$this->presenter->flashMessage( 'Sazba byla uložena', \Flashes::$success );
			if( $this->presenter->isAjax() )
				$this->presenter->invalidateControl();
			else
				$this->presenter->redirect( 'this' );
			
		}
	}
	
	public function handleEdit( $id_partner, $id_rate = null )
	{
		$id_partner = $this->presenter->getParam('id_partner');

		
		$d = array();
		$d['id_partner'] = $id_partner;
		$f = $this['rateForm'];
		if( $id_rate !== null )
		{
			$rate = $this->partnersFacade->getPartnerFee( $id_rate );
			$d = array_merge( $rate->__toArray() );
		}
		$f->setDefaults( $d );
		
		$this->showModal = 1;
		$this->presenter->invalidateControl();
	}

	public function render()
	{
		$this->template->showModal = $this->showModal;
		$this->template->setFile(dirname(__FILE__) . '/template.phtml');
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
	}




}
