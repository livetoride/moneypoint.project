<?php

namespace MoneyPoint;

use Nette\Application\UI\Form;

class PeriodForm
{
	/** @var MoneyPoint\PeriodFacade */
	protected $periodFacade;
	
	function __construct( PeriodFacade $periodFacade, $presenter ) {
		$this->periodFacade = $periodFacade;
		$this->presenter = $presenter;
	}
	
	public function getForm() {
		$form = new Form();
		$renderer = $form->getRenderer();

		$form->addHidden('id');
		$form->addText('starts', 'počátek období');
		$form->addText('ends', 'konec období - včetně');
		$form->addText('period', 'název období');

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedPeriodForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		$this->form = $form;
		return $form;
	}

	function translate( $period )
	{
		return array(
			'id' => $period->getId(),
			'starts' => $period->getStarts()->format('d.n.Y'),
			'ends' => $period->getEnds()->format('d.n.Y'),
			'period' => $period->getPeriod()
		);
	}

	function submitedPeriodForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			
			try {
				$this->periodFacade->store( $v );
				$this->presenter->flashMessage('Uloženo', 'SUCCESS');
			} catch ( \Exception $e )
			{
				if( !$this->presenter->tryCatcherDebug )
					throw $e;
				
				
				$this->presenter->flashMessage('Uložení se nezdařilo', 'ERROR');
				if( $e instanceof \Doctrine\DBAL\DBALException && $e->getPrevious()->getCode() == "22001" )
					$this->presenter->flashMessage( "Maximální délka řetezce je omezena na 32 znaků", 'INFO');
				else
					$this->presenter->flashMessage( $e->getMessage(), 'INFO');
				
				return;
			}
			$this->presenter->redirect('default');
		}
	}
}
?>
