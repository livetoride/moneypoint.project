<?php

namespace MoneyPoint;

use Nette\Application\UI\Form;

class ChangeRankControl
{
	/** @var MoneyPoint\PeriodFacade */
	protected $periodFacade;
	
	protected $ranks;
	
	/** @var RankFacade */
	protected $rankFacade;

	function __construct( $presenter, $ranks, RankFacade $rankFacade ) {
//		$this->periodFacade = $periodFacade;
		$this->presenter = $presenter;
		$this->ranks = $ranks;
		$this->rankFacade = $rankFacade;
	}
	
	public function getForm() {
		$form = new Form();
		$renderer = $form->getRenderer();

		$form->addHidden('id');
		
		$r = array();
		foreach( $this->ranks as $k => $rank )
		{
			$r[$k]=$rank->getKey() . ' ' . $rank->getCarrier();
		}
		
		$form->addSelect('actual_setted', 'aktuální období', $r );
		$form->addSelect('last_setted', 'minulé období', $r );
		

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedPeriodForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		$this->form = $form;
		return $form;
	}

	function translate( $period )
	{
		return array(
			'last_counted' => $period->getId(),
			'last_setted' => $period->getStarts()->format('d.n.Y'),
			'actual_counted' => $period->getEnds()->format('d.n.Y'),
			'actual_setted' => $period->getPeriod()
		);
	}


	function submitedPeriodForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			if(empty($v->last_setted))
				@$v->last_setted = null;
			
//			try {
				$this->rankFacade->setRankChanges( $this->presenter->getParameter('idu'), $v->actual_setted, $v->last_setted );
				$this->presenter->flashMessage('Uloženo', 'SUCCESS');
//			} catch ( \Exception $e )
//			{
//				if( !$this->presenter->tryCatcherDebug )
//					throw $e;
//				
//				$this->presenter->flashMessage('Uložení se nezdařilo', 'ERROR');
//				
//				return;
//			}
			$this->presenter->redirect('this');
		}
	}
}
?>
