<?php
/**
 * Základní presenter apliakce
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */

use Nette\Environment;

abstract class Base_AMoneypointPresenter extends \Base_MlmPresenter
{

	protected function startup()
	{
		parent::startup();
		$this->template->registerHelperLoader('AdminModule\CmsModule\CmsHelpers::loader');
	}


}
