<?php

namespace MoneyPoint;

use Nette\Application\UI\Form;

class RankForm
{

	/** @var MoneyPoint\CommisionsFacade */
	protected $commisionsFacade;
	
	function __construct( CommisionsFacade $commisionsFacade, $presenter ) {
		$this->commisionsFacade = $commisionsFacade;
		$this->presenter = $presenter;
	}
	
	public function setFormInfo( $f, $ranks )
	{
		/* @var $ranks \MoneyPoint\Entity\CarrierGroup */
		/* @var $baseCommision \AntoninRykalsky\Entity\CommisionEntity */
//		\Doctrine\Common\Util\Debug::dump($ranks);exit;
		$baseCommision = $ranks->getBaseCommision();
		
		# defaults
		$d= array();
		$d['commisionName'] = $baseCommision->getName();
		$d['tsInserted'] = \FormatingHelpers::dateTimeCzf( $baseCommision->getTsInsert() );
			
		if( !empty( $baseCommision->getTsActive() )) {
			$d['tsUsedFrom'] = \FormatingHelpers::dateTimeCzf( $baseCommision->getTsActive() );
		}
		$f->setDefaults( $d );
		
		if( $baseCommision->getIsUsed() )
		{
			foreach( $this->commisionsFacade->getRanksKeys() as $k )
			{
				$f['key_'.$k]->setDisabled();
				$f['carrier_'.$k]->setDisabled();
				$f['min_'.$k]->setDisabled();
				$f['max_'.$k]->setDisabled();
				$f['priceCoef_'.$k]->setDisabled();
			}
			$f['useThis']->setDisabled();
		}
		$f['tsInserted']->setDisabled();
		$f['tsUsedFrom']->setDisabled();
		
	}


	public function getForm() {
		$form = new Form();
		$renderer = $form->getRenderer();

		$form->addText('commisionName', "Jméno této verze");
		$form->addText('tsInserted', "Datum vytvoření");
		$form->addText('tsUsedFrom', "Datum vytvoření");
		
		foreach( $this->commisionsFacade->getRanksKeys() as $k )
		{
			$form->addText('key_'.$k, 'klíč');
			$form->addText('carrier_'.$k, 'počátek období');
			$form->addText('min_'.$k, 'konec období - včetně');
			$form->addText('max_'.$k, 'název období');
			$form->addText('priceCoef_'.$k, 'název období');
		}
		$form->addCheckbox('useThis', 'Chci k tomuto momentu aktivovat tuto tabulku. Změnou se deaktivuje aktuálně platná tabulka.' );

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedRankForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		$this->form = $form;
		return $form;
	}

	function translate( $ranks )
	{
		$r= array();
		foreach( $ranks as $rank )
		{
			$k = $rank->getKey();
			$r['key_'.$k] = $rank->getKey();
			$r['carrier_'.$k] = $rank->getCarrier();
			$r['min_'.$k] = $rank->getCreditsMin();
			$r['max_'.$k] = $rank->getCreditsMax();
			$r['priceCoef_'.$k] = $rank->getPriceCoef();

		}
		return $r;
	}

	function submitedRankForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			
			$versionId = $this->presenter->getParameter('id');
			
			// require_once 'C:\wamp\www\beta.moneypoint.cz\app\modules\ApplicationBase\models\FormTableMapping\FormTableMapping';
			$formTableMapping = new \AntoninRykalsky\FormTableMapping();
			$keys = array('key', 'carrier', 'min', 'max', 'priceCoef');
			$formResults = $formTableMapping->getResults( $v, $keys );
//			try {
				$this->commisionsFacade->storeRankTable( $versionId, $formResults, $v->commisionName );
				
				$this->commisionsFacade->storeActive( $versionId, @$v->useThis );
				$this->presenter->flashMessage('Uloženo', 'SUCCESS');
//			} catch ( \Exception $e )
//			{
//				if( !$this->presenter->tryCatcherDebug )
//					throw $e;
//				
//				
//				$this->presenter->flashMessage('Uložení se nezdařilo', 'ERROR');
//				if( $e instanceof \Doctrine\DBAL\DBALException && $e->getPrevious()->getCode() == "22001" )
//					$this->presenter->flashMessage( "Maximální délka řetezce je omezena na 32 znaků", 'INFO');
//				else
//					$this->presenter->flashMessage( $e->getMessage(), 'INFO');
//				
//				return;
//			}
			$this->presenter->redirect('this');
		}
	}
}
?>
