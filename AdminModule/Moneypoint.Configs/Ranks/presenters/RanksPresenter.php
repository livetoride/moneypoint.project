<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule;

use MoneyPoint;

class RanksPresenter extends \Base_AdminPresenter
{
	/** @var MoneyPoint\CommisionsFacade */
	protected $commisionsFacade;

	function injectPartner( MoneyPoint\CommisionsFacade $commisionsFacade) {
		$this->commisionsFacade = $commisionsFacade;
	}
	
	public function actionDefault()
	{
		$this->template->title = 'Editace ranků';
		
		$form = $this['rankForm'];
		if (!$form->isSuccess()) {
			$this->flashMessage('Slouží pouze pro testovací účely', \Flashes::$info );
		}
		
		$this->template->ranksKeys = $this->commisionsFacade->getRanksKeys();
		$ranks = $this->template->ranks = $this->commisionsFacade->getRankTable();
		$this['rankForm']->setDefaults( $this->rankFormBuilder->translate( $ranks->getCarriers() ) );
	}
	
	protected $rankFormBuilder;
	protected function createComponentRankForm() {
		$this->rankFormBuilder = new MoneyPoint\RankForm( $this->commisionsFacade, $this );
		return $this->rankFormBuilder->getForm();
	}


}
