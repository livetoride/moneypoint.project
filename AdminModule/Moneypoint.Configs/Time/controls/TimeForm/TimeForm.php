<?php

namespace MoneyPoint;

use Nette\Application\UI\Form;

class TimeForm
{

	/** @var \AntoninRykalsky\TimeService */
	protected $timeService;
	
	function __construct( \AntoninRykalsky\TimeService $timeService, $presenter ) {
		$this->timeService = $timeService;
		$this->presenter = $presenter;
	}
	
	public function getForm() {
		$form = new Form();
		$renderer = $form->getRenderer();


		$form->addText('useThisTime', 'Datum a čas');
		$form->addCheckbox('useActualTime', 'Používat aktuální datum a čas');
		

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedTimeForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		$this->form = $form;
		return $form;
	}

	function translate()
	{
		$r= array();
		$r['useThisTime'] = $this->timeService->getDateTime()->format('j.n.Y H:i:s');
		$r['useActualTime'] = $this->timeService->shouldIUseActual();
		return $r;
	}

	function submitedTimeForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			
			$this->timeService->storeTime( $v->useActualTime, $v->useThisTime );
			$this->presenter->redirect(':Admin:default:default');
		}
	}
}
?>
