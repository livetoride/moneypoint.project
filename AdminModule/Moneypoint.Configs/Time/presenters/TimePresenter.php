<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule;

use MoneyPoint;

class TimePresenter extends \Base_AdminPresenter
{
	/** @var \AntoninRykalsky\TimeService */
	protected $timeService;

	function inject(\AntoninRykalsky\TimeService $timeService) {
		$this->timeService = $timeService;
	}
	public function actionDefault()
	{
		$this->template->title = 'Vlastní nastavení času systému';
		
		$form = $this['timeForm'];
		$form->setDefaults( $this->rankFormBuilder->translate() );
		if (!$form->isSuccess()) {
			$this->flashMessage('Slouží pouze pro testovací účely', \Flashes::$info );
		}
	}
	
	protected $rankFormBuilder;
	protected function createComponentTimeForm() {
		$this->rankFormBuilder = new MoneyPoint\TimeForm( $this->timeService, $this );
		return $this->rankFormBuilder->getForm();
	}


}
