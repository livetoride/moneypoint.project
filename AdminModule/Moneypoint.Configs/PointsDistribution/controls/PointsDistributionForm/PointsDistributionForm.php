<?php

namespace MoneyPoint;

use Nette\Application\UI\Form;

class PointsDistributionForm
{

	/** @var MoneyPoint\FixedCommisionFacade */
	protected $fixedCommisionFacade;
	
	function __construct( FixedCommisionFacade $fixedCommisionFacade, $presenter ) {
		$this->fixedCommisionFacade = $fixedCommisionFacade;
		$this->presenter = $presenter;
	}
	
	public function getForm() {
		$form = new Form();
		$renderer = $form->getRenderer();

		$types=array(1=>'bodů', 2=>'%');
		foreach( $this->fixedCommisionFacade->getDistributionLevelKeys() as $k )
		{
			$form->addText('level_'.$k, 'klíč');
			$form->addText('points_'.$k, 'počátek období');
		}
		
		$form->addRadioList('type', 'odměna zadána v', $types);
		$form->addCheckbox('useThis', 'Chci k tomuto momentu aktivovat tuto tabulku. Změnou se deaktivuje aktuálně platná tabulka.' );
		

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedRankForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		$this->form = $form;
		return $form;
	}

	function translate( $distributionsGroup )
	{
		$distributions = $distributionsGroup->getDistributions();
		$r= array();
		foreach( $distributions as $distribution )
		{
			$k = $distribution->getLevel();
			$r['level_'.$k] = $distribution->getLevel();
			$r['points_'.$k] = $distribution->getPoints( false );
		}
		$r['type'] = $distributionsGroup->getUnit();
	
		return $r;
	}
	function disableIfYouNeed( $distributionsGroup, $form )
	{
		$distributions = $distributionsGroup->getDistributions();
		if( $distributionsGroup->getBaseCommision()->getIsActive() )
		{
			foreach( $distributions as $distribution )
			{
				$k = $distribution->getLevel();
				$form['level_'.$k]->setDisabled();
				$form['points_'.$k]->setDisabled();
			}
			$form['type']->setDisabled();
			$form['save']->setDisabled();
		}
	}

	function submitedRankForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			$distributionTableId = $this->presenter->getParameter('id');
			
			$formTableMapping = new \AntoninRykalsky\FormTableMapping();
			$keys = array('level', 'points');
			$formResults = $formTableMapping->getResults( $v, $keys );
//			try {
				$this->fixedCommisionFacade->storeDistributionTable( $distributionTableId, $formResults, $v->type );
				$this->fixedCommisionFacade->storeActive( $distributionTableId, $v->useThis );
				$this->presenter->flashMessage('Uloženo', 'SUCCESS');
//			} catch ( \Exception $e )
//			{
//				if( !$this->presenter->tryCatcherDebug )
//					throw $e;
//				
//				
//				$this->presenter->flashMessage('Uložení se nezdařilo', 'ERROR');
//				if( $e instanceof \Doctrine\DBAL\DBALException && $e->getPrevious()->getCode() == "22001" )
//					$this->presenter->flashMessage( "Maximální délka řetezce je omezena na 32 znaků", 'INFO');
//				else
//					$this->presenter->flashMessage( $e->getMessage(), 'INFO');
//				
//				return;
//			}
			$this->presenter->redirect('this');
		}
	}
}
?>
