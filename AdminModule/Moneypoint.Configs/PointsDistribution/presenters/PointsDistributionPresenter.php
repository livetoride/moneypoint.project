<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule;

use MoneyPoint;

class PointsDistributionPresenter extends PointsDistributionShowPresenter
{
	/** @var MoneyPoint\FixedCommisionFacade */
	protected $fixedCommisionFacade;

	function injectPartner( MoneyPoint\FixedCommisionFacade $fixedCommisionFacade) {
		$this->fixedCommisionFacade = $fixedCommisionFacade;
	}
	
	public function actionEdit()
	{
		$this->template->title = 'Editace ranků';
		
		$form = $this['pointsDistributionForm'];
		if (!$form->isSuccess()) {
			$this->flashMessage('Slouží pouze pro testovací účely', \Flashes::$info );
		}
		
		$this->template->ranksKeys = $this->fixedCommisionFacade->getDistributionLevelKeys();
		$distributionGroup = $this->template->ranks = $this->fixedCommisionFacade->getDistributionGroup();
		$this['pointsDistributionForm']->setDefaults( $this->pointsDistributionFormBuilder->translate( $distributionGroup->getDistributions() ) );
	}
	
	protected $pointsDistributionFormBuilder;
	protected function createComponentPointsDistributionForm() {
		$this->pointsDistributionFormBuilder = new MoneyPoint\PointsDistributionForm( $this->fixedCommisionFacade, $this );
		return $this->pointsDistributionFormBuilder->getForm();
	}


}
