<?php
/**
 * Presenter obsluhující požadavky operací s kredit
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule;
use AntoninRykalsky\ProduktyLevelDistribution;
use AntoninRykalsky\CreditsDiscount;
use Nette\Application\UI\Form;


class PointsDistributionShowPresenter extends \Base_MlmPresenter
{
	public function actionDefault()
	{
		$this->template->title = 'Odměny šéfů za nákupy kreditu';
		$l = ProduktyLevelDistribution::get()->findAll()->fetchAll();
		$this->template->l = $l;

		$cd = CreditsDiscount::get()->findAll()->orderBy('id')->fetchAll();
		$this->template->cd = $cd;

		$this->template->coef =CreditsDiscount::get()->findAll()->orderBy('id', 'desc')->fetch()->price;

		$this->template->distribution = $this['distribution'];

		$d=array();$i=0;
		foreach ($l as $value) {
			$i++;
			$d['points_'.$i]=$value['points'];
		}
		$this['distribution']->setDefaults( $d );

		$this->template->distributor = ProduktyLevelDistribution::get();

//		$s = $this->getComponent('submenu');
//		$sec = $s->addSection('Možnosti:');
//		$sec->addItem('aktuální tabulka', $this->link('default' ) );
//		$sec->addItem('editace tabulky', $this->link('edit' ) );
//		$sec->addItem('seznam tabulek', $this->link('list' ) );
	}

	protected function createComponentDistribution()
    {
        $form = new Form($this, 'distribution');
		$renderer = $form->getRenderer();

		$form->addCheckbox('active', 'Nastav jako aktivní balíček');

		for( $i=1; $i<=10; $i++)
		{
			$form->addText('points_'.$i, "kreditu / $i balíček " );
		}


		$form->addSubmit('save', 'Uložit')->getControlPrototype()->class('default');

        $form->onSubmit[] = array($this, 'submitedForm');

		// přidej bezpečnostní ochranu, když je test režim vypnut
		if(!\AntoninRykalsky\Configs::get()->byContent('settings.testMode'))
			$form->addProtection('Please submit this form again (security token has expired).');

		return $form;
    }

	public function handleChangeCredits( $credits )
	{
		$this->template->coef = $credits;
		$this->invalidateControl('distributiontable');
	}
//	public function handleRecount( $credits )
//	{
//		$this->template->coef = $credits;
//		$this->invalidateControl('distributiontable');
//	}
}
