<?php

namespace MoneyPoint;

use Nette\Application\UI\Form;

class CommisionsForm
{
	/** @var MoneyPoint\CommisionsFacade */
	protected $commisionsFacade;
	
	function __construct( CommisionsFacade $CommisionsFacade, $presenter ) {
		$this->commisionsFacade = $commisionsFacade;
		$this->presenter = $presenter;
	}
	
	public function getForm() {
		$form = new Form();
		$renderer = $form->getRenderer();

		$form->addHidden('id');
		$form->addText('code', 'kód diagnózy');

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedCommisionsForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		$this->form = $form;
		return $form;
	}

	function translate( $commisions )
	{
		return array(
			'id' => $commisions->getId(),
			'code' => $commisions->getCode()
		);
	}

	function submitedCommisionsForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			
			try {
				$this->commisionsFacade->store( $v );
			} catch ( \Exception $e )
			{
				$this->presenter->flashMessage('Uloženo', 'SUCCESS');
				$this->presenter->flashMessage('Není uloženo', 'SUCCESS');
			}
			$this->presenter->redirect('default');
		}
	}
}
?>
