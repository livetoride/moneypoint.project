<?php

namespace MoneyPoint;

use DataGrid;

class CommisionProductList
{
	public function getList( CommisionProductFacade $commisionsFacade, $commonTranslator = null )
	{
		$ds = $commisionsFacade->getDatasource();
		$model = new \DataGrid\DataSources\PHPArray\PHPArray($ds);

		$grid = new DataGrid\DataGrid;
		$grid->setTranslator($commonTranslator);
		$renderer = new DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setRenderer($renderer);

		$grid->itemsPerPage = 10;
		$grid->setDataSource($model);
		$grid->multiOrder = FALSE;
		
		$operations = array();
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, 'id' );

		$grid->addColumn('groupname', 'Verze');
		$grid->addColumn('ts_insert', 'Vloženo');
		$grid->addColumn('active', 'Aktivní');
		$grid['active']->formatCallback[] = 'FormatingHelpers::yesNo';
		$grid->addColumn('is_used', 'Použito');
		$grid['is_used']->formatCallback[] = 'FormatingHelpers::yesNo';
		

		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
		$icon = \Nette\Utils\Html::el('span');
		$grid->addAction('Edit', 'edit', clone $icon->class('icon icon-edit'));
		return $grid;
	}

}
?>
