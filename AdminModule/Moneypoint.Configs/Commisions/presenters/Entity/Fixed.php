<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky\Commision;

class Fixed
{
	
	private $commisionTableId;
	
	private $produktyLevelDistributionGroup;
	
	public function init( $group )
	{
		$this->produktyLevelDistributionGroup = $group;
		if( empty( $group ))
		{
			$this->produktyLevelDistributionGroup = array();
		}
	}
	
	public function mapping( $v )
	{
		foreach( $v as $k=>$v )
		{
			if( property_exists( $this, $k ))
			{
				$this->$k = $v;
			}
		}
	}
	
	
	public function getCommisionTableId() {
		return $this->commisionTableId;
	}

	public function setCommisionTableId($commisionTableId) {
		$this->commisionTableId = $commisionTableId;
	}
	
	public function getLevelDistributionGroup() {
		return $this->produktyLevelDistributionGroup;
	}



	
}
