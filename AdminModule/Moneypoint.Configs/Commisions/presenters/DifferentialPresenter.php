<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule\CommisionsModule;

use MoneyPoint;
use DataGrid;

class DifferentialPresenter extends \Base_AdminPresenter
{
	const KEY=2;
	
	/** @var \MoneyPoint\CommisionsFacade */
	protected $commisionsFacade;
	
	/** @var MoneyPoint\FixedCommisionFacade */
	protected $fixedCommisionFacade;

	function injectPartner( MoneyPoint\CommisionsFacade $commisionsFacade,
			MoneyPoint\FixedCommisionFacade $fixedCommisionFacade 
	) {
		$this->commisionsFacade = $commisionsFacade;
		$this->fixedCommisionFacade = $fixedCommisionFacade;
	}

	
	public function actionDefault()
	{
		$this->template->title = "Rozdílové provize";
	}
	
	private function initStorage( $id )
	{
		$s = new \AntoninRykalsky\Commision\Fixed();
//		\DAO\BCommision::get()->update( $id, array('storage', json_encode($s))  )->fetch();
		return $s;
	}
	
	
	public function actionEdit( $id )
	{
		$this->template->title = "Rozdílové provize";
		
		$this->template->ranksKeys = $this->commisionsFacade->getRanksKeys();
		$ranks = $this->template->ranks = $this->commisionsFacade->getRankTable( $id );
		
		$this->template->isUsed = $ranks->getBaseCommision()->getIsUsed();
		
		if( !empty( $ranks ))
		{
			$this['rankForm']->setDefaults( $this->rankFormBuilder->translate( $ranks->getCarriers() ) );
		}
		$this['rankForm']; # need construct form
		$this->rankFormBuilder->setFormInfo( $this['rankForm'], $ranks );
		
				
		
		

	}
	
	private function getStorage( $id )
	{
		$commision = \DAO\BCommision::get()->find( $id )->fetch();
		if( $commision->storage === null )
		{
			$this->initStorage( $id );
			$commision = \DAO\BCommision::get()->find( $id )->fetch();
		}
//		$commision->storage->mapping( array('commisionTableId'=>2));
		$storage = $commision->storage;
	}
	
	protected $rankFormBuilder;
	protected function createComponentRankForm() {
		$this->rankFormBuilder = new MoneyPoint\RankForm( $this->commisionsFacade, $this );
		return $this->rankFormBuilder->getForm();
	}
	
	public function createComponentVersionList()
	{
		$ds = \DAO\BCommision::get()->findAll()->where('type_id=%i', self::KEY)->fetchAll();
		
		$model = new \DataGrid\DataSources\PHPArray\PHPArray($ds);

		$grid = new DataGrid\DataGrid;
//		$grid->setTranslator($commonTranslator);
		$renderer = new DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setRenderer($renderer);

		$grid->itemsPerPage = 10;
		$grid->setDataSource($model);
		$grid->multiOrder = FALSE;
		
		$operations = array();
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, 'id' );

		$grid->addColumn('key', 'název');
		$grid->addColumn('ts_insert', 'vloženo');
		$grid['ts_insert']->formatCallback[] = 'FormatingHelpers::czechDateShort';
		$grid['ts_insert']->addDefaultSorting('asc');
		$grid->addColumn('ts_active', 'aktivováno');
		$grid['ts_active']->formatCallback[] = 'FormatingHelpers::czechDateShort';
		$grid->addColumn('active', 'aktivní');
		$grid['active']->formatCallback[] = 'FormatingHelpers::yesNo';
		

		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
		$icon = \Nette\Utils\Html::el('span');
		$grid->addAction('Edit', 'edit', clone $icon->class('icon icon-edit'));
		return $grid;
	}
	
	public function actionCreate()
	{
		$id = $this->commisionsFacade->createNewDifferentialSet();
		$this->redirect( 'edit', $id );
	}

}
