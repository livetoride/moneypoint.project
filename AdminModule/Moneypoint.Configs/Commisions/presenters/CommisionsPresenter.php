<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule\CommisionsModule;

use MoneyPoint;

class CommisionsPresenter extends \Base_AdminPresenter
{
	/** @var MoneyPoint\CommisionsFacade */
	protected $commisionsFacade;

	/** @var MoneyPoint\CommisionProductFacade */
	protected $commisionProductFacade;

	function injectPartner( 
			MoneyPoint\CommisionsFacade $commisionsFacade,
			MoneyPoint\CommisionProductFacade $commisionProductFacade ) {
		$this->commisionsFacade = $commisionsFacade;
		$this->commisionProductFacade = $commisionProductFacade;
	}
	
	
	public function actionDefault()
	{
		$this->template->title = 'Nastavení provizního systému';
	}
	
	
	public function actionEdit( $id )
	{
		$key = $this->commisionsFacade->getCommisionKey($id);
		$this->redirect(":Admin:MoneyPoint:Commisions:".ucfirst($key).":default" );
		return;
	}
	
	protected function createComponentCommisionsList()     {
		$l = new MoneyPoint\CommisionsList;
		return $l->getList( $this->commisionsFacade, $this->commonTranslator );
	}
	
	protected function createComponentCommisionProductList()     {
		$l = new MoneyPoint\CommisionProductList;
		return $l->getList( $this->commisionProductFacade, $this->commonTranslator );
	}
}
