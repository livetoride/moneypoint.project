<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule\CommisionsModule;

use MoneyPoint;
use DataGrid;
use Nette\Application\UI\Form;

class FixedPresenter extends \Base_AdminPresenter
{
	const KEY=1;
	
	/** @var \MoneyPoint\CommisionsFacade */
	protected $commisionsFacade;
	
	/** @var \MoneyPoint\FixedCommisionFacade */
	protected $fixedCommisionFacade;

	function injectPartner( MoneyPoint\CommisionsFacade $commisionsFacade,
			MoneyPoint\FixedCommisionFacade $fixedCommisionFacade 
	) {
		$this->commisionsFacade = $commisionsFacade;
		$this->fixedCommisionFacade = $fixedCommisionFacade;
	}

	public function actionCreate()
	{
		$id = $this->fixedCommisionFacade->createNewSet();
		$this->redirect( 'edit', $id );
	}
	
	public function actionDefault()
	{
		$this->template->title = "Fixní provize";
	}
	
	private function initStorage( $id )
	{
		$s = new \AntoninRykalsky\Commision\Fixed();
//		\DAO\BCommision::get()->update( $id, array('storage', json_encode($s))  )->fetch();
		return $s;
	}
	
	private function getStorage( $id )
	{
		$commision = \DAO\BCommision::get()->find( $id )->fetch();
		if( $commision->storage === null )
		{
			$this->initStorage( $id );
			$commision = \DAO\BCommision::get()->find( $id )->fetch();
		}
//		$commision->storage->mapping( array('commisionTableId'=>2));
		$storage = $commision->storage;
	}
	
	public function actionEdit( $id )
	{
		$this->template->title = "Fixní provize";
		
		$commision = $this->getStorage( $id );
		
		
		$form = $this['pointsDistributionForm'];
		$this->template->ranksKeys = $this->fixedCommisionFacade->getDistributionLevelKeys();
		$distributionGroup = $this->template->ranks = $this->fixedCommisionFacade->getDistributionGroupByCommisionId( $id );
		
		$this->pointsDistributionFormBuilder->disableIfYouNeed( $distributionGroup, $this['pointsDistributionForm'] );
		$this['pointsDistributionForm']->setDefaults( $this->pointsDistributionFormBuilder->translate( $distributionGroup ) );
		
		$modifications = array(
			"firstBuyBonus" => "BPO",
			"firstBuyBonus" => "BPO",
		);
		
		
//		$this['fixedForm']->setDefaults(
//			array(
//				'commisionTableId' => $commision->storage->getCommisionTableId()
//			)
//		);
//		print_r( $commision->storage->getCommisionTableId() );
//		exit;
		
	}
	
	protected function createComponentFixedForm() {
		$form = new Form();
		$renderer = $form->getRenderer();

		$form->addText('commisionTableId', 'ID provizní tabulky');

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}


	function submitedForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = (array) $form->getValues();
			print_r($v);
			exit;
		}
	}
	
	public function createComponentVersionList()
	{
		$ds = \DAO\BCommision::get()->findAll()->where('type_id=%i', self::KEY)->fetchAll();
		
		$model = new \DataGrid\DataSources\PHPArray\PHPArray($ds);

		$grid = new DataGrid\DataGrid;
//		$grid->setTranslator($commonTranslator);
		$renderer = new DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setRenderer($renderer);

		$grid->itemsPerPage = 10;
		$grid->setDataSource($model);
		$grid->multiOrder = FALSE;
		
		$operations = array();
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, 'id' );

		$grid->addColumn('key', 'název');
		$grid->addColumn('ts_insert', 'vloženo');
		$grid->addColumn('active', 'aktivní');
		

		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
		$icon = \Nette\Utils\Html::el('span');
		$grid->addAction('Edit', 'edit', clone $icon->class('icon icon-edit'));
		return $grid;
	}
	
	
	

	protected $pointsDistributionFormBuilder;
	protected function createComponentPointsDistributionForm() {
		$this->pointsDistributionFormBuilder = new \MoneyPoint\PointsDistributionForm( $this->fixedCommisionFacade, $this );
		return $this->pointsDistributionFormBuilder->getForm();
	}
	
}
