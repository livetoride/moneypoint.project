<?php

namespace MoneyPoint;

use Nette\Application\UI\Form;

class CreditDiscountForm
{

	private $ALLOW_BUSINESS = 1;
	private $ALLOW_RANK = 1;
	private $ALLOW_BPO = 1;
	
	/** @var MoneyPoint\CreditDiscountFacade */
	protected $creditDiscountFacade;
	
	function __construct( CreditDiscountFacade $creditDiscountFacade, $presenter ) {
		$this->creditDiscountFacade = $creditDiscountFacade;
		$this->presenter = $presenter;
		
		$this->presenter->template->ALLOW_BUSINESS = $this->ALLOW_BUSINESS;
		$this->presenter->template->ALLOW_RANK = $this->ALLOW_RANK;
		$this->presenter->template->ALLOW_BPO = $this->ALLOW_BPO;
	}
	
	public function getForm() {
		$form = new Form();
		$renderer = $form->getRenderer();

		$form->addHidden('creditsDiscountId');
		foreach( $this->creditDiscountFacade->getDiscountKeys() as $k )
		{
			$form->addText('credits_'.$k, 'klíč');
			$form->addText('price_'.$k, 'počátek období');
			$form->addText('rewardratio_'.$k, 'koeficient fixních odměn*');
			if( $this->ALLOW_BUSINESS )
				$form->addCheckbox('business_'.$k, 'konec období - včetně');
			if( $this->ALLOW_RANK )
				$form->addText('activaterank_'.$k );
			if( $this->ALLOW_BPO )
				$form->addText('bpocoef_'.$k );
		}
		
		$form->addCheckbox('useThis', 'Chci k tomuto momentu aktivovat tuto tabulku. Změnou se deaktivuje aktuálně platná tabulka.' );
		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedRankForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		$this->form = $form;
		return $form;
	}

	function translate( $id, $discounts, $form )
	{
		$r= array(
			'creditsDiscountId' => $id
		);
		$k=0;
		foreach( $discounts->getDiscounts() as $discount )
		{
			$k++;
			$r['credits_'.$k] = $discount->getCredits();
			$r['price_'.$k] = $discount->getPrice();
			$r['rewardratio_'.$k] = $discount->getLevelDistributionCoef();
			if( $this->ALLOW_BUSINESS )
				$r['business_'.$k] = $discount->isBusinessActivation();
			if( $this->ALLOW_RANK )
				$r['activaterank_'.$k] = $discount->getActivateRank();
			if( $this->ALLOW_BPO )
				$r['bpocoef_'.$k] = $discount->getBpoCoef();
		}
		$r['useThis'] = $discounts->isUsed();
		
		
		if( $discounts->isUsed() )
		{
			$form['useThis']->setDisabled();
			$form['save']->setDisabled();
			foreach( $this->creditDiscountFacade->getDiscountKeys() as $k )
			{
				$form['credits_'.$k]->setDisabled();
				$form['price_'.$k]->setDisabled();
				$form['rewardratio_'.$k]->setDisabled();
				if( $this->ALLOW_BUSINESS )
					$form['business_'.$k]->setDisabled();
				if( $this->ALLOW_RANK )
					$form['activaterank_'.$k]->setDisabled();
				if( $this->ALLOW_BPO )
					$form['bpocoef_'.$k]->setDisabled();
			}
		}
		
		
		return $r;
	}

	function submitedRankForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			

			$formTableMapping = new \AntoninRykalsky\FormTableMapping();
			$keys = array('credits', 'price', 'business', 'activaterank', 'bpocoef', 'rewardratio' );
			$formResults = $formTableMapping->getResults( $v, $keys );
			
//			try {
				$this->creditDiscountFacade->storeCreditDiscountTable( $v['creditsDiscountId'], $formResults, $v['useThis'] );
				$this->presenter->flashMessage('Uloženo', 'SUCCESS');
//			} catch ( \Exception $e )
//			{
//				if( !$this->presenter->tryCatcherDebug )
//					throw $e;
//				
//				
//				$this->presenter->flashMessage('Uložení se nezdařilo', 'ERROR');
//				if( $e instanceof \Doctrine\DBAL\DBALException && $e->getPrevious()->getCode() == "22001" )
//					$this->presenter->flashMessage( "Maximální délka řetezce je omezena na 32 znaků", 'INFO');
//				else
//					$this->presenter->flashMessage( $e->getMessage(), 'INFO');
//				
//				return;
//			}
			$this->presenter->redirect('this');
		}
	}
}
?>
