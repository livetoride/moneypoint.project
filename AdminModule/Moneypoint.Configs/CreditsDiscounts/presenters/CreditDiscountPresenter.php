<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule;

use MoneyPoint;

class CreditDiscountPresenter extends \Base_AdminPresenter
{
	/** @var MoneyPoint\CommisionProductFacade */
	protected $commisionProductFacade;

	/** @var MoneyPoint\CreditDiscountFacade */
	protected $creditDiscountFacade;

	function injectPartner(
			MoneyPoint\CreditDiscountFacade $creditDiscountFacade,
			MoneyPoint\CommisionProductFacade $commisionProductFacade ) {
		$this->creditDiscountFacade = $creditDiscountFacade;
		$this->commisionProductFacade = $commisionProductFacade;
	}
	
	
	public function actionDefault()
	{
		$this->template->title = 'Možnosti zakoupení kreditu';
	}
	
	public function actionCreate()
	{
		$id = $this->creditDiscountFacade->createNewSet();
		$this->redirect( 'edit', $id );
	}
			
	public function actionEdit( $id )
	{
		$this->template->title = 'Možnosti zakoupení kreditu';
		
		$form = $this['creditDiscountForm'];
		$key = $this->creditDiscountFacade->getDiscountKeys($id);
		$this->template->discountKeys = $key;
		
		$discounts = $this->creditDiscountFacade->getDiscountGroup( $id );

		$this['creditDiscountForm']->setDefaults( $this->creditDiscountFormBuilder->translate( $id, $discounts, $this['creditDiscountForm'] ) );
		
	}
	
	protected function createComponentCreditDiscountList()     {
		$l = new MoneyPoint\CreditDiscountList;
		return $l->getList( $this->creditDiscountFacade, $this->commonTranslator );
	}
	
	protected $creditDiscountFormBuilder;
	protected function createComponentCreditDiscountForm() {
		$this->creditDiscountFormBuilder = new MoneyPoint\CreditDiscountForm( $this->creditDiscountFacade, $this );
		return $this->creditDiscountFormBuilder->getForm();
	}
	
	protected function createComponentCommisionProductList()     {
		$l = new MoneyPoint\CommisionProductList;
		return $l->getList( $this->commisionProductFacade, $this->commonTranslator );
	}


}
