<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class CreditsDiscount extends BaseModel
{

	/** @var string  table name */
	protected $table = 'mp_credits_discount';

	/** @var string|array  primary key column name */
	protected $primary = 'id';

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new CreditsDiscount();
		return $me;
	}

	public function __construct() {
		parent::__construct();
		$this->uniques = array( 'id_product', 'quantity' );

	}

	protected $vals = array(
		'id' => 'int',
		'id_product' => 'int',
		'quantity' => 'int',
		'credits' => 'int',
		'id' => 'int',
		'bonus' => 'int',
		'price' => 'int'

	);


	public function createOrReplace( $id_product, $quantity, $credits )
	{
		$value = array( 'credits'=> $credits );
		$ok = $this->updateWhere("id_product=$id_product AND quantity=$quantity", $value );
		if( !$ok )
		{
			$data = array('id_product'=>$id_product, 'quantity'=>$quantity,
				'credits'=>$credits, 'bonus'=>$bonus);
			$ok= $this->insert( $data )->fetchAll();
		}
	}

	public function getProductList()
	{
		$pT = Produkty::get()->table;
		$pP = Produkty::get()->primary;
		return Produkty::get()->findAll()
				->leftJoin($this->table)->as('cd')->on("[cd.id_product]=[$pT.$pP]")->where("[$pT.active]=1");
	}

}
