<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class ProduktyLevelDistribution extends BaseModel
{

	/** @var string  table name */
	protected $table = 'mp_produkty_level_distribution';

	/** @var string|array  primary key column name */
	protected $primary = 'id';

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new ProduktyLevelDistribution();
		return $me;
	}

	protected $vals = array(
		'id' => 'int',
		'id_product' => 'int',
		'level_depth' => 'int',
		'points' => 'int'
	);

	public function __construct() {
		parent::__construct();
		$this->uniques[] = array( 'id_product', 'leval_depth' );
	}

	public function createOrReplace($id_produkt, $depth, $val)
	{
		$value = array( 'points' => $val );

		$produkty = Produkty::get()->find($id_produkt)->fetchAll();
		if( count( $produkty ))
			$ok =  $this->updateWhere("id_product=$id_produkt AND level_depth=$depth", $value );
		if( !$ok )
		{
			$data = array('id_product'=>$id_produkt, 'level_depth'=>$depth, 'points'=>$value);
			$ok= $this->insert( $data )->fetchAll();
			print_r($ok);
		}
	}

	var $distrubutionTable;
	var $lowerPrice;
	var $businessKoef;
	/**
	 *
	 * @param type $price cena za kredit
	 * @param type $line linie v ktere doslo k nakupu
	 * @param type $business status business
	 * @param type $activatedLine aktivovane linie / nepouziva se
	 * @return int
	 */
	public function getKoeficient( $price, $line, $business = 0, $activatedLine = 5 )
	{
		$activatedLine = 10;
		$activatedLineBussiness = 10;

		if( empty($this->distrubutionTable ))
		{
			$this->distrubutionTable = $this->findAll()->fetchAssoc('level_depth', 'points');
			$this->lowerPrice = \AntoninRykalsky\CreditsDiscount::get()->findAll()->orderBy('price')->fetch()->price;
		}

		// pokud je status business, narust odměny o 100%
		$businessKoef=1; 
		if( $business )
			$businessKoef= \AntoninRykalsky\Configs::get()->byContent('multilevel.businessPlusFee')/100;

		$this->businessKoef = $businessKoef;
		
		// basic, jen 5 linií
		if( !$business && $line>$activatedLine ) return 0;

		// pokus je business, ale nema aktivovane linie
		if( $business && $line>$activatedLineBussiness ) return 0;

		$quantityCoef = $price/$this->lowerPrice;

//		print_r(array(
//			'$businessKoef' => $businessKoef,
//			'$quantityCoef' => $quantityCoef,
//			'$line' => $line,
//			'$this->distrubutionTable' => $this->distrubutionTable
//
//		)
//		);exit;

		return $businessKoef*$quantityCoef*$this->distrubutionTable[$line]->points;
	}
	
	private function getDistributionTable()
	{
		if( empty($this->distrubutionTable ))
		{
			$this->distrubutionTable = $this->findAll()->fetchAssoc('level_depth', 'points');
		}
		return $this->distrubutionTable;
	}
	
	private function getLowerPrice()
	{
		if( empty($this->lowerPrice ))
		{
			$this->lowerPrice = CreditsDiscount::get()->findAll()->orderBy('price')->fetch()->price;
		}
		return $this->lowerPrice;
	}


	public function getClaimByPercent( $price, $line, $business = 0 )
	{
		$distributionTable = $this->getDistributionTable();
		return ($price/100) * $distributionTable[$line]->points;
	}

}
