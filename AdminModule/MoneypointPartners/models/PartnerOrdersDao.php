<?php
/**
 * Model zacházející s tabulkou získaných bodů
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2011 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;
use dibi;
use AntoninRykalsky\DbDataTypes;

class PartnerOrdersDao extends BaseModel
{
	protected $table = 'partner_orders';
	protected $primary = 'id_order';

	/** skeleton pattern */
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new PartnerOrdersDao();
		return $me;
	}

	public function __construct()
	{
		$this->vals['id_order%i']					= DbDataTypes::$null;
		$this->vals['id_partner']				= DbDataTypes::$int;
		$this->vals['insert_timestamp'] 		= DbDataTypes::$timestamp;
		$this->vals['settle_timestamp'] 		= DbDataTypes::$timestamp;
		$this->vals['settled']					= DbDataTypes::$bool;
		$this->vals['utrata']					= DbDataTypes::$double;
		$this->vals['zakladni_odmena']			= DbDataTypes::$int;
		$this->vals['extra_odmena']				= DbDataTypes::$int;
		$this->vals['idu']						= DbDataTypes::$int;
		$this->vals['exp_settle_timestamp']		= DbDataTypes::$int;
		$this->vals['id_status']				= DbDataTypes::$int;

		parent::__construct(null, null);
	}


	protected $vals = array();

	public function getDataSource()
    {
		$this->connection = dibi::getConnection();
        return $this->connection->dataSource("
			SELECT t.*,
			(CASE WHEN t.partner_provider_id IN (3, 1, 2, 0) THEN ppf.shop
				ELSE '*manual*'
				END) as partnername,
			(CASE WHEN u.idu=0 THEN '-'
				ELSE u.jmeno || ' ' || u.prijmeni 
				END) as fullname

				
			FROM $this->table t
			LEFT JOIN uzivatele u ON u.idu = t.idu
			LEFT JOIN partner_fee_rate pfr ON t.fee_rate_id = pfr.id
			LEFT JOIN partners p ON p.id_partner = pfr.partner_id
			LEFT JOIN partner_provider_fee AS ppf ON t.id_order = ppf.partner_order_id
			WHERE cast(ppf.fee_original as double precision) > 0");
    }
	


}
