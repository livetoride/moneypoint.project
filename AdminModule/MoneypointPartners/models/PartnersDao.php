<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class PartnersDao extends BaseModel
{

	/** @var string  table name */
	protected $table = 'partners';

	/** @var string|array  primary key column name */
	protected $primary = 'id_partner';

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new PartnersDao();
		return $me;
	}


	protected $vals = array(
		'id_partner' => 'int',
		'type' => 'int',
		'link' => 'varchar',
		'name' => 'varchar',
		'full_link' => 'varchar',
		'img' => 'varchar',
		'path' => 'varchar',
		'provize' => 'int',
		'desc' => 'text',
		'instructions' => 'text'
	);

}
