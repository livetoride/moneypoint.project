CREATE TABLE "partner_orders" (
	"id_order" serial,
	"id_partner" int4,
	"insert_timestamp" timestamp,
	"settle_timestamp" timestamp,
	"settled" numeric,
	"idu" int4,
	"utrata" float8,
	"zakladni_odmena" int4,
	"extra_odmena" int4,
	"exp_settle_timestamp" timestamp,
	"id_status" int4
);

ALTER TABLE partner_orders
  ADD PRIMARY KEY (id_order);
