<?php
/**
 *
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule;
use AntoninRykalsky\PartnersDao;
use Nette\Utils\Html;
use Nette\Application\UI\Form;
use AntoninRykalsky\Configs;
use Datagrid;

class EshopPresenter extends \Base_MlmPresenter
{
	/** @var \MoneyPoint\eshopFacade */
	protected $eshopFacade;
	
	/** @var \MoneyPoint\TagFacade */
	protected $tagFacade;
	/** @var \MoneyPoint\CountryFacade */
	protected $countryFacade;
	
	function injectEshopFacade( \MoneyPoint\EshopFacade $eshopFacade,
			\MoneyPoint\TagFacade $tagFacade, \MoneyPoint\CountryFacade $countryFacade )
    {
        $this->eshopFacade = $eshopFacade;
        $this->tagFacade = $tagFacade;
        $this->countryFacade = $countryFacade;
    }
	
	
	
	public function actionDefault( )
    {
		$this->template->title = 'E-shopy';
    }

	public function actionArchived( )
    {
		$this->template->title = 'Deaktivované obchody';
		$this->setView("default");
    }

	
	
	public function actionEdit( $id_partner = null )
    {	
		$this->template->title = 'E-shop';
		$f = $this['form'];
		$this->template->form = $f;
		
		$tags = \DAO\BTags::get()->findAll()->orderBy('tag')->fetchPairs('id', 'tag');
		$dd = '"' . implode('","', $tags) . '"';
		$this->template->allTags = $dd;
		
		$country = \DAO\BCountry::get()->findAll()->orderBy('country')->fetchPairs('id', 'country');
		$im = '"' . implode('","', $country) . '"';
		$this->template->allCountries = $im;		
		
		
		/*$def = \DAO\PartnerTags::get()->findAll()
				->join(\DAO\BTags::get()->getTable())
				->on('b_tags.id=partner_tags.tag_id')
				->where('partner_tags.partner_id=%i',$id_partner)
				->fetchPairs('id', 'tag'); */

		
		if(!empty( $id_partner ))
		{
			$defaults = array();
			/* @var $shop Entity\Eshop */
			$shop = $this->eshopFacade->getEshop( $id_partner );
			$defaults += $shop->__toArray();
			
			$defaultsTag = $this->tagFacade->getEshopTags( $shop );
	//		$defaults += array("tag"=>implode(',', $defaultsTag)  );
			$defaults['tag']=implode(',', $defaultsTag);
			$defaultsCountry = $this->countryFacade->getEshopCountry($shop);
	//		print_r($defaultsCountry);exit();
	//		$defaults += array("country"=>implode(',', $defaultsCountry)  );
			$defaults['country']=implode(',', $defaultsCountry);
			
			
			$this->template->partner = $shop;
			$this->template->partnerFees = $this->eshopFacade->getPartnerFees( $shop );
			$this->template->partnerImg = $shop->getImg();
			$this->template->title .= ' '.$shop->getName();
			
			
			$f->setDefaults( $defaults );
		}
//		\Doctrine\Common\Util\Debug::dump( $defaults ) ;exit;
		
    }

	protected function createComponentPartnerRate()
	{
		return new \MoneyPoint\Control\PartnerRate( $this->eshopFacade );
	}

	protected function createComponentForm()
    {
        $form = new Form($this, 'form');
		$renderer = $form->getRenderer();


        $form->addHidden('id_partner');
        $form->addText('name', 'Jméno E-shopu:')
				->addRule( Form::FILLED, 'Jméno E-shopu je povinná položka' );

		$form->addText('reward', 'Odměna:');
		$items = \MoneyPoint\Entity\Eshop::$PROVIDERS;
		$form->addSelect('provider_id', 'Provider', $items)
			->setPrompt('Vyberte');
		$form->addText('link', 'Provizní odkaz:')
				->addRule( Form::REGEXP, 'Zadejte referenční odkaz v platném tvaru (např. http://seznam.cz/)', '/(^http.+$)/' );
		$form->addText('img', 'Odkaz na obrázek:');
		$form->addUpload('imgUpload');

		/*provize
		$form->addText('provize', 'Odměna v %:')
			->addRule( Form::RANGE, 'Zadejte odměnu v procentech v intervalu %d-%d', array(0, 50 ));
		*/

		$form->addTextArea('desc', 'popis');

		$form->addTextArea('instructions', 'instrukce k nákupu');
		
		$form->addHidden('tag');
		$form->addHidden('country');

		$form->addSubmit('save', 'Uložit');#->getControlPrototype()->class('ajax default');

        $form->onSuccess[] = array($this, 'submitedForm');

		// přidej bezpečnostní ochranu, když je test režim vypnut
		if(!Configs::get()->byContent('settings.testMode'))
			$form->addProtection('Please submit this form again (security token has expired).');

		return $form;
    }

	/** ConfirmationDialog factory */
	public function createComponentConfirmForm()
	{
		$form = new \ConfirmationDialog();

		// you can easily create AJAX confirm form with eg. jquery.ajaxforms.js
		$form->getFormElementPrototype()->addClass('ajax');
		$form->dialogClass = 'static_dialog';

		// create dynamic signal for 'confirmDelete!'
		$form->addConfirmer(
			'removeFee',
			array($this, 'confirmedRemoveFee'),
			sprintf('Opravdu chcete odstranit tuto sazbu?')
			);
		// create dynamic signal for 'confirmDelete!'
		$form->addConfirmer(
			'removeEshop',
			array($this, 'confirmedRemoveEshop'),
			sprintf('Opravdu chcete odstranit E-shop?')
			);
		$form->addConfirmer(
			'removeTag',
			array($this, 'confirmedRemoveTag'),
			sprintf('Opravdu chcete odstranit kategorii?')
			);

		return $form;
	}

	public function confirmedRemoveEshop( $id_partner )
	{
		$r = $this->eshopFacade->deletePartner( $id_partner );
		if( $r )
			$this->flashMessage('E-shop byl odstraněn', \Flashes::$success );
		else
			$this->flashMessage('E-shop se neporařilo odstranit', \Flashes::$error );
		$this->redirect('this');
	}
	
	public function confirmedRemoveTag( $id )
	{
		
		$r = $this->eshopFacade->deleteTag( $id );
		if( $r )
			$this->flashMessage('Kategorie byla odstraněna', \Flashes::$success );
		else
			$this->flashMessage('Kategorii se neporařilo odstranit', \Flashes::$error );
		$this->redirect('this');
	}
	
	public function confirmedRemoveFee( $id_rate )
	{
		$r = $this->eshopFacade->removePartnerFee( $id_rate );
		if( $r )
			$this->flashMessage('Sazba byla odstraněna', \Flashes::$success );
		else
			$this->flashMessage('Sazbu se nepodařilo odstranit', \Flashes::$success );
		$this->redirect('this');
	}


	function submitedForm( Form $form )
	{
		$v = (array) $form->getValues();
		
		$idPartner = $this->getParameter('id_partner');
		$idPartner = $this->tryCatcher( '$this->saveEshop', $v );
		
		$v['tag'] = explode(',', $v['tag'] );
		$this->tagFacade->storeTagToEshop( $idPartner, $v['tag']);
		
		$v['country'] = explode(',', $v['country'] );
		$this->countryFacade->storeCountryToEshop( $idPartner, $v['country']);
		
		
		$this->flashMessage("Uloženo", \Flashes::$success );
		if( $this->isAjax() && !empty($idPartner) )
			$this->invalidateControl();
		else
			$this->redirect('edit', $idPartner);
		
	}

	protected function saveEshop( $v )
	{
		$idPartner = $this->getParameter('id_partner');
		
		$v['type'] = 1; // PIO
		$v['path'] = \Nette\Utils\Strings::webalize( $v['name'] );
		

		$photo = $v['imgUpload'];
		unset( $v['imgUpload'] );
		unset( $v['id_partner'] );
		
		if( !empty( $idPartner ))
		{
			\DAO\Partners::get()->update( $idPartner, $v );
			$id = $idPartner;
		} else {
			\DAO\Partners::get()->insert( $v );
			$id = \dibi::getInsertId();
		}
		
		if( $photo->isOk() )
		{
			$fileSmall = WWW_DIR."/images/partners/small/$idPartner-".$photo->getName();
			$fileOrig = WWW_DIR."/images/partners/orig/$idPartner-".$photo->getName();
			@unlink( $fileOrig );
			move_uploaded_file(
				$photo->getTemporaryFile(),
				$fileOrig
				);

			$image = \Nette\Image::fromFile( $fileOrig );
			$image->resize(100, 100);
			$image->save( $fileSmall );
			$v['img'] = "[+baseUri+]/images/partners/small/$idPartner-".$photo->getName();
			\DAO\Partners::get()->update( $idPartner, $v );
		}
		
		return $id;
	}
	
	protected function createComponentList()
    {
		$action = $this->getAction();
		$model = $this->eshopFacade->getEshopDatasource( $action );
		//datagrid
		$model = new \DataGrid\DataSources\Dibi\DataSource($model);
		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setDataSource( $model );
		$grid->setRenderer($renderer);
		$grid->itemsPerPage = 10;
		$grid->multiOrder = FALSE;

		$operations = array(); // define operations
		$operations['default'] = array('deactivateItem'=>"deaktivovat");
		$operations['archived'] = array('activateItem'=>"aktivovat");
		$callback = array($this, 'EshopListOperationHandler');
		$grid->allowOperations($operations[$action], $callback, PartnersDao::get()->getPrimary()); // allows checkboxes to do operations with more rows

		// if no columns are defined, takes all cols from given data source
		// id_partner	type	link	name	full_link	img
		$grid->addColumn('name', 'Eshop');
		$grid->addNumericColumn('provider_id', 'provider');

		$grid['provider_id']->replacement = \MoneyPoint\Entity\Eshop::$PROVIDERS;
		
		$grid['name']->addFilter();
		$grid['provider_id']->addSelectboxFilter(\MoneyPoint\Entity\Eshop::$PROVIDERS);

		// archivní položky ?
		$archive = $this->getParam('archived');


		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
		$icon = Html::el('span');
		$grid->addAction('Edit', 'edit', clone $icon->class('icon icon-edit'));
		$grid->addAction('Delete', 'confirmForm:confirmRemoveEshop!', clone $icon->class('icon icon-del'));


		return $grid;
    }
	
	public function EshopListOperationHandler( $button)
	{
		// presmeruj podle typu operace
		$form = $button->getParent();
		$values = $form->getValues();
		call_user_func( array($this, $values->operations), (array)($values->checker) ); 
	}
	
	
	private function activateItem( $items )
	{
		$count = $this->eshopFacade->activatePartnerWhatever( $items );
		$this->flashMessage("Aktivace ".$count." obchodů byla úspěšná.", \Flashes::$success );
		$this->redirect('this');
		
	}
	private function deactivateItem( $items )
	{
		$count = $this->eshopFacade->deactivatePartnerWhatever( $items );
		$this->flashMessage("Deaktivace ".$count." obchodů byla úspěšná.", \Flashes::$success );
		$this->redirect('this');
	}



}
