<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule;
use Nette\Utils\Html;

use \AntoninRykalsky\PartnerOrdersDao;
use AntoninRykalsky\Uzivatele;
use MoneyPoint;

class PartnerOrdersPresenter extends \Base_MlmPresenter
{
	/** @var \MoneyPoint\PartnerOrderFacade */
	protected $partnerOrderFacade;
	function injectPartnerOrder( \MoneyPoint\PartnerOrderFacade $partnerOrderFacade )
    {
        $this->partnerOrderFacade = $partnerOrderFacade;
    }
	
	/** @var \MoneyPoint\PartnersFacade */
	protected $partnersFacade;
	function injectPartner( \MoneyPoint\PartnersFacade $partnersFacade )
    {
		$this->partnersFacade = $partnersFacade;
    }
	
	/** @var \MoneyPoint\PartnerProviderInfoFacade */
	protected $partnerProviderInfoFacade;
	function injectPartnerProviderInfoFacade(\MoneyPoint\PartnerProviderInfoFacade $partnerProviderInfoFacade )
    {
		$this->partnerProviderInfoFacade = $partnerProviderInfoFacade;
    }
	
	

	// <editor-fold defaultstate="collapsed" desc=" default, datagrid">
	public function actionDefault( )
    {
		$this->template->title = "Nákupy v obchodech";
		$this->template->mvcAction = $this->getAction();

		$this->breadDefault = 0;
		$section = $this->getComponent('navigation');
		$subsection = $section->add( 'nákupy v obchodech', $this->link("default") );
		$subsubsection = $subsection->add( 'čekací lhůta', '' );
		$section->setCurrent($subsubsection);
    }
	
	public function actionCjDefault( )
    {
		$this->template->title = "CJ notifikace";
    }


	public function actionCjWaiting( )
    {
		$this->actionDefault();
		$this->template->title = "Nové objednávky ze CJ";
		$this->template->mvcAction = $this->getAction();
		$this->setView('default');

		$this->breadDefault = 0;
		$section = $this->getComponent('navigation');
		$subsection = $section->add( 'nákupy v obchodech', $this->link("default") );
		$subsubsection = $subsection->add( 'nové objednávky ze CJ', '' );
		$section->setCurrent($subsubsection);
    }
	
	public function actionSettled( )
    {
		$this->actionDefault();
		$this->template->title = "Nákupy v obchodech";
		$this->template->mvcAction = $this->getAction();
		$this->setView('default');

		$this->breadDefault = 0;
		$section = $this->getComponent('navigation');
		$subsection = $section->add( 'nákupy v obchodech', $this->link("default") );
		$subsubsection = $subsection->add( 'připsané', '' );
		$section->setCurrent($subsubsection);
    }

	public function actionCreate( $idu=null )
    {
//		$c = new \MoneyPoint\Entity\PartnerOrderEntity( 1 );
//		echo $c->getUtrata();
//		echo $c->getClient()->getJmeno();
//		echo $c->getPartner()->getName();
		
		$this->template->title = 'Nákup v obchodě [nový záznam]';

		$this->template->type = 'create';
		$f = $this['partnerOrderForm'];
		$d['create'] = $this->getAction() == 'create';
		if( !empty( $idu ))
		{
			$this->template->user = Uzivatele::get()->find( $idu )->fetch();
			$d = array(); $d['idu'] = $idu;
			
		}
		$f->setDefaults( $d );

		$this->setView('edit');
    }

	public function actionCjEdit( $id )
    {
		$this->template->title = 'Provize od CJ.com';

			$this['providerInfo']->setProviderInfo( $id );
    }
	
	/** @var \MoneyPoint\Entity\PartnerOrder */
	public $partnerOrder;
	
	private function getPartnerOrder()
	{
		$id = $this->getParameter( PartnerOrdersDao::get()->getPrimary() );
		$this->partnerOrder = $this->partnerOrderFacade->details( $id );

		return $this->partnerOrder;
		
	}
	
	public function actionEdit( $id_order )
    {
		$this->template->title = 'Nákup v obchodě';

		$this->getPartnerOrder();
		$def=array();
		$def['order']=$this->partnerOrder;
		$def['create']=false;
		$this['partnerOrderForm']->setDefaults( $def );

		$statuses = array('CJ nové', 'čekající', 'ke kontrole', 'vyřízeno');
		$statusesLinks = array('cjWaiting', 'waiting', 'default', 'settled');

		$this->breadDefault = 0;
		$section = $this->getComponent('navigation');
		$subsection = $section->add( 'nákupy v obchodech', $this->link("default") );
		
		$subsubsection = $subsection->add( $statuses[$this->partnerOrder->getIdStatus()] , $statusesLinks[$this->partnerOrder->getIdStatus()] );
		$subsubsection2 = $subsubsection->add( 'editace', '' );
		$section->setCurrent($subsubsection2);

    }

	/** @var MoneyPoint\ProviderInfoFactory */
	protected $providerInfoFactory;

	function injectProviderInfoFactory( MoneyPoint\ProviderInfoFactory $providerInfoFactory) {
		$this->providerInfoFactory = $providerInfoFactory;
	}
	
	protected function createComponentProviderInfo()
	{
		return $this->providerInfoFactory->create();
	}
	
	protected function createComponentList()
    {
        $model = PartnerOrdersDao::get()->getDataSource();

		// pouze aktivní/archivni
		switch ($this->getAction()) {
			case 'cj-waiting':
//				$model->where('[settled]=0 AND [exp_settle_timestamp]<=now()');
				$model->where('[id_status]='.\MoneyPoint\Entity\PartnerOrder::STATE_CJ_NEW );
				break;
			
			case 'default':
//				$model->where('[settled]=0 AND [exp_settle_timestamp]<=now()');
				$model->where('[id_status]='.\MoneyPoint\Entity\PartnerOrder::STATE_WAITING);
				break;

			case 'settled':
//				$model->where('[settled]=1');
				$model->where('[id_status]='.\MoneyPoint\Entity\PartnerOrder::STATE_REWARDED );
				break;
		}

		//datagrid
		$model = new \DataGrid\DataSources\Dibi\DataSource($model);
		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setDataSource( $model );
		$grid->setRenderer($renderer);
		$grid->itemsPerPage = 10;
		$grid->multiOrder = FALSE;

		$operations = array(); // define operations
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, PartnerOrdersDao::get()->getPrimary()); // allows checkboxes to do operations with more rows

		// if no columns are defined, takes all cols from given data source
		// id_partner	type	link	name	full_link	img
		$grid->addColumn('fullname', 'klient');
		$grid->addColumn('partnername', 'partner');
		$grid->addColumn('utrata', 'cena objednávky')
			->formatCallback[] = 'MpHelpers::currency2';
		$grid->addColumn('insert_timestamp', 'datum akce')
				->formatCallback[] = 'MpHelpers::datetime';
		$grid['insert_timestamp']->addDefaultSorting("DESC");
		
		$grid->addColumn('id_status', 'stav');
		$grid['id_status']->replacement = \Moneypoint\PartnerOrderFacade::$states;



		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
		$icon = Html::el('span');
		$grid->addAction('Upravit', 'edit', clone $icon->class('icon icon-edit'));
		return $grid;
    }
	protected function createComponentCjList()
    {
		$model = \DAO\PartnerProviderFee::get()->getDataSource();
	
		//datagrid
		$model = new \DataGrid\DataSources\Dibi\DataSource($model);
		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setDataSource( $model );
		$grid->setRenderer($renderer);
		$grid->itemsPerPage = 10;
		$grid->multiOrder = FALSE;

		$operations = array(); // define operations
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, \DAO\PartnerProviderFee::get()->getPrimary() ); // allows checkboxes to do operations with more rows

		// if no columns are defined, takes all cols from given data source
		// id_partner	type	link	partnername	full_link	img
		$grid->addColumn('provider', 'provider');
		$grid->addColumn('ts_event', 'ts_event')->formatCallback[] = 'FormatingHelpers::czechDate';
		$grid->addColumn('fee', 'fee')->formatCallback[] = 'FormatingHelpers::currency';
		$grid->addColumn('original_id', 'original_id');
		$grid->addColumn('fee_original', 'fee_original')->formatCallback[] = 'MpHelpers::partnerEuro';
		$grid->addColumn('fee_currency', 'fee_currency')->formatCallback[] = 'MpHelpers::partnerEuroCoef';
		$grid->addColumn('shop', 'shop');
		


		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
		$icon = Html::el('span');
		$grid->addAction('Upravit', 'cjEdit', clone $icon->class('icon icon-edit'));
		return $grid;
    }
	// </editor-fold>


	// <editor-fold defaultstate="collapsed" desc=" form">

	
	
	
	
	private $cjFormControlFactory;
	private $partnerFormControlFactory;

	public function injectRegistrationControlFactory(
			MoneyPoint\CjFormControlFactory $cjFormControlFactory,
			MoneyPoint\PartnerFormControlFactory $partnerFormControlFactory
	) {
		$this->cjFormControlFactory = $cjFormControlFactory;
		$this->partnerFormControlFactory = $partnerFormControlFactory;
	}
	
	protected function createComponentPartnerOrderForm()
    {
		$this->getPartnerOrder();
		$po = $this->partnerOrder;
		//\Doctrine\Common\Util\Debug::dump($po);exit(); 
		//\Doctrine\Common\Util\Debug::dump( $po->getPartnerProvider() ); exit;
		if( !empty($po) && $po->getPartnerProvider()->isCjType()  )
		{
			//echo 'jsem v cj proč?'; exit();
			$form = $this->cjFormControlFactory->create();
		} else {
			$form = $this->partnerFormControlFactory->create();
		}
		return $form;
    }

	// </editor-fold>


}
