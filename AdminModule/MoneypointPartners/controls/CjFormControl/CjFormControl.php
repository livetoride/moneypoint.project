<?php

/**
 * Nette Framework Extras
 *
 * This source file is subject to the New BSD License.
 *
 * For more information please see http://extras.nettephp.com
 *
 * @copyright  Copyright (c) 2009 David Grudl
 * @license    New BSD License
 * @link       http://extras.nettephp.com
 * @package    Nette Extras
 * @version    $Id: VisualPaginator.php 4 2009-07-14 15:22:02Z david@grudl.com $
 */

namespace MoneyPoint;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use AntoninRykalsky\Configs;
use AntoninRykalsky as AR;


class CjFormControlFactory extends AR\ControlFactory
{
	protected $className = 'MoneyPoint\CjFormControl';
}

class CjFormControl extends Control
{
	
	private $partnersFacade;
	private $partnerProviderInfoFacade;
	
	/** @var PartnerOrderFacade */
	protected $partnerOrderFacade;

	/** @var Entity\PartnerOrder */
	private $partnerOrder;
	
	public function __construct( PartnersFacade $partnersFacade, PartnerProviderInfoFacade $partnerProviderInfoFacade, PartnerOrderFacade $partnerOrderFacade )
    {
        $this->partnersFacade = $partnersFacade;
        $this->partnerProviderInfoFacade = $partnerProviderInfoFacade;
		$this->partnerOrderFacade = $partnerOrderFacade;

    }
	
	protected function createComponentProviderInfo()
	{
		return new \MoneyPoint\ProviderInfo( $this->partnerProviderInfoFacade );
	}
	
	protected function createComponentCjForm()
	{
		$form = new Form($this, 'cjForm');

		// parametry
		$form->addHidden('idu');
		$form['idu']->getControlPrototype()->class='bigdrop select2-offscreen';
		$form['idu']->getControlPrototype()->style='width:300px';
				
		$form->addHidden('id_order');
		
		$WITH_CATEGORIES = 0;
		$partners = $this->partnersFacade->getPartnerFeesSelect( $WITH_CATEGORIES );
		$form->addSelect('provider', 'Nákup u partnera', $partners);
		$form['provider']->setDisabled();
		$form->addText('shop', 'Obchod');

		$status = array('odměna nepřiřazena', 'odměna přiřazena');
		$form->addSelect('settled', 'Stav objednávky', $status);

		
//		$status = array(1=>'čekající',  'vyřízeno');
		$form->addText('state', 'Stav objednávky' );
		$form['state']->setDisabled();
		
		$states=array();
		if(!empty($this->presenter->partnerOrder))
		{
			$states = $this->partnersFacade->getStateMachine()->getAvailableStatesUserTexts( $this->presenter->partnerOrder->getIdStatus() );
			$form->addSelect('id_status', 'Stav objednávky', $states );
		}
		


		$form->addText('utrata', 'Nákup v ceně [bez DPH?]');
		
		$form->addText('reward', 'Odměna [body, základ pro výpočet]');

		$form->addSubmit('save', 'Uložit')->getControlPrototype()->class('default');

        $form->onSuccess[] = array($this, 'submitedForm');

		// přidej bezpečnostní ochranu, když je test režim vypnut
		if(!Configs::get()->byContent('settings.testMode'))
			$form->addProtection('Please submit this form again (security token has expired).');

		return $form;
	}
	
	public function setDefaults( $partnerOrder )
	{
		$d=array();
				
		// gets partnerOrder from presenter
		$this->partnerOrder = $partnerOrder['order'];

		// mapping
		$d['utrata'] = $this->partnerOrder->getUtrata();
		$d['reward'] = $this->partnerOrder->getZakladniOdmena();
		$d['idu'] = $this->partnerOrder->getUser()->getIdu();
		$d['id_order'] = $this->partnerOrder->getIdOrder();
		$d['settled'] = $this->partnerOrder->getSettled();
		$d['shop'] = $this->partnerOrder->getPartnerProviderFeeOne()->getShop();
		
		
////		if( !empty( $d['ppf_id'] ) )
////			$this['providerInfo']->setProviderInfo( $d['ppf_id'] );
//		
//		// tecky a carky
//		if( !empty($d['utrata']))  $d['utrata'] = str_replace('.', ',', $d['utrata']);
//		$d['fee_rate'] = $d['fee_rate_id'];
//		$d['reward'] = $d['zakladni_odmena'];
		
		$d['state'] = $this->partnersFacade->getStateMachine()->getStateUserTexts( $this->partnerOrder->getIdStatus() );
		$this->createComponentCjForm()->setDefaults( $d );
		
		if( $this->presenter->partnerOrder->getUser()->getIdu()==0 )
		{
			$this->presenter->flashMessage("Nakujupující klient není znám", \Flashes::$info );
		}
		
		$this['providerInfo']->setPartnerOrder( $this->partnerOrder );
	}
	
	public function render()
	{
		$this->template->partnerOrder = $this->presenter->partnerOrder;
		
		
		
		$this->template->setFile(dirname(__FILE__) .'/cjFormControl.phtml' );
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
	}

	
	function submitedForm( Form $form )
    {
        if(isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
//			$v->createNew = (int)($this->getAction() === 'create');
			$this->partnerOrder->getPartnerProviderFeeOne()->setShop($v['shop']);
			$partnerOrder = $this->partnerOrderFacade->changeState( $v );
			switch ( $partnerOrder->getIdStatus() ){
				case 3:
					$this->presenter->redirect('settled');
					break;

				case 2:
					$this->presenter->redirect('waiting');
					break;

				default:
					$this->presenter->redirect('default');
					break;
			}

		}
	}



}
