<?php
namespace MoneyPoint;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use AntoninRykalsky\Configs;

interface IMakeTagsControlFactory
{
    /** @return \MoneyPoint\MakeTagsControl */
    function create();
}

class MakeTagsControlFactory implements IMakeTagsControlFactory
{
    
	public function __construct( PartnerFacade $partnerFacade, ProviderInfoFactory $providerInfoFactory, PartnerOrderFacade $partnerOrderFacade )
    {
        $this->partnerFacade = $partnerFacade;
      
    }

    public function create()
    {
        return new MakeTagsControl( $this->partnerFacade);
    }
}
class MakeTagsControl extends Control {
	
	/** @var PartnerFacade */
	protected $partnerFacade;
	
	public function __construct( PartnerFacade $partnerFacade) {
		$this->partnerFacade = $partnerFacade;
		
	}
	

	protected function createComponentMakeTagsForm() {

		$form = new Form($this, 'makeTagsForm');
		$renderer = $form->getRenderer();
		
		
		$form->addText('tag', 'Zadejte kategorii');
		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedMakeTagsForm');

		// přidej bezpečnostní ochranu, když je test režim vypnut
		if(!Configs::get()->byContent('settings.testMode'))
			$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}
	
	function submitedMakeTagsForm( Form $form ) {
		if(isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			try {
				$this->partnerFacade->addTag( $v->tag );
				
			} catch (\LogicException $exc) {
				$this->presenter->flashMessage( $exc->getMessage(), \Flashes::$error );
			}


			
		}	
	}
	
		
	
	public function render()
	{
		
		
		$this->template->setFile(dirname(__FILE__) .'/makeTagsControl.latte' );
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
	}

}