<?php

/**
 * Nette Framework Extras
 *
 * This source file is subject to the New BSD License.
 *
 * For more information please see http://extras.nettephp.com
 *
 * @copyright  Copyright (c) 2009 David Grudl
 * @license    New BSD License
 * @link       http://extras.nettephp.com
 * @package    Nette Extras
 * @version    $Id: VisualPaginator.php 4 2009-07-14 15:22:02Z david@grudl.com $
 */

namespace MoneyPoint;

use Nette\Application\UI\Control;

class ProviderInfoFactory {
	public function __construct( \MoneyPoint\PartnerProviderInfoFacade $partnerProviderInfoFacade ) {
		$this->partnerProviderInfoFacade = $partnerProviderInfoFacade;
	}
	
	public function create()
	{
		return new ProviderInfo( $this->partnerProviderInfoFacade );
	}
}

class ProviderInfo extends Control
{
	/** @var \MoneyPoint\PartnerProviderInfoFacade */
	protected $partnerProviderInfoFacade;
	public function __construct( \MoneyPoint\PartnerProviderInfoFacade $partnerProviderInfoFacade ) {
		$this->partnerProviderInfoFacade = $partnerProviderInfoFacade;
	}
	private $providerInfo;
	
	/** @var Entity\PartnerProviderFee */
	private $providerFee;
	
	public function setPartnerOrder( $partnerOrder ) {
		$this->partnerOrder = $partnerOrder;
	}
	
	public function setProviderInfo( $ppi_id ) {
		
		$pi = $this->partnerProviderInfoFacade->secureFindByProviderFee( $ppi_id );
		$this->providerInfo = $pi->getProviderInfos();
		$this->providerFee = $pi;
	}

	public function render()
	{
		if( empty( $this->providerInfo )) return;
		
		$cjInfo = new CjService();
		$this->template->cjInfo = $cjInfo;
		$this->template->providerInfo = $this->providerInfo;
		$this->template->setFile(dirname(__FILE__) .'/template.phtml' );
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
	}

	private $partnerOrder;

	public function renderList()
	{
		$this->template->partnerOrder = $this->partnerOrder;
			
		$this->template->setFile(dirname(__FILE__) .'/list.phtml' );
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
	}



}
