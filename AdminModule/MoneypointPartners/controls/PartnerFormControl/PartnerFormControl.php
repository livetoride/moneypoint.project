<?php
namespace MoneyPoint;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use AntoninRykalsky\Configs;


interface IPartnerFormControlFactory
{
    /** @return \MoneyPoint\PartnerFormControl */
    function create();
}

class PartnerFormControlFactory implements IPartnerFormControlFactory
{
    
	public function __construct( PartnersFacade $partnersFacade, ProviderInfoFactory $providerInfoFactory, PartnerOrderFacade $partnerOrderFacade )
    {
        $this->partnersFacade = $partnersFacade;
        $this->providerInfoFactory = $providerInfoFactory;
		$this->partnerOrderFacade = $partnerOrderFacade;
    }

    public function create()
    {
        return new PartnerFormControl( $this->partnersFacade, $this->providerInfoFactory, $this->partnerOrderFacade );
    }
}

class PartnerFormControl extends Control
{
	/** @var PartnersFacade */
	protected $partnersFacade;

	/** @var \MoneyPoint\ProviderInfoFactory */
	protected $providerInfoFactory;
	
	/** @var \MoneyPoint\PartnerOrderFacade */
	protected $partnerOrderFacade;
	
	public function __construct( PartnersFacade $partnersFacade, ProviderInfoFactory $providerInfoFactory, PartnerOrderFacade $partnerOrderFacade ) {
		$this->partnersFacade = $partnersFacade;
		$this->providerInfoFactory = $providerInfoFactory;
		$this->partnerOrderFacade = $partnerOrderFacade;
	}
	
	protected function createComponentProviderInfo()
	{
		return $this->providerInfoFactory->create();
	}
	
	public function setDefaults( $d )
	{
//		throw new \Exception;
		if( $d['create'] )
		{
			$this['form']['id_status']->setDisabled();
			$this['form']['settled']->setDisabled();
		} else {
			/* @var $order \MoneyPoint\Entity\PartnerOrder */
			$order = $d['order'];
//			\Doctrine\Common\Util\Debug::dump($order->getPartnerProviderFee()->getShop());exit;
//			$this['form']['id_status']->setDisabled();
			$this['form']->setDefaults( 
				array( 
					'idu' => $order->getUser()->getIdu(),
					'utrata' => $order->getUtrata(),
					'reward' => $order->getZakladniOdmena(),
					'shop' => $order->getPartnerProviderFee()->getShop(),
					'provider' => $order->getPartnerProvider()->getId(),
				)
			);
		}
		#$d['order']->getUser()->getIdu();
	}
	
	protected function createComponentForm()
	{
		$form = new Form($this, 'form');
		$renderer = $form->getRenderer();

		// parametry
		$form->addHidden('idu');
		$form['idu']->getControlPrototype()->class='bigdrop select2-offscreen';
		$form['idu']->getControlPrototype()->style='width:300px';
		$providers = Entity\Eshop::$PROVIDERS;
		
		$form->addSelect('provider', 'Provider', $providers)->setDefaultValue(0)->setDisabled();
		$form->addText('shop', 'Obchod');
		
		
				
		$form->addHidden('id_order');
		

		#$attr = 'id_partner, name || " (" || reward || "%)" as partner';
		#$partners = \DAO\Partners::get()->findAll($attr)->fetchPairs('id_partner', "partner");

	/*	$partners = $this->partnerFacade->getPartnerFeesSelect();
		$form->addSelect('fee_rate', 'Nákup u partnera', $partners);*/

		$status = array('odměna nepřiřazena', 'odměna přiřazena');
		$form->addSelect('settled', 'Stav objednávky', $status)
			->setPrompt('--ponechat stav--');
		
		$states = \MoneyPoint\PartnerOrderFacade::$states;
		unset( $states[0] );
		$form->addSelect('id_status', 'Stav objednávky', $states );
			

		$form->addText('utrata', 'Nákup v ceně [bez DPH?]');
		$form->addText('reward', 'Odměna');
		$date = new \Datetime; 
		$date->add(new \DateInterval('P1M'));
		$form->addText('expdate', 'Rozhodný okamžik')->setDefaultValue( $date->format('d.m.Y H:i'));

		$form->addSubmit('save', 'Uložit')->getControlPrototype()->class('default');

        $form->onSuccess[] = array($this, 'submitedForm');

		// přidej bezpečnostní ochranu, když je test režim vypnut
		if(!Configs::get()->byContent('settings.testMode'))
			$form->addProtection('Please submit this form again (security token has expired).');

		return $form;
	}
	
	function submitedForm( Form $form )
    {
		if(isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			
			$v->createNew = (int)($this->presenter->getAction() === 'create');
			@$v->id_order = $this->presenter->getParameter('id_order');

			

			try {
				$partnerOrder = $this->partnerOrderFacade->changeState( $v );
			} catch( \LogicException $e )
			{
				$this->presenter->flashMessage( $e->getMessage(), \Flashes::$error );
				return;
			} 

			switch ( $partnerOrder->getIdStatus() ){
				case 3:
					$this->presenter->redirect('settled');
					break;

				case 2:
					$this->presenter->redirect('waiting');
					break;

				default:
					$this->presenter->redirect('default');
					break;
			}


			$this->presenter->redirect('default');
		}

	}
	public function render()
	{
		
		
		$this->template->setFile(dirname(__FILE__) .'/partnerFormControl.phtml' );
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
	}
}
