<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;
use dibi;

class MPKariera extends BaseModel
{
	/** @var string  table name */
	protected $table = 'mp_kariera';

	/** @var string|array  primary key column name */
	protected $primary = 'id';

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new MPKariera();
		return $me;
	}


	protected $vals = array(
		'id' => 'int',
		'idu' => 'int',
		'trida' => 'varchar',
		'bonus' => 'int',
		'st_credits_min' => 'int',
		'st_credits_max' => 'int',
		'credits_min' => 'int',
		'credits_onebuy' => 'int'
	);

	public function getClassByIdu( $idu ) {
		$credVlastni =  (int) @CreditsAccounts::get()->actualState( $idu )->credits_sum;
		$credSit = (int) @CreditsStructure::get()->actualState( $idu )->credits_structure;
		$class = MPKariera::get()->findAll()->
				where('[st_credits_min]<=%i AND [st_credits_max]>%i AND [credits_min]<=%i', $credSit, $credSit, $credVlastni )->
				orderBy('credits_min', dibi::DESC)->
				fetch();
		return $class;
	}
}
