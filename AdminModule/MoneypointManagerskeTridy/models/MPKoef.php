<?php
/**
 * Pro zacházení s manažerskými třídami a VIP u Moneypoint.cz
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class MPKoef
{
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new MPKoef();
		return $me;
	}

	public function getCoef( $idu ) {
		$vip = VipDao::get()->getVipByIdu( $idu );
		if(!empty( $vip->vip ))
			return $vip;

		$class = MPKariera::get()->getClassByIdu( $idu );
		return $class;
	}

}
