<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule;
use AntoninRykalsky\MPKariera;
class MTPresenter extends \Base_MlmPresenter
{
	public function actionDefault()
	{
		$this->template->title = 'Managerské třídy';
		$k = MPKariera::get()->findAll()->orderBy('bonus')->fetchAll();
		$this->template->classes = $k;
	}
}
