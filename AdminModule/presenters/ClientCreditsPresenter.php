<?php
/**
 * Presenter obsluhující požadavky eshopu
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule;

use AntoninRykalsky\Configs;
use AntoninRykalsky\CreditsAccounts;
use DAO\Uzivatele;
use Nette\Application\UI\Form;
use Nette\Environment;

class ClientCreditsPresenter extends \FrontModule\CreditsPresenter
{
	var $idu;
	var $resource = 'mlmadmin_resource';

	public function actionAccount($idu)
	{
		$this->template->title = 'Kredit konto';
		$this->template->idu = $idu;

		$user = Environment::getSession('user');
		$this->idu = $idu;
	}

	public function actionModify($idu, $really = 0)
	{
		if( !$really )
		{
			foreach( $this['form']->getComponents() as $c )
				$c->setDisabled();
		}

		$this->template->title = 'Ručně přidat/odebrat kredit';
		$this->template->idu = $idu;
		$this->template->really = $really;
		$this->template->user = Uzivatele::get()->find( $idu )->fetch();

		$this['form']->setDefaults(array('reason'=>'Ruční změna kreditu administrátorem.'));

		$user = Environment::getSession('user');
		$this->idu = $idu;
	}

	protected function createComponentForm()
    {
        $form = new Form($this, 'form');
		$renderer = $form->getRenderer();

		// parametry
		$form->addHidden('idu', $this->getParam('idu'));
		$form->addText('credits_change', 'Změna na kreditovém účtu');

		$form->addText('reason', 'Důvod změny');

		$form->addSubmit('save', 'Uložit')->getControlPrototype()->class('default');

        $form->onSuccess[] = array($this, 'submitedForm');

		// přidej bezpečnostní ochranu, když je test režim vypnut
		if(!Configs::get()->byContent('settings.testMode'))
			$form->addProtection('Please submit this form again (security token has expired).');

		return $form;
    }

	function submitedForm( Form $form )
    {
        if(isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();


			$return = $this->tryCatcher( '$this->changeCredits', $v );
			if( $return == 'fail' )
				return;
			else
				$this->redirect('account', array('idu'=>$v['idu'] ) );
		}
	}

	protected function changeCredits( $v )
	{
		$v = (object)$v;
		CreditsAccounts::get()->modifyCredits( $v->idu ,$v->reason, $v->credits_change );
	}

}
