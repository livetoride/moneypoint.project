<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule;

use MoneyPoint;

class DiferentialCommisionPresenter extends \Base_AdminPresenter
{
	/** @var DirefentialCommisionFacade */
	private $direfentialCommisionFacade;
	
	public function injectDifferentialCommision( MoneyPoint\DiferentialCommisionFacade $direfentialCommisionFacade ) {
		$this->direfentialCommisionFacade = $direfentialCommisionFacade;
	}
	
	protected function startup() {
		parent::startup();
		$this->template->submenu = $this->getComponent('submenu');
	}

	public function createComponentSubmenu() {
		$komponenta = new \MlmadminSubmenu($this, 'submenu');
		$sec = $komponenta->addSection('Možnosti:');
		$sec->addItem('importuj nový', $this->link(':Admin:Aid:import' ) );
		return $komponenta;
	}
	
	public function actionTestAccounting()
	{
		$this->direfentialCommisionFacade->testAccounting();
		exit;
	}
	
	public function actionDefault()
	{
//		$this->direfentialCommisionFacade->countCommision( 1 );
		$this->direfentialCommisionFacade->countCommision();
		exit;
	}
	
	public function actionEdit( $id )
	{
		$this->template->title = 'Editace údajů sestřičky';

		$differentialCommision = $this->differentialCommisionFacade->secureFind( $id );
		$this['differentialCommisionForm']->setDefaults( $this->differentialCommisionFormBuilder->translate( $differentialCommision ) );
	}
	
	protected function createComponentDifferentialCommisionList()     {
		$l = new Lastrona\DifferentialCommisionList;
		return $l->getList( $this->differentialCommisionFacade, $this->commonTranslator );
	}
	
	protected $differentialCommisionFormBuilder;
	protected function createComponentDifferentialCommisionForm() {
		$this->differentialCommisionFormBuilder = new Lastrona\DifferentialCommisionForm( $this->differentialCommisionFacade, $this );
		return $this->differentialCommisionFormBuilder->getForm();
	}


}
