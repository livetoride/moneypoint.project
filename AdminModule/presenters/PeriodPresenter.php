<?php
/**
 * Nastavení systému
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule;

use MoneyPoint;

use Nette\Application\UI\Form;

class PeriodPresenter extends \Base_AdminPresenter
{
	/** @var MoneyPoint\PeriodFacade */
	protected $periodFacade;

	function injectPartner( MoneyPoint\PeriodFacade $insurranceCompanyFacade) {
		$this->periodFacade = $insurranceCompanyFacade;
	}
	
	public function actionDefault()
	{
		$this->template->title = 'Zúčtovací období';
//		 $this->periodFacade->install();
	}
	
	public function actionEdit( $id )
	{
		$this->template->title = 'Editace zúčtovacího období';

		$insurranceCompany = $this->periodFacade->secureFind( $id );
		$this['periodForm']->setDefaults( $this->limitationFormBuilder->translate( $insurranceCompany ) );
	}
	
	public function actionSettings()
	{
		$this->template->title = 'Generování účetních období';
	}
	
	protected function createComponentPeriodGenerate() {
		$form = new Form();
//		$renderer = $form->getRenderer();

		$interval = array(1=>"1 rok dopředu", "2 rok dopředu", "3 rok dopředu", "4 rok dopředu", "5 rok dopředu" );
		$form->addRadioList('interval', 'Období generování', $interval);
		$form['interval']->addRule(Form::FILLED);
		
		$year = date('Y', strtotime('now'));
		$titles = array(
			1=>"Leden $year, Únor $year, ..",
			2=>"1/$year, 2/$year,.. - pořadové číslo", 
		);
		$form->addRadioList('titling', 'Názvy období', $titles);
		$form['titling']->addRule(Form::FILLED);

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedPeriodGenerate');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}


	function submitedPeriodGenerate(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			
			$this->periodFacade->install( $v->titling, $v->interval );
			$this->flashMessage('Účetní období vygenerována.', \Flashes::$info );
		}
	}
	
	protected function createComponentPeriodList()     {
		$l = new MoneyPoint\PeriodList;
		return $l->getList( $this->periodFacade, $this->commonTranslator );
	}
	
	protected $limitationFormBuilder;
	protected function createComponentPeriodForm() {
		$this->limitationFormBuilder = new MoneyPoint\PeriodForm( $this->periodFacade, $this );
		return $this->limitationFormBuilder->getForm();
	}

	public function actionJson( $q='', $idu=null )
	{
		if( !empty( $idu ))
		{
			$u = \DAO\MoneyPointInsurraceCompany::get()->find( $idu )->fetchAll();
		} else {
			$q = strtolower($q);
			$u = \DAO\MoneyPointInsurraceCompany::get()->findAll()->where('lower(name) || code SIMILAR TO %~like~', $q )->fetchAll();
			$users = array();
		}
		
		foreach( $u as $k => $v )
		{
			$this->payload->results[] = 
					array(
				'id'=> $v['id'],
				'text'=> ' ('.$v['code'].') '.$v['name'],
				);
		}
		$this->payload->more = false;
		
		$this->sendPayload();
		exit;
	}

}
