<?php
/**
 * Newsletter
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule;

use MoneyPoint;
use Datagrid;

use Nette\Application\UI\Form;

class NewsletterPresenter extends \Base_AdminPresenter {
	public function actionDefault() { 
		$this->template->title = 'Newsletter';
	}
	
	public function actionList(){
		$this->template->title = 'Seznam newsletterů';
	}
	
	public function actionContinue($id){
		
		$this->template->title = 'Pokračování po nezdařeném rozesílání newsletterů';
		$values = \DAO\Newsletters::get()->findAll()->where('id = %i', $id)->fetch();
		if ($values->to_whom == 'všem')
			$values->to_whom = 0;
		else
			$values->to_whom = 1;
	$this['newsLetterForm']->setDefaults(array(
		'id' => $values->id,
		'subject' => $values->subject,
		'letter' => $values->letter,
		'whom' => $values->to_whom
		));
	$this->template->form = $this['newsLetterForm'];
		
		
	}
	
	
	function parseEmails( $str )
	{
		$str = preg_replace('#,#', ';', $str );
		$a = explode(';', $str);
		$return = array();
		$nonValid = array();
		foreach( $a as $k => $dontUseThis )
		{
			$a[$k] = trim( $a[$k] );
			if( !empty( $a[$k] ))
			{
				if( !\Nette\Utils\Validators::isEmail($a[$k]) )
				{
					$nonValid[] = $a[$k];
				}
				$return[] = $a[$k];
			}
		}
		if( count( $nonValid) )
		{
			throw new \LogicException("Emaily ".implode($nonValid, ', ')." nejsou validní");
		}
		return $return;
	}

	
	
	public function createComponentNewsLetterForm() {
		$form = new Form();
		$renderer = $form->getRenderer();
		$form->addHidden('id');
		$u= array( 0=>'všichni uživatelé', 1=>'výběr');
		$form->addRadioList('whom','Uživatelé', $u);
		if ($u[1]) { $form->addText('to_whom', 'E-mail'); }
		$form->addText('subject', 'Předmět');
		$form->addTextArea('letter')->getControlPrototype()->class('ckeditor');
		$form->addSubmit('save', 'Odeslat');
		$form->onSuccess[] = array($this, 'submitedNewsLetterForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}
	
	protected function createComponentNewsletterList()     {
		$ds = \DAO\Newsletters::get()->getDataSource();
		$ds->where('id>0');
		$model = new \DataGrid\DataSources\Dibi\DataSource($ds);

		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setRenderer($renderer);

		$grid->itemsPerPage = 10;
		$grid->setDataSource($model);
		$grid->multiOrder = FALSE;

		$operations = array();
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, \DAO\Newsletters::get()->getPrimary());

		$grid->addNumericColumn('id', 'ID');
		$grid['id']->addFilter();
		$grid->addColumn('to_whom', 'Komu');
		$grid['to_whom']->addFilter();
		$grid->addColumn('subject', 'Předmět');
		$grid['subject']->addFilter();
		$grid->addColumn('time', 'Čas');
		

		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
		$icon = \Nette\Utils\Html::el('span');
		$grid->addAction('Structure:detail', 'continue', clone $icon->class('icon icon-edit'));
		return $grid;
	}


	function submitedNewsLetterForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			
			
			
			$v = (array) $form->getValues();
			if(empty($v['id'])) {
				if ($v['whom']==0) {
					$letter = \DAO\Uzivatele::get()->findAll()->where('idrole=2 AND idu>0')->orderBy('idu')->fetchPairs('idu', 'email');
				//	print_r($letter);exit;
					$v['to_whom']= 'všem';
					
					
				} else {
					try {
						$letter = $this->parseEmails($v['to_whom']);
						$v['to_whom']="'" . implode(',', $letter ) . "'";

					} catch (\LogicException $e ) {
						$this->flashMessage($e->getMessage(), 'ERROR');
						return;
					}
				}
			}
			
			//print_r($v['id']);exit();
			
			$text = $v['letter']; 
			$subject = $v['subject'];
			$from = "system@moneypoint.cz";
			
			
			if(empty($v['id'])) {
				\DAO\Newsletters::get()->insert(array('to_whom'=>$v['to_whom'], 'subject' => $v['subject'],'letter'=> $v['letter'] ));
				$v['id'] = \Dibi::insertId();
			} else  {
				$last = $this->resumeSubmitingMails($v['id']); 
				$letter = \DAO\Uzivatele::get()->findAll()->where('idrole=2 AND idu>%i', $last)->orderBy('idu')->fetchPairs('idu', 'email');
				
				
				
			}
			
			foreach ( $letter as $key => $maily ) {	
				$mail = new \MoneyPoint\Mailer();
				$mail->setFrom($from);
				$mail->addTo( $maily );
				$mail->setSubject($subject);
				$mail->setHTMLBody($text);
				$date = new \Datetime;
				$mail->send();
				\DAO\Newsletters::get()->update($v['id'], array('ended_where'=>$key, 'ended_when' => $date->format('d-m-Y H:i') ));
				
				
			}
			
			$date = new \Datetime;			
			\DAO\Newsletters::get()->update($v['id'], array('ended_where'=>-1, 'ended_when' => $date->format('d-m-Y H:i') ));
			
			$this->flashMessage("Odesláno", \Flashes::$success );
		}
	}
	
	function resumeSubmitingMails($id) {
		$lastSended = \DAO\Newsletters::get()->findAll('ended_where')->where('id = %i', $id)->fetchSingle();
		return $lastSended;
		
	}
}