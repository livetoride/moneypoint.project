<?php
/**
 * Presenter obsluhující požadavky eshopu
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule;

use AntoninRykalsky\Configs;
use AntoninRykalsky\PointsAccounts;
use Nette\Environment;
use Nette\Forms\Form;
use Nette\Utils\Html;

class ClientRewardsPresenter extends \FrontModule\ActionNRewardsPresenter
{
	var $idu;
	var $resource = 'mlmadmin_resource';


	protected function createComponentCreditsAccount()
	{
		// prehodime akcni column nakonec
		$grid = parent::createComponentCreditsAccount();

		unset( $grid['akce'] );


		$grid->addActionColumn('akce')->getHeaderPrototype()->style('width: 20px;');
		$icon = Html::el('span');

		$action = $grid->addAction('Detail', 'mpTransactionDetail:detail!', clone $icon->class('icon icon-detail'))->getHtml();
		$action->class('ajax');

		return $grid;
	}

	public function actionAccount($idu)
	{
		$this->template->pageStyle = 'front-credits-account.css';

		$this->template->title = 'Akce a odměny';
		$this->template->idu = $idu;

		$this['mpTransactionDetail']->setAdminMode();
		$this['mpFilters']->setAdminMode();

		$user = Environment::getSession('user');
		$this->idu = $idu;
	}

	public function actionModify($idu, $really = 0)
	{
		if( !$really )
		{
			foreach( $this['form']->getComponents() as $c )
				$c->setDisabled();
		}

		$this->template->title = 'Ručně přidat/odebrat body';
		$this->template->idu = $idu;
		$this->template->really = $really;
		$this->template->user = Uzivatele::get()->find( $idu )->fetch();

		$this['form']->setDefaults(array('reason'=>'Ruční změna bodů administrátorem.'));

		$user = Environment::getSession('user');
		$this->idu = $idu;
	}

	protected function createComponentForm()
    {
        $form = new Form($this, 'form');
		$renderer = $form->getRenderer();

		// parametry
		$form->addHidden('idu', $this->getParam('idu'));
		$form->addText('points_change', 'Změna na bodovém účtu');

		$form->addText('reason', 'Důvod změny');

		$form->addSubmit('save', 'Uložit')->getControlPrototype()->class('default');

        $form->onSuccess[] = array($this, 'submitedForm');

		// přidej bezpečnostní ochranu, když je test režim vypnut
		if(!Configs::get()->byContent('settings.testMode'))
			$form->addProtection('Please submit this form again (security token has expired).');

		return $form;
    }

	function submitedForm( Form $form )
    {
        if(isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();


			$return = $this->tryCatcher( '$this->changePoints', $v );
			if( $return == 'fail' )
				return;
			else
				$this->redirect('account', array('idu'=>$v['idu'] ) );
		}
	}

	protected function changePoints( $v )
	{
		$v = (object)$v;
		PointsAccounts::get()->manualChange( $v->idu ,$v->reason, $v->points_change );
	}

}
