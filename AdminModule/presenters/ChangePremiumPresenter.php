<?php
/**
 * Manuální nastavení ranku klienta
 */
namespace AdminModule\MoneypointModule;
use MoneyPoint;
use Nette;

class ChangePremiumPresenter extends \Base_AdminPresenter
{
	/** @var MoneyPoint\UserFacade */
	protected $userFacade;

	function injectPartner(MoneyPoint\UserFacade $userFacade) {
		$this->userFacade = $userFacade;
	}
	
	public function actionChangePremium($idu)
	{
		$this->template->title = 'Nastavení účtu Premium';
		$this->template->idu = $idu;
		
		/* @var $user Entity\Member */
		$user = $this->userFacade->getUser($idu);
		
		$defaults = array();
		$defaults['has_premium'] = (int)$user->hasBusiness();
		$defaults['idu'] = $user->getIdu();
		$this['manualPremiumControl']->setDefaults( $defaults );
	}
	
	
	protected function createComponentManualPremiumControl() {

		$form = new Nette\Application\UI\Form();
		$form->addHidden('idu');
		
		$radio = array(
			0=>'Tento uživatel NEMÁ účet Premium',
			1=>'Tento uživatel MÁ účet Premium'
		);
		$form->addRadioList('has_premium', '', $radio );

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedPeriodForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}
	
	function submitedPeriodForm( Nette\Application\UI\Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			
			try {
				$this->userFacade->setPremium( $v->idu, $v->has_premium );
				$this->flashMessage('Uloženo', 'SUCCESS');
			} catch ( \Exception $e )
			{
				if( !$this->tryCatcherDebug )
					throw $e;
				
				$this->flashMessage('Uložení se nezdařilo', 'ERROR');
				
				return;
			}
			$this->redirect('this');
		}
	}

}
