<?php

namespace AdminModule\MoneypointModule;

use Nette\Application\UI\Form;
use Nette\Utils\Html;

class InvitationPresenter extends \Base_AdminPresenter
{
	/** @var \MoneyPoint\PartnerOrderFacade */
	protected $partnerOrderFacade;
	function injectPartnerOrder( \MoneyPoint\PartnerOrderFacade $partnerOrderFacade )
    {
        $this->partnerOrderFacade = $partnerOrderFacade;
    }

	/** @var \MoneyPoint\UserInvitationFacade */
	protected $userInvitationFacade;
	function injectPartner(\MoneyPoint\UserInvitationFacade $userInvitationFacade) {
		$this->userInvitationFacade = $userInvitationFacade;
	}
	
	/** @var \MoneyPoint\TransactionFacade */
	protected $transactionFacade;
	public function __construct( 
		\MoneyPoint\TransactionFacade $transactionFacade 
	) {
		$this->transactionFacade = $transactionFacade;
	}

	
	public function renderDefault()
	{
		$this->template->title = 'Pozvánky';
	}
	
	public function renderCreate()
	{
		$this->template->title = 'Tvorba pozvánky';
		$d= array();
		$d['email'] = '@';
		$d['text'] = \AntoninRykalsky\Configs::get()->byContent('mailer-invitation.email');
		$d['text'] .= \AntoninRykalsky\Configs::get()->byContent('mailer-invitation.email_padding');
		
		$this->getComponent('form')->setDefaults( $d );
	}
	
	protected function createComponentList()
    {
        $ds = \DAO\UserInvitation::get()->findAll();
		$ds->orderBy('id', 'desc');
		$ds = $ds->fetchAll();
		foreach( $ds as &$v )
		{
			$v['generated'] = empty($v['credits_account_id']) ? ' standartní' : ' generovaná';
			if( $v['used'] )
				$v['success'] = 'Využitá';
			elseif( strtotime($v['ts_expires']) < time() )
				$v['success'] = 'Nevyužitá';
			else{
				$v['success'] = 'Odeslaná';
			}
			$v['success'] .= $v['generated'];
		}
		$model = new \DataGrid\DataSources\PHPArray\PHPArray( $ds );

		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setTranslator($this->commonTranslator);
		$grid->setRenderer($renderer);

		$grid->itemsPerPage = 10;
		$grid->setDataSource($model);
		$grid->multiOrder = FALSE;

		$operations = array();
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, \DAO\UserInvitation::get()->getPrimary());

		// if no columns are defined, takes all cols from given data source
		$grid->addActionColumn('akce')->getHeaderPrototype()->style('widthHtmlpx;');
		$icon = Html::el('span');
		// <a href="{$detailComponent->link('detail!', array(path => $p->path))}" onclick="showtable()" class="ajax">

		$action = $grid->addAction('Detail', 'mpTransactionDetail:invitationDetail!', clone $icon->class('icon mp-info'))->getHtml();
		$action->class('ajax');
		$action->onClick('showtable()');
		
		$grid->addColumn('invitation_id', 'číslo pozvánky');
		$grid->addColumn('jmeno', 'jméno');
		$grid->addColumn('prijmeni', 'přijmení');
		$grid->addColumn('email', 'email');
		$grid->addColumn('success', 'stav pozvánky');
		//$grid->addColumn('generated', 'typ');

//		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
//		$icon = Html::el('span');
//		$grid->addAction('Structure:detail', 'Users:detail', clone $icon->class('icon icon-detail'));
		return $grid;
    }
	
	protected function createComponentMpTransactionDetail()
    {
		return new \MpTransactionDetail( $this->partnerOrderFacade, $this->transactionFacade );
	}
	
	protected function createComponentForm() {
		$form = new Form($this, 'form');
		#$form->setMethod('post');

		$form->addText('jmeno', 'Jméno adresáta:');
		$form['jmeno']->getControlPrototype()->tabindex(1);

		$form->addText('prijmeni', 'Přijmení adresáta:');

		$form->addText('email', 'Emailová adresa:')
				->addRule(Form::EMAIL, 'Zadejte platný email');
		
		$form->addTextarea('text', 'Text doporučovacího emailu');
		
		if( \MoneyPoint\LoggedMember::get()->invitation()->leftInvitations() == 0 )
		{
			$form->addCheckbox('agree_nonstandard', 'Souhlasím s odečtením 100 kreditu pro vygenerování pozvánky.')
					->addRule(Form::EQUAL, 'Již nemáte žádné volné pozvánky. Novou pozvánku bez odečtení 100 kreditu nelze vygenerovat', TRUE);
		}

		$form->addSubmit('save', 'Odeslat pozvánku')->getControlPrototype()->class('mybutton');
		$form->onSuccess[] = array($this, 'formSubmitted');

//		if( $this->getContext()->configs->allowProtection() )
//			$form->addProtection('Vypršel časový limit, odešlete formulář znovu');
		return $form;
	}
	
	public function formSubmitted(Form $form) {
		if ($form['save']->isSubmittedBy()) {
			$data = (array) $form->getValues();
			$data['idu'] = 0;
			
			$agree = @$data['agree_nonstandard'];
			try {
				$invitation = $this->userInvitationFacade->createInvitation($data, \MoneyPoint\Invitation::ADMIN );
			} catch( \LogicException $e )
			{
				if( $e->getCode() == 1)
				{
					$span = Html::el('span');
					$span->setHtml('Nemáte dostatečný zůstatek kreditu pro vygenerování nové pozvánky! Pro vygenerování pozvánky potřebujete 100 kreditu, které Vám budou po vygenerování pozvánky odečteny! Klikněte <a target="_blank" href="'.$this->link(':Front:eshop:credits-order').'">sem</a> pro přechod k nákupu kreditu.');
					$this->flashMessage( $span, \Flashes::$info );
					return;
				} elseif( $e instanceof \AntoninRykalsky\LogicException ) {
					$this->flashMessage($e->getMessage(), 'ERROR');
					return;
				} else { throw $e; }
			}
			
			$invitation->send( \MoneyPoint\Invitation::ADMIN );

			$this->flashMessage('Pozvánka byla odeslána.', \Flashes::$success);
			$this->redirect('this');
		}
	}
}
