<?php
/**
 * Manuální nastavení ranku klienta
 */
namespace AdminModule\MoneypointModule;
use MoneyPoint;

class RankPresenter extends \Base_AdminPresenter
{
	/** @var DirefentialCommisionFacade */
	private $direfentialCommisionFacade;
	
	/** @var MoneyPoint\RankFacade */
	protected $rankFacade;

	function injectPartner( MoneyPoint\RankFacade $rankFacade) {
		$this->rankFacade = $rankFacade;
	}
	
	public function injectDifferentialCommision( MoneyPoint\DiferentialCommisionFacade $direfentialCommisionFacade ) {
		$this->direfentialCommisionFacade = $direfentialCommisionFacade;
	}

	
	protected $ranks;
	
	public function actionChangeRank($idu)
	{
	/*	$this->rankFacade->setCorrectRank();
		exit; */
		
		#$this->direfentialCommisionFacade->installDefaultAccount($idu);
		$this->direfentialCommisionFacade->countCommision( null, $idu  );
		
		$this->flashMessage("Minulá období editujte pouze v případech před jejich rozhodným okamžikem", \Flashes::$info );
		$this->template->title = 'Změna ranku';
		$this->template->idu = $idu;
		
		$defaults=array();
		if( $this->ranks->last !== null )
		{
			$defaults['last_setted']= $this->ranks->last->getCarrier()->getKey();
		}
			
			
		$defaults['actual_setted']= $this->ranks->actual->getCarrier()->getKey(); 
				
		$this['changeRank']->setDefaults( $defaults );
		if( empty($defaults['last_setted'] ))
		{
			$this['changeRank']['last_setted']->setDisabled();
		}
	}
	
	public function startup()
	{
		parent::startup();
		$idu = $this->getParameter('idu');
		if( !empty( $idu ))
		{
			$this->ranks = $this->rankFacade->getRankChangesDefaults($idu);
		}
	}
	
	protected $limitationFormBuilder;
	protected function createComponentChangeRank() {
		
		$this->limitationFormBuilder = new MoneyPoint\ChangeRankControl( $this, $this->ranks->ranks->getCarriers(), $this->rankFacade );
		return $this->limitationFormBuilder->getForm();
	}

}
