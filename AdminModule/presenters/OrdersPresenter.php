<?php
/**
 * Controller sloužící k administraci mlm, produktů,
 * účetních období a speciálních uživatelů.
 *
 * / - výpis všech objednávek
 *
 * - akce směřované na akci default
 * /inbox
 * /paid
 * /toInvoice
 * /canceled
 *
 * - akce směřované na detail objednávky
 * /detail
 *
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule\MoneypointModule;
use AntoninRykalsky\Orders;
use Nette\Utils\Html;
use Nette\Application\UI\Form;
use AntoninRykalsky\Uzivatele;
use FormatingHelpers;
use AntoninRykalsky\OrdersStatuses;
use AntoninRykalsky\OrderProcessing;
use Nette\Environment;

class OrderSettings {
	public $userDetailLink = ':Admin:Structure:UserDetail';
}


class OrdersPresenter extends \Base_MlmPresenter
{
	/** @var \MoneyPoint\CreditOrderFacade */
	protected $creditOrderFacade;
	
	protected $orderSettings;
	
	function inject(
		\MoneyPoint\CreditOrderFacade $creditOrderFacade
	)
    {
        $this->creditOrderFacade = $creditOrderFacade;
        $this->orderSettings = new OrderSettings;
    }
	
	/** @var DataSource drží objekt datasource */
	private $ordersListDatasource;

		/** základ pro společné oprávnění */
	var $resource = 'backend_resource';

	/** @return Datasource vrací objekt datasource, nebo vytvori novy */
	private function getOrdersListDatasource() {
		// vrať datasource
		if( $this->ordersListDatasource != null )
				return $this->ordersListDatasource;

		// vyrob nový
		$model = Orders::get()->getDataSource_orderListAdmin();

		$type = 1;
		$type = $this->getParam('id');
		if( $this->action == 'findnotpaid') $type = 1;






		// MP: k fakturaci = zaplacena + starší 14ti dní

			if(!empty($type))
				$model->where('[id_status]='.(int)$type );



		return $model;
	}

	var $admin;
	public function renderCreate( $idu )
	{
		$this->template->title = 'Vytvořit objednávku';
		$this->admin = 'admin_';
		$this->template->showCategoryControl = 1;
		if( $idu != null )
		{
			$this->template->client = Uzivatele::get()->find($idu)->fetch();
			$u = Environment::getSession($this->admin.'user');
			$u['idu'] = $idu;
		}



	}


	var $statuses = array(); /* beforeRender sets */


	public function renderFindNotPaid()
	{
		$this->template->title = 'Hledej neuhrazenou objednávku';

		// upravim orderList
		$grid = $this['ordersList'];
		$grid['id_order']->addFilter();
		//$grid['id_order']->addDefaultFiltering('zadej'); // výchozí filtrování
		unset( $grid['price_sum'] );

		$this->template->ordersList = $grid;

	}



	protected function beforeRender() {
		parent::beforeRender();
		$this->statuses =  \DAO\OrdersStatuses::get()->findAll()->fetchPairs('id_status', 'status');

	}

	public function renderDefault( $id = 0 )
	{

		// update čekajících objednávek
		// OrdersHandling::get()->updateWaitingOrders();
//		$reward = new \MoneyPoint\CreditOrdersToReward();
//		$reward->reward();


		if( $id==0 )
			$this->template->title = 'Všechny objednávky';
		else
			$this->template->title = 'Objednávky se statusem - '. $this->statuses[ $id ];

		$model = $this->getOrdersListDatasource();
		$this->template->empty = $t = !count($model->fetchAll());
	}

	/** @return DataGrid tovarnicka na orderslist */
	protected function createComponentOrdersList()
    {
		// získej DS z funkce
        $model = $this->getOrdersListDatasource();

		$model = new \DataGrid\DataSources\Dibi\DataSource($model);
		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setDataSource( $model );
		$grid->setRenderer($renderer);
		$grid->itemsPerPage = 10;
		$grid->multiOrder = FALSE;

		$operations = array(); // define operations
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, Orders::get()->getPrimary()); // allows checkboxes to do operations with more rows

		$grid->addNumericColumn('id_mporder', 'číslo')->addFilter();
		$grid->addNumericColumn('id_order', 'ID')->addFilter();
		$grid->addColumn('client', 'objednavatel');
		$grid->addColumn('full_order_time', 'datum' ); #, "%a %d.%m.%Y, %H:%M:%S"
		$grid->addColumn('price_sum', 'cena&nbsp;s&nbsp;DPH');
		$grid->addColumn('status', 'status');
		
		$grid['price_sum']->formatCallback[] = 'FormatingHelpers::currency';
		


		$grid['full_order_time']->formatCallback[] = 'FormatingHelpers::czechDate';


		// výchozí řazení
		$grid['full_order_time']->addDefaultSorting('desc');




		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
		$icon = Html::el('span');
		$grid->addAction('Detail', 'detail', clone $icon->class('icon icon-detail'));
		if( $this->getParam('id') == 1 ) {
			$action = $grid->addAction('Uhrazeno', 'setPaid!', null, true );
			//$action->setClass();

		}

		return $grid;
    }



	var $invoice = 1;

	// scrypt for backup and restore postgres database
	function dl_file($file){
		$folder = WWW_DIR.'/files/generated-invoices/';
		$file = $folder . $file;
	   if (!is_file($file)) { die("<b>404 File not found!</b>"); }
	   $len = filesize($file);
	   $filename = basename($file);
	   $filename = preg_replace('/invoice/', 'faktura', $filename);

	   $file_extension = strtolower(substr(strrchr($filename,"."),1));
	   $ctype="application/force-download";
	   header("Pragma: public");
	   header("Expires: 0");
	   header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	   header("Cache-Control: public");
	   header("Content-Description: File Transfer");
	   header("Content-Type: $ctype");
	   $header="Content-Disposition: attachment; filename=".$filename.";";
	   header($header );
	   header("Content-Transfer-Encoding: binary");
	   header("Content-Length: ".$len);
	   @readfile($file);
	   exit;
	}

	public function handleDownload( $file )
	{
		$this->dl_file($file);
	}

	/** @return DataGrid tovarnicka na orderForm */
	protected function createComponentOrderForm()
    {
        $form = new Form($this, 'orderForm');
		$renderer = $form->getRenderer();

		$id_order = $this->getParameter('id_order');
		$this->creditOrder = $this->em->find("MoneyPoint\Entity\CreditOrder", $id_order );
		$states = $this->creditOrderFacade->getAvailableStates( $this->creditOrder );
		$form->addSelect('idStatus', 'Stav objednávky: *', $states );

		if( $this->invoice )
		{
			$form->addUpload('invoice_file', 'Faktura:');
		}

		$form->addTextArea('employee_comment', 'Komentář obsluhy', 50, 4);


		$form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = array($this, 'submitedOrdersForm');
        //$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
    }


	/** @return DataGrid tovarnicka na orderForm */
	protected function createComponentSetPaid()
    {
        $form = new Form($this, 'setPaid');
		$renderer = $form->getRenderer();

		$form->addText('price', 'Výše částky')
				->getControlPrototype()->class('text ui-widget-content ui-corner-all');

		$form->addHidden('id_order', 'ID objednávky');

		$form->addSubmit('save', 'Potvrdit úhradu');
        $form->onSuccess[] = array($this, 'submitedSetPaid');
        //$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
    }

	public function renderInvoiceTest()
	{
		// test generování FA
		$generator = new OrdersInvoice();
		$generator->setPresenter( $this );
		$o = Orders::get()->find( 1 )->fetch();
		$generator->setObjednavka( $o );

		$generator->generate( 0, 1 );
		exit;
	}

	public function renderInvoiceStornoTest()
	{
		// test generování FA
		$generator = new OrdersInvoice();
		$generator->setPresenter( $this );
		$o = Orders::get()->find( 29 )->fetch();
		$generator->setObjednavka( $o );
		$generator->title = 'Storno faktury';
		$generator->storno = 1;

		$generator->generate( 0, 1 );

		exit;
	}


	protected function setPaid( $v )
	{
		$order = Orders::get()->find( $v->id_order )->fetch();
		try {
			if(empty($order->id_order)) 			throw new \LogicException('Špatné ID objednávky.');
			if( $order->id_status!=1 ) 			throw new \LogicException('Již uhrazená objednávka.');
			if( $order->price_sum!=$v->price ) 			throw new \LogicException("Špatná výše objednávky. Cena kreditu je ".FormatingHelpers::currency($order->price_sum).". Kontaktujte klieta.");
		} catch( \LogicException $e )
		{
			$this->flashMessage($e->getMessage(), 'ERROR');
			$this->redirect('this');
		}
		$return = $this->creditOrderFacade->setPaidByIdOrder( $v->id_order );

		if( !($return instanceof \MoneyPoint\Entity\CreditOrder ))
		{
			$message = OrderProcessing::get()->feedback;
			$this->flashMessage( 'Změna objednávky selhala. ( '.$message.' )', 'ERROR' );
			#$_SESSION['old-form'] = $form->getValues();
			$this->redirect( 'this' );
		} else
		{
//				$this->handleZpracuj();
			// smažeme zboží z košíku
			$basket = Environment::getSession('basket');
			unset( $basket->zbozi );
			$this->flashMessage('Objednávka byla úspěšně změněna', 'SUCCESS');
			$this->redirect( 'this' );
		}
			
		




	}

	function submitedSetPaid( Form $form )
    {
		if(isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = (object)$form->getValues();

//			$this->tryCatcher('$this->setPaid', $v );
			$this->setPaid($v);
			$this->redirect('default');
		}
	}

	function submitedOrdersForm( Form $form )
    {
        if(isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = (array)$form->getValues();
			$idOrder = $this->getParam('id_order');
			$o = Orders::get()->find($idOrder)->fetch();

			$result = $this->creditOrderFacade->processing( $idOrder, $v );
			#exit;

			#$order = new \MoneyPoint\CreditOrder( $idOrder );
			#$result = $order->processing()->process( $v );

			if( $result == 'FAIL' )
			{
				$message = OrderProcessing::get()->feedback;
				$this->flashMessage( 'Změna objednávky selhala. ( '.$message.' )', 'ERROR' );
				#$_SESSION['old-form'] = $form->getValues();
				$this->redirect( 'this' );
			} else
			{
//				$this->handleZpracuj();
				// smažeme zboží z košíku
				$basket = Environment::getSession('basket');
				unset( $basket->zbozi );
				$this->flashMessage('Objednávka byla úspěšně změněna', 'SUCCESS');
				$this->redirect( 'this' );
			}

		}
	}

	/** @var \AntoninRykalsky\EntityManager */
	protected $em;

	function injectEm(\AntoninRykalsky\EntityManager $em) {
		$this->em = $em->getEm();
	}
	
	private $creditOrder;
	
	public function createComponentBasketControl() {
		$product = $this->em->find("MoneyPoint\Entity\CreditProduct", 1 );
		$basket = new \MoneyPoint\Basket();
		$basket->addCredits($product, $this->creditOrder->getRelatedDiscount() );
		$basketControl = new \MoneyPoint\BasketControl( $basket );
		$basketControl->setAdminLayout();
		return $basketControl;
	}
	
	public function renderDetail( $id_order )
	{
		$this->template->orderSettings = $this->orderSettings;
		
		$this->template->stateMachine = $stateMachine = new \MoneyPoint\CreditOrderStateMachine();

		$this->creditOrder = $this->em->find("MoneyPoint\Entity\CreditOrder", $id_order );
		
		$documents = array();
		$tmp = $this->creditOrder->getInvoiceFilename();
		if(!empty( $tmp ))
		{
			$documents[] = array(
				'name' => 'Faktura',
				'filename' => $this->creditOrder->getInvoiceFilename(),
			);
		}
		$tmp = $this->creditOrder->getStornoFilename();
		if(!empty( $tmp ))
		{
			$documents[] = array(
				'name' => 'Storno faktura',
				'filename' => $this->creditOrder->getStornoFilename(),
			);
		}
		$this->template->documents = $documents;
		
		

		/* @var $order \MoneyPoint\Entity\CreditOrder */
		$order = $this->template->order = $this->creditOrder;

		/* @var $client \MoneyPoint\Entity\Member */
		$this->template->client = $client = $this->creditOrder->getUser();

		// breadcrumb
		$this->template->title = 'Objednávka';
		$this->breadDefault = 0;
		$section = $this->getComponent('navigation');
		$subsection = $section->add( $this->statuses[ $order->getIdStatus() ], $this->link("Orders:default", array('id'=> $order->getIdStatus()) ));
		$subsubsection = $subsection->add( $client->getNick().' '.FormatingHelpers::dateTimejnY( $order->getTsOrder() ), '');
		$section->setCurrent($subsubsection);



	}

}
