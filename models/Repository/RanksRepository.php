<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use Doctrine\ORM\EntityRepository;

class RanksRepository extends EntityRepository
{
	public function __construct(  $em, $metadata ) {
		parent::__construct( $em, $metadata );
	
		$container = \Nette\Environment::getContext();
		$memberRepository = $container->getByType('MoneyPoint\MemberRepository');
		
		$this->memberRepository = $memberRepository;
		$this->periodNetworkRepository = $this->_em->getRepository('MoneyPoint\Entity\PeriodNetwork');
		$this->periodRepository = $this->_em->getRepository('MoneyPoint\Entity\Period');
		$this->orderRepository = $this->_em->getRepository('MoneyPoint\Entity\CreditOrder');
		$this->userCarrierRepository = $this->_em->getRepository('MoneyPoint\Entity\UserCarrier');
	}
	
	private $entity = "\MoneyPoint\Entity\Carrier";

	/** @var \MoneyPoint\UserCarrierRepository */
	protected $userCarrierRepository;
	
	/** @var MemberRepository */
	protected $memberRepository;
	
	/** @var PeriodNetworkRepository */
	protected $periodNetworkRepository;

	/** @var PeriodRepository */
	protected $periodRepository;
	
	/** @var CreditOrderRepository */
	protected $orderRepository;
	
	public function fixActualCarrier( Entity\PeriodNetwork $solvedCommision )
	{
		$carrier = $this->findOutActualCarrier($solvedCommision);
		$solvedCommision->setCarrier( $carrier );
		
		foreach ($solvedCommision->getChildrenCommision() as $children )
		{
			$carrier = $this->findOutActualCarrier($children);
			$children->setCarrier( $carrier );
		}
	}
			
	public function findOutActualCarrier( Entity\PeriodNetwork $solvedCommision )
	{
		$rankTable = $solvedCommision->getPeriod()->getCarrierGroup();
		
		$creditsParent = $solvedCommision->getNetworkPointsPeriod();
		

		
		// ++ last carrier level
		$carriers = array();
		$lastCarrier = $this->getLastCarrierLevel( $solvedCommision );
		if( $lastCarrier instanceof Entity\Carrier ) {
			$carriers[ $lastCarrier->getKey() ] = $lastCarrier;
		}
		
		// ++ actual carrier level
		$actCarrier = $solvedCommision->getCarrier();
		if( $actCarrier instanceof Entity\Carrier ) {
			$carriers[ $actCarrier->getKey() ] = $actCarrier;
		}
		
		// ++ counted carrier level
		$countedCarrier = $rankTable->getCarrier( $creditsParent );
		$carriers[ $countedCarrier->getKey() ] = $countedCarrier;
		
		
		$orderRelatedCarrier = $this->userCarrierRepository->findOrderRelatedCarrier( $solvedCommision );
		if( $orderRelatedCarrier !== null ) {
			$carriers[ $orderRelatedCarrier->getRankFlag() ] = $orderRelatedCarrier->getCarrier();
		}
		
		$manualCarrier = $this->userCarrierRepository->findManualCarrier( $solvedCommision );
		if( $manualCarrier !== null ) {
			$carriers[ $manualCarrier->getRankFlag() ] = $manualCarrier->getCarrier();
		}
		
		return $carriers[ max(array_keys($carriers) ) ];
	}
	
	
	public function fixActualCarriers()
	{
		$period = $this->_em->getRepository("MoneyPoint\Entity\Period")->getActualPeriod();
		$lastPeriod = $this->_em->getRepository("MoneyPoint\Entity\Period")->getLastPeriod( $period );
		$commisions = $this->_em->getRepository("MoneyPoint\Entity\PeriodNetwork")->findByPeriod( $lastPeriod );
		foreach( $commisions as $commision )
		{
			$this->findOutActualCarrier( $commision );
		}
		$this->_em->flush();
	}
	
	
	/**
	 * Nastaví rank podle nejvyšší objednávky kreditu v období mimo stornované.
	 * Neřeší přenosy do dalšího období
	 * @param array $periodCreditsOrders
	 * @param \MoneyPoint\Entity\CreditOrder $cancelledOrder
	 * @throws \LogicException
	 */
	public function decreaseRank( array $periodCreditsOrders, Entity\CreditOrder $cancelledOrder )
	{
		$maxIncreasedRankKey = 0;
		foreach( $periodCreditsOrders as $cOrder ) {
			if( !$cOrder instanceof Entity\CreditOrder ) {
				throw new \LogicException("Period credits order should be instance of credit order!");
			}
			
			$activationRank = 0;
			$discount = $cOrder->getRelatedDiscount();
			if( !empty( $discount ))
			{
				$activationRank += (int)$discount->getActivateRank();
			}
			if( $cancelledOrder !== $cOrder &&
				( $activationRank > $maxIncreasedRankKey) &&
				( !in_array($cOrder->getStatus(), array( Entity\CreditOrder::STATE_STORNO_AFTER, Entity\CreditOrder::STATE_STORNO_BEFORE, Entity\CreditOrder::STATE_NEW ))))  {
				$maxIncreasedRankKey = $activationRank;
			}
		}
		
		$iduToIncreaseRank = array( $cancelledOrder->getUser()->getIdu() );
		$period = $cancelledOrder->getPeriodPayment();
		$ranks = $this->periodNetworkRepository->getByIdStructureAndPeriod( $iduToIncreaseRank, $period );
		foreach( $ranks as $periodNetwork )
		{
			if( $periodNetwork->getCarrier()->getKey() > $maxIncreasedRankKey ) {
				$maxIncreasedRank = $period->getCarrierGroup()->getCarrierById( $maxIncreasedRankKey );
				$periodNetwork->setCarrier( $maxIncreasedRank );
			}
		}
	}
	
	/**
	 * Zvedá ranky struktuře nahoru. Po koupi kreditu
	 * @param array $ranks ordered by rank key ASC
	 * @param type $minimalRank minumum level
	 * @param type $carriers
	 */
	public function increaseRanks( $idu, $minimalRank, $creditsOrder, $includeStructure = false )
	{
		$iduToIncreaseRank = array();
		if( $includeStructure )
		{
			$iduToIncreaseRank = $this->memberRepository->getStructure( $idu );
		}
		$iduToIncreaseRank[] = $idu;

		$period = $this->periodRepository->getActualPeriod();
		
		$ranks = $this->periodNetworkRepository->getByIdStructureAndPeriod( $iduToIncreaseRank, $period );
		


		$shouldHave = $period->getCarrierGroup()->getCarrierById( $minimalRank );
	//	\Doctrine\Common\Util\Debug::dump($shouldHave);exit;

		foreach( $ranks as $periodNetwork )
		{
			// TONIK-TODO neignoruj
			if( !$periodNetwork->getCarrier() instanceof Entity\Carrier ) continue;
			if( $periodNetwork->getCarrier()->getKey() < $minimalRank )
				$periodNetwork->setCarrier( $shouldHave );
			
			$parentCarrier = $periodNetwork->getParentCarrier();
			if( !empty( $parentCarrier ))
			if( $periodNetwork->getParentCarrier()->getKey() < $minimalRank )
				$periodNetwork->setParentCarrier( $shouldHave );
		}
		
		

		
		/* @var MoneyPoint\Entity\UserCarrier */
		$usrcarrep = $this->_em->getRepository('MoneyPoint\Entity\UserCarrier');
		$userCarrier = new Entity\UserCarrier;
		$userCarrier->setOrderRelated(
			$shouldHave,
			$period,
			$this->_em->getReference('MoneyPoint\Entity\Member', $idu),
			$creditsOrder
		); 
		


		$this->_em->persist($userCarrier);
		$this->_em->flush();
	}
	
	/**/
	
	public function getRanksForStructure()
	{
		$q = $this->_em->createQuery("SELECT a FROM a WHERE a.starts <= :now AND a.ends >= :now" );
		$q->setParameter('now', $now );
		
		foreach( $q->getResult() as $p )
			return $p;

	}
	
	/**
	 * Vrací kariérní tabulku předaného období
	 * @param type Doctrine entity účetního období
	 * @return Entity\CarrierGroup Kolekce-pole kariér
	 */
	public function getRankTable( $period  )
	{
		return $this->_em->getRepository("\MoneyPoint\Entity\PeriodRepository")->getRankTable( $period );
	}

	/**
	 * Zjistí rankový stav uživatele
	 * @param type $userId
	 * @return \MoneyPoint\Entity\Carrier
	 */
	public function getLastCarrierLevel( Entity\PeriodNetwork $solvedCommision  )
	{
		
		
		$userId = $solvedCommision->getIdu();
		$actualPeriod = $solvedCommision->getPeriod();
		$solvedPeriod = $this->periodRepository->getLastPeriod( $actualPeriod );
		
		
		
		// vyhledej entity pro výpočet
		$entity = '\MoneyPoint\Entity\PeriodNetwork';
		$q = $this->_em->createQuery("SELECT a FROM $entity a WHERE a.period = :period AND a.idu = :idu" );
		$q->setParameter('period', $solvedPeriod );
		$q->setParameter('idu', $userId );
		$toCount = $q->getResult();
		
		foreach( $toCount as $result )
		{
			if( $result instanceof $entity ) {
				return $result->getCarrier();
			}
		}
	}
}
