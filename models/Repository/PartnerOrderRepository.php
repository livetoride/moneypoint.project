<?php
namespace MoneyPoint;

use Doctrine\ORM\EntityRepository;

class PartnerOrderRepository extends EntityRepository
{
	
	
	public function findoutOrdersForAutomaticSettle()
	{
		$q = $this->_em->createQuery("SELECT po FROM MoneyPoint\Entity\PartnerOrder po "
				. "WHERE po.expSettleTimestamp <= :now "
				. "AND po.idStatus IN (:statuses)" );
		$q->setParameter('now', new \DateTime );
		$statuses = array( Entity\PartnerOrder::STATE_WAITING );
		$q->setParameter('statuses', empty($statuses)?null:implode(',', $statuses ));
		return $q->getResult();
	}
}
