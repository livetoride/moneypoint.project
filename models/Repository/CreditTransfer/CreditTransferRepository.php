<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class CreditTransferRepository extends \Doctrine\Orm\EntityRepository
{
	public function canBeSendedTo( $variableSymbol )
	{
		$member = $this->_em->getRepository("MoneyPoint\Entity\Member")->findOneBy(array("variableSymbol"=>$variableSymbol ));
		$can = $this->findOneBy( array(
			"recipient" => $member,
			"stateId" => Entity\CreditTransfer::S_RECEIVED
		));
		return $can === null;
	}
}
