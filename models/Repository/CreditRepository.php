<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class CreditRepository
{
	private $lastTransaction;

	public function getLastTransaction( $idu )
	{
		$lastTransaction = \AntoninRykalsky\CreditsAccounts::get()->actualState( $idu );
		$this->lastTransaction[ $idu ] = $lastTransaction;
		return $lastTransaction;
	}

	public function getState( $idu )
	{
		$this->getLastTransaction( $idu );
		return (int)@$this->lastTransaction[ $idu ]->credits_left;
	}
}
