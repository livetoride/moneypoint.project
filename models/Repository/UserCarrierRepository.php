<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use Doctrine\ORM\EntityRepository;


class UserCarrierRepository extends EntityRepository
{
	private function findUserCarrier( Entity\PeriodNetwork $periodNetwork, $type )
	{
		$q = $this->_em->createQuery(
				"SELECT uc, c FROM \MoneyPoint\Entity\UserCarrier uc "
				. "JOIN uc.carrier c "
				. "WHERE uc.type = :type "
				. "AND uc.period = :period "
				. "AND uc.user = :user "
				. "ORDER BY uc.tsInsert DESC "
		);
		$q->setMaxResults( 1 );
		$q->setParameter('type', $type );
		$q->setParameter('period', $periodNetwork->getPeriod() );
		$q->setParameter('user', $periodNetwork->getUser() );
		
		// return null or something
		$r = $q->getResult();
		foreach( $r as $ret )
		{
			return $ret;
		}
	}
	
	public function findOrderRelatedCarrier( Entity\PeriodNetwork $periodNetwork )
	{
		return $this->findUserCarrier($periodNetwork, Entity\UserCarrier::TYPE_ORDER_RELATED );
	}
	
	public function findManualCarrier( Entity\PeriodNetwork $periodNetwork )
	{
		return $this->findUserCarrier($periodNetwork, Entity\UserCarrier::TYPE_MANUAL );
	}
	
	
	
}
