<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use Doctrine\ORM\EntityRepository;


class CreditOrderRepository extends EntityRepository
{
	public function getOrder( $id )
	{
		return $this->_em->find('MoneyPoint\Entity\CreditOrder', $id );
	}
	
	public function findNewerOrders( Entity\CreditOrder $myOrder )
	{
		$qb = $this->_em->createQueryBuilder();
		$qb->select('a')
			->from('MoneyPoint\Entity\CreditOrder', 'a')
			->where('a.user = :user')
			->andWhere('a.setpaidTimestamp >= :date')
			->andWhere('a.idOrder != :id')
			->setParameter('user', $myOrder->getUser() )
			->setParameter('date', $myOrder->getTsPaid() )
			->setParameter('id', $myOrder->getId() );

		return $qb->getQuery()->getResult();
	}
	
	public function findSamePeriodOrders( Entity\CreditOrder $creditsOrder )
	{
		return $this->_em->getRepository('MoneyPoint\Entity\CreditOrder')
				->findBy( array(
					'user' => $creditsOrder->getUser(),
					'periodPayment'=>$creditsOrder->getPeriodPayment()
				));
	}
	
	
}
