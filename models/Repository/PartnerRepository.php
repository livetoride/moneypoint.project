<?php
namespace MoneyPoint;

use Doctrine\ORM\EntityRepository;

class PartnerRepository extends EntityRepository
{
	
	public function find( $partnerId )
	{
		return max(
			$this->_em->find('\MoneyPoint\Entity\Shop', $partnerId ),
			$this->_em->find('\MoneyPoint\Entity\Eshop', $partnerId ),
			$this->_em->find('\MoneyPoint\Entity\Partner', $partnerId )
		);
	}

}
