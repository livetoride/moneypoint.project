<?php

namespace MoneyPoint;
use Doctrine\ORM\EntityRepository;

class TransactionRepository extends EntityRepository
{
	public function getDatasource()
	{
		$qb = $this->_em->createQueryBuilder();
		return $qb->select('a')
			->from('MoneyPoint\Entity\Transaction', 'a');
	}
	
	public function createDifferentialCommisionTransaction( Entity\PeriodNetwork $commision )
	{
		$transaction = $commision->getTransaction();
		if( $transaction === null )
		{
			$transaction = new Entity\Transaction();
			$this->_em->persist( $transaction );
			$transaction->setDifferentialCommision( $commision );
			$transaction->setMpTransactionId( $this->getNextTransactionId( $transaction ) );
		}
		return $transaction;
	}
	
	public function getNextTransactionIdSolid( $transactionType, $member )
	{
		if( !in_array( $transactionType, array(1,2,5))  )
			throw new \Exception ('Nee, podporuje pouze partner order a credits order ');

		$y=date('y', strtotime('now'));
		$nextTransactionId = $member->getVariableSymbol().'-'.$transactionType.'-'.$y;
		
		// dohledání konečného pořadového čísla
		$a = \DAO\MpTransaction::get()->findAll()->where("mp_trans_id LIKE %like~", $nextTransactionId)->orderBy('mp_trans_id')->desc()->fetch();
		if( !empty($a->mp_trans_id))
		{
			preg_match('#-([^-$]+)$#', $a->mp_trans_id, $matches );
			if( empty($matches[1])) throw new \Exception('Neco se pokazilo pri zpracovani regularniho vyrazu v MP transaction id'.$a->mp_trans_id );
			$nextTransactionId .= '-'.sprintf('%1$03d', (int)$matches[1]+1);
		} else {
			$nextTransactionId .= '-001';
		}
		return $nextTransactionId;
	}
	
	public function getNextTransactionId( $transaction )
	{
		if( !in_array( $transaction->getType(), array(1,2,5))  )
			throw new \Exception ('Nee, podporuje pouze partner order a credits order ');
		
		$user = $transaction->getUser();
		$y=date('y', strtotime('now'));
		$nextTransactionId = $user->getVariableSymbol().'-'.$transaction->getType().'-'.$y;
		
		// dohledání konečného pořadového čísla
		$a = \DAO\MpTransaction::get()->findAll()->where("mp_trans_id LIKE %like~", $nextTransactionId)->orderBy('mp_trans_id')->desc()->fetch();
		if( !empty($a->mp_trans_id))
		{
			preg_match('#-([^-$]+)$#', $a->mp_trans_id, $matches );
			if( empty($matches[1])) throw new \Exception('Neco se pokazilo pri zpracovani regularniho vyrazu v MP transaction id'.$a->mp_trans_id );
			$nextTransactionId .= '-'.sprintf('%1$03d', (int)$matches[1]+1);
		} else {
			$nextTransactionId .= '-001';
		}
		return $nextTransactionId;
	}

}
