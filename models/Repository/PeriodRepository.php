<?php
namespace MoneyPoint;

use Doctrine\ORM\EntityRepository;

class PeriodRepository extends EntityRepository
{
	/** @var \AntoninRykalsky\TimeService */
	protected $timeService;
	
	public function __construct($em, \Doctrine\ORM\Mapping\ClassMetadata $class)
	{
		parent::__construct($em, $class);
		$container = \Nette\Environment::getContext();
		$this->timeService = $container->getByType('\AntoninRykalsky\TimeService');
	}
	
	public function find( $id )
	{
		$object = $this->getEntityManager()->find( 'MoneyPoint\Entity\Period', $id );
		return $object;
	}
	
	public function getDatasource()
	{
		$qb = $this->getEntityManager()->createQueryBuilder();
		return $qb->select('a')
			->from('MoneyPoint\Entity\Period', 'a');
	}

	
	public function getActualPeriod( $now = null )
	{
		if( empty( $now ))
		{
			$now = $this->timeService->getDateTime();
		}
		
		$q = $this->getEntityManager()->createQuery("SELECT a FROM \MoneyPoint\Entity\Period a WHERE a.starts <= :now AND a.ends >= :now" );
		$q->setParameter('now', $now );

		foreach( $q->getResult() as $p )
			return $p;

		throw new \Exception("Pro tento okamžik neexistuje účetní období");
	}
	
	public function getLastPeriod( Entity\Period $actualPeriod )
	{
		$now = clone $actualPeriod->getStarts(); // delete clone and i will kill you
		$interval = new \DateInterval('P1D');
		$now->sub( $interval );
		
		$q = $this->getEntityManager()->createQuery("SELECT a FROM \MoneyPoint\Entity\Period a WHERE a.starts <= :now AND a.ends >= :now" );
		$q->setParameter('now', $now );
		
		foreach( $q->getResult() as $p )
			return $p;

		
		#throw new \Exception("Pro tento okamžik neexistuje účetní období");
	}
	
	/**
	 * Vrací kariérní tabulku předaného období
	 * @param type Doctrine entity účetního období
	 * @return Entity\CarrierGroup Kolekce-pole kariér
	 */
	public function getRankTable( $period = null )
	{
		if(empty($period))
			$period=$this->getActualPeriod();
		
		if(!method_exists( $period, 'getCarrierGroup' ))
		{
//			\Doctrine\Common\Util\Debug::dump($period);
//			exit;

			throw new \Exception( "Missing period" );
		}



		$rankTable = $period->getCarrierGroup();
		return $rankTable;
	}

}
