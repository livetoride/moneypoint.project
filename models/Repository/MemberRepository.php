<?php

namespace MoneyPoint;
use Doctrine\ORM\EntityRepository;

class MemberRepository2 extends EntityRepository
{
	
	public function find( $id )
	{
		return $this->_em->find('MoneyPoint\Entity\Member', $id );
	}
	
	public function getStructure( $idu )
	{
		$structure = \AntoninRykalsky\GetAscendant::get()->find( $idu );

		$sCorrect = array();
		foreach( $structure as $k => $v )
			if( $v != 0) $sCorrect[ $k+1 ] = $v;

		return $sCorrect;
	}
	
	public function getUndirectOnly( $idu )
	{
		$return = array_values($this->getStructure($idu));
		unset( $return[0] );
		return $return;
	}
	
	public function getDirectOnly( $idu )
	{
		$return = array_values($this->getStructure($idu));
		if( empty( $return[0] ))
			return;
		
		return $return[0];
	}
	
	public function getChildenDirect( $idu )
	{
		$r=array();
		foreach( \DAO\Uzivatele::get()->findAll('idu')->where('idusponzor=%i', $idu )->fetchAll() as $i )
		{
			$r[]=$i->idu;
		}
		return $r;
	}
	
	public function getAsc( $idu )
	{
		$q = $this->_em->createQuery('SELECT a.idu FROM MoneyPoint\Entity\Member a WHERE a.idusponzor=:sponzor');
		$q->setParameter('sponzor', $idu );
		return array_values($q->getResult());
	}
	
	public function getOnlyIdsOnlyMembers()
	{
		/** @var \MoneyPoint\Entity\Member */
		$entity = 'MoneyPoint\Entity\Member';
		$q = $this->_em->createQuery("SELECT a FROM $entity a WHERE a.idu>0 ORDER BY a.idu" );
		return $q->getResult();
	}
	
}
