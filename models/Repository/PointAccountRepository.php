<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use Doctrine\ORM\EntityRepository;


class PointAccountRepository extends EntityRepository
{
	public function storeDifferentialTransaction( $owner, $change, $reason )
	{
		$type = Entity\PointAccount::TYPE_DIFFERENTIAL_COMMISSION;
		return $this->pointChange($owner, $change, $reason, $type );
	}
	
	public function storeCreditRewardTransaction( $owner, $change, $reason )
	{
		$type = Entity\PointAccount::TYPE_FIXED_COMMISSION;
		$this->pointChange($owner, $change, $reason, $type );
		$this->_em->flush();
	}
	
	public function pointChange( $owner, $change, $reason, $type, $line = null )
	{
		if( $type == null ) throw new \LogicException('O jaký typ provize jde?');
		$change = round($change*10)/10;
		
		$q = $this->_em->createQuery('select pa from MoneyPoint\Entity\PointAccount pa '
				. 'where pa.owner = :owner order by pa.id desc');
		
		$q->setParameter("owner", $owner );
		$q->setMaxResults(1);
		$lastChange = $q->getResult();
	
		if( !empty( $lastChange[0] ))
		{
			$left = $change + $lastChange[0]->getLeft();
		} else {
			$left = $change;
		}

		$points1 = new Entity\PointAccount();
		$points1->setPointChange($owner, $change, $reason, $left );
		$this->_em->persist($points1);
		
		
		return $points1;
	}
	
	
}
