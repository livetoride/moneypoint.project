<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use Doctrine\ORM\EntityRepository;


class CreditDiscountRepository extends EntityRepository
{
	public function findDiscountByCredits( $creditsSum )
	{
		$q = $this->_em->createQuery('SELECT a FROM MoneyPoint\Entity\CreditDiscount a WHERE a.credits = :credits');
		$q->setParameter('credits', $creditsSum );
		$result = $q->getResult();
		
		if( empty( $result ))
			throw new \LogicException('Tento produkt byl právě deaktivován a již nejde objednat, zkuste vybrat z aktuálních produktů.');
		
		foreach( $result as $r )
		{
			return $r;
		}
	}
	
	
}
