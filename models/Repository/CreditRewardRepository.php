<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use Doctrine\ORM\EntityRepository;


class CreditRewardRepository extends EntityRepository
{
	public function storeCreditRewardTransaction( Entity\CreditReward $rewardEntity, RewardCounting $reward, $creditsOrder )
	{
		$pointAccountRepository = $this->_em->getRepository("MoneyPoint\Entity\PointAccount");
		
		$owner = $this->_em->getReference("MoneyPoint\Entity\Member", $rewardEntity->getIdu() );
		$reason = "Fixní odměna za nákup v ".$rewardEntity->getLine()." linii.";
		$type = Entity\PointAccount::TYPE_FIXED_COMMISSION;
		$pointAccountChange = $pointAccountRepository->pointChange( $owner, (int)$reward->reward, $reason, $type );
		

		
		\AntoninRykalsky\CreditsAccounts::get()->modifyCredits( $rewardEntity->getIdu() , $reason, $reward->usedCredits, $creditsOrder->getIdOrder() );
		$credits_account_id = \dibi::getInsertId();

		// store transaction
		$rewardedMember = $this->_em->getReference('MoneyPoint\Entity\Member', $rewardEntity->getIdu() );
		
		$transaction = new Entity\Transaction;
		$transaction->setReward( $rewardEntity, $pointAccountChange, $credits_account_id, $rewardedMember, $creditsOrder );
		$this->_em->persist( $transaction );
		
		return $transaction;
	}
	
	
}
