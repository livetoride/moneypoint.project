<?php

namespace MoneyPoint;

class PosibleRewardRepository
{
    private $em;
	public function __construct( $em ) {
		$this->em = $em;
	}
	
	public function getCollection( $structure, $date )
	{
		return new PosibleRewardCollection( $this->em, $structure, $date );
	}
	
	
}
