<?php
namespace MoneyPoint;

use Doctrine\ORM\EntityRepository;

class LevelDistributionGroupRepository extends EntityRepository
{
	
	public function getActiveGroup()
	{
		$q = $this->_em->createQuery("SELECT ldg FROM ".$this->_entityName." ldg "
				. "JOIN ldg.baseCommision bc "
				. "WHERE bc.isActive=1 AND bc.typeId = :typeId");
		$q->setParameter('typeId', \AntoninRykalsky\Entity\Commision::FIXED );
		$result = $q->getResult();
		
		foreach( $result as $r )
		{
			return $r;
		}
	}
}
