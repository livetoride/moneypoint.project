<?php

namespace MoneyPoint;

class PartnerProviderInfoRepository
{
    private $em;
	public function __construct(
			\AntoninRykalsky\EntityManager $c
		) {
		$this->em = $c->getEm();
	}
	
	/** @deprecated since version number */
	public function getStorage( $id )
	{
		return new PartnerProviderInfoStorage( $this->em, $id );
	}
	
	/** @deprecated since version  */
	public function newStorage( $id = null )
	{
		if( empty( $id ))
		{
		$last = \DAO\PartnerProviderInfo::get()->findAll()->orderBy('id', 'desc')->fetch();
		if( empty( $last->id ))
			$next = 1;
		else
			$next = $last->ppi_id + 1;
		} else {
			$next = $id;
		}
		
		return new PartnerProviderInfoStorage( $this->em, $next );
	}
	
	public function findByOriginalId( $origId )
	{
		$q = $this->em->createQuery("SELECT p FROM \MoneyPoint\Entity\PartnerProviderInfo p WHERE p.originalId=:id");
		$q->setParameter( 'id', $origId );
		return $q->getResult();
	}
}
