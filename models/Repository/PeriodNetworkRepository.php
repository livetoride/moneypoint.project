<?php
namespace MoneyPoint;

use Doctrine\ORM\EntityRepository;

class PeriodNetworkRepository extends EntityRepository
{
	/** @var PeriodRepository */
	protected $periodRepository;
	
	private $mainEntity = 'MoneyPoint\Entity\PeriodNetwork';
	
	public function __construct($em, $class) {
		parent::__construct($em, $class);
		$this->periodRepository = $this->_em->getRepository('MoneyPoint\Entity\Period');
	}
	
	/** @return MoneyPoint\Entity\PeriodNetwork */
	public function getUserRank( $idu, $idPeriod=null )
	{
		if( empty( $idPeriod ))
		{
			$period = $this->getActualPeriod();
		} else {
			$period = $this->getEntityManager()->getReference('MoneyPoint\Entity\Period', $idPeriod );
		}
		
		$user = $this->getEntityManager()->getReference('MoneyPoint\Entity\Member', $idu );
		$userRank = $this->getEntityManager()->getRepository('MoneyPoint\Entity\PeriodNetwork')->findOneBy(array('user' => $user, 'period' => $period ));
		return $userRank;
	}
	
	public function getActualPeriod( $now = null )
	{
		if( empty( $now ))
		{
			$now = new \DateTime();
		}
		
		$q = $this->getEntityManager()->createQuery("SELECT a FROM \MoneyPoint\Entity\Period a WHERE a.starts <= :now AND a.ends >= :now" );
		$q->setParameter('now', $now );
		
		foreach( $q->getResult() as $p )
			return $p;

		throw new \Exception("Pro tento okamžik neexistuje účetní období");
	}
	
	public function find( $id )
	{
		$object = $this->_em->find( $this->mainEntity, $id );
		return $object;
	}
	
	public function getDatasource( $idu=null )
	{
		$qb = $this->_em->createQueryBuilder();
		$r = $qb->select('a, p')
			->from($this->mainEntity, 'a')
			
			->join('a.period', 'p');
		
		
		if( !empty( $idu ))
		{
			$r->where('a.user = ?1');
			$r->setParameter( 1, $this->_em->getReference('MoneyPoint\Entity\Member', $idu ));
		}
		
		return $r;
	}

	public function findPartialItems( Entity\PeriodNetwork $differentialCommision )
	{
		$q = $this->_em->createQuery("SELECT a FROM $this->mainEntity a INDEX BY a.idu WHERE a.parentIdu IN(:idu) AND a.period = :period" );
		$q->setParameter('idu', $differentialCommision->getUser()->getIdu() );
		$q->setParameter('period', $differentialCommision->getPeriod() );
		return $q->getResult();
	}
	
	public function findPartialMissingItems( $period )
	{
		$q = $this->_em->createQuery("SELECT a.idu FROM $this->mainEntity a LEFT JOIN a.transaction t INDEX BY a.idu WHERE t.idTransaction IS NULL AND a.period = :period" );
		$q->setParameter('period', $period );
		return $q->getResult();
	}
	
	/**
	 * 
	 * @param type $idu
	 * @param type $period
	 * @return PeriodNetwork[]
	 */
	public function findActual( $idu, $period = null )
	{
		if( empty( $period ))
			$period = $this->periodRepository->getActualPeriod();
	
		if( $period !== null && !($period instanceof \MoneyPoint\Entity\Period) )
		{
			throw new \Exception("Need period entity");
		}
			
		$entity = '\MoneyPoint\Entity\PeriodNetwork';
		$q = $this->_em->createQuery(
				"SELECT a "
				. "FROM $entity a "
				. "LEFT JOIN a.childrenCommision cc "
				. "INDEX BY a.idu "
				. "WHERE a.idu IN(:idu) "
				. ($period !== null ? "AND a.period = :period" : "" )
		);
		$q->setParameter('idu', $idu );
		if( $period !== null )
		{
			$q->setParameter('period', $period );
		}
		

		foreach( $q->getResult() as $r )
		{
			$return = $this->_em->find($entity, $r->getId() ); 
			return $return;
			#\Doctrine\Common\Util\Debug::dump($return);
		}
		
//		echo $q->getSql();
//		\Doctrine\Common\Util\Debug::dump( $q->getResult() );
//		exit;
		
		// DONT COMMENT THIS LINE, needed for lazy counting carrier after new period starts!
		try {
			throw new \Exception("provize pro idu $idu neexistuje");
		} catch( \Exception $e )
		{
			$entity = 'MoneyPoint\Entity\PeriodCreditState';
			$structure = array( $idu );
			$this->createDifferentialPointsIfNotExists( $structure, $period, $entity, 0 );
			$this->_em->flush();
			return $this->findActual($idu, $period = null );
		}
	}
	
	
	public function getByIdStructureAndPeriod( array $idu, Entity\Period $period )
	{
		$q = $this->_em->createQuery("SELECT a FROM {$this->mainEntity} a WHERE a.idu IN (:in) AND a.period = :period");
		$q->setParameter('in', $idu );
		$q->setParameter('period', $period );
		return $q->getResult();
	}
	
	public function getDeadlineInterval()
	{
		$days = \AntoninRykalsky\Configs::get()->byContent('general.safety_deadline');
		return new \DateInterval('P'.$days.'D');
		 
	}
	
	public function resetNetworkPoints( $passwd, Entity\Period $period )
	{
		if( $passwd !== 'iddqd' )
		{
			throw new \Exception("dont use this function");
		}
		$q = $this->_em->createQuery("UPDATE {$this->mainEntity} u SET "
				. "u.parentNetworkPeriod=0, "
				. "u.parentDirectPeriod=0, "
				. "u.parentNetworkTotal=0, "
				. "u.parentDirectTotal=0, "
				. "u.parentNetworkReward=0, "
				. "u.parentTotalReward=0 "
				. "WHERE u.period = :period"
		);
		$q->setParameter( 'period', $period );
		$q->getResult();
	}
	
	private $actualPointStateHolder=array();
	
	/**
	 * 
	 * @param type $structureIncludeBuyer because we storing points from network separatelly from each son!
	 * @param type $period
	 * @param type $entity
	 * @param type $points
	 */
	public function createDifferentialPointsIfNotExists($structureIncludeBuyer, $period, $entity, $points, $actualPointStateHolder = null )
	{
		if( !empty( $actualPointStateHolder ))
		{
			$this->actualPointStateHolder = $actualPointStateHolder;
		}
		
		
		
		if( empty( $this->actualPointStateHolder[$period->getId()] ))
			$this->actualPointStateHolder[$period->getId()] = array();
		
		$missing = array_diff($structureIncludeBuyer, array_keys($this->actualPointStateHolder[$period->getId()]) );
		if( !empty( $missing ))
		{
			$q = $this->_em->createQuery("SELECT a FROM $entity a INDEX BY a.idu WHERE a.idu IN(:idu) AND a.period = :period" );
			$q->setParameter('idu', $missing );
			$q->setParameter('period', $period );
			$pointStates = $q->getResult();
			foreach( $pointStates as $idu => $pcs )
			{
				$this->actualPointStateHolder[$period->getId()][ $idu ] = $pcs;
			}
		}
		
		

		// create what i still havent
		$missing2 = array_diff($structureIncludeBuyer, array_keys($this->actualPointStateHolder[$period->getId()]) );
		foreach( $missing2 as $fillIdu )
		{
			 $pcs = new Entity\PeriodCreditState();
			 $this->_em->persist( $pcs );
			 $pcs->init( $fillIdu, $period );
			 $this->actualPointStateHolder[$period->getId()][ $fillIdu ] = $pcs;
		}
		
//		\Doctrine\Common\Util\Debug::dump($structureIncludeBuyer);exit;

		foreach( $structureIncludeBuyer as $line => $iduToAddPoints )
		{
			$this->actualPointStateHolder[$period->getId()][ $iduToAddPoints ]->addToState( $points, $line );
		}
		return $this->actualPointStateHolder;
	}
	
	
	public function getEntitiesForAssigningCommision( $period, $parentUserId = null )
	{
		// zgroupujeme vysledky
		$entity = 'MoneyPoint\Entity\PeriodNetwork';
		$qb = $this->_em->createQueryBuilder();
		$q = $qb->select('a.parentIdu, SUM(a.parentNetworkReward) as network, SUM(a.parentTotalReward) as direct')
			->from($entity, 'a')
			->where('a.period = :period AND a.parentIdu IS NOT NULL')
			->groupBy('a.parentIdu')
			->setParameter('period', $period );
				
				
				// limitace pro uživatele
				if (!empty($parentUserId)) 		{
					$q->andWhere('(a.idu=:user OR a.parentIdu=:user)');
					$q->setParameter('user', $parentUserId);
				}


		$q  =  $q->getQuery();
		$q  = $q->setDQL(str_replace('WHERE', 'INDEX BY a.parentIdu WHERE', $q->getDQL()));

		
		
		$groupedResult = $q->getResult();
		return $groupedResult;
	}
	
	public function getEntitiesForAssigningOneCommision( $period, $parentUserId )
	{
		$return = array(
			'network' => 0,
			'direct' => 0
		);
				
		// zgroupujeme vysledky
		$entity = 'MoneyPoint\Entity\PeriodNetwork';
		$qb = $this->_em->createQueryBuilder();
		$q = $qb->select('a.parentIdu, SUM(a.parentNetworkReward) as network, SUM(a.parentTotalReward) as direct')
			->from($entity, 'a')
			->where('a.period = :period AND a.parentIdu IS NOT NULL')
			->groupBy('a.parentIdu')
			->setParameter('period', $period );
				
				
				// limitace pro uživatele
				if (!empty($parentUserId)) 		{
					$q->andWhere('(a.parentIdu=:user)');
					$q->setParameter('user', $parentUserId);
				}


		$q  =  $q->getQuery();
//		$q  = $q->setDQL(str_replace('WHERE', 'INDEX BY a.parentIdu WHERE', $q->getDQL()));
		
		$groupedResult = $q->getResult();
		foreach( $groupedResult as $result )
		{
			$return['network'] += $result['network'];
			$return['direct'] += $result['direct'];
		}
		
		return $return;
	}
}
