<?php

namespace AntoninRykalsky\Gopay;

use GopayConfig;
use GopaySoap;
use GopayHelper;

class GopayService
{
	private $em;
	/** @var type MoneyPoint\CreditOrderFacade */
	private $creditOrderService;
	
	private $goid;
	private $secureKey;
	private $lang;
	private $successUrl;
	private $failedUrl;
	private $callbackUrl;
	private $abadonUrl;
	
	/** @var \AntoninRykalsky\OrderHistoryService */
	private $orderHistoryService;
	
	public function __construct( 
			\MoneyPoint\Container $c,
			\MoneyPoint\CreditOrderService $creditOrderService,
			\AntoninRykalsky\OrderHistoryService $orderHistoryService
		) {
		$this->em = $c->getEm();
		$this->baseUrl = $c->getBaseUrl();
		$this->creditOrderService = $creditOrderService;
		$this->orderHistoryService = $orderHistoryService;
		
		$settings = $c->getGopaySettings();
		$this->goid = $settings['id'];
		$this->secureKey = $settings['secretKey'];
		$this->lang = 'cs';
		
		$this->callbackUrl = 'http://moneypoint.cz/integration/OvtTzVlGkH/gopay-test';
		
		
		
		GopayConfig::init( $settings['mode'] ); 
	}

	
	public function setSuccessUrl( $url )
	{
		$this->successUrl = $url;
	}
	
	public function setFailedUrl( $url )
	{
		$this->failedUrl = $url;
	}

	public function addPayment( $creditsOrder )
	{
		$gopayOrder = new GopayOrder();
		$gopayOrder->load( $creditsOrder );
		
		$paymentKey = $creditsOrder->getPayment()->getPaymentKey();
		if( $paymentKey == 'gopay' )
		{
			$paymentChannels = array("cz_cs_c", "eu_gp_u", "eu_cg" );
			$defaultPaymentChannel = ""; 
		} else {
			$paymentChannels = array( $paymentKey );
			$defaultPaymentChannel = $paymentKey; 
		}
		$p1 = null;
		$p2 = null;
		$p3 = null;
		$p4 = null;
		
		
		try {
			$paymentSessionId = GopaySoap::createPayment((float)$this->goid,
														$gopayOrder->getProductName(),
														(int)$gopayOrder->getTotalPrice(),
														$gopayOrder->getCurrency(),
														$gopayOrder->getOrderNumber(),
														$this->callbackUrl,
														$this->callbackUrl,
														$paymentChannels,
														$defaultPaymentChannel,
														$this->secureKey,
														$gopayOrder->firstName,
														$gopayOrder->lastName,
														$gopayOrder->city,
														$gopayOrder->street,
														$gopayOrder->postalCode,
														$gopayOrder->countryCode,
														$gopayOrder->email,
														$gopayOrder->phoneNumber,
														$p1,
														$p2,
														$p3,
														$p4,
														$this->lang);

		} catch (Exception $e) {
			/*
			 *  Osetreni chyby v pripade chybneho zalozeni platby
			 */
			header('Location: ' . $this->failedUrl . "?sessionState=" . GopayHelper::FAILED);
			exit;
		}
		
		$creditsOrder->setGopayPaymentSid($paymentSessionId);
	}
	
	public function redirectToGopay( $creditsOrder ) {
		$paymentSessionId = $creditsOrder->getGopayPaymentSid();
		
		$encryptedSignature = GopayHelper::encrypt(
						GopayHelper::hash(
							GopayHelper::concatPaymentSession((float)$this->goid,
															(float)$paymentSessionId, 
															$this->secureKey)
							), $this->secureKey);			

		/*
		 * Presmerovani na platebni branu GoPay s predvybranou platebni metodou GoPay penezenka ($defaultPaymentChannel)
		 */
		header('Location: ' . GopayConfig::fullIntegrationURL() . "?sessionInfo.targetGoId=" . $this->goid . "&sessionInfo.paymentSessionId=" . $paymentSessionId . "&sessionInfo.encryptedSignature=" . $encryptedSignature);
		exit;


		exit;
	}
	
		
	public function gwCallback( $returned )
	{
		$creditsOrder = $this->getOrderByPaymentSessionId( $returned->paymentSessionId );
		if( empty( $creditsOrder ))
			throw new \LogicException('Objednávka neexistuje');
		$gopayOrder = new GopayOrder();
		$gopayOrder->load( $creditsOrder );

		/* Kontrola validity parametru v redirectu, opatreni proti podvrzeni potvrzeni / zruseni platby */
		try {
			GopayHelper::checkPaymentIdentity(
						(float)$returned->goId,
						(float)$returned->paymentSessionId,
						null,
						$returned->orderNumber,
						$returned->encryptedSignature,
						(float)$this->goid,
						$gopayOrder->getOrderNumber(),
						$this->secureKey);

			/*
			 * Kontrola zaplacenosti objednavky na serveru GoPay
			 */
			$result = GopaySoap::isPaymentDone(
						(float)$returned->paymentSessionId,
						(float)$this->goid,
						$gopayOrder->getOrderNumber(),
						(int)$gopayOrder->getTotalPrice(),
						$gopayOrder->getCurrency(),
						$gopayOrder->getProductName(),
						$this->secureKey );

			if ($result["sessionState"] == GopayHelper::PAID ) {
				/*
				 * Zpracovat pouze objednavku, ktera jeste nebyla zaplacena 
				 */
				#echo $gopayOrder->getState();exit;
				if ($gopayOrder->getState() != GopayHelper::PAID) {

					/*
					 *  Zpracovani objednavky  ! UPRAVTE !
					 */
					$this->creditOrderService->setPaid( $creditsOrder );
					$this->orderHistoryService->changeStateGopay( $creditsOrder, $gopayOrder->getState() );
				}

				/*
				 * Presmerovani na prezentaci uspesne platby
				 */
				$location = $this->successUrl;

			} else if ( $result["sessionState"] == GopayHelper::PAYMENT_METHOD_CHOSEN) {
				/* Platba ceka na zaplaceni */
				$location = $this->successUrl;


			} else if ( $result["sessionState"] == GopayHelper::CREATED) {
				/* Platba nebyla zaplacena */
				$location = $this->failedUrl;

			} else if ( $result["sessionState"] == GopayHelper::CANCELED) {
				/* Platba byla zrusena objednavajicim */
				$gopayOrder->cancelPayment();
				$this->creditOrderService->setStorno( $creditsOrder );
				$location = $this->failedUrl;

			} else if ( $result["sessionState"] == GopayHelper::TIMEOUTED) {
				/* Platnost platby vyprsela  */
				$gopayOrder->timeoutPayment();
				$this->creditOrderService->setStorno( $creditsOrder );
				$location = $this->failedUrl;

			} else if ( $result["sessionState"] == GopayHelper::AUTHORIZED) {
				/* Platba byla autorizovana, ceka se na dokonceni  */
				$gopayOrder->autorizePayment();
				$location = $this->successUrl;

			} else if ( $result["sessionState"] == GopayHelper::REFUNDED) {
				/* Platba byla vracena - refundovana  */
				$gopayOrder->refundPayment();
				$location = $this->successUrl;

			} else {
				/* Chyba ve stavu platby */
				$location = $this->failedUrl;
				$result["sessionState"] = GopayHelper::FAILED;
				throw new \Exception("Chyba ve stavu platby");
				
			}

			# redirect to order detail
			#header('Location: ' . $location . "?sessionState=" . $result["sessionState"] . "&sessionSubState=" . $result["sessionSubState"]);
			#exit;

		} catch (Exception $e) {
			/*
			 * Nevalidni informace z redirectu
			 */
			throw new \Exception("Nevalidní informace z redirectu gopay");
			header('Location: ' . $this->failedUrl . "?sessionState=" . GopayHelper::FAILED);
			exit;

		}
		
		$result['success'] = $location == $this->successUrl;
		return (object)array(
			'order' => $creditsOrder, 
			'result' => $result 
			);
	}
	
	public function getOrderByPaymentSessionId( $paymentSessionId )
	{
		$q = $this->em->getRepository('MoneyPoint\Entity\CreditOrder')->findBy(
             array('gopayPaymentSid'=> $paymentSessionId )
           );
		foreach( $q as $order )
			return $order;
	}
    
}
