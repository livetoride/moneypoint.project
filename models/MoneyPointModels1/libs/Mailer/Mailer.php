<?php
/**
 * Model zacházející s tabulkou získaných bodů
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */

namespace MoneyPoint;
use Nette\Templating\FileTemplate;
use Nette\Environment;
use AntoninRykalsky\Configs;

class Mailer extends \Nette\Mail\Message {
	

	protected $myHtml;
	protected $myBasePath;
	protected $replacement = array();
	
	private function setTemplate($text) {
			$template = new FileTemplate(dirname(__FILE__).'/email.latte');
			$template->registerFilter(new \Nette\Latte\Engine);
			$template->registerHelperLoader('Nette\Templating\Helpers::loader');
			$template->text = $text; 
			return $template;
	}
	
	public function addReplacement( $key, $value )
	{
		$this->replacement[ $key ]=$value;
	}
	
	public function replace()
	{
		$mail = &$this->myHtml;
		foreach( $this->replacement as $k => $v )
		{
			if( $v instanceof \DateTime )
				$v = $v->format('j.n.Y H:i:s');
			$mail = preg_replace('/\[\['.$k.'\]\]/', $v, $mail);
		}
		$this->setHTMLBody( $this->myHtml, $this->myBasePath );
		return $mail;
	}
	
	/**
	 * Přilepí k html body adresář s obrázky a podpis
	 */
	public function setHTMLBody( $html, $basePath = null )
	{
		if( $basePath == null ) $basePath = WWW_DIR.'/images/moneypoint';
	
		$this->myHtml = $html;
		$this->myBasePath = $basePath;
		

		$footer = Configs::get()->byContent('system-mailer.logo');

		$a = file_get_contents(dirname(__FILE__).'/a.htmlpart');
		$b = file_get_contents(dirname(__FILE__).'/b.htmlpart');
		$html = $a.$html.$b;

		parent::setHTMLBody( $html , $basePath );
	}
	
	protected function setPrimitiveHTMLBody($html, $basePath = null )
	{
		if( $basePath == null ) $basePath = WWW_DIR.'/images';
		parent::setHTMLBody($html, $basePath);
	}

	/**
	 * vrací info zda jsme nejsme localu tj. jestli lze odeslat email
	 */
	public function canISendMails()
	{
		return !empty($_SERVER['SERVER_NAME']) && $_SERVER['SERVER_NAME'] != 'localhost' && Environment::isProduction();
	}

	/**
	 * Pokud nejsme na localhostu odešli email
	 */
	public function send()
	{
		$this->replace();
		try {
			$m = \Nette\Environment::getService('smtpSettings');
			$mailer = $m->setSmtpServers( $this );
			if( $mailer != null )
				$this->setMailer( $mailer );
		} catch( \Nette\DI\MissingServiceException $e )
		{ /* use classic mail() */ }
		
		if($this->canISendMails())
			parent::send();
		else {
			// ulozime
			$i=0;
			do {
				$i++;
				$append = ($i>1?'-'.$i:'');
				$file = 'c:/tmp/emails/'.time().$append.'.eml';
			} while( file_exists( $file ));
			file_put_contents( $file, $this->generateMessage() );
		}
	}

	/**
	 * pokud je cc prázdné neodesílej
	 */
	public function addCc($email, $name = NULL)
	{
		if( !empty( $email ))
			parent::addCc($email, $name);
	}
}
