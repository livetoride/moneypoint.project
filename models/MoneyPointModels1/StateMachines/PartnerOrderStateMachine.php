<?php

namespace MoneyPoint;

class PartnerOrderStateMachine extends \AntoninRykalsky\StateMachineService
{
	public function __construct() {
		
		$this->addState( \MoneyPoint\Entity\PartnerOrder::STATE_CJ_NEW, 'STATE_CJ_NEW' );
		$this->addState( \MoneyPoint\Entity\PartnerOrder::STATE_WAITING, 'STATE_WAITING' );
		$this->addState( \MoneyPoint\Entity\PartnerOrder::STATE_REWARDED, 'STATE_REWARDED' );
		$this->addState( \MoneyPoint\Entity\PartnerOrder::STATE_STORNO, 'STATE_STORNO' );
		
		// klasické přechody
		$this->addTransition(
				'assignCommision',
				\MoneyPoint\Entity\PartnerOrder::STATE_CJ_NEW,
				\MoneyPoint\Entity\PartnerOrder::STATE_WAITING
			);
		$this->addTransition(
				'stornoCommision',
				\MoneyPoint\Entity\PartnerOrder::STATE_CJ_NEW,
				\MoneyPoint\Entity\PartnerOrder::STATE_STORNO
			);
		$this->addTransition(
				'setRewarded',
				\MoneyPoint\Entity\PartnerOrder::STATE_WAITING,
				\MoneyPoint\Entity\PartnerOrder::STATE_REWARDED
			);
		$this->addTransition(
				'rewardWaiting',
				\MoneyPoint\Entity\PartnerOrder::STATE_WAITING,
				\MoneyPoint\Entity\PartnerOrder::STATE_STORNO
			);
		
		$this->langDesc = array(
			\MoneyPoint\Entity\PartnerOrder::STATE_CJ_NEW => "CJ nový",
			\MoneyPoint\Entity\PartnerOrder::STATE_WAITING => "čekací období",
			\MoneyPoint\Entity\PartnerOrder::STATE_REWARDED => "odměny přiděleny",
			\MoneyPoint\Entity\PartnerOrder::STATE_STORNO => "storno"
		);
	}
	
	
}
