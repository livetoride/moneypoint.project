<?php

namespace MoneyPoint;

class CreditOrderStateMachine extends \AntoninRykalsky\StateMachineService
{
	public function __construct() {
		
		$this->addState(\MoneyPoint\Entity\CreditOrder::STATE_NEW, 'STATE_NEW' );
		$this->addState( \MoneyPoint\Entity\CreditOrder::STATE_PAID, 'STATE_PAID' );
		$this->addState( \MoneyPoint\Entity\CreditOrder::STATE_STORNO_BEFORE, 'STATE_STORNO_BEFORE' );
		$this->addState( \MoneyPoint\Entity\CreditOrder::STATE_STORNO_AFTER, 'STATE_STORNO_AFTER' );
		$this->addState( \MoneyPoint\Entity\CreditOrder::STATE_REWARDED, 'STATE_REWARDED' );
		
		// klasické přechody
		$this->addTransition(
				'setPaid',
				\MoneyPoint\Entity\CreditOrder::STATE_NEW,
				\MoneyPoint\Entity\CreditOrder::STATE_PAID
			);
		$this->addTransition(
				'setStorno',
				\MoneyPoint\Entity\CreditOrder::STATE_NEW,
				\MoneyPoint\Entity\CreditOrder::STATE_STORNO_BEFORE
			);
		$this->addTransition(
				'setRewarded',
				\MoneyPoint\Entity\CreditOrder::STATE_PAID,
				\MoneyPoint\Entity\CreditOrder::STATE_REWARDED
			);
		$this->addTransition(
				'setCanceled',
				\MoneyPoint\Entity\CreditOrder::STATE_PAID,
				\MoneyPoint\Entity\CreditOrder::STATE_STORNO_AFTER
			);
		
		$this->langDesc = array(
			\MoneyPoint\Entity\CreditOrder::STATE_NEW => "nezaplacená",
			\MoneyPoint\Entity\CreditOrder::STATE_PAID => "zaplacená",
			\MoneyPoint\Entity\CreditOrder::STATE_STORNO_BEFORE => "storno před úhradou",
			\MoneyPoint\Entity\CreditOrder::STATE_STORNO_AFTER => "storno po úhradě",
			\MoneyPoint\Entity\CreditOrder::STATE_REWARDED => "odměny přiděleny"
		);
	}
	
	
}
