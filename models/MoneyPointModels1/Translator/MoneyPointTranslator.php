<?php
class MoneyPointTranslator implements Nette\Localization\ITranslator
{
    /**
     * Translates the given string.
     * @param  string   message
     * @param  int      plural count
     * @return string
     */
    public function translate($message, $count = NULL)
    {
		$messages=array();
		$messages['carrier'] = 'stupeň';
		$messages['Carriers and rates'] = 'Stupně a sazby';
        return $messages[$message];
    }
}

?>