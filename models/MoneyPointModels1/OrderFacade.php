<?php
namespace BaseEshop;

class OrderFacade {
	private $em;
	
	
	/** @var orderRepository */
	protected $orderRepository;
	/** @var userRepository */
	protected $userRepository;
	
	public function __construct(
			\AntoninRykalsky\EntityManager $em ) {
		$this->em = $em->getEm();
		$this->orderRepository = $this->em->getRepository('BaseEshop\Entity\Order');
		$this->userRepository = $this->em->getRepository('BaseEshop\Entity\User');
		
	}
	
	public function setPrice($id = 1, $price = 150 )
			
	{
		//print_r($id); exit();
		$order = $this->orderRepository->find($id);
		$order->setPrice($price);
		$this->em->persist($order);
		$this->em->flush();
	
	}
	
	public function setUser($id = 1, $idu=2) {
		$order = $this->orderRepository->find($id);
		$user = $this->userRepository->find($idu);
		//\Doctrine\Common\Util\Debug::dump($user);exit;

		$order->setUser($user);
		$this->em->persist($order);
		$this->em->flush();
		
		
	}
	
	public function setFinalUser($id = 1, $idu=2) {
		$order = $this->orderRepository->find($id);
		$user = $this->userRepository->find($idu);
		//\Doctrine\Common\Util\Debug::dump($user);exit;

		$order->setFinalUser($user);
		$this->em->persist($order);
		$this->em->flush();
		
		
	}
	
	
	
}


