<?php

use AntoninRykalsky\UnivesalModels\ConfigurationFacade as CF;

class ThissiteSettingsPlugin
{
	private $plugins = array();
	public function getPlugins() {
		$this->plugins[] = array(
			'groupname' => 'Provizní systém MoneyPoint',
			'icon' => 'blocks.jpeg',
			'link_to' => ':Admin:MoneyPoint:Commisions:Commisions:default',
			'type' => CF::OWN_PRESENTER
		);
		
		return $this->plugins;
	}
	
	
}

?>