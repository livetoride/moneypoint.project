<?php

namespace MoneyPoint;

class MemberFacade
{
    private $em;
	
	/** @var MoneyPoint\DiferentialCommisionFacade */
	var $direfentialCommisionFacade;
	
	/** @var RanksService */
	protected $ranksService;
	
	/** @var PeriodRepository */
	protected $periodRepository;
	
	public function __construct(
			\AntoninRykalsky\EntityManager $em,
			RanksService $ranksService,
			DiferentialCommisionFacade $diferentialCommisionFacade
		) {
		$this->em = $em->getEm();
		$this->repository = new MemberRepository( $em );
		$this->ranksService = $ranksService;
		$this->direfentialCommisionFacade = $diferentialCommisionFacade;
		
		$this->periodRepository = $this->em->getRepository('MoneyPoint\Entity\Period');
	}

	private function fixDuplicity2() {
		$duplicitiesToCorrect = \dibi::query('select * from mp_period_network
where commision_parent_id NOT IN (
	select id from mp_period_network
);')->fetchAll();
		
		
		foreach( $duplicitiesToCorrect as $duplicityToCorrect )
		{
			print_r( $duplicityToCorrect );
			exit;
		}



	}
	private function fixDuplicity() {
//		$this->fixDuplicity2();
		$duplicitiesToCorrect = \dibi::query('select idu, period_id, 

count(*),
sum(parent_network_period) as parent_network_period,
sum(parent_direct_period) as parent_direct_period,
sum(parent_network_total) as parent_network_total,
sum(parent_direct_total) as parent_direct_total,
sum(parent_network_reward) as parent_network_reward,
sum(parent_total_reward) as parent_total_reward,
max(carrier_id) as carrier_id, 
max(parent_carrier_id) as parent_carrier_id, 
max(network_reward) as network_reward, 
max(total_reward) as total_reward, 
min(commision_parent_id) as commision_parent_id

from mp_period_network
group by idu, period_id
having count(*) > 1
order by idu')->fetchAll();
		
		$correctParents = array();
		foreach( $duplicitiesToCorrect as $duplicityToCorrect )
		{
			$duplicityRows = \DAO\MpPeriodNetwork::get()
					->findAll()
					->where('period_id=%i AND idu = %i', $duplicityToCorrect->period_id, $duplicityToCorrect->idu )
					->orderBy('id')
					->fetchAll();
			
			$i=0;
			$correctParent = null;
			foreach( $duplicityRows as $duplicityRow )
			{
				$i++;
				if( $i == 1 )
				{
					$update = array(
						'parent_network_period' => $duplicityToCorrect->parent_network_period,
						'parent_direct_period' => $duplicityToCorrect->parent_direct_period,
						'parent_network_total' => $duplicityToCorrect->parent_network_total,
						'parent_direct_total' => $duplicityToCorrect->parent_direct_total,
						'parent_network_reward' => $duplicityToCorrect->parent_network_reward,
						'parent_total_reward' => $duplicityToCorrect->parent_total_reward,
						'carrier_id' => $duplicityToCorrect->carrier_id,
						'parent_carrier_id' => $duplicityToCorrect->parent_carrier_id,
						'network_reward' => $duplicityToCorrect->network_reward,
						'total_reward' => $duplicityToCorrect->total_reward,
						'commision_parent_id' => $duplicityToCorrect->commision_parent_id
					);
					$correctParent = $duplicityRow->id;
					foreach( $update as $k => $v ) {
						if( empty( $v )) {
							unset($update[$k]);
						}
					}
					\DAO\MpPeriodNetwork::get()->update($duplicityRow->id, $update);
				} else {
					$correctParents[ $duplicityRow->id ] = $correctParent;
					\DAO\MpPeriodNetwork::get()->updateWhere('commision_parent_id='.$duplicityRow->id, array('commision_parent_id'=>$correctParent));
					\DAO\MpPeriodNetwork::get()->delete($duplicityRow->id);
				}
			}
		}
		$pm = \DAO\MpPeriodNetwork::get()->findAll()->orderBy('id')->fetchAssoc('id');
		foreach ($pm as $key => $value) {
			$user = \DAO\Uzivatele::get()->find( $value->idu )->fetch();
			$correctPn = \DAO\MpPeriodNetwork::get()->findAll()->where('idu=%i and period_id=%i', $user->idusponzor, $value->period_id )->fetch();
			$correctPnId = empty( $correctPn->id ) ? null : $correctPn->id;
			$correctPnId = $correctPnId<=0 ? null : $correctPnId;
			$correctPnId = $user->idusponzor==0 ? null : $correctPnId;
//			echo $value->idu.'->'.$user->idusponzor."\n";
//			echo $key.'->'.$correctPnId."\n";
//			echo "\n";
			\DAO\MpPeriodNetwork::get()->update($key, array('commision_parent_id'=> $correctPnId ));
		}
		// dont remove this line, this should be just fix script
		exit;
	}

	/**
	 * Vrací textový řetezec názvu aktuálního získaného ranku uživatele
	 * @param type $id
	 * @return string
	 */
	public function getMemberRank( $id )
	{
//		$this->fixDuplicity();

		
//		try {
//			\DAO\MpPeriodNetwork::get()->deleteWhere('parent_network_total IS NULL AND parent_direct_total IS NULL');
			/* @var $periodNetwork Entity\PeriodNetwork */
			$periodNetwork = $this->ranksService->getMemberRank( $id );
//		} catch ( \Exception $e)
//		{
//			// v případě, že neexistuje - lazy chování, dopočítej
//			$this->direfentialCommisionFacade->installDefaultAccount($id);
//			$period = $this->periodRepository->getActualPeriod();
//			$lastPeriod = $this->periodRepository->getLastPeriod( $period );
//			$this->direfentialCommisionFacade->countCommision($period->getId(), $id);
//			try {
//				$periodNetwork = $this->ranksService->getMemberRank( $id );
//			} catch( \Exception $e)
//			{}
//		}
//		
		if( !empty( $periodNetwork ) && $periodNetwork instanceof Entity\PeriodNetwork )
			if( $periodNetwork->getCarrier() instanceof Entity\Carrier )
			{
				$this->em->refresh( $periodNetwork->getCarrier() );
//				\Doctrine\Common\Util\Debug::dump($periodNetwork->getCarrier());exit;

				return $periodNetwork->getCarrier()->getCarrier();
			}

		return '-';
	}
			
	public function secureFind( $id )
	{
		$object = $this->repository->find( $id );
		if( empty( $object ))
			throw new \Nette\Application\BadRequestException('Hledané omezení neexistuje');
		
		return $object;
	}


}
