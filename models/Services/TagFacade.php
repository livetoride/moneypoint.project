<?php

namespace MoneyPoint;

class TagFacade
{
	private $em;
	public function __construct( \AntoninRykalsky\EntityManager $c2 ) {
		$this->em = $c2->getEm();
	}
	
	public function getTags()
	{
		//$model = $this->em->createQuery("SELECT * FROM \AntoninRykalsky\Entity\Tags");
		$model = \DAO\BTags::get()->getDataSource();
		return $model;
	}
	public function getShopTags($partnerId ) {
		if ( $partnerId !== null ) {
			$partner = $this->em->getRepository("MoneyPoint\Entity\Shop")->find( $partnerId );
			$c = $partner->getTags();

			$return = array();
			foreach ($c as $value) {
				/* @var $value \AntoninRykalsky\Entity\Tags */
				$return[] = $value->getTagName();

			}
			return $return;
		}
	}
	public function getPartnerTags($partnerId ) {
		if ( $partnerId !== null ) {
			$partner = $this->em->getRepository("MoneyPoint\Entity\Partner")->find( $partnerId );
			$c = $partner->getTags();

			$return = array();
			foreach ($c as $value) {
				/* @var $value \AntoninRykalsky\Entity\Tags */
				$return[] = $value->getTagName();

			}
			return $return;
		}
	}
	public function getEshopTags($partnerId ) {
		if ( $partnerId !== null ) {
			$partner = $this->em->getRepository("MoneyPoint\Entity\Eshop")->find( $partnerId );
			$c = $partner->getTags();

			$return = array();
			foreach ($c as $value) {
				/* @var $value \AntoninRykalsky\Entity\Tags */
				$return[] = $value->getTagName();

			}
			return $return;
		}
	}
	
	public function deleteTag( $id)
	{
		$tag = $this->em->find('\AntoninRykalsky\Entity\Tags', $id);
		$this->em->remove($tag);
		$this->em->flush();
		return 1;
	}

	public function addTag($tagName)
	{
		//$check = $this->em->getRepository("AntoninRykalsky\Entity\Tags" )->findBy(array('tagName'=>$tagName ));
		
		
	/*	if (!empty($check) ){
			throw new \LogicException('kategorie již existuje');
		} else { */
		$tag1 = new \AntoninRykalsky\Entity\Tags();
		$tag1->storeTag($tagName);
		$this->em->persist( $tag1 );
		$this->em->flush(); 
		return $tag1;
	//}		

		
//		\Doctrine\Common\Util\Debug::dump();
//		\Doctrine\Common\Util\Debug::dump( $partner->getTags() );
		
	}
	
	public function storeTagToPartner($partnerId, $tags )
	{
//		if( is_numeric($partnerId))
//		{ 
//			throw new \Exception;
//		}
		foreach ($tags as $t)
		{
			$tag = $this->em->getRepository("AntoninRykalsky\Entity\Tags")->findOneBy(array('tagName'=> $t ));
			if (empty($tag) ){
				$tag = $this->addTag($t); 
				
			}
			/* @var $partner Entity\Partner */
			$partner = $this->em->getRepository("MoneyPoint\Entity\Partner")->find( $partnerId );

			$exist = 0;
			$c = $partner->getTags();
			foreach($c as $tagKey => $itemTag)
			{
				if(!in_array($itemTag->getTagName(), $tags))
				{
					$partner->removeTag( $itemTag );
				}
				if( $itemTag->getTagName() == $tag->getTagName() )
				{
					$exist = 1;
					continue;
				}
			}
			
			if (!$exist){
				$partner->addTag($tag);
			}
		}
		$this->em->flush();
	}
	public function storeTagToShop($partnerId, $tags )
	{
//		if( is_numeric($partnerId))
//		{ 
//			throw new \Exception;
//		}
		foreach ($tags as $t)
		{
			$tag = $this->em->getRepository("AntoninRykalsky\Entity\Tags")->findOneBy(array('tagName'=> $t ));
			if (empty($tag) ){
				$tag = $this->addTag($t); 
				
			}
			/* @var $partner Entity\Shop */
			$partner = $this->em->getRepository("MoneyPoint\Entity\Shop")->find( $partnerId );

			$exist = 0;
			$c = $partner->getTags();
			foreach($c as $tagKey => $itemTag)
			{
				if(!in_array($itemTag->getTagName(), $tags))
				{
					$partner->removeTag( $itemTag );
				}
				if( $itemTag->getTagName() == $tag->getTagName() )
				{
					$exist = 1;
					continue;
				}
			}
			
			if (!$exist){
				$partner->addTag($tag);
			}
		}
		$this->em->flush();
	}
	public function storeTagToEshop($partnerId, $tags )
	{
//		if( is_numeric($partnerId))
//		{ 
//			throw new \Exception;
//		}
		foreach ($tags as $t)
		{
			$tag = $this->em->getRepository("AntoninRykalsky\Entity\Tags")->findOneBy(array('tagName'=> $t ));
			if (empty($tag) ){
				$tag = $this->addTag($t); 
				
			}
			/* @var $partner Entity\Eshop */
			$partner = $this->em->getRepository("MoneyPoint\Entity\Eshop")->find( $partnerId );

			$exist = 0;
			$c = $partner->getTags();
			foreach($c as $tagKey => $itemTag)
			{
				if(!in_array($itemTag->getTagName(), $tags))
				{
					$partner->removeTag( $itemTag );
				}
				if( $itemTag->getTagName() == $tag->getTagName() )
				{
					$exist = 1;
					continue;
				}
			}
			
			if (!$exist){
				$partner->addTag($tag);
			}
		}
		$this->em->flush();
	}
}
