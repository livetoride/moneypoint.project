<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class UserFacade 
{
	private $em;
	public function __construct(
			\AntoninRykalsky\EntityManager $em
		) {
		$this->em = $em->getEm();
	}
	
	public function setPremium( $idu, $hasPremium )
	{
		/** @var \MoneyPoint\Entity\UserInvitation */
		$member = $this->em->find('MoneyPoint\Entity\Member', $idu );
		$member->setHasBussiness( $hasPremium );
		$this->em->flush();
	}
	
	public function getUser( $idu )
	{
		return $this->em->find('MoneyPoint\Entity\Member', $idu );
	}
}
