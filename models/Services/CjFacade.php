<?php

namespace MoneyPoint;

use FrontModule\MyDebugger as Debugger;

class CjFacade extends \Nette\Object
{
	private $em;
	/** @var CjJsonRepository */
	private $cjRepository;
	/** @var PartnerOrderFacade */
	private $partnerOrderFacade;
	/** @var PartnerProviderInfoRepository */
	private $partnerProviderInfoRepository;
	public function __construct(
			\Nette\DI\Container $c,
			\AntoninRykalsky\EntityManager $c2,
			\MoneyPoint\CjJsonRepository $cjRepository,
			\MoneyPoint\PartnerOrderFacade $partnerOrderFacade,
			\MoneyPoint\PartnerProviderInfoRepository $partnerProviderInfoRepository
		) {
		$this->em = $c2->getEm();
		//$this->baseUrl = $c->getBaseUrl();
		
		$this->cjRepository = $cjRepository;
		$this->partnerOrderFacade = $partnerOrderFacade;
		$this->partnerProviderInfoRepository = $partnerProviderInfoRepository;
	}
	
	public function getDebugCjString()
	{
		// findout select * from partners WHERE link like '%10903718%'
		return '<cj-api><commissions total-matched="12">'
//				. '<commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>10903718</aid><commission-id>1649308349</commission-id><country></country><event-date>2013-10-31T06:45:55-0700</event-date><locking-date>2013-12-30</locking-date><order-id>701346934</order-id><original>true</original><original-action-id>1393851130</original-action-id><posting-date>2013-10-31T07:31:29-0700</posting-date><website-id>7306242</website-id><action-tracker-id>344096</action-tracker-id><action-tracker-name>Kase_item Sale</action-tracker-name><cid>3342166</cid><advertiser-name>Kasa - CZ</advertiser-name><commission-amount>1.36</commission-amount><order-discount>0.00</order-discount><sid>4</sid><sale-amount>45.41</sale-amount></commission>'
//				. '<commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>10903718</aid><commission-id>1658490504</commission-id><country></country><event-date>2013-11-20T06:47:33-0800</event-date><locking-date>2014-01-19</locking-date><order-id>701370959</order-id><original>true</original><original-action-id>1402548932</original-action-id><posting-date>2013-11-20T07:32:37-0800</posting-date><website-id>7306242</website-id><action-tracker-id>344096</action-tracker-id><action-tracker-name>Kase_item Sale</action-tracker-name><cid>3342166</cid><advertiser-name>Kasa - CZ</advertiser-name><commission-amount>2.50</commission-amount><order-discount>0.00</order-discount><sid></sid><sale-amount>95.44</sale-amount></commission>'
//				. '<commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>11477764</aid><commission-id>1662145851</commission-id><country></country><event-date>2013-11-27T14:27:18-0800</event-date><locking-date>2014-01-10</locking-date><order-id>2013551810</order-id><original>true</original><original-action-id>1406089643</original-action-id><posting-date>2013-11-27T15:30:20-0800</posting-date><website-id>7306242</website-id><action-tracker-id>363495</action-tracker-id><action-tracker-name>SportObchod.cz_itembased</action-tracker-name><cid>4065356</cid><advertiser-name>SportObchod.cz</advertiser-name><commission-amount>17.60</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>352.04</sale-amount></commission>'
//				. '<commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>11584310</aid><commission-id>1662415816</commission-id><country></country><event-date>2013-11-28T02:48:46-0800</event-date><locking-date>2013-12-10</locking-date><order-id>1016996</order-id><original>true</original><original-action-id>1406350397</original-action-id><posting-date>2013-11-28T03:30:50-0800</posting-date><website-id>7306242</website-id><action-tracker-id>362777</action-tracker-id><action-tracker-name>Apollostore.cz_itemsale</action-tracker-name><cid>4039712</cid><advertiser-name>Apollostore.cz</advertiser-name><commission-amount>0.83</commission-amount><order-discount>0.00</order-discount><sid>0</sid><sale-amount>16.65</sale-amount></commission>'
				. '<commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>11333944</aid><commission-id>1667664084</commission-id><country></country><event-date>2013-12-05T05:29:10-0800</event-date><locking-date>2014-02-03</locking-date><order-id>1300012800</order-id><original>true</original><original-action-id>1411442155</original-action-id><posting-date>2013-12-05T06:30:17-0800</posting-date><website-id>7306242</website-id><action-tracker-id>359209</action-tracker-id><action-tracker-name>Svetbot_itemsale</action-tracker-name><cid>3936389</cid><advertiser-name>Svetbot.cz</advertiser-name><commission-amount>4.67</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>58.33</sale-amount></commission>'
				. '<commission><action-status>closed</action-status><action-type>lead</action-type><aid>11086275</aid><commission-id>1667683578</commission-id><country></country><event-date>2013-12-05T06:10:41-0800</event-date><locking-date>2014-01-10</locking-date><order-id>169841</order-id><original>true</original><original-action-id>1411459342</original-action-id><posting-date>2013-12-05T07:01:50-0800</posting-date><website-id>7306242</website-id><action-tracker-id>352823</action-tracker-id><action-tracker-name>Hotely_cz_2</action-tracker-name><cid>3662813</cid><advertiser-name>Hotely.cz</advertiser-name><commission-amount>0.00</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>0.00</sale-amount></commission>'
//				. '<commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>10903718</aid><commission-id>1670675616</commission-id><country></country><event-date>2013-10-31T06:45:55-0700</event-date><locking-date>2013-12-30</locking-date><order-id>701346934</order-id><original>false</original><original-action-id>1393851130</original-action-id><posting-date>2013-12-10T06:55:36-0800</posting-date><website-id>7306242</website-id><action-tracker-id>344096</action-tracker-id><action-tracker-name>Kase_item Sale</action-tracker-name><cid>3342166</cid><advertiser-name>Kasa - CZ</advertiser-name><commission-amount>-1.36</commission-amount><order-discount>0.00</order-discount><sid>4</sid><sale-amount>-45.41</sale-amount></commission>'
//				. '<commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>10903718</aid><commission-id>1680647306</commission-id><country></country><event-date>2013-11-20T06:47:33-0800</event-date><locking-date>2014-01-19</locking-date><order-id>701370959</order-id><original>false</original><original-action-id>1402548932</original-action-id><posting-date>2014-01-03T01:56:04-0800</posting-date><website-id>7306242</website-id><action-tracker-id>344096</action-tracker-id><action-tracker-name>Kase_item Sale</action-tracker-name><cid>3342166</cid><advertiser-name>Kasa - CZ</advertiser-name><commission-amount>-2.50</commission-amount><order-discount>0.00</order-discount><sid></sid><sale-amount>-95.44</sale-amount></commission>'
//				. '<commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>11477764</aid><commission-id>1681776844</commission-id><country></country><event-date>2013-11-27T14:27:18-0800</event-date><locking-date>2014-01-10</locking-date><order-id>2013551810</order-id><original>false</original><original-action-id>1406089643</original-action-id><posting-date>2014-01-06T00:19:49-0800</posting-date><website-id>7306242</website-id><action-tracker-id>363495</action-tracker-id><action-tracker-name>SportObchod.cz_itembased</action-tracker-name><cid>4065356</cid><advertiser-name>SportObchod.cz</advertiser-name><commission-amount>-17.60</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>-352.04</sale-amount></commission>'
				. '<commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>11623583</aid><commission-id>1689048433</commission-id><country></country><event-date>2014-01-26T03:12:43-0800</event-date><locking-date>2014-02-10</locking-date><order-id>1114602049</order-id><original>true</original><original-action-id>1431702631</original-action-id><posting-date>2014-01-26T04:00:55-0800</posting-date><website-id>7306242</website-id><action-tracker-id>364303</action-tracker-id><action-tracker-name>Parfemy-Elnino.cz_itemsale</action-tracker-name><cid>4098645</cid><advertiser-name>Parfemy-Elnino.cz</advertiser-name><commission-amount>1.93</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>38.66</sale-amount></commission>'
				. '<commission><action-status>new</action-status><action-type>advanced sale</action-type><aid>11632185</aid><commission-id>1689049286</commission-id><country></country><event-date>2014-01-26T03:25:40-0800</event-date><locking-date>2014-03-13</locking-date><order-id>19639424</order-id><original>true</original><original-action-id>1431703341</original-action-id><posting-date>2014-01-26T04:30:05-0800</posting-date><website-id>7306242</website-id><action-tracker-id>337058</action-tracker-id><action-tracker-name>ASTRATEX_item_Sale</action-tracker-name><cid>2983194</cid><advertiser-name>ASTRATEX - CZ</advertiser-name><commission-amount>7.48</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>62.36</sale-amount></commission>'
				. '<commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>11333944</aid><commission-id>1690659981</commission-id><country></country><event-date>2013-12-05T05:29:10-0800</event-date><locking-date>2014-02-03</locking-date><order-id>1300012800</order-id><original>false</original><original-action-id>1411442155</original-action-id><posting-date>2014-01-30T08:46:24-0800</posting-date><website-id>7306242</website-id><action-tracker-id>359209</action-tracker-id><action-tracker-name>Svetbot_itemsale</action-tracker-name><cid>3936389</cid><advertiser-name>Svetbot.cz</advertiser-name><commission-amount>-4.67</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>-58.33</sale-amount></commission>'
				. '</commissions></cj-api>';
		// 4 - 3 
		// <cj-api><commissions total-matched="2"><commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>10903718</aid><commission-id>1649308349</commission-id><country></country><event-date>2013-10-31T06:45:55-0700</event-date><locking-date>2013-12-30</locking-date><order-id>701346934</order-id><original>true</original><original-action-id>1393851130</original-action-id><posting-date>2013-10-31T07:31:29-0700</posting-date><website-id>7306242</website-id><action-tracker-id>344096</action-tracker-id><action-tracker-name>Kase_item Sale</action-tracker-name><cid>3342166</cid><advertiser-name>Kasa - CZ</advertiser-name><commission-amount>1.36</commission-amount><order-discount>0.00</order-discount><sid>4</sid><sale-amount>45.41</sale-amount></commission><commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>10903718</aid><commission-id>1658490504</commission-id><country></country><event-date>2013-11-20T06:47:33-0800</event-date><locking-date>2014-01-19</locking-date><order-id>701370959</order-id><original>true</original><original-action-id>1402548932</original-action-id><posting-date>2013-11-20T07:32:37-0800</posting-date><website-id>7306242</website-id><action-tracker-id>344096</action-tracker-id><action-tracker-name>Kase_item Sale</action-tracker-name><cid>3342166</cid><advertiser-name>Kasa - CZ</advertiser-name><commission-amount>2.50</commission-amount><order-discount>0.00</order-discount><sid></sid><sale-amount>95.44</sale-amount></commission></commissions></cj-api>
		// 3-2
		// <cj-api><commissions total-matched="5"><commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>11477764</aid><commission-id>1662145851</commission-id><country></country><event-date>2013-11-27T14:27:18-0800</event-date><locking-date>2014-01-10</locking-date><order-id>2013551810</order-id><original>true</original><original-action-id>1406089643</original-action-id><posting-date>2013-11-27T15:30:20-0800</posting-date><website-id>7306242</website-id><action-tracker-id>363495</action-tracker-id><action-tracker-name>SportObchod.cz_itembased</action-tracker-name><cid>4065356</cid><advertiser-name>SportObchod.cz</advertiser-name><commission-amount>17.60</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>352.04</sale-amount></commission><commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>11584310</aid><commission-id>1662415816</commission-id><country></country><event-date>2013-11-28T02:48:46-0800</event-date><locking-date>2013-12-10</locking-date><order-id>1016996</order-id><original>true</original><original-action-id>1406350397</original-action-id><posting-date>2013-11-28T03:30:50-0800</posting-date><website-id>7306242</website-id><action-tracker-id>362777</action-tracker-id><action-tracker-name>Apollostore.cz_itemsale</action-tracker-name><cid>4039712</cid><advertiser-name>Apollostore.cz</advertiser-name><commission-amount>0.83</commission-amount><order-discount>0.00</order-discount><sid>0</sid><sale-amount>16.65</sale-amount></commission><commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>11333944</aid><commission-id>1667664084</commission-id><country></country><event-date>2013-12-05T05:29:10-0800</event-date><locking-date>2014-02-03</locking-date><order-id>1300012800</order-id><original>true</original><original-action-id>1411442155</original-action-id><posting-date>2013-12-05T06:30:17-0800</posting-date><website-id>7306242</website-id><action-tracker-id>359209</action-tracker-id><action-tracker-name>Svetbot_itemsale</action-tracker-name><cid>3936389</cid><advertiser-name>Svetbot.cz</advertiser-name><commission-amount>4.67</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>58.33</sale-amount></commission><commission><action-status>closed</action-status><action-type>lead</action-type><aid>11086275</aid><commission-id>1667683578</commission-id><country></country><event-date>2013-12-05T06:10:41-0800</event-date><locking-date>2014-01-10</locking-date><order-id>169841</order-id><original>true</original><original-action-id>1411459342</original-action-id><posting-date>2013-12-05T07:01:50-0800</posting-date><website-id>7306242</website-id><action-tracker-id>352823</action-tracker-id><action-tracker-name>Hotely_cz_2</action-tracker-name><cid>3662813</cid><advertiser-name>Hotely.cz</advertiser-name><commission-amount>0.00</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>0.00</sale-amount></commission><commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>10903718</aid><commission-id>1670675616</commission-id><country></country><event-date>2013-10-31T06:45:55-0700</event-date><locking-date>2013-12-30</locking-date><order-id>701346934</order-id><original>false</original><original-action-id>1393851130</original-action-id><posting-date>2013-12-10T06:55:36-0800</posting-date><website-id>7306242</website-id><action-tracker-id>344096</action-tracker-id><action-tracker-name>Kase_item Sale</action-tracker-name><cid>3342166</cid><advertiser-name>Kasa - CZ</advertiser-name><commission-amount>-1.36</commission-amount><order-discount>0.00</order-discount><sid>4</sid><sale-amount>-45.41</sale-amount></commission></commissions></cj-api>
		// 2-1
		// <cj-api><commissions total-matched="2"><commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>10903718</aid><commission-id>1680647306</commission-id><country></country><event-date>2013-11-20T06:47:33-0800</event-date><locking-date>2014-01-19</locking-date><order-id>701370959</order-id><original>false</original><original-action-id>1402548932</original-action-id><posting-date>2014-01-03T01:56:04-0800</posting-date><website-id>7306242</website-id><action-tracker-id>344096</action-tracker-id><action-tracker-name>Kase_item Sale</action-tracker-name><cid>3342166</cid><advertiser-name>Kasa - CZ</advertiser-name><commission-amount>-2.50</commission-amount><order-discount>0.00</order-discount><sid></sid><sale-amount>-95.44</sale-amount></commission><commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>11477764</aid><commission-id>1681776844</commission-id><country></country><event-date>2013-11-27T14:27:18-0800</event-date><locking-date>2014-01-10</locking-date><order-id>2013551810</order-id><original>false</original><original-action-id>1406089643</original-action-id><posting-date>2014-01-06T00:19:49-0800</posting-date><website-id>7306242</website-id><action-tracker-id>363495</action-tracker-id><action-tracker-name>SportObchod.cz_itembased</action-tracker-name><cid>4065356</cid><advertiser-name>SportObchod.cz</advertiser-name><commission-amount>-17.60</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>-352.04</sale-amount></commission></commissions></cj-api>
		// 
		
		return '<cj-api><commissions total-matched="3"><commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>11623583</aid><commission-id>1689048433</commission-id><country></country><event-date>2014-01-26T03:12:43-0800</event-date><locking-date>2014-02-10</locking-date><order-id>1114602049</order-id><original>true</original><original-action-id>1431702631</original-action-id><posting-date>2014-01-26T04:00:55-0800</posting-date><website-id>7306242</website-id><action-tracker-id>364303</action-tracker-id><action-tracker-name>Parfemy-Elnino.cz_itemsale</action-tracker-name><cid>4098645</cid><advertiser-name>Parfemy-Elnino.cz</advertiser-name><commission-amount>1.93</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>38.66</sale-amount></commission><commission><action-status>new</action-status><action-type>advanced sale</action-type><aid>11632185</aid><commission-id>1689049286</commission-id><country></country><event-date>2014-01-26T03:25:40-0800</event-date><locking-date>2014-03-13</locking-date><order-id>19639424</order-id><original>true</original><original-action-id>1431703341</original-action-id><posting-date>2014-01-26T04:30:05-0800</posting-date><website-id>7306242</website-id><action-tracker-id>337058</action-tracker-id><action-tracker-name>ASTRATEX_item_Sale</action-tracker-name><cid>2983194</cid><advertiser-name>ASTRATEX - CZ</advertiser-name><commission-amount>7.48</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>62.36</sale-amount></commission><commission><action-status>closed</action-status><action-type>advanced sale</action-type><aid>11333944</aid><commission-id>1690659981</commission-id><country></country><event-date>2013-12-05T05:29:10-0800</event-date><locking-date>2014-02-03</locking-date><order-id>1300012800</order-id><original>false</original><original-action-id>1411442155</original-action-id><posting-date>2014-01-30T08:46:24-0800</posting-date><website-id>7306242</website-id><action-tracker-id>359209</action-tracker-id><action-tracker-name>Svetbot_itemsale</action-tracker-name><cid>3936389</cid><advertiser-name>Svetbot.cz</advertiser-name><commission-amount>-4.67</commission-amount><order-discount>0.00</order-discount><sid>1</sid><sale-amount>-58.33</sale-amount></commission></commissions></cj-api>';
	}
	
	public function completeStoreFromCjWebService( $debug = 0 )
	{
		for( $i = 5; $i!=0; $i-- )
		{
			Debugger::log("<br /><br />");
			$this->storeFromCjWebService($debug, 'P'.($i+1).'M', 'P'.$i.'M' );
		}
	}
	
	private function checkoutForCjErrors( $commision )
	{
		if(in_array('ERROR-MESSAGE', array_keys($commision)))
		{
			foreach( $commision as $commisionKey => $commisionValue )
			{
				throw new \LogicException("CJ error: ".$commisionValue );
			}
		}
	}


	/** CRON loading */
	public function storeFromCjWebService( $debug = 0, $intervalTextStart = 'P1M', $intervalTextTo = 'P0M' )
	{
		// CJ limitation - only 1 to 31 days period is allowed !!!
		// If you want todays commisions, set to to tomorow
		$from = new \DateTime();
		$from->sub(new \DateInterval( $intervalTextStart ));
		$from->add( new \DateInterval('P1D') );
		$to = new \DateTime();
		$to->sub(new \DateInterval( $intervalTextTo ));
		$to->add( new \DateInterval('P1D') );
		
		Debugger::log('období '.$from->format('j.n.Y').' do '.$to->format('j.n.Y')); // textová zpráva
		
		if( !$debug )
		{
			$file = $this->cjRepository->downloadNewFromCj( $from, $to );
		} else {
			$file = $this->getDebugCjString();
		}
		$commisions = $this->parseXml( $file );
		
		foreach( $commisions as $commision )
		{
			$this->checkoutForCjErrors( $commision );
			$this->storeLoadedCommision( $commision );
		}
		
		$this->em->flush();
		
		\FrontModule\MyDebugger::log( $commisions );
		return 1;
	}
		
	function getCnbEuro()
	{
		$e = file_get_contents('http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt');
		preg_match('#EMU\|euro\|1\|EUR\|([\d,]+)#', $e, $matches );
		return $matches[1];
	}
	
	
	protected function checkCjCommisionInfo( $commision )
	{
		$needed = array('AID', 'SALE-AMOUNT', 'COMMISSION-AMOUNT', 'EVENT-DATE', 'COMMISSION-ID', 'ADVERTISER-NAME');
		foreach( $needed as $key )
		{
			if( empty( $commision[$key] )) {
				#throw new \LogicException("CJ commision has wrong $key"); 
			}
		}
		if( empty( $commision['SID'] )) {
	//			throw new \LogicException("CJ commision has refferer info"); 
		}
		return 1;
	}
	
	protected function createCjEntity( $commision )
	{
		$cjRecord = new Entity\PartnerProviderFee();
		$cjRecord->setCjFee( $commision, $this->getCnbEuro() );
		return $cjRecord;
	}
	
	protected function fillCjInfo( Entity\PartnerProviderFee $providerFee, array $commision )
	{
		$e = $providerFee->getProviderInfo('EURO');
		if( empty($e) )
		{
			$info = new Entity\PartnerProviderInfo();
			$info->setInfo('EURO', (string)$this->getCnbEuro() );
			$this->em->persist( $info );
			$info->setProviderFee( $providerFee );
		}
		
		foreach( $commision as $k => $v )
		{
			$info = new Entity\PartnerProviderInfo();
			$info->setInfo($k, (string)$v);
			$this->em->persist( $info );
			$info->setProviderFee( $providerFee );
		}
		Debugger::log("Adding commision from ");
		Debugger::log($commision);
		return $commision;
		
	}
	
	
	protected function storeLoadedCommision( array $commision )
	{
		$this->checkCjCommisionInfo( $commision );

		$exist = \DAO\PartnerProviderInfo::get()->findAll()->where("key='ORDER-ID' AND value=%s", $commision['ORDER-ID'])->fetch();
//		print_r( $exist );exit;
		if(empty( $exist->ppi_id ))
		{
			$providerFee = $this->createCjEntity( $commision );
			$this->em->persist( $providerFee );

			$this->fillCjInfo( $providerFee, $commision );
			
			$partnerOrder = $providerFee->getPartnerOrder();
			if( $partnerOrder === null )
			{
				$this->partnerOrderFacade->createCj( $providerFee );
			}
			
			
		}  else {
			/* @var $providerFeeOld \MoneyPoint\Entity\PartnerProviderFee */
			$providerFeeOld = $this->em->find('MoneyPoint\Entity\PartnerProviderFee', $exist->ppi_id );
			
			// dont add commision whitch is already exists
			if( $providerFeeOld->getProviderInfo("ORIGINAL-ACTION-ID") == $commision['ORIGINAL-ACTION-ID'] )
			{
				return;
			}
			
			$providerFee = $this->createCjEntity( $commision );
			$this->em->persist( $providerFee );
			$this->fillCjInfo( $providerFee, $commision );
			$providerFee->setPartnerOrder( $providerFeeOld->getPartnerOrder() );
		}
		return 1;
	}
			
	public function getItemId()
	{
		$file = $this->cjRepository->downloadCjItemDetail( 1431702631 );
		echo $file;
		exit;
	}
	
	public function getCategories()
	{
		$file = $this->cjRepository->downloadCategories();
		echo $file;
		exit;
	}

	
	public function getCommisions( $from, $to )
	{
		$file = $this->cjRepository->downloadNewFromCj( $from, $to  );
		return $this->parseXml( $file );
	}
	
	/** @deprecated */
	public function getCommision( $id )
	{
		$file = $this->cjRepository->downloadCommision( $id  );
//		<cj-api>
//			<item-details original-action-id="1393851130">
//				<item>
//					<sku>120</sku>
//					<quantity>1</quantity>
//					<posting-date>2013-10-31T07:31:29-0700</posting-date>
//					<commission-id>1649308349</commission-id>
//					<sale-amount>45.406</sale-amount>
//					<discount>0.</discount>
//					<publisher-commission>1.362</publisher-commission>
//				</item>
//			</item-details>
//		</cj-api>
		return $this->parseXml( $file );
	}
	
	
	
	protected function beautifuller( $input )
	{
		$output=array();
		foreach( $input as $iCom => $commision )
		foreach( $commision as $k => $v )
		{
			if( in_array($k, array('EVENT-DATE','LOCKING-DATE','POSTING-DATE' )))
			{
				$date = new \DateTime($v);
				$date->setTimezone(new \DateTimeZone('Europe/Prague'));
				$input[ $iCom ][ $k ] = $date->format('Y-m-d H:i:s');
			}
		}
		return $input;
	}
			
	protected function parseXml( $simple )
	{
		/*
		$simple  = '<cj-api>'
				. '<commissions total-matched="1"><commission><action-status>new</action-status><action-type>advanced sale</action-type><aid>10903718</aid><commission-id>1649308349</commission-id><country></country><event-date>2013-10-31T06:45:55-0700</event-date><locking-date>2013-12-30</locking-date><order-id>701346934</order-id><original>true</original><original-action-id>1393851130</original-action-id><posting-date>2013-10-31T07:31:29-0700</posting-date><website-id>7306242</website-id><action-tracker-id>344096</action-tracker-id><action-tracker-name>Kase_item Sale</action-tracker-name><cid>3342166</cid><advertiser-name>Kasa - CZ</advertiser-name><commission-amount>1.36</commission-amount><order-discount>0.00</order-discount><sid>4</sid><sale-amount>45.41</sale-amount></commission></commissions>'
				. '<commissions total-matched="1"><commission><action-status>new</action-status><action-type>advanced sale</action-type><aid>10903718</aid><commission-id>1649308349</commission-id><country></country><event-date>2013-10-31T06:45:55-0700</event-date><locking-date>2013-12-30</locking-date><order-id>701346934</order-id><original>true</original><original-action-id>1393851130</original-action-id><posting-date>2013-10-31T07:31:29-0700</posting-date><website-id>7306242</website-id><action-tracker-id>344096</action-tracker-id><action-tracker-name>Kase_item Sale</action-tracker-name><cid>3342166</cid><advertiser-name>Kasa - CZ</advertiser-name><commission-amount>1.36</commission-amount><order-discount>0.00</order-discount><sid>4</sid><sale-amount>45.41</sale-amount></commission></commissions>'
				. '<commissions total-matched="1"><commission><action-status>new</action-status><action-type>advanced sale</action-type><aid>10903718</aid><commission-id>1649308349</commission-id><country></country><event-date>2013-10-31T06:45:55-0700</event-date><locking-date>2013-12-30</locking-date><order-id>701346934</order-id><original>true</original><original-action-id>1393851130</original-action-id><posting-date>2013-10-31T07:31:29-0700</posting-date><website-id>7306242</website-id><action-tracker-id>344096</action-tracker-id><action-tracker-name>Kase_item Sale</action-tracker-name><cid>3342166</cid><advertiser-name>Kasa - CZ</advertiser-name><commission-amount>1.36</commission-amount><order-discount>0.00</order-discount><sid>4</sid><sale-amount>45.41</sale-amount></commission></commissions>'
				. '</cj-api>';
		 */
		/*
		 * <cj-api><error-message>Invalid date interval: only 1 to 31 days period is allowed.</error-message></cj-api>
		 */
		
		$p = xml_parser_create();
		xml_parse_into_struct($p, $simple, $vals, $index);
		xml_parser_free($p);
		#print_r( $vals );

		$read = false;
		$iProperty = 0;
		$commision = array();
		$i=0;
		foreach( $vals as $key => $tag )
		{
			if( $read )
			{
				$commision[ $i ][ $tag['tag'] ] = @$tag['value'];
			}
			if( $tag['tag'] == 'COMMISSION' && $tag['type'] == 'open' )
			{
				$read = true;
			}
			if( $tag['tag'] == 'COMMISSION' && $tag['type'] == 'close' )
			{
				$read = false;
				$i++;
			}
		
			if( $tag['tag'] == 'ERROR-MESSAGE'  )
			{
				$commision[ $i ][ $tag['tag'] ] = @$tag['value'];
				break;
			}
		}
		return $this->beautifuller( $commision );
	}
    
}
