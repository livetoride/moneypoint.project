<?php

namespace MoneyPoint;

class CommisionProductFacade
{
	private $em;
	private $commisions = array(
			2 => 'credits'
		);
	

	public function getDatasource()
	{
		$product = \DAO\MpCreditsDiscountGroup::get()->findAll()->fetchAssoc('id');
		$r = array();
		foreach( $product as $k => $values )
		{
			$r[ $k ] = array('id'=>$k);
			$r[ $k ] += (array)$values;
			
		}
		return $r;
	}
	
}
