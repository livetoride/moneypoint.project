<?php

namespace MoneyPoint;

class RewardCounting
{
	/**
	 * Reálná odměna
	 * @var type 
	 */
	public $reward;
	
	/**
	 * Redukovaná odměna
	 * @var type 
	 */
	public $rewardReduced;
	
	/**
	 * Plná odměna
	 * @var type 
	 */
	public $rewardFull;
	
	/**
	 * Čerpané kredity
	 * @var type 
	 */
	public $usedCredits;
	
	/**
	 * Typ výpočtu
	 * @var type 
	 */
	public $usedVariant;
	
	/**
	 * Nárok na odměnu - základní podle tabulky
	 * @var type 
	 */
	public $claimReward;

	public function fromTransaction( $transaction )
	{
		$ca = $transaction->getCreditsAccount();
		if( $ca instanceof \MoneyPoint\Entity\CreditAccount )
		{
			$creditsState = $ca->getLeft() - $ca->getChange();
			$commision = $transaction->getDifferentialCommision();
			$claimPoints = $commision->getTotalReward();
			return $this->extraRewardsWithReduction( $creditsState, $claimPoints );
		}
	}
	
	public function extraRewards( $creditsState, $claimPoints )
	{
		throw new \Exception("dont use this method");
		if( $creditsState >= $claimPoints)
		{
			// nárok na plnou extra odměnu
			$this->reward = $claimPoints;
		} elseif( $creditsState == 0 )
		{
			// nemá nárok
			$this->reward = 0;
		} else {
			// částečná extra odměna
			$this->reward = $creditsState;
		}
		$this->usedCredits = $this->reward;
		return $this;
	}

	public function extraRewardsWithReduction( $creditsState, $claimPoints )
	{
		if( $creditsState instanceof \AntoninRykalsky\IAccountable )
		{
			$creditsState = $creditsState->getLeft();
		}
		if( empty( $creditsState ))
		{
			$creditsState = 0;
		}
			
		$this->claimReward = $claimPoints;
		$creditsState = (int) $creditsState;
		$claimPoints = (int) $claimPoints;
		
		$reductionTo = 0.4;
		$creditsConsumtion = 1/5;

		if( $creditsState*5 >= $claimPoints)
		{
			// nárok na plnou odměnu
			$this->rewardReduced = 0;
			$this->rewardFull = $claimPoints;
			$this->reward = $claimPoints;
			
			$this->usedCredits = -$claimPoints*$creditsConsumtion;
			$this->usedVariant = "full-reward";
		} elseif( $creditsState == 0 )
		{
			// nemá nárok
			$this->rewardReduced = $claimPoints * $reductionTo;
			$this->rewardFull = 0;
			$this->reward = $this->rewardFull + $this->rewardReduced;
			
			$this->usedCredits = 0;
			$this->usedVariant = "reduced-reward";
		} else {
			// částečná extra odměna
			
			$this->rewardReduced = ($claimPoints - $creditsState*5) * $reductionTo;
			$this->rewardFull = $creditsState*5;
			$this->reward = $this->rewardFull + $this->rewardReduced;
			
			$this->usedCredits = -$creditsState;
			$this->usedVariant = "partially-reduced-reward";
		}
		
		$this->rewardReduced = floor( $this->rewardReduced );
		$this->rewardFull = floor( $this->rewardFull );
		$this->reward = floor( $this->reward );
		$this->usedCredits = ceil( $this->usedCredits );
		
		

		return $this;
	}
}


?>