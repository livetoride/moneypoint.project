<?php

namespace MoneyPoint;

class PremiumStatusService
{
	private $em;

	public function __construct( 
		\AntoninRykalsky\EntityManager $em
	) {
		$this->em = $em->getEm();
	}
	
	public function stornoPremiumOrder( Entity\CreditOrder $cancelledOrder, array $nextCreditsOrders )
	{
		/* @var $member Entity\Member */
		$member = $cancelledOrder->getUser();
		
		// storno first
		if( $member->getTsBusiness() == $cancelledOrder->getTsPaid() )
		{
			$member->setHasBussiness( 0 );
		}
		
		// and checkout next premium order
		foreach( $nextCreditsOrders as $nextOrder )
		{
			/* @var $nextOrder Entity\CreditOrder */
			/* @var $discount Entity\CreditDiscount */
			$discount = $nextOrder->getRelatedDiscount();
			if( $discount->isBusinessActivation() )
			{
				$member->setHasBussiness( 1, $nextOrder->getTsPaid() );
			}
		}
	}

	
}
