<?php

namespace MoneyPoint;

class PosibleRewardCollection
{
	private $data = array();
    private $structure;
    private $em;
	public function __construct( $em, $structure = null, $date = null ) {
		$this->em = $em;
		
		if( !empty( $structure ))
		{
			$this->structure = $structure;
			$this->date = $date;
			$q = $this->em->createQuery('SELECT a FROM MoneyPoint\Entity\PosibleRewardEntity a INDEX BY a.idu WHERE a.tsResolution = :date AND a.idu IN(:structure)');
			$q->setParameter('structure', $structure );
			$q->setParameter('date', $date );
			$this->data = $q->getResult();
		}
		
		return $this;
	}
	
	public function get( $idu = null )
	{
		if( empty( $idu ))
			return $this->data;
		
		return $this->data[ $idu ];
	}
	
	/**
	 * Přídá potencionální odměny do kolekce a pod kontrolu entity manageru
	 * @param type $iduFor id získatele
	 * @param type $claim základ odměny
	 * @param type $firstCreditsBonus bpo bonus
	 */
	public function add( $iduFor, $claim, $firstCreditsBonus = 0 )
	{
		if( empty( $this->data[$iduFor] ))
		{
			$this->data[ $iduFor ] = new Entity\PosibleRewardEntity( $this->structure );
			$this->data[ $iduFor ]->setIdu( $iduFor );
			$this->data[ $iduFor ]->setTsresolution( $this->date );
			$this->em->persist( $this->data[ $iduFor ] );
		}
		$this->data[ $iduFor ]->addReward($claim, $firstCreditsBonus);
	}
	
	public function getData()
	{
		return $this->data;
	}
	
	public function exist( $type )
	{
		return isset( $this->data[ $type ] );
	}
}
