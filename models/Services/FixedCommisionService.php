<?php

namespace MoneyPoint;
use \DAO\MpCreditsDiscount as CreditsDiscount;

class FixedCommisionService
{
	/**
	 * 
	 * @param type $creditsOrder
	 * @param type $structure by IDU
	 * @return type
	 */
	public function countRewards( $creditsOrder, $structure )
	
		{
		/* @var $commisionRepository AntoninRykalsky\CommisionRepository */
		$commisionRepository = $this->em->getRepository('AntoninRykalsky\Entity\Commision');
		$commisionDetails = $commisionRepository->getCommisionDetails( \AntoninRykalsky\Entity\Commision::FIXED );
		
		$rewards = array();
		$fullPrice = $creditsOrder->getPriceSum();
		foreach ($commisionDetails as $com) {
			$distributions = $com->getDistributions();
			$unit = $com->getUnit();
		}
		switch ($unit) {
			case Entity\LevelDistributionGroup::UNIT_POINTS:
				$rewards += $this->pointsCount( $fullPrice, $distributions, $structure );
				break;
			
			case Entity\LevelDistributionGroup::UNIT_PERCENT:
				$rewards += $this->percentCount( $fullPrice, $distributions, $structure );
				break;
			default:
				break;
		}
		
		foreach( $rewards as $reward )
		{
			$reward->setCreditOrder( $creditsOrder );
		}
		
		#$this->appendPremium()
		#this->appendVip()

		return $rewards;
	}
	
	
	private function pointsCount( $fullPrice, $distributions, $structure )
	{
		$rewards = array();
		$lowestPrice = CreditsDiscount::get()->findAll()->orderBy('price')->fetch()->price;
		foreach( $distributions as $distributionRule )
		{
			$user = @$structure[ $distributionRule->getLevel() ];
			if( $user !== null ) {
				
				$rewardEntity = new Entity\CreditReward();
				$rewardEntity->setCountedReward( 
					$distributionRule->getLevel(), // line
					($fullPrice/$lowestPrice) * $distributionRule->getPoints(), // claim 
					$user // idu
				);
				$rewards[] = $rewardEntity;
			}
		}

		return $rewards; 
	}
	private function percentCount( $fullPrice, $distributions, $structure )
	{
		$rewards = array();
		foreach( $distributions as $distributionRule )
		{
			$user = @$structure[ $distributionRule->getLevel() ];
			if( $user !== null ) {
				$rewardEntity = new Entity\CreditReward();
				$rewardEntity->setCountedReward( 
					$distributionRule->getLevel(), // line
					$fullPrice / 100 * $distributionRule->getPoints(), // claim 
					$user // idu
				);
				$rewards[] = $rewardEntity;
			}
		}
		return $rewards;
	}


	private $em;
	
	public function __construct(
			\AntoninRykalsky\EntityManager $em
		) {
		$this->em = $em->getEm();
	}

	public function getClaimedPoints($priceSum, $predciLevel, $isPremium)
	{
		return \AntoninRykalsky\ProduktyLevelDistribution::get()->getKoeficient($priceSum, $predciLevel, $isPremium);
	}
	
	public function vipstuff()
	{
		throw new \Exception;
		$isVip = $memberBuying->user()->vip_coef;
		if( !empty( $isVip ))
		{
			$claimRespectingPremium = (int)($claimRespectingPremium / 100 * $isVip);
			$totalClaim = $claimRespectingPremium;
		}
	}
	
	public function bpostuff()
	{
		throw new \Exception;
		// BPO
		$claimFirstCreditsBonus=0;
		if( $predciLevel == 1 )
		{
			/* @var $firstBuyBonusEntity Entity\FirstBuyBonus */
			$firstBuyBonusEntity = $creditsOrder->getFirstBuyBonus();
			$claimFirstCreditsBonus = $firstBuyBonusEntity->getBonus();
			$totalClaim+=$claimFirstCreditsBonus;
		}
	}
}
