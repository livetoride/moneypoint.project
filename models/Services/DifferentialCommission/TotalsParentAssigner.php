<?php

namespace MoneyPoint\DirefentialCommision;
use MoneyPoint\Entity;

class ParentTotalsAssigner extends Nette\Object
{
	/**
	 * 
	 * @param type $values vypočtené sumy přímých a rozdílových odměn
	 * @return \MoneyPoint\Entity\PeriodNetwork 
	 * @throws \LogicException
	 */
	
	/**
	 * Nastaví entitě správné 
	 * @param \MoneyPoint\Entity\PeriodNetwork $periodNetwork
	 * @param int $direct
	 * @param int $network
	 * @return \MoneyPoint\Entity\PeriodNetwork
	 */
	public function assignTotalCommisionPerEntity( Entity\PeriodNetwork $periodNetwork, $direct=0, $network=0 )
	{
		$periodNetwork->setMyReward( $direct, $network );
		return $periodNetwork;
	}
}
