<?php

namespace MoneyPoint;

/**
 * Třída řešící rozdílové provize
 * 
 * $this->direfentialCommisionService->rmDifferentialPoints( $creditsOrder );
 * 
 * #changeRankPresenter
 * actionChangeRank
 * 
 */
class DiferentialCommisionFacade
{
	private $em;
	
	/** @var DirefentialCommisionService */
	private $direfentialCommisionService;


//	/** @var \MoneyPoint\ApplicationSetup */
//	private $applicationSetup;
	

	/** @var PeriodRepository */
	protected $periodRepository;
	
	/** @var PeriodNetworkRepository */
	protected $periodNetworkRepository;
	
	public function __construct( 
		\MoneyPoint\Container $c,
		DirefentialCommisionService $direfentialCommisionService
	) {
		$this->em = $c->getEm();
		$this->direfentialCommisionService = $direfentialCommisionService;
		
		$this->periodRepository = $this->em->getRepository('MoneyPoint\Entity\Period');
		$this->periodNetworkRepository = $this->em->getRepository("MoneyPoint\Entity\PeriodNetwork");
	}

	
	
	public function recountPeriodNetworkPoints() {
		
		$this->em->createQuery("UPDATE MoneyPoint\Entity\PeriodNetwork pn SET "
				. "pn.parentNetworkPeriod = 0, "
				. "pn.parentDirectPeriod = 0, "
				. "pn.parentNetworkTotal = 0, "
				. "pn.parentDirectTotal = 0, "
				. "pn.parentNetworkReward = 0, "
				. "pn.parentTotalReward = 0, "
				. "pn.networkReward = 0, "
				. "pn.totalReward = 0, "
				. "pn.isGroupedReward = 0 "
			)->getResult();
		

		/* @var $order \MoneyPoint\Entity\CreditOrder */
		$q = $this->em->createQuery(
				"SELECT o, u "
				. "FROM MoneyPoint\Entity\CreditOrder o "
				. "JOIN o.user u "
				. "WHERE o.idStatus IN (:statuses) "
				. "ORDER BY o.idOrder");
		$q->setParameter('statuses', array( Entity\CreditOrder::STATE_PAID, Entity\CreditOrder::STATE_REWARDED ) );
		$orders = $q->getResult();
		

		foreach( $orders as $order )
		{
			$period = $this->periodRepository->getActualPeriod( $order->getTsOrder() );
			$this->direfentialCommisionService->addDifferentialPoints( $order->getUser(), $order->getPriceSum(), $period );
		}
		$this->em->flush();
	}

	public function fixPeriodNetworkRewards( $periodId )
	{
		$q = $this->em->createQuery("select pn, c1, pn2, c2 "
				. "from \MoneyPoint\Entity\PeriodNetwork pn "
				. "join pn.carrier as c1 "
				. "join pn.childrenCommision as pn2 "
				. "join pn2.carrier as c2 "
				. "WHERE pn.period = :period ORDER BY pn.idu");
		$q->setParameter('period', $this->em->getReference('MoneyPoint\Entity\Period', $periodId ) );
		$pns= $q->getResult();

		foreach ( $pns as $pn )
		{
			$pn = $this->direfentialCommisionService->countIntermediateResults( $pn );
		}

		$this->em->flush();
	}
			
	public function fixPeriodNetworkPoints( $periodId )
	{
		$q = $this->em->createQuery("select pn from \MoneyPoint\Entity\PeriodNetwork pn WHERE pn.period = :period ORDER BY pn.idu");
		$q->setParameter('period', $this->em->getReference('MoneyPoint\Entity\Period', $periodId ) );
		$pns= $q->getResult();

		foreach ( $pns as $k => $pn )
		{
			$pns[$k] = $this->recountParentTotalsRewards( $pn );
		}


		$this->em->flush();
		return count( $pns );
	}

		public function recountParentTotalsRewards(Entity\PeriodNetwork $periodNetwork)
		{
			$groupedResult = $this->periodNetworkRepository->getEntitiesForAssigningOneCommision( $periodNetwork->getPeriod(), $periodNetwork->getUser()->getIdu() );
			return $this->direfentialCommisionService->recountPointTotals( $periodNetwork, $groupedResult );
		}
	
	public function fixRanks()
	{

		$manual = array(
			array(1, 5, new \DateTime('2014-06-01')),
			array(2, 5, new \DateTime('2014-06-01'))
		);
		
		$this->em->createQuery("DELETE FROM MoneyPoint\Entity\UserCarrier o")->getResult();
		$orders = $this->em->createQuery("SELECT o, u FROM MoneyPoint\Entity\CreditOrder o JOIN o.user u ORDER BY o.idOrder")->getResult();
		
		/* @var $order \MoneyPoint\Entity\CreditOrder */
		foreach( $orders as $order )
		{
			if( in_array( $order->getStatus(), array( \MoneyPoint\Entity\CreditOrder::STATE_REWARDED, \MoneyPoint\Entity\CreditOrder::STATE_PAID)  ))
			{
				$discount = $order->getRelatedDiscount();
				$user = $order->getUser();
				try {
					$rank = $discount->getActivateRank();
				} catch( \Exception $e )
				{
					$rank = null;
				}
				if( !empty( $rank ))
				{
					$rankService = $this->em->getRepository("MoneyPoint\Entity\Carrier");
					$rankService->increaseRanks( $user->getIdu(), $rank, $order );
				}
			}
		}
		$this->em->flush();
	}
	
	public function countCommisionForUser( $idu )
	{
		$this->countCommision( DirefentialCommisionService::ACTUAL_PERIOD, $idu );
	}
	
	
	
	public function countCommision( $periodId=null, $parentIdu = null  )
	{
		$period = null;
		if( !empty( $periodId ))
			$period = $this->em->getReference('MoneyPoint\Entity\Period', $periodId );
		
		$periodNetwork = $this->direfentialCommisionService->countDiferentials( $period, $parentIdu );
		$this->em->flush();

		
//		$uow = $this->em->getUnitOfWork();
//		$inserts = $uow->getScheduledEntityInsertions();
//		\Doctrine\Common\Util\Debug::dump($inserts);



		$DONT_MAKE_TRANSACTIONS = false;
		$ASSIGN_COMMISIONS = false;
		

		$this->direfentialCommisionService->countDiferentialTotals( $period, $parentIdu, $DONT_MAKE_TRANSACTIONS, $ASSIGN_COMMISIONS, $periodNetwork );
		$this->em->flush();
	}
	
	private function createTransaction( $parentIdu, $differentialCommision )
	{
		$transaction = new Entity\Transaction();
		$transaction->setDifferentialCommision( $differentialCommision );
		$partnerOrder->setTransation($transaction);
		
		$id = $this->transactionFacade->getNextTransactionId( $transaction );
		$transaction->setMpTransactionId( $id );
		$transaction->

		$this->_em->persist( $partnerOrder );
		$this->em->persist( $transaction );
		$this->em->flush();
	}

	public function afterClearCounting()
	{
		$this->direfentialCommisionService->simulateStructurePointAddition( null, 0 );
		$this->em->flush();
		$this->direfentialCommisionService->countDiferentials( );
		$this->em->flush();
		$this->direfentialCommisionService->countDiferentialTotals( );
		$this->em->flush();
	}
	
	/**
	 * Lazy nastavení rozdílových provizí na nuly
	 * @param type $idu
	 */
	public function installDefaultAccount( $idu )
	{
//		return;
//		try {
			// přidá 0 kreditu na účet uživatele
			$this->direfentialCommisionService->checkForNewLirstLineUsers( $idu );
			$this->direfentialCommisionService->installDefaultAccount( $idu );
			$this->em->flush();
			// zjistí klientovo zařazení a kariéru
			$this->countCommision( null, $idu);
			$this->countCommisionForUser( $idu );
			$this->em->flush();

			$volumeCommision = $this->secureFindBy( $idu );
			$this->em->getRepository("MoneyPoint\Entity\Carrier")->fixActualCarrier( $volumeCommision );
			if( empty( $volumeCommision ))
				throw new \Nette\Application\BadRequestException('Záznam o provizích neexistuje');
			
//		} catch ( \Nette\Application\BadRequestException $e ) {
//			$this->em->flush();
//			$this->countCommisionForUser( $idu );
//			$this->direfentialCommisionService->installDefaultAccount( $idu );
//			$this->em->flush();
//			
//			$volumeCommision = $this->secureFindBy( $idu );
//
//			if(empty( $volumeCommision ))
//			{
//				throw new \Exception('Neznámá kariera');
//			}
//		}
		$this->em->flush();
		$this->em->refresh($volumeCommision);

	}

	public function testAccountingWithoutClear( $clear = true )
	{
		$this->testAccounting(false);
	}
	public function testAccounting( $clear = true )
	{
//		if( $clear )
//			$this->applicationSetup->clearOrders();
//		$periodLast = $this->em->find('MoneyPoint\Entity\Period', 1 );
//		$this->direfentialCommisionService->testAccounting( $periodLast );
//		$this->em->flush();
//		$this->direfentialCommisionService->countDiferentials( $periodLast );
//		$this->em->flush();
//		$this->direfentialCommisionService->countDiferentialTotals( $periodLast );
//		$this->em->flush();
//		exit;

		$this->direfentialCommisionService->simulateStructurePointAddition( null );
		$this->em->flush();
		$this->direfentialCommisionService->countDiferentials( );
		$this->em->flush();
		$this->direfentialCommisionService->countDiferentialTotals( );
		$this->em->flush();
		
		echo "OK";
	}
	
	public function getDatasource( $idu=null)
	{
		return $this->periodNetworkRepository->getDatasource( $idu );
	}

	public function getPeriod( $id )
	{
		$repository = $this->em->getRepository('MoneyPoint\Entity\Period');
		$object = $repository->find( $id );
		if( empty( $object ))
			throw new \Nette\Application\BadRequestException('Hledané omezení neexistuje');
		
		return $object;
	}
	
	public function findWithLazyCounting( $id, $period = null )
	{
//		$this->direfentialCommisionService->countDiferentials( $period, $id );
//		$this->direfentialCommisionService->countDiferentialTotals( $period, $id );
//		$this->em->flush();
		$periodNetwork = $this->secureFindBy($id, $period);
		return $periodNetwork;
	}
			
	public function secureFindBy( $id, $period = null )
	{
		if( empty( $period )) {
			$period = $this->periodNetworkRepository->getActualPeriod();
		}
		$periodNetwork = $this->periodNetworkRepository->getUserRank( $id, $period->getId() );
		
		return $periodNetwork;
	}

	public function getDifferentialCommisionDetails( $differentialCommision )
	{
		if( !isset( $differentialCommision ))
			return;
		return $this->periodNetworkRepository->findPartialItems( $differentialCommision );
	}
	
	/**
	 * After closing period
	 * @return type
	 */
	public function makeTransactions()
	{
		$period = $this->periodRepository->getActualPeriod();
		$lastPeriod = $this->periodRepository->getLastPeriod( $period );
	
		// Počítá dílčí částky - všichni
		$this->direfentialCommisionService->countDiferentials( $period );
		$this->direfentialCommisionService->countDiferentialTotals( $period, null, true );
		$this->em->flush();
	}
	
	/**
	 * Cron bude tuto funkci volat po skončení účetního období.
	 */
	public function cronMakeTransactions()
	{
		$this->makeTransactionsLastPeriod();
	}
	
	
	/**
	 * Cron bude tuto funkci volat uplynutí ochranné lhůty účetního období
	 */
	public function cronAssignCommisions()
	{
		$this->assignCommisions();
	}
	
	
	
	/**
	 * After closing period - cron
	 * @return type
	 */
	public function makeTransactionsLastPeriod()
	{
		$period = $this->periodRepository->getActualPeriod();
		$lastPeriod = $this->periodRepository->getLastPeriod( $period );
		
		#$this->direfentialCommisionService->countDiferentials( $lastPeriod );
//
//		// Počítá dílčí částky - všichni
//		$this->direfentialCommisionService->countDiferentials( $lastPeriod );
//		$this->direfentialCommisionService->countDiferentialTotals( $lastPeriod, null, false );
		
		
		$entity = 'MoneyPoint\Entity\PeriodNetwork';
		$allRows = $this->em->getRepository($entity)->findBy( 
			array(
				'isTransactionGenerated' => 0,
				'period' => $period,
			) 
		);

		$this->direfentialCommisionService->makeTransactionForLastPeriod( $allRows );

		
		$this->em->flush();
	}
	
	/**
	 * After safety deadline from ended period
	 * @return type
	 */
	public function assignCommisions()
	{
//		$this->direfentialCommisionService->countDiferentials( );
//		$this->em->flush();
		
		$period = $this->periodRepository->getActualPeriod();
		$lastPeriod = $this->periodRepository->getLastPeriod( $period );
		$entity = 'MoneyPoint\Entity\PeriodNetwork';
		$allRows = $this->em->getRepository($entity)->findBy( 
			array(
				'isTransactionGenerated' => 1,
				'period' => $period,
			) 
		);

		foreach( $allRows as $row )
		{
			$this->direfentialCommisionService->countDiferentialTotals( null, null, false, true, $row );
		}
		
		$this->em->flush();
	}
	
	public function _initialization( $idu )
	{
		$this->direfentialCommisionService->_initialization($idu);
		$this->em->flush();
	}

	
	
	
}
