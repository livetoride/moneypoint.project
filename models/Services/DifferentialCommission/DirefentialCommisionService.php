<?php

namespace MoneyPoint;
use Nette;

/**
 * Třída řešící rozdílové provize
 * 
 * #setStorno, #setStornoAfterPay
 * rmDifferentialPoints( $creditsOrder );
 * 
 * #buyCredits
 * addDifferentialPoints
 * 
 * countIntermediateResults
 */
class DirefentialCommisionService extends Nette\Object
{
	private $em;
	
	/** @var type MemberRepository */
	private $memberRepository;
	
	/** @var TransactionFacade */
	private $transactionFacade;
	
	/** @var PeriodNetworkRepository */
	private $_periodNetworkRepository;

	/** @var PeriodRepository */
	protected $_periodRepository;
	
	/** @var \MoneyPoint\TransactionRepository */
	protected $_transactionRepository;
	
	const ACTUAL_PERIOD = null;
	
	/** @var RanksRepository */
	protected $_ranksRepository;
	
	public function __construct( 
		\AntoninRykalsky\EntityManager $em,
		\MoneyPoint\TransactionFacade $transactionFacade,
		MemberRepository $memberRepository
	) {
		$this->em = $em->getEm();
		$this->transactionFacade = $transactionFacade;
		$this->memberRepository = $memberRepository;
	}
	
	// <editor-fold defaultstate="collapsed" desc=" repository getters, take long time in constructor ">

	public function getTransactionRepository() 	{
		if (empty($this->_transactionRepository)) 		{
			$this->_transactionRepository = $this->em->getRepository('MoneyPoint\Entity\Transaction');
		}
		return $this->_transactionRepository;
	}


	public function getRanksRepository() 	{
		if (empty($this->_ranksRepository)) 		{
			$this->_ranksRepository = $this->em->getRepository('MoneyPoint\Entity\Carrier');
		}
		return $this->_ranksRepository;
	}


	public function getPeriodRepository() 	{
		if (empty($this->_periodRepository)) 		{
			$this->_periodRepository = $this->em->getRepository('MoneyPoint\Entity\Period');
		}
		return $this->_periodRepository;
	}


	public function getPeriodNetworkRepository() 	{
		if (empty($this->_periodNetworkRepository)) 		{
			$this->_periodNetworkRepository = $this->em->getRepository('MoneyPoint\Entity\PeriodNetwork');
		}
		return $this->_periodNetworkRepository;
	}

// </editor-fold>
	
	public function checkForNewLirstLineUsers( $idu )
	{
		$ch = $this->memberRepository->getChildenDirect($idu);
		foreach( $ch as $iduChildren )
		{
			$this->_initialization( $iduChildren );
		}
	}
	
	public function removeSonsWitchParentHasNoActivatedDifferentials( $structure )
	{
		throw new \Exception("Nepoužívat, rozhodí to stavy po add a remove differe.points.");
		$hasNoActivation = array_keys( \DAO\Uzivatele::get()->findAll()->where('has_activated_differencials=0 AND idu IN %in', $structure)->fetchPairs('idu', 'idu') ); 
		
		
		// bere i ostatní z větve rodiče, ale nevadi. Množinový rozdíl vyřeší
		$s = \DAO\Uzivatele::get()->findAll('idu, idusponzor')->where("idusponzor IN %in", $structure )->fetchAll();
		foreach( $s as $itemToRemove )
		{
			$hasNoActivationSons[] = $itemToRemove->idu;
		}
		
		$hasNoActivationSons = $hasNoActivation;
		$structure = array_diff($structure, $hasNoActivationSons );
		return $structure;
	}
	
	/**
	 * Přidává body na bodové konto rozdílových provizí
	 * @param type $member uživatel který nakoupil
	 * @param type $points	body náležející přidat do struktury
	 * @param type $period účetní období nákupu
	 */
	public function addDifferentialPoints( $member, $points, $period = null, $structure = null, $actualStateHolder = null )
	{
		$entity = 'MoneyPoint\Entity\PeriodCreditState';
		if( empty( $structure ))
		{
			$structure = $this->memberRepository->getStructure( $member->getIdu() );
			$structure[0] = $member->getIdu();
		}
		
		
		# $structure = $this->removeSonsWitchParentHasNoActivatedDifferentials( $structure );
		
		// pokud neni specifikovano, pouzij aktuální
		if(empty( $period )) {
			$period = $this->getPeriodRepository()->getActualPeriod(); }
			
		$pointStates = $this->getPeriodNetworkRepository()->createDifferentialPointsIfNotExists( $structure, $period, $entity, $points, $actualStateHolder );
		return $pointStates;
//		
//		$this->em->flush();
//		$this->countDiferentials( $period );
	}
	
	/**
	 * Odebere body z bodového konta rozdílových provizí
	 * @param \MoneyPoint\Entity\CreditOrder $creditsOrder
	 */
	public function rmDifferentialPoints( Entity\CreditOrder $creditsOrder )
	{
		$period = $this->getPeriodRepository()->getActualPeriod($creditsOrder->getOrderTimestamp() );
		$this->addDifferentialPoints( $creditsOrder->getUser(), -$creditsOrder->getCreditsSum(), $period );
		
		$this->countDiferentials( $period, $creditsOrder->getUser()->getIdu() );
	}
	
	
	/**
	 * Počítá dílčí částky
	 * @param null|Entity\Period $actualPeriod
	 * @return type
	 */
	public function countDiferentials( $actualPeriod, $parentUserId )
	{
		// <editor-fold defaultstate="collapsed" desc="vyhledej vstupy do výpočtu">
		if (empty($actualPeriod))
		{
			$actualPeriod = $this->getPeriodRepository()->getActualPeriod();
		}

		// vyhledej entity pro výpočet
		$entity = '\MoneyPoint\Entity\PeriodNetwork';
		$q = $this->em->createQueryBuilder()
				->select('pn, childPn')
				->from($entity, 'pn')
				->leftJoin('pn.childrenCommision', 'childPn')
				->where('pn.period = :period');

			$q->andWhere('pn.idu=:user');
			$q->setParameter('user', $parentUserId);

		$q->setParameter('period', $actualPeriod);
		$q = $q->getQuery();
		
//		echo $actualPeriod->getId();
//		echo "<br />";
//		echo $parentUserId;
//		
//		echo $q->getSQL();
//		exit;

		$periodNetwork = $q->getSingleResult();
		$this->countIntermediateResults( $periodNetwork );
		return $periodNetwork;
	}
	
	public function setCorrectCarriers( Entity\PeriodNetwork $commissionParent )
	{
		$carrierParent = $this->getRanksRepository()->findOutActualCarrier( $commissionParent );
		$commissionParent->setCarrier( $carrierParent );

		$childrenCommisions = $commissionParent->getChildrenCommision();
		
		/* @var $commissionChildren \MoneyPoint\Entity\PeriodNetwork */
		foreach( $childrenCommisions as $commissionChildren )
		{
			$carrierChild = $this->getRanksRepository()->findOutActualCarrier( $commissionChildren );
			$commissionChildren->setCarrier( $carrierChild );
		} 
		return $commissionParent;
	}
	
	/**
	 * Počítá dílčí částky (karieru a odměny)
	 * @param \MoneyPoint\Entity\PeriodNetwork $commissionParent
	 * @param \MoneyPoint\Entity\CarrierGroup $rankTable
	 * @return \MoneyPoint\Entity\PeriodNetwork
	 */
	public function countIntermediateResults( Entity\PeriodNetwork $commissionParent )
	{

		$carrierParent = $commissionParent->getCarrier();
		$childrenCommisions = $commissionParent->getChildrenCommision();
		
		$directTotal = 0;
		$networkTotal = 0;
		
		/* @var $commissionChildren \MoneyPoint\Entity\PeriodNetwork */
		foreach( $childrenCommisions as $commissionChildren )
		{
			// zjisti kredit a karieru v období
			$networkCreditsChild = $commissionChildren->getNetworkPointsPeriod();
			$directCreditsChild = $commissionChildren->getDirectPointsPeriod();

			// omrkni jestli již nemá vyšší kariéru
			$carrierChild = $commissionChildren->getCarrier();
			
			$carrierDiff = $carrierParent->getPriceCoef() - $carrierChild->getPriceCoef();

			$networkTotal += $networkReward = $carrierDiff * $networkCreditsChild;
			$directTotal += $directReward = $carrierParent->getPriceCoef() * $directCreditsChild;
			
			$commissionChildren->setParentNetworkReward( $networkReward );
			$commissionChildren->setParentTotalReward( $directReward + $networkReward );
		} 
		$commissionParent->setMyReward( $directTotal, $networkTotal );
		
		return $commissionParent;
	}
	
	/**
	 * Vytváří transakce
	 * @param \MoneyPoint\Entity\PeriodNetwork[] $allRows
	 */
	public function makeTransactionForLastPeriod( $allRows )
	{
		$transactions = array();

		/* @var $periodNetwork \MoneyPoint\Entity\PeriodNetwork */
		foreach( $allRows as $periodNetwork )
		{
			$transaction = $this->getTransactionRepository()->createDifferentialCommisionTransaction( $periodNetwork );
			$periodNetwork->setTransaction( $transaction );
			$transactions[] = $transaction;
		}

		
		$this->em->flush();
		return $transactions;
	}
	
	public function recountPointTotals( $periodNetwork, $groupedResult )
	{
		$periodNetwork->setMyReward( $groupedResult['direct'], $groupedResult['network'] );
		return $periodNetwork;
	}
	
	/**
	 * 
	 * @param Entity\Period|null $period
	 * @param type $parentUserId
	 * @param type $makeTransactions
	 * @param type $assignCommisions
	 */
	public function countDiferentialTotals( $period=null, $parentUserId = null, $makeTransactions = false, $assignCommisions = false, $periodNetwork = null )
	{
		// gets actual period id if not passed
		if( empty($period))
		{
			$period = $this->getPeriodRepository()->getActualPeriod();
		}
		
	
		$now = new \DateTime();
		
		$groupedResult = $this->getPeriodNetworkRepository()->getEntitiesForAssigningOneCommision( $period, $parentUserId );
		$periodNetwork->setMyReward( $groupedResult['direct'], $groupedResult['network'] );

		if( $assignCommisions )
		{
			$this->assignCommision( $periodNetwork );
		}
		// creating transaction
		if( $makeTransactions || $period->getEnds() <= $now )					
		{

			$transaction = $this->getTransactionRepository()->createDifferentialCommisionTransaction( $periodNetwork );
			$periodNetwork->setTransaction( $transaction );
		}
	
	}
	
	
	/**
	 * 
	 * @param type $groupedForUserId ID uživatele pro kterého vyčítáme celkovou rozdílovou provizi
	 * @param type $values vypočtené sumy přímých a rozdílových odměn
	 * @param type $period účetní období
	 * @return \MoneyPoint\Entity\PeriodNetwork 
	 * @throws \LogicException
	 */
	public function assignTotalCommisionPerEntity($groupedForUserId, $values = null, $period, Entity\PeriodNetwork $periodNetwork  )
	{
		$direct = 0;
		$network = 0;
		if(is_numeric($values['direct'])) {
			$direct += $values['direct'];
		}
		if(is_numeric($values['network'])) {
			$network += $values['network'];
		}
		
		$periodNetwork->setMyReward( $direct, $network );
		return $periodNetwork;
	}
	
	public function manualAssignCommision( $commision )
	{
		return $this->assignCommision($commision);
	}
	
	public function reassignCommision( $commision )
	{
		$IGNORE_NEGATIVE_BALALANCE = true;
		return $this->assignCommision( $commision, $IGNORE_NEGATIVE_BALALANCE );
	}
	
	/**
	 * Vytvoří záznamy v bodových a kreditových účtech a přiřadí je do transakce
	 * @param \MoneyPoint\Entity\PeriodNetwork  $commision
	 * @throws \LogicException
	 */
	private function assignCommision( $commision, $ignoreNegativeBalance = false )
	{
		// extra odměna
		$userId = $commision->getIdu();
		$reason = 'Základní odměna';

		
		$userEntity = $commision->getUser();
		if( !$userEntity instanceof \AntoninRykalsky\IUser )
		{
			throw new \LogicException('Není předána funkční instance Member');
		}

		$claimPointsTotal = $commision->getTotalReward();
//		$member = new Member( $commision->getIdu() );
//		$creditsState = $member->credits()->getState();
		
		/* @var $creditAccountRepository \MoneyPoint\CreditAccountRepository */
		$creditAccountRepository = $this->em->getRepository('MoneyPoint\Entity\CreditAccount');
		$creditsState = $creditAccountRepository->getLast( $userEntity );
		
		$counting = new RewardCounting();
		$reward = $counting->extraRewardsWithReduction($creditsState, $claimPointsTotal);

		if( !$userEntity->hasBusiness() )
		{
			$reward->reward = 0;
			$reward->usedCredits = 0;
		}

		
		

		$transaction = $commision->getTransaction();
		if( !$transaction instanceof Entity\Transaction )
		{
			throw new \LogicException('Provize nemá přiřazenu transakci');
		}
		
		$pointAccount = $transaction->getPointsAccount();
		if( !$pointAccount instanceof Entity\PointAccount )
		{
			$repo = $this->em->getRepository("MoneyPoint\Entity\PointAccount");
			$pointAccount = $repo->storeDifferentialTransaction( $userEntity, (int)$reward->reward, $reason );
			$transaction->setPointsAccount( $pointAccount );
		} else {
			// změna bod. transakce
			$pointAccount->fixChange( $reward->reward );
		}
		
		$creditAccountChange = $transaction->getCreditsAccount();
		if( !$creditAccountChange instanceof Entity\CreditAccount )
		{
			$creditAccountChange = $creditAccountRepository->modifyCredits( $userEntity, $reason, $reward->usedCredits, $ignoreNegativeBalance );
			$transaction->setCreditsAccount( $creditAccountChange );
		} else {
			// změna bod. transakce
			$creditAccountChange->fixChange( $reward->usedCredits );
		}
		
		$transaction->setCreditsAccount( $creditAccountChange );
		return $commision;
	}

	
	private function getParentMember( $idu )
	{
		// ziskej potřebné data
		
		$asc = $this->memberRepository->getStructure( $idu );
		foreach( (array)$asc as $parent )
		{
			return $parent;
		}
	}
	
	private function getStructure( $idu )
	{
		// ziskej potřebné data
		
		return $asc = $this->memberRepository->getStructure( $idu );
		
	}
	private function getAsc( $idu )
	{
		// ziskej potřebné data
		
		$asc = $this->memberRepository->getAsc( $idu );
		foreach( (array)$asc as $parent )
		{
			return $parent;
		}
	}
	
	/**
	 * Přidá 0 kreditu na účet uživatele
	 * @param type $idu
	 */
	public function installDefaultAccount( $idu )
	{
		$this->addDifferentialPoints( $this->em->getReference('MoneyPoint\Entity\Member', $idu), 0 );
	}

	/**
	 * Simulace přidělení X bodů - kreditu
	 * @param type $period
	 * @param type $credits
	 */
	public function simulateStructurePointAddition( $period = null, $credits = 100000 )
	{
		$users = $this->memberRepository->getOnlyIdsOnlyMembers();
		
		foreach( $users as $member )
//		for( $i=1; $i<=14; $i++ )
		{
			$this->addDifferentialPoints( $member, $credits, $period );
		}
	}
	
	
	/**/
	
	
	public function _initialization( $idu )
	{
		if(!is_integer($idu))
			throw new \InvalidArgumentException('Očekávám ID uživatele');
		
		$member = $this->em->getReference('MoneyPoint\Entity\Member', $idu );
		
//		// pokud neni specifikovano, pouzij aktuální
		if(empty( $period )) {
			$period = $this->getPeriodRepository()->getActualPeriod(); 
		}
			
		// findout parent commision
		$parentIdu = $this->memberRepository->getDirectOnly( $idu );
		if( !empty( $parentIdu )) {
			$parentCommision = $this->em->getRepository('MoneyPoint\Entity\PeriodNetwork')->findOneBy(array('idu'=> $parentIdu, 'period' => $period ));
		}
		
		$commision = $this->em->getRepository('MoneyPoint\Entity\PeriodNetwork')->findOneBy(array('idu'=> $idu, 'period' => $period ));
		if( empty( $commision ))
		{
			$commision = new Entity\PeriodNetwork();
			$this->em->persist( $commision );
			
			$periodRanks = $this->getPeriodRepository()->getRankTable( $period );
			$basicRank = $periodRanks->getBasicCarrier();

			$commision->init( $member, $period, $basicRank );
			$commision->setParentIdu( $parentIdu );
			if(!empty( $parentCommision ))
				$commision->setParentCommision( $parentCommision );
		}
		
		
	}
}
