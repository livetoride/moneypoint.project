<?php

namespace MoneyPoint;


class DiferentialCommisionAssignerFacade
{
	
	/** @var \MoneyPoint\DirefentialCommisionService */
	protected $direfentialCommisionService;

	/** @var \MoneyPoint\PeriodNetworkRepository */
	protected $periodNetworkRepository;
	
	/** @var \MoneyPoint\PeriodRepository */
	protected $periodRepository;

	
	public function __construct(
			\MoneyPoint\DirefentialCommisionService $differentialCommisionService,
			\AntoninRykalsky\EntityManager $em
	) {
		$this->direfentialCommisionService = $differentialCommisionService;
		$this->em = $em->getEm();
		$this->periodNetworkRepository = $this->em->getRepository("MoneyPoint\Entity\PeriodNetwork");
		$this->periodRepository = $this->em->getRepository("MoneyPoint\Entity\Period");
	}
	
	public function assignCommisions()
	{
		$q = $this->em->createQuery(
				"select pn, t "
				. "from MoneyPoint\Entity\PeriodNetwork pn "
				. "join pn.transaction t "
				. "left join t.pointsAccount pa "
				. "where pa.id is null "
				. "order by pn.id "
				);
		$unassigned = $q->getResult();
	
		foreach( $unassigned as $commision )
		{
			$newCommision = $this->direfentialCommisionService->manualAssignCommision( $commision );
		}
		
		$this->em->flush();

	}
	
	public function reassignCommisions()
	{
		$q = $this->em->createQuery(
				"select pn, t, ca, u "
				. "from MoneyPoint\Entity\PeriodNetwork pn "
				. "join pn.transaction t "
				. "left join t.user u "
				. "left join t.pointsAccount pa "
				. "left join t.creditsAccount ca "
				. "order by pn.id "
				);
		$unassigned = $q->getResult();
		
		
		
		foreach( $unassigned as $commision )
		{
			$this->direfentialCommisionService->reassignCommision( $commision );
		}
		
		$this->em->flush();

	}
	
}
