<?php
namespace MoneyPoint;
use Nette;
use Nette\Environment;

class OrderingFacade extends Nette\Object
{
	private $em;
		
	private $isAdmin = false;

	/** @var \MoneyPoint\OrderingService */
	protected $orderingService;
	
	/** @var \MoneyPoint\Basket */
	protected $basket;

	public function __construct(
			\AntoninRykalsky\EntityManager $em,
			\MoneyPoint\OrderingService $orderingService
		) {
		$this->em = $em->getEm();
		$this->orderingService = $orderingService;
		
		// vytvoříme košík pokud neexistuje
		$container = Environment::getContext();
		$sessionService = $container->getService('session');
		$sessionSection = $sessionService->getSection('basket');
		$this->basket = $sessionSection->basketObject;
		
		if(!( $this->basket instanceof \Moneypoint\Basket ))
		{
			$this->basket = $sessionSection->basketObject = new \Moneypoint\Basket();
		}
	}
	
	public function getIsAdmin() {
		return $this->isAdmin;
	}

	public function setIsAdmin($isAdmin) {
		$this->isAdmin = $isAdmin;
	}

	public function clearBasket()
	{
		if(!( $this->basket instanceof \Moneypoint\Basket ))
		{
			// vytvoříme košík pokud neexistuje
			$container = Environment::getContext();
			$sessionService = $container->getService('session');
			$sessionSection = $sessionService->getSection('basket');
			$this->basket = $sessionSection->basketObject = new \Moneypoint\Basket();
			$this->basket = new \Moneypoint\Basket();
		}
			
		$this->basket->setEmpty();
	}
		
	public function addToBasket( $productId, $discountId )
	{
		/* @var $product \MoneyPoint\Entity\CreditDiscount */
		$discount = $this->em->find("\MoneyPoint\Entity\CreditDiscount", $discountId );
		/* @var $product \MoneyPoint\Entity\CreditProduct */
		$product = $this->em->find("\MoneyPoint\Entity\CreditProduct", $productId );

		$this->basket->addCredits( $product, $discount );
		
		return $product;
	}
	
	public function getBasket()
	{
		return $this->basket;
	}
	
	public function getBasketSession()
	{
		$prefix = ($this->isAdmin? 'admin' : '');
		return Environment::getSession($prefix.'basket');
	}
	
	private function getBuyerSession()
	{
		$prefix = ($this->isAdmin? 'admin' : '');
		return Environment::getSession($prefix.'user');
	}
}