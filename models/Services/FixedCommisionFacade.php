<?php

namespace MoneyPoint;

class FixedCommisionFacade
{
	private $em;
	
	public function __construct(
			\AntoninRykalsky\EntityManager $em
		) {
		$this->em = $em->getEm();
	}

	const MAX_ALLOWED_LEVELS = 10;
	
	/**
	 * Vrací klíče hloubek úrovní distribuce
	 * @return type
	 */
	public function getDistributionLevelKeys()
	{
		// možnost uložit až 10 záznamů, nicméně prázdné se neukládají
		// co není uloženo, není použito
		$return = array();
		for($i=1;$i<=self::MAX_ALLOWED_LEVELS;$i++){
			$return[]=$i;
		}
		return $return;
	}
	
	public function getLastLevel()
	{
		return \dibi::query('SELECT max(level_depth) FROM mp_produkty_level_distribution GROUP BY group_id')->fetchSingle();
	}

	/**
	 * Vrací skupinu distribuční tabulky
	 * @return \MoneyPoint\Entity\LevelDistributionGroup
	 */
	public function getDistributionGroup( $id = null )
	{
		if( empty( $id ))
		{
			return $this->em->getRepository("MoneyPoint\Entity\LevelDistributionGroup")->getActiveGroup();
		} else {
			return $this->em->find('MoneyPoint\Entity\LevelDistributionGroup', $id );
		}
	}

	public function getDistributionGroupByCommisionId( $id = 1)
	{
		return $this->em->getRepository('MoneyPoint\Entity\LevelDistributionGroup')->findOneBy(array('baseCommision'=> $id ));
	}
	
	public function getCoefDistributions( $price )
	{
		foreach( \DAO\MpCreditsDiscount::get()->findAll()->orderBy('price')->fetchAll() as $p )
		{
			if( $p->price = $price )
			{
				$ratio = $p->level_distribution_coef;
			}
		}
		
		$distributionGroup = $this->fixedCommisionFacade->getDistributionGroup();
		$dp = $this->template->rewards = $distributionGroup->getDistributions();
		foreach( $dp as $item )	{
			$item->setTemporaryCoefPoints( $item->getPoints() * $ratio );
		}
		return $dp;
	}

	private function renameOverviewPage( )
	{
		$max = $this->getLastLevel();
		$pages = \DAO\Menu::get()->findAll()->where('link=":Front:Structure:overview" AND parent_id<>2')->fetchAll();
		foreach( $pages as $p )
		{
			if( $p['menu'])
			\DAO\Menu::get()->update( $p['id_menu'], array('menu'=>"1. - $max. linie") );
		}
		
	}


	public function storeActive( $baseCommisionId, $useThis )
	{
		$actived = $this->em->getRepository('AntoninRykalsky\Entity\CommisionEntity')->findBy( array('isActive'=>1, 'typeId' => \AntoninRykalsky\Entity\Commision::FIXED ));
		
		/* @var $rankGroup Entity\LevelDistributionGroup */
		$rankGroup = $this->em->getRepository('MoneyPoint\Entity\LevelDistributionGroup')->findOneByBaseCommision( $baseCommisionId );
		if( $rankGroup === null )
		{
			throw new \LogicException("Cant find level distribution group");
		}
		
		$bc = $rankGroup->getBaseCommision();
		
		/* @var $commisionRepository \AntoninRykalsky\CommisionRepository */
		$commisionRepository = $this->em->getRepository('AntoninRykalsky\Entity\CommisionEntity');
		$commisionRepository->setActive( $bc, $useThis, @$actived );

		$this->em->flush();
	}
	
	public function storeDistributionTable( $distributionTableId, $arrayOfDistribution, $unitType )
	{
		$actived = $this->em->getRepository('AntoninRykalsky\Entity\CommisionEntity')->findBy( array('isActive'=>1, 'typeId' => \AntoninRykalsky\Entity\Commision::FIXED ));
		foreach( $actived as $a )
		{
			$this->em->persist( $a );
		}
		
		
		/* @var $rankGroup Entity\LevelDistributionGroup */
		$rankGroup = $this->em->getRepository('MoneyPoint\Entity\LevelDistributionGroup')->findOneByBaseCommision( $distributionTableId );
		if( $rankGroup === null )
		{
			throw new \LogicException("Cant find level distribution group");
		}

		$rankGroup->setUnit( $unitType );
		$d = $rankGroup->getDistributions();
		
		foreach( $d as $dItem)
		{
			$this->em->remove( $dItem );
		}
			
//		print_r( $arrayOfDistribution );exit;
		foreach( $arrayOfDistribution as $levelToStore )
		{
			$levelToStore['points'] = preg_replace('#,#', '.', $levelToStore['points']);
			if(is_numeric($levelToStore['level'])&&  is_numeric($levelToStore['points']))
			{
				$levelDistribution = new Entity\LevelDistribution();
				$levelDistribution->setLevel( $levelToStore['level'], $levelToStore['points'], $rankGroup );
				$this->em->persist( $levelDistribution );
			}
		}
		
		
		$this->renameOverviewPage();
		return $rankGroup;
	}

	public function createNewSet()
	{
		$bc = new \AntoninRykalsky\Entity\CommisionEntity;
		$bc->setBaseCommision( \AntoninRykalsky\Entity\Commision::FIXED, "Verze" );
		$this->em->persist( $bc );
		
		$group = new Entity\LevelDistributionGroup;
		$group->setGroup( "Verze", $bc  );
		$this->em->persist( $group );
		
		$this->em->flush();
		return $bc->getId();
	}
	
	public function automaticFixedCommisionSettle()
	{
//		$q = $this->em->createQuery("SELECT a FROM MoneyPoint\Entity\PosibleRewardEntity a WHERE a.tsResolution<:tsResolution");
//		$q->setParameter('tsResolution', new \DateTime );
//		$rewardItems = $q->getResult();
//		
//		foreach( $rewardItems as $item )
//		{
//			
//		}
		
		echo "ok";
		exit;
	}

	
}
