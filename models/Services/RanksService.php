<?php

namespace MoneyPoint;

class RanksService
{
	private $em;
	
	/** @var PeriodRepository */
	protected $periodRepository;
	
	public function __construct( \AntoninRykalsky\EntityManager $em ) {
		$this->em = $em->getEm();
		$this->periodRepository = $this->em->getRepository('MoneyPoint\Entity\Period');
	}
	
	
	
	
	
	public function store( $ranksFromForm, $rankGroup = null )
	{
		/* @var $rankGroup Entity\CarrierGroup */

		// create new rankgroup if not exists
		if( empty( $rankGroup ))
		{
			$rankGroup = new \MoneyPoint\Entity\CarrierGroup();
			$this->em->persist( $rankGroup );
			$rankGroup->setGroup('nová tabulka z '.date('j.n.Y H:i:s'), new \DateTime() );
		}
		
		// store (replacing way)
		$carriers = $rankGroup->getCarriers();
		$keys = array();
		foreach( $ranksFromForm as $rank )
		{
			$rank = (object)$rank;
			$keys[] = $rank->key;

			// create carrier row
			if( empty( $carriers[$rank->key] ))
			{
				$carrier = new Entity\Carrier();
				$carriers[ $rank->key ] = $carrier;
				$this->em->persist( $carrier );
			} else {
				$carrier = $carriers[$rank->key];
			}
			
			if( 
					$rank->key !== "" &&
					!empty( $rank->carrier ) &&
					$rank->min !== "" &&
					!empty( $rank->max ) &&
					$rank->priceCoef !== ""
			) {
				$carrier->setCarrier( 
						$rank->key,
						$rank->carrier,
						$rank->min,
						$rank->max,
						$rank->priceCoef );
			}
		}
		
		// remove other items
		foreach( $carriers as $carrier )
		{
			if( !in_array($carrier->getKey(), $keys))
			{
				$this->em->remove( $carrier );
			}
		}
		
		$rankGroup->setCarriers( $carriers );
		return $rankGroup;
	}



	
	public function getMemberRank( $id )
	{
		$periodNetworkRepository = $this->em->getRepository("MoneyPoint\Entity\PeriodNetwork");
		$period = $periodNetworkRepository->getActualPeriod();
		$periodNetwork = $periodNetworkRepository->getUserRank( $id, $period->getId() );
		
		return $periodNetwork;
	}
}
