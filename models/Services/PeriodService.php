<?php

namespace MoneyPoint;
use DateTime;
use DateInterval;

class PeriodService
{
	private $em;
	
	/** @var \MoneyPoint\ApplicationSetup */
	private $applicationSetup;
	
	public function __construct( \AntoninRykalsky\EntityManager $em, ApplicationSetup $applicationSetup ) {
		$this->em = $em->getEm();
		$this->applicationSetup = $applicationSetup;
	}
	
	
	
	public function store( $v )
	{
		$object = new \MoneyPoint\Entity\Period();
		if( empty( $v->id )) {
			$this->em->persist( $object );
		} else 
			$object = $this->em->find( get_class($object), $v->id );
		
		$object->setPeriod( $v->starts, $v->ends, $v->period );
		return $object;
	}

	const INSTALL_CZECH = 1; // Leden 2014, Únor 2014, ..
	const INSTALL_PERIODBYYEAR = 2; // 1/2014, 2/2014
	
	public function install( $start = 'now', $intervalInput = 'P10Y', $titlingConstant )
	{
		$this->applicationSetup->clearPeriods();
		
		$now = new DateTime( $start );
		$interval = new DateInterval( 'P1M' );
		
		$end = new DateTime( $start );
		$endInterval = new DateInterval( $intervalInput );
		$end->add($endInterval);
		
		if( $now >= $end ) {
		throw new \Exception('Nelze generovat účetní období které již skončilo.'); }
		
		while( $now <= $end )
		{
			$period = new Entity\Period();
			$this->em->persist($period);
			$period->setPeriod( 
				$now->format('Y-m-1'),
				$now->format('Y-m-t'),
				$this->getTitle($titlingConstant, $now )
			);
			$now->add($interval);
		}
	}

	private function getTitle( $titlingConstant, $start )
	{
		switch ($titlingConstant) {
			case self::INSTALL_CZECH:
				$months = array( 1=>'Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec');
				return $months[$start->format('n')] . " ". $start->format('Y');
				break;
			
			case self::INSTALL_PERIODBYYEAR:
				return $start->format('n') . "/". $start->format('Y');
				break;
		}
		throw new \LogicException('Nastala chyba, vyberte prosím jiný typ názvů období nebo kontaktujte výrobce.');
	}
}
