<?php

namespace MoneyPoint;

class CountryFacade
{
	private $em;
	public function __construct( \AntoninRykalsky\EntityManager $c2 ) {
		$this->em = $c2->getEm();
	}
	
	public function getCountry()
	{
		//$model = $this->em->createQuery("SELECT * FROM \AntoninRykalsky\Entity\country");
		$model = \DAO\BCountry::get()->getDataSource();
		return $model;
	}
	public function getPartnerCountry($partnerId) {
		if (!empty($partnerId)) {
			/* @var $partner Entity\Eshop */
			$partner = $this->em->getRepository("MoneyPoint\Entity\Partner")->find( $partnerId );
			$c = $partner->getCountry();

			$return = array();
			foreach ($c as $value) {
				/* @var $value \AntoninRykalsky\Entity\Country */
				$return[] = $value->getCountryName();

			}
			return $return;
		}
	}
	public function getEshopCountry($partnerId) {
		if (!empty($partnerId)) {
			/* @var $partner Entity\Eshop */
			$partner = $this->em->getRepository("MoneyPoint\Entity\Eshop")->find( $partnerId );
			$c = $partner->getCountry();

			$return = array();
			foreach ($c as $value) {
				/* @var $value \AntoninRykalsky\Entity\Country */
				$return[] = $value->getCountryName();

			}
			return $return;
		}
	}
	public function getShopCountry($partnerId) {
		if (!empty($partnerId)) {
			/* @var $partner Entity\Eshop */
			$partner = $this->em->getRepository("MoneyPoint\Entity\Shop")->find( $partnerId );
			$c = $partner->getCountry();

			$return = array();
			foreach ($c as $value) {
				/* @var $value \AntoninRykalsky\Entity\Country */
				$return[] = $value->getCountryName();

			}
			return $return;
		}
	}
	
	public function deleteCountry( $id)
	{
		$country = $this->em->find('\AntoninRykalsky\Entity\Country', $id);
		$this->em->remove($country);
		$this->em->flush();
		return 1;
	}

	public function addCountry($countryName)
	{
		//$check = $this->em->getRepository("AntoninRykalsky\Entity\country" )->findBy(array('countryName'=>$countryName ));
		
		
	/*	if (!empty($check) ){
			throw new \LogicException('kategorie již existuje');
		} else { */
		$country1 = new \AntoninRykalsky\Entity\country();
		$country1->storeCountry($countryName);
		$this->em->persist( $country1 );
		$this->em->flush(); 
		return $country1;
	//}		

		
//		\Doctrine\Common\Util\Debug::dump();
//		\Doctrine\Common\Util\Debug::dump( $partner->getCountry() );
		
	}
	public function storeCountryToPartner($partnerId, $countries )
	{
		foreach ($countries as $t)
		{
			$country = $this->em->getRepository("\AntoninRykalsky\Entity\Country")->findOneBy(array('countryName'=> $t ));
			if (empty($country) ){
				$country = $this->addCountry($t); 
				
			}
			
			/* @var $partner Entity\Shop */
			$partner = $this->em->getRepository("MoneyPoint\Entity\Partner")->find( $partnerId );
			
		
			$exist = 0;
			$partnerCountries = $partner->getCountry();
			foreach($partnerCountries as $kCountry => $iCountry)
			{
				if(!in_array($iCountry->getCountryName(), $countries))
				{
					$partner->removeCountry( $iCountry );
				}
				if( $iCountry->getCountryName() == $country->getCountryName() )
				{
					$exist = 1;
					continue;
				}
			}
			
			if (!$exist){
				$partner->addCountry($country);
			}
		}

		$this->em->flush();
	}
	public function storeCountryToShop($partnerId, $countries )
	{
		foreach ($countries as $t)
		{
			$country = $this->em->getRepository("\AntoninRykalsky\Entity\Country")->findOneBy(array('countryName'=> $t ));
			if (empty($country) ){
				$country = $this->addCountry($t); 
				
			}
			
			/* @var $partner Entity\Shop */
			$partner = $this->em->getRepository("MoneyPoint\Entity\Shop")->find( $partnerId );
			
		
			$exist = 0;
			$partnerCountries = $partner->getCountry();
			foreach($partnerCountries as $kCountry => $iCountry)
			{
				if(!in_array($iCountry->getCountryName(), $countries))
				{
					$partner->removeCountry( $iCountry );
				}
				if( $iCountry->getCountryName() == $country->getCountryName() )
				{
					$exist = 1;
					continue;
				}
			}
			
			if (!$exist){
				$partner->addCountry($country);
			}
		}

		$this->em->flush();
	}
	public function storeCountryToEshop($partnerId, $countries )
	{
		foreach ($countries as $t)
		{
			$country = $this->em->getRepository("\AntoninRykalsky\Entity\Country")->findOneBy(array('countryName'=> $t ));
			if (empty($country) ){
				$country = $this->addCountry($t); 
				
			}
			
			/* @var $partner Entity\Eshop */
			$partner = $this->em->getRepository("MoneyPoint\Entity\Eshop")->find( $partnerId );
			
		
			$exist = 0;
			$partnerCountries = $partner->getCountry();
			foreach($partnerCountries as $kCountry => $iCountry)
			{
				if(!in_array($iCountry->getCountryName(), $countries))
				{
					$partner->removeCountry( $iCountry );
				}
				if( $iCountry->getCountryName() == $country->getCountryName() )
				{
					$exist = 1;
					continue;
				}
			}
			
			if (!$exist){
				$partner->addCountry($country);
			}
		}

		$this->em->flush();
	}
}


