<?php

namespace MoneyPoint;

class PeriodFacade
{
	private $em;
	
	/** @var PeriodRepository */
	private $repository;
	public function __construct(
			\AntoninRykalsky\EntityManager $em,
			PeriodService $periodService
		) {
		$this->em = $em->getEm();
		$this->repository = $this->em->getRepository('MoneyPoint\Entity\Period');
		$this->service = $periodService;
	}

	public function getDatasource()
	{
		return $this->repository->getDatasource();
	}
	
	public function secureFind( $id )
	{
		$object = $this->repository->find( $id );
		if( empty( $object ))
			throw new \Nette\Application\BadRequestException('Hledané omezení neexistuje');
		
		return $object;
	}
	
	public function store( $v )
	{
		$this->service->store( $v );
		$this->em->flush();
	}
	
	public function install( $titling = 1, $interval = 5 )
	{
		$this->service->install( 'now', 'P'.$interval.'Y', $titling );
		$this->em->flush();
	}
	
	public function getActualRankTable()
	{
		return $this->repository->getRankTable();
	}
}
