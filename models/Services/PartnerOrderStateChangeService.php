<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;
use Nette;

// used in addRewards
use AntoninRykalsky\CreditsAccounts;
use AntoninRykalsky\PointsAccounts;
use dibi;

class PartnerOrderStateChangeService extends \Nette\Object
{
	private $em;
	
	/** @var PartnerOrderFacade */
	protected $partnerOrderFacade;

	function setPartnerOrderFacade(PartnerOrderFacade $partnerOrderFacade) {
		$this->partnerOrderFacade = $partnerOrderFacade;
	}
	
	public function __construct(
			\AntoninRykalsky\EntityManager $c
		) {
		$this->em = $c->getEm();
	}
	
	private function setReward( $formValues, $partnerOrder )
	{
		$partnerOrder->setUtrata( $formValues->utrata );
		if( isset( $formValues->reward ))
		{
			$partnerOrder->setRewardManual( $formValues->reward );
		}
		
	}
	
	private function assignToClient( $formValues, $partnerOrder )
	{
		$this->partnerOrderFacade->setAdminCjAgreed( $partnerOrder );
	}

	public function partnerOrderAutomaticSettle()
	{
		$orders = $this->em->getRepository('MoneyPoint\Entity\PartnerOrder')->findoutOrdersForAutomaticSettle();
		foreach( $orders as $partnerOrder )
		{
			$this->settle( null, $partnerOrder );
		}
	}
	
	private function addRewards( $partnerOrder )
	{
		$user = $partnerOrder->getUser();
	//	print_r($partnerOrder);exit();
		$partner = $partnerOrder->getPartner();
		$creditsRepository = new CreditRepository();
		$creditsState = $creditsRepository->getState( $user->getIdu() );
		
		// napocitej vysi odmen
		$counting = new RewardCounting();
		$reward = $counting->extraRewardsWithReduction($creditsState, $partnerOrder->getZakladniOdmena() );

		// uloz odmeny, odecti kredit, uloz transakci
		PointsAccounts::get()->pioPoints( $user->getIdu() , "Nákup v obchodě ".$partner, $reward->reward, $partnerOrder->getIdOrder() );
		$points_account_id = dibi::insertId();
		//CreditsAccounts::get()->modifyCredits( $order->idu , "Kredity čerpané za EO z partnerskeho obchodu ".$partner['name'].".", -$extraOdmena,  $o->id_order );
		CreditsAccounts::get()->modifyCredits( $user->getIdu() , "Nákup v obchodě ".$partner, $reward->usedCredits,  null, $partnerOrder->getIdOrder() );
		$credits_account_id = dibi::insertId();

		$transaction = array(
			'points_account_id' => $points_account_id,
			'credits_account_id' => $credits_account_id,
		);

		$trans = \Moneypoint\DAO\Transactions::get()->findAll()->where('id_pio_order=%i', $partnerOrder->getIdOrder() )->fetch();
		\Moneypoint\DAO\Transactions::get()->update( $trans->id_transaction, $transaction);

	}
	
	private function settle( $formValues, Entity\PartnerOrder $partnerOrder )
	{
		$partnerOrder->setSettled();

		if( $partnerOrder->getIdStatus() == 3 )
		{
			$this->addRewards( $partnerOrder );
		}
		$this->em->persist( $partnerOrder );
	}
	
	private function stateLogicExecutor( $new, $old )
	{
		$whatToDo = array();
		if( array($old, $new) == array(Entity\PartnerOrder::STATE_CJ_NEW, Entity\PartnerOrder::STATE_WAITING ) ) {
			$whatToDo[] = 'assignToClient'; 		}
		if( array($old, $new) == array(Entity\PartnerOrder::STATE_CJ_NEW, Entity\PartnerOrder::STATE_REWARDED ) ) {
			$whatToDo[] = 'assignToClient'; } 
		if( $new ==  Entity\PartnerOrder::STATE_REWARDED ) {
			$whatToDo[] = 'settle';	}
		return $whatToDo;
	}
	
	/**
	 * 
	 * @param Nette\ArrayHash $formValues
	 * @param Entity\PartnerOrder|null $partnerOrder
	 * @throws \Exception
	 */
	public function process( Nette\ArrayHash $formValues, $partnerOrder = null )
	{
		if( empty($this->partnerOrderFacade)) {
		throw new \Exception('Call setPartnerOrderFacade with PartnerOrderFacade object first.'); }
		
		$this->parseInput( $formValues );

		if( !empty($formValues->createNew) && $formValues->createNew ) {
			$partnerOrder = $this->createNew( $formValues );
		} else {
			$whatTodo = $this->stateLogicExecutor( $formValues->id_status, $partnerOrder->getIdStatus() );
			foreach( $whatTodo as $wtd )
			{
				call_user_func( array($this, $wtd), $formValues, $partnerOrder );
			}
		}

		return $partnerOrder;
	}
	
	/**
	 * @param Nette\ArrayHash $formValues
	 * @return PartnerOrder
	 */
	protected function createNew( Nette\ArrayHash $formValues )
	{
		return $this->partnerOrderFacade->create( $formValues );
	}
	
	protected function parseInput( Nette\ArrayHash &$formValues )
	{
		if( !empty($formValues->utrata)) $formValues->utrata = str_replace(',', '.', $formValues->utrata);
	}
	
	
}
