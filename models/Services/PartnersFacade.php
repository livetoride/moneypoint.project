<?php

namespace MoneyPoint;

class PartnersFacade
{
	/** @var PartnerWhateverService */
	protected $partnerWhateverService;

	private $em;
	public function __construct( \AntoninRykalsky\EntityManager $c2, PartnerWhateverService $partnerWhateverService ) {
		$this->em = $c2->getEm();
		$this->partnerWhateverService = $partnerWhateverService;
	}
	
	/**
	 * findout partner by id
	 * @param type $id
	 * @return type \MoneyPoint\Entity\Eshop
	 */
	public function getPartner( $id )
	{
		return $this->partnerWhateverService->getPartner($id);
	}
	
	public function getPartnerFeesNew( $partner )
	{
		return $this->partnerWhateverService->getPartnerFeesNew($partner);
	}
	
/*	public function getEshops( $tagId=null, $partnerName=null )
	{
		return $this->partnerWhateverService->getEshops($tagId, $partnerName );
	} */
	
	public function getPartners( $tagId=null, $partnerName=null, $country )
	{
		return $this->partnerWhateverService->getPartners($tagId, $partnerName, $country );
	}
	
/*	public function getShops( $tagId=null, $partnerName=null )
	{
		$q = $this->em->createQuery("SELECT p FROM \MoneyPoint\Entity\Shop p WHERE p.active=1");
		return $q->getResult();		
				//\Doctrine\Common\Util\Debug::dump($q->getResult());exit();  
	}
	
	*/
	public function getPartnersNames(  ) {
		
		return $this->partnerWhateverService->getPartnerNames();
	} 
	
	
	public function getPartnerFees( $partner )
	{
	return $this->partnerWhateverService->getPartnerFees( $partner );
	}
	
	/**
	 * returns items into partner fee selector
	 * @return array
	 */
	public function getPartnerFeesSelect( $withCategories = 1 )
	{
		
		return $this->partnerWhateverService->getPartnerFeesSelect( $withCategories = 1 );
	}
	
	public function getPartnerFee( $id )
	{
		return $this->partnerWhateverService->getPartnerFee( $id );
	}
	
	public function saveRate( $v )
	{
		$this->partnerWhateverService->saveRate($v);
		
	}
	
	public function removePartnerFee( $id_rate )
	{
		$this->partnerWhateverService->removePartnerFee($id_rate);
	}
	
	public function deletePartner( $id_partner )
	{
		$this->partnerWhateverService->deletePartner($id_partner);
		
	}

    
	/** @return \AntoninRykalsky\StateMachineService */
	public function getStateMachine()
	{
		return $this->partnerWhateverService->getStateMachine();
	}
	
	
	public function getPartnerDataSource( $action = null )
	{
		$model = \DAO\Partners::get()->getDataSource();
		switch ($action) {
			case 'default':
				$model->where('type=2 AND active=1');
				break;
			case 'archived':
				$model->where('type=2 AND active=0');
				break;

			default:
				break;
		}
		return $model;
	}
	
	
	
	public function deactivatePartnerWhatever( $items )
	{
		$count = 0;
//		print_r( $items );exit;
		foreach( $items as $id => $deactivate )
		{
			if( $deactivate ) $count++;
			\DAO\Partners::get()->update($id, array('active'=>!$deactivate));
		}
		return $count;
	}
	
	public function activatePartnerWhatever( $items )
	{
		$count = 0;
//		print_r( $items );exit;
		foreach( $items as $id => $activate )
		{
			if( $activate ) $count++;
			\DAO\Partners::get()->update($id, array('active'=>$activate));
		}
		return $count;
	}
	
}
