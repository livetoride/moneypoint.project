<?php

namespace MoneyPoint;

use AntoninRykalsky\ProduktyLevelDistribution;
use AntoninRykalsky\StructureOverviewDao;
use DAO\MpCreditFutureRewards;
use DAO\Orders;
use DateTime;
use MoneyPoint\Entity\CreditOrder;
use MoneyPoint\Entity\CreditReward;
use MoneyPoint\FutureReward\FirstLineFutureReward;
use MoneyPoint\FutureReward\FirstLineFutureRewards;
use MoneyPoint\FutureReward\FutureRewards;

class PosibleRewardsFacade
{
	private $em;
	
	/** @var FirstBuyBonus\FirstBuyBonusService */
	protected $firstBuyBonusService;
	
	/** @var \MoneyPoint\FixedCommisionService */
	protected $fixedCommisionService;
	
	/** @var MemberRepository */
	protected $memberRepository;
	
	/** @var PosibleRewardsService */
	private $posibleRewardsService;
	
	public function __construct(
			\AntoninRykalsky\EntityManager $em,
			FirstBuyBonus\FirstBuyBonusService $firstBuyBonusService,
			\MoneyPoint\FixedCommisionService $fixedCommisionService,
			MemberRepository $memberRepository,
			\MoneyPoint\PosibleRewardsService $posibleRewardsService
		) {
		$this->em = $em->getEm();
		$this->firstBuyBonusService = $firstBuyBonusService;
		$this->fixedCommisionService = $fixedCommisionService;
		$this->memberRepository = $memberRepository;
		$this->posibleRewardsService = $posibleRewardsService;
	}
	
	public function recountNextRewards() {
		$this->em->createQuery("DELETE FROM MoneyPoint\Entity\PosibleRewardEntity o")->getResult();
		$orders = $this->em->createQuery("SELECT o, u FROM MoneyPoint\Entity\CreditOrder o JOIN o.user u ORDER BY o.idOrder")->getResult();
		
		/* @var $order \MoneyPoint\Entity\CreditOrder */
		foreach( $orders as $order )
		{
			if( in_array( $order->getStatus(), array( \MoneyPoint\Entity\CreditOrder::STATE_NEW, \MoneyPoint\Entity\CreditOrder::STATE_PAID)  ))
			{
				$r  =$this->posibleRewardsService->addPosibleRewards( $order->getUser(), $order, 0 );
			}
		}
		$this->em->flush();
	}

	/**
	 * Budoucí odměny v následujících (21) dnech
	 * Datum rozhodného okamžiku - Celkový počet odměn - Celkem maximální možné odměny
	 * @param type $idu
	 * @return \MoneyPoint\FutureReward\FutureRewards
	 */
	public function getRewards( $idu ) {
//		$this->recountNextRewards();
		$this->em->getRepository("MoneyPoint\Entity\Carrier")->fixActualCarriers();
				
		$days = \AntoninRykalsky\Configs::get()->byContent('general.safety_deadline');
		$rewards = new FutureRewards(null, $days);
		
		$member = $this->em->find('MoneyPoint\Entity\Member', $idu );
		
		$date = new DateTime();
		$co = new CreditOrder();
		$co->setOrderTimestamp( $date->format('Y-m-d') );
		
		$q = $this->em->createQuery("SELECT a FROM MoneyPoint\Entity\PosibleRewardEntity a WHERE a.idu=:idu");
		$q->setParameter('idu', $idu );
		$rewardItems = $q->getResult();
		/* @var $item \MoneyPoint\Entity\PosibleRewardEntity */
		foreach( $rewardItems as $item )
		{
			$rewards->addReward( $item->getTsresolution()->format('Y-m-d'), $item->getClaim(), $item->getCount() );
		}
		return $rewards;
		
	}
	
	/**
	 * Budoucí odměny v první linii
	 * @param type $idu
	 * @return \MoneyPoint\FutureReward\FirstLineFutureRewards
	 */
	public function getFirstLineRewards( $idu )
	{
		$rewards = new FirstLineFutureRewards();
		
		$structure = $this->em->getRepository("MoneyPoint\Entity\Member")->getChildenDirect( $idu );
		

		$iStructure = '';
		if( !empty( $structure ))
		{
			$iStructure .= "AND u.idu IN(". implode(',', (array)$structure) .")";
		} else {
			$iStructure .= "AND 1=0";
		}

		$q = $this->em->createQuery(
			"SELECT r, a, u FROM MoneyPoint\Entity\CreditReward r "
				. "JOIN r.creditOrder a "
				. "JOIN a.user u "
				. "WHERE r.line=1 "
				. $iStructure
			);
		$firstLineOrders = $q->getResult();
	
		foreach( $firstLineOrders as $creditReward )
		{
			$reward = new FirstLineFutureReward();
			$reward->setReward(
					$creditReward->getCreditOrder()->getUser(),
					$creditReward->getCreditOrder(),
					$creditReward);
			$rewards->addReward($reward);
		}
		
		return $rewards;
	}
	
	public function removePosibleRewards( $creditsOrder )
	{
		// ziskej potřebné data
		$memberRepository = new MemberRepository( $this->em );
		$structure = $memberRepository->getStructure( $creditsOrder->getUser()->getIdu() );
		
		$q = $this->em->createQuery('SELECT a FROM MoneyPoint\Entity\PosibleRewardEntity a WHERE a.idu IN(:structure)');
		$q->setParameter('structure', $structure );
		$q->setParameter('date', $creditsOrder->getTsResolution()->format('Y-m-d') );
		$r = $q->getResult();
//		print_r( $r );
//			exit;
//		foreach( $r as $result )
//		{
//			
//		}

	}
	
	
	
    
}
