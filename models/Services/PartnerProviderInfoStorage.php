<?php

namespace MoneyPoint;

class PartnerProviderInfoStorage
{
	private $data = array();
    private $id;
    private $em;
	public function __construct( $em, $ppiid = null ) {
		throw new \Exception("MADAFAKA");
		$this->em = $em;
		
		if( !empty( $ppiid ))
		{
			$this->id = $ppiid;
			$q = $this->em->createQuery('SELECT a FROM MoneyPoint\Entity\PartnerProviderInfo a INDEX BY a.key WHERE a.ppiId = :ppiid');
			$q->setParameter('ppiid', $ppiid );
			$this->data = $q->getResult();
		}
		
		return $this;
	}
	
	public function getCommonKey()
	{
		return $this->id;
	}
	
	public function get( $type )
	{
		return $this->data[ $type ]->getValue();
	}
	
	public function rm( $type )
	{
		if( !isset( $this->data[$type] )) return;
		
		$this->em->remove( $this->data[$type] );
		unset( $this->data[$type] );
	}
	
	public function set( $type, $value )
	{
		if( empty( $this->data[$type] ))
		{
			$this->data[ $type ] = new Entity\PartnerProviderInfo( $this->id );
			$this->data[ $type ]->setKey( $type );
			$this->em->persist( $this->data[ $type ] );
		}
		
		$this->data[ $type ]->setValue( $value );
	}
	
	public function exist( $type )
	{
		return isset( $this->data[ $type ] );
	}
}
