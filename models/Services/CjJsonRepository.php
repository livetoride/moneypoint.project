<?php

namespace MoneyPoint;

class CjJsonRepository
{
	private function getOnlyXml( $file )
	{
		preg_match('#(<cj-api>.+</cj-api>)#', $file, $matches );
		if( empty( $matches[1] ))
			throw new \Exception('Nastala chyba při parsování odměz z cj.com');
		
		echo $matches[1]."\n";
		
		return $matches[1];
	}
	
	public function downloadNewFromCj( \DateTime $startDate, \DateTime $endDate )
	{
		$startDate = $startDate->format('Y-m-d');
		$endDate = $endDate->format('Y-m-d');

		$buffer = "";
		$errno=null;
		$errstr=null;
		$fp = fsockopen("ssl://commission-detail.api.cj.com", 443, $errno, $errstr, 5);
		echo $errstr;
		if (!$fp) {
			echo "chyba";
			return 0;
		} else {
			$data = "/v3/commissions?date-type=posting&start-date=".$startDate."&end-date=".$endDate;

			$key = '00a2b9836cd949015da37be23e5f8d46f62a6427ff073e867bd1bc15933e5d93ab4bf89f32330198af8cbd5b3bdbe4d2caaba9431e395b95a11b822cdbb433dcdd/45f456a99ea7a1ecad717e8badec404b6761f347d1c7716d1d772c1746c257cea687f5c6d183b9ed5ad15b5edb5bd6a63f66fd5f16d16b67ec27f94a67e2d501';
			$out = "GET " . $data . " HTTP/1.1\r\n";
			$out .= "Host: link-search.api.cj.com\r\n";
			$out .= "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8\r\n";
			$out .= "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n";
			$out .= "Accept-Language: en-us,en;q=0.5\r\n";
			$out .= "Accept-Charset: utf-8;q=0.7,*;q=0.7\r\n";
			$out .= "Keep-Alive: 300\r\n";
			$out .= "Connection: keep-alive\r\n";
			$out .= "Cookie: __utma=128446558.2099392322683464700.1239639722.1239639722.1239927095.2; __utmz=128446558.1239639722.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); CONTID=8073; cjuMember=0; JSESSIONID=aM5RSWdqdd_5\r\n";
			$out .= "Authorization: " . $key . "\r\n";
			$out .= "\r\n";
			$out .= "HTTP/1.x 200 OK\r\n";
			$out .= "Server: Resin/2.1.17\r\n";
			$out .= "Content-Type: application/xml\r\n";
			$out .= "Transfer-Encoding: chunked\r\n";
			$out .= "Date: Thu, 26 Apr 2009 10:25:03 GMT\r\n";
			$out .= "\n\n";

			#$myFile = "c:/wamp/www/beta.moneypoint.cz/app/temp/dump14.sql";
			#$fh = fopen($myFile, 'w') or die("can't open file");

			fwrite($fp, $out);

			$i = 0; $return = '';
			while (!feof($fp)) {
				$i++;
				if (preg_match('#</cj-api>#', $return ))
						break;
				#if ($i == 5) return $return;
				$return .= fread($fp, 256);
			}
			fclose($fp);
			return $this->getOnlyXml( $return );
		}
	}
	
	public function downloadCjItemDetail( $itemId )
	{
		
//		<cj-api>
//			<item-details original-action-id="1431703341">
//				<item>
//					<sku>408</sku>
//					<quantity>2</quantity>
//					<posting-date>2014-01-26T04:30:05-0800</posting-date>
//					<commission-id>1689049286</commission-id>
//					<sale-amount>31.182</sale-amount>
//					<discount>0.</discount>
//					<publisher-commission>7.484</publisher-commission>
//				</item>
//			</item-details>
//		</cj-api>


		$buffer = "";
		$errno=null;
		$errstr=null;
		$fp = fsockopen("ssl://commission-detail.api.cj.com", 443, $errno, $errstr, 5);
		echo $errstr;
		if (!$fp) {
			echo "chyba";
			return 0;
		} else {
			$data = "/v3/item-detail/".$itemId ;

			$key = '00a2b9836cd949015da37be23e5f8d46f62a6427ff073e867bd1bc15933e5d93ab4bf89f32330198af8cbd5b3bdbe4d2caaba9431e395b95a11b822cdbb433dcdd/45f456a99ea7a1ecad717e8badec404b6761f347d1c7716d1d772c1746c257cea687f5c6d183b9ed5ad15b5edb5bd6a63f66fd5f16d16b67ec27f94a67e2d501';
			$out = "GET " . $data . " HTTP/1.1\r\n";
			$out .= "Host: link-search.api.cj.com\r\n";
			$out .= "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8\r\n";
			$out .= "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n";
			$out .= "Accept-Language: en-us,en;q=0.5\r\n";
			$out .= "Accept-Charset: utf-8;q=0.7,*;q=0.7\r\n";
			$out .= "Keep-Alive: 300\r\n";
			$out .= "Connection: keep-alive\r\n";
			$out .= "Cookie: __utma=128446558.2099392322683464700.1239639722.1239639722.1239927095.2; __utmz=128446558.1239639722.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); CONTID=8073; cjuMember=0; JSESSIONID=aM5RSWdqdd_5\r\n";
			$out .= "Authorization: " . $key . "\r\n";
			$out .= "\r\n";
			$out .= "HTTP/1.x 200 OK\r\n";
			$out .= "Server: Resin/2.1.17\r\n";
			$out .= "Content-Type: application/xml\r\n";
			$out .= "Transfer-Encoding: chunked\r\n";
			$out .= "Date: Thu, 26 Apr 2009 10:25:03 GMT\r\n";
			$out .= "\n\n";

			#$myFile = "c:/wamp/www/beta.moneypoint.cz/app/temp/dump14.sql";
			#$fh = fopen($myFile, 'w') or die("can't open file");

			fwrite($fp, $out);

			$i = 0; $return = '';
			while (!feof($fp)) {
				$i++;
				if (preg_match('#</cj-api>#', $return ))
						break;
				#if ($i == 5) return $return;
				$return .= fread($fp, 256);
			}
			fclose($fp);
			
			return $this->getOnlyXml( $return );
		}
	}
	
	public function downloadCategories( )
	{
		
//		<cj-api>
//			<item-details original-action-id="1431703341">
//				<item>
//					<sku>408</sku>
//					<quantity>2</quantity>
//					<posting-date>2014-01-26T04:30:05-0800</posting-date>
//					<commission-id>1689049286</commission-id>
//					<sale-amount>31.182</sale-amount>
//					<discount>0.</discount>
//					<publisher-commission>7.484</publisher-commission>
//				</item>
//			</item-details>
//		</cj-api>


		$buffer = "";
		$errno=null;
		$errstr=null;
		$fp = fsockopen("ssl://support-services.api.cj.com", 443, $errno, $errstr, 5);
		echo $errstr;
		if (!$fp) {
			echo "chyba";
			return 0;
		} else {
			$data = "/v2/categories";

			$key = '00a2b9836cd949015da37be23e5f8d46f62a6427ff073e867bd1bc15933e5d93ab4bf89f32330198af8cbd5b3bdbe4d2caaba9431e395b95a11b822cdbb433dcdd/45f456a99ea7a1ecad717e8badec404b6761f347d1c7716d1d772c1746c257cea687f5c6d183b9ed5ad15b5edb5bd6a63f66fd5f16d16b67ec27f94a67e2d501';
			$out = "GET " . $data . " HTTP/1.1\r\n";
			$out .= "Host: link-search.api.cj.com\r\n";
			$out .= "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8\r\n";
			$out .= "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n";
			$out .= "Accept-Language: en-us,en;q=0.5\r\n";
			$out .= "Accept-Charset: utf-8;q=0.7,*;q=0.7\r\n";
			$out .= "Keep-Alive: 300\r\n";
			$out .= "Connection: keep-alive\r\n";
			$out .= "Cookie: __utma=128446558.2099392322683464700.1239639722.1239639722.1239927095.2; __utmz=128446558.1239639722.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); CONTID=8073; cjuMember=0; JSESSIONID=aM5RSWdqdd_5\r\n";
			$out .= "Authorization: " . $key . "\r\n";
			$out .= "\r\n";
			$out .= "HTTP/1.x 200 OK\r\n";
			$out .= "Server: Resin/2.1.17\r\n";
			$out .= "Content-Type: application/xml\r\n";
			$out .= "Transfer-Encoding: chunked\r\n";
			$out .= "Date: Thu, 26 Apr 2009 10:25:03 GMT\r\n";
			$out .= "\n\n";

			#$myFile = "c:/wamp/www/beta.moneypoint.cz/app/temp/dump14.sql";
			#$fh = fopen($myFile, 'w') or die("can't open file");

			fwrite($fp, $out);

			$i = 0; $return = '';
			while (!feof($fp)) {
				$i++;
				if (preg_match('#</cj-api>#', $return ))
						break;
				#if ($i == 5) return $return;
				$return .= fread($fp, 256);
			}
			fclose($fp);
			
			return $this->getOnlyXml( $return );
		}
	}
	
	public function downloadFromCj( \DateTime $startDate, \DateTime $endDate )
	{
		$startDate = $startDate->format('Y-m-d');
		$endDate = $endDate->format('Y-m-d');

		$buffer = "";
		$fp = fsockopen("ssl://commission-detail.api.cj.com", 443, $errno, $errstr, 5);
		echo $errstr;
		if (!$fp) {
			echo "chyba";
			return 0;
		} else {
			$data = "/v3/commissions?date-type=posting&start-date=".$startDate."&end-date=".$endDate;

			$key = '00a2b9836cd949015da37be23e5f8d46f62a6427ff073e867bd1bc15933e5d93ab4bf89f32330198af8cbd5b3bdbe4d2caaba9431e395b95a11b822cdbb433dcdd/45f456a99ea7a1ecad717e8badec404b6761f347d1c7716d1d772c1746c257cea687f5c6d183b9ed5ad15b5edb5bd6a63f66fd5f16d16b67ec27f94a67e2d501';
			$out = "GET " . $data . " HTTP/1.1\r\n";
			$out .= "Host: link-search.api.cj.com\r\n";
			$out .= "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8\r\n";
			$out .= "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n";
			$out .= "Accept-Language: en-us,en;q=0.5\r\n";
			$out .= "Accept-Charset: utf-8;q=0.7,*;q=0.7\r\n";
			$out .= "Keep-Alive: 300\r\n";
			$out .= "Connection: keep-alive\r\n";
			$out .= "Cookie: __utma=128446558.2099392322683464700.1239639722.1239639722.1239927095.2; __utmz=128446558.1239639722.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); CONTID=8073; cjuMember=0; JSESSIONID=aM5RSWdqdd_5\r\n";
			$out .= "Authorization: " . $key . "\r\n";
			$out .= "\r\n";
			$out .= "HTTP/1.x 200 OK\r\n";
			$out .= "Server: Resin/2.1.17\r\n";
			$out .= "Content-Type: application/xml\r\n";
			$out .= "Transfer-Encoding: chunked\r\n";
			$out .= "Date: Thu, 26 Apr 2009 10:25:03 GMT\r\n";
			$out .= "\n\n";

			#$myFile = "c:/wamp/www/beta.moneypoint.cz/app/temp/dump14.sql";
			#$fh = fopen($myFile, 'w') or die("can't open file");

			fwrite($fp, $out);

			$i = 0; $return = '';
			while (!feof($fp)) {
				$i++;
				if (preg_match('#</cj-api>#', $return ))
						break;
				#if ($i == 5) return $return;
				$return .= fread($fp, 256);
			}
			fclose($fp);
			
			return $this->getOnlyXml( $return );
		}
	}
	
	public function downloadCommision( $id )
	{
		$errno = null;
		$errstr = null;
		$fp = fsockopen("ssl://commission-detail.api.cj.com", 443, $errno, $errstr, 30);
		if (!$fp) {
			echo "chyba";
			echo $errstr;
			return 0;
		} else {
			$data = "/v3/item-detail/".$id;

			$key = '00a2b9836cd949015da37be23e5f8d46f62a6427ff073e867bd1bc15933e5d93ab4bf89f32330198af8cbd5b3bdbe4d2caaba9431e395b95a11b822cdbb433dcdd/45f456a99ea7a1ecad717e8badec404b6761f347d1c7716d1d772c1746c257cea687f5c6d183b9ed5ad15b5edb5bd6a63f66fd5f16d16b67ec27f94a67e2d501';
			$out = "GET " . $data . " HTTP/1.1\r\n";
			$out .= "Host: link-search.api.cj.com\r\n";
			$out .= "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8\r\n";
			$out .= "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n";
			$out .= "Accept-Language: en-us,en;q=0.5\r\n";
			$out .= "Accept-Charset: utf-8;q=0.7,*;q=0.7\r\n";
			$out .= "Keep-Alive: 300\r\n";
			$out .= "Connection: keep-alive\r\n";
			$out .= "Cookie: __utma=128446558.2099392322683464700.1239639722.1239639722.1239927095.2; __utmz=128446558.1239639722.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); CONTID=8073; cjuMember=0; JSESSIONID=aM5RSWdqdd_5\r\n";
			$out .= "Authorization: " . $key . "\r\n";
			$out .= "\r\n";
			$out .= "HTTP/1.x 200 OK\r\n";
			$out .= "Server: Resin/2.1.17\r\n";
			$out .= "Content-Type: application/xml\r\n";
			$out .= "Transfer-Encoding: chunked\r\n";
			$out .= "Date: Thu, 26 Apr 2009 10:25:03 GMT\r\n";
			$out .= "\n\n";

			#$myFile = "c:/wamp/www/beta.moneypoint.cz/app/temp/dump14.sql";
			#$fh = fopen($myFile, 'w') or die("can't open file");

			fwrite($fp, $out);

			$i = 0; $return = '';
			while (!feof($fp)) {
				$i++;
				if (preg_match('#</cj-api>#', $return ))
						break;
				#if ($i == 5) return $return;
				$return .= fread($fp, 256);
			}
			fclose($fp);
			
			return $this->getOnlyXml( $return );
		}
	}
    
}
