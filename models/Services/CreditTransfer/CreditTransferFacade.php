<?php

namespace MoneyPoint;

class CreditTransferFacade
{
	private $em;

	/** @var CreditTransferService */
	protected $creditTransferService;
	
	public function __construct( \AntoninRykalsky\EntityManager $em,
		CreditTransferService $creditTransferService ) {
		$this->em = $em->getEm();
		$this->creditTransferService = $creditTransferService;
	}
	
	public function createTransfer( $loggedIdu, $v )
	{
		$recipient = $this->em->getRepository("MoneyPoint\Entity\Member")->findOneBy(array("variableSymbol"=> $v->userId ));
		@$v->recipient = $recipient;
		@$v->loggedIdu = $loggedIdu;
		$ok = $this->validateTransfer( $v );
		if( $ok )
		{
			$this->creditTransferService->createTransfer( $loggedIdu, $recipient, $v->credit );
			$this->em->flush();
		}
	}
	
	public function answerToTransfer( $idt, $allow )
	{
		if( $allow == 'allow' ) {
			$this->creditTransferService->allowTransfer( $idt );
		} elseif( $allow == 'deny' ) {
			$this->creditTransferService->stornoTransfer( $idt );
		} else {
			throw new \LogicException("Špatný parametr ". $allow . " pro answerToTransfer");
		}
		$this->em->flush();
	}
	
	public function getUserInboxTransfers( $idu )
	{
		$query = $this->em->createQuery("SELECT a FROM MoneyPoint\Entity\CreditTransfer a WHERE a.stateId=" . Entity\CreditTransfer::S_NEW . " AND a.recipient=:recipient" );
		$query->setParameter('recipient', $this->em->getReference("MoneyPoint\Entity\Member", $idu ));
		
		return $query->getResult();
	}
	
	private function validateTransfer( $values )
	{
		$ok = 1;
		
		if( $values->loggedIdu == $values->recipient->getIdu() )
		{
			\Flashes::error("Převedení kreditu sám sobě nemá valného významu :)");
			$ok = 0;
		}
		
		/* @var $repo CreditTransferRepository */
		$repo = $this->em->getRepository("MoneyPoint\Entity\CreditTransfer");
		if( !$repo->canBeSendedTo($values->userId))
		{
			\Flashes::error("Uvedený uživatel nemá nárok na přijetí kreditu formou převodu!");
			$ok = 0;
		}


		if( $values->credit < Entity\CreditTransfer::TRANSFER_MIN || $values->credit > Entity\CreditTransfer::TRANSFER_MAX )
		{
			$err = sprintf(
					"Lze odeslat pouze kredit v rozmezí %d%s až %d%s", 
					Entity\CreditTransfer::TRANSFER_MIN,
					"Kč",
					Entity\CreditTransfer::TRANSFER_MAX,
					"Kč"
			);
			\Flashes::error($err);
			$ok = 0;
		}
		return $ok;
	}
}
