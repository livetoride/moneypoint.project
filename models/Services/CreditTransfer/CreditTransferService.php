<?php

namespace MoneyPoint;
use AntoninRykalsky as AR;

class CreditTransferService
{
	private $em;
	
	public function __construct( \AntoninRykalsky\EntityManager $em ) {
		$this->em = $em->getEm();
	}
	
	public function createTransfer( $senderId, $recipient, $price )
	{
		/* @var $recipient Entity\Member */
		/* @var $sender Entity\Member */
		$sender = $this->em->getReference("MoneyPoint\Entity\Member", $senderId );
		
		$reason = "Převod kreditu uživateli ".$recipient->getVariableSymbol();
		try {
			\AntoninRykalsky\CreditsAccounts::get()->modifyCredits( $sender->getIdu(), $reason, -$price );
		} catch ( \LogicException $e ) 
		{
			if( $e->getCode() == 1 )
			{
				\Flashes::error("Nedostatečný zůstatek kreditu pro převod!");
				return;
			}
		}
		$caSenderId = \dibi::insertId();
		$caSender = $this->em->getReference("MoneyPoint\Entity\CreditAccount", $caSenderId );
		
		$transfer = new Entity\CreditTransfer();
		$transfer->setTransfer($sender, $recipient, $price, $caSender );
		$this->em->persist( $transfer );
		
	}
	
	public function stornoTransfer( $id )
	{
		/* @var $transfer Entity\CreditTransfer */
		$transfer = $this->em->find("MoneyPoint\Entity\CreditTransfer", $id );
		$sender = $transfer->getSender();
		$recipient = $transfer->getRecipient();
		
			$reason = "Storno převodu kreditu uživateli ".$recipient->getVariableSymbol();
			
			\AntoninRykalsky\CreditsAccounts::get()->modifyCredits( $sender->getIdu(), $reason, $transfer->getPrice() );
			$caGivebackId = \dibi::insertId();
			$caGiveback = $this->em->getReference("MoneyPoint\Entity\CreditAccount", $caGivebackId );

		$transfer->setStorno($caGiveback);
	}
	
	public function allowTransfer( $id )
	{
		/* @var $transfer Entity\CreditTransfer */
		$transfer = $this->em->find("MoneyPoint\Entity\CreditTransfer", $id );
		$sender = $transfer->getSender();
		$recipient = $transfer->getRecipient();
		
			$reason = "Převod kreditu od uživatele ".$sender->getVariableSymbol();
			if( $recipient->getIdu() !== AR\SystemUser::get()->idu )
			{
				throw new \LogicException("Fuck off!");
			}
			
			\AntoninRykalsky\CreditsAccounts::get()->modifyCredits( $recipient->getIdu(), $reason, $transfer->getPrice() );
			$caRecipientId = \dibi::insertId();
			$caRecipient = $this->em->getReference("MoneyPoint\Entity\CreditAccount", $caRecipientId );

		$transfer->setReceived($caRecipient);
	}
	
}
