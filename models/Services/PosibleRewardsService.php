<?php

namespace MoneyPoint;

class PosibleRewardsService
{
	private $em;
	
	/** @var MemberRepository */
	protected $memberRepository;

	/** @var FirstBuyBonus\FirstBuyBonusService */
	protected $firstBuyBonusService;
	
	/** @var \MoneyPoint\FixedCommisionService */
	protected $fixedCommisionService;
	
	public function __construct( 
		\MoneyPoint\Container $c,
		MemberRepository $memberRepository,
		FirstBuyBonus\FirstBuyBonusService $firstBuyBonusService,
		\MoneyPoint\FixedCommisionService $fixedCommisionService
	) {
		$this->em = $c->getEm();
		$this->memberRepository = $memberRepository;
		$this->firstBuyBonusService = $firstBuyBonusService;
		$this->fixedCommisionService = $fixedCommisionService;
	}
	
	public function removePosibleRewards( $creditsOrder )
	{	
		// ziskej potřebné data
		$structure = $this->memberRepository->getStructure( $creditsOrder->getUser()->getIdu() );
		
		$posibleRewardRepository = new PosibleRewardRepository( $this->em );
		$posibleRewardCollection = $posibleRewardRepository->getCollection( $structure, $creditsOrder->getTsResolution() );
		
		$rewards = $this->fixedCommisionService->countRewards( $creditsOrder, $structure );
		foreach( $rewards as $rewardItem )
		{
			$posibleRewardCollection->add(
				$rewardItem->getIdu(),
				-$rewardItem->getClaim(),
				0
			);
		}
		return $posibleRewardCollection;
	}
	
	
	
	public function addPosibleFirstLineRewards( Entity\Member $member,  Entity\CreditOrder $creditOrder )
	{
		$structure = $this->memberRepository->getStructure( $member->getIdu() );
		$rewards = $this->fixedCommisionService->countRewards( $creditOrder, $structure );

		foreach( $rewards as $rewardItem )
		{
			$this->em->persist( $rewardItem );
		}
	}
	
	public function addPosibleRewards( Entity\Member $member,  Entity\CreditOrder $creditOrder, $bonus = null )
	{
//		$date = new \DateTime();
//		$creditOrder = new \MoneyPoint\Entity\CreditOrder();
//		$creditOrder->setOrderTimestamp( $date->format('Y-m-d') );
		
		$structure = $this->memberRepository->getStructure( $member->getIdu() );
			
		$posibleRewardRepository = new PosibleRewardRepository( $this->em );
		$posibleRewardCollection = $posibleRewardRepository->getCollection( $structure, $creditOrder->getTsResolution() );
		
		$rewards = $this->fixedCommisionService->countRewards( $creditOrder, $structure );
		/* @var $rewardItem \MoneyPoint\Entity\CreditReward */
		foreach( $rewards as $rewardItem )
		{
			// auto persist
			$posibleRewardCollection->add(
				$rewardItem->getIdu(),
				$rewardItem->getClaim(),
				0
			);
		}
		
		return $posibleRewardCollection;
	}
    
}
