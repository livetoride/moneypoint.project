<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use AntoninRykalsky\Configs;
use Nette;

class PartnerOrderFacade extends \Nette\Object
{
	private $em;
	
	/** @var TransactionFacade */
	private $transactionFacade;
	
	/** @var PartnerProviderInfoRepository */
	private $ppiRepo;
	
	/** @var PartnerOrderStateChangeService */
	protected $partnerOrderStateChangeService;

	public function __construct(
			\AntoninRykalsky\EntityManager $c,
			\MoneyPoint\TransactionFacade $transactionFacade,
			PartnerProviderInfoRepository $ppiRepo,
			PartnerOrderStateChangeService $partnerOrderStateChangeService
		) {
		$this->em = $c->getEm();
		$this->transactionFacade = $transactionFacade;
		$this->ppiRepo = $ppiRepo;
		
		$this->partnerOrderStateChangeService = $partnerOrderStateChangeService;
			$this->partnerOrderStateChangeService->setPartnerOrderFacade( $this );
	}
	
	static $states = array(
		Entity\PartnerOrder::STATE_CJ_NEW => 'CJ nové',
		Entity\PartnerOrder::STATE_WAITING => 'čekací lhůta',
		Entity\PartnerOrder::STATE_REWARDED => 'připsaná',
		Entity\PartnerOrder::STATE_STORNO => 'storno'
	);

	/**
	 * return parttnerOrderDetail
	 * @param type $id
	 * @return \Moneypoint\Entity\PartnerOrder
	 */
	public function details( $id )
	{
		if (!empty($id)) {
			$d = $this->em->find('Moneypoint\Entity\PartnerOrder', $id );
			$this->em->refresh($d->getPartnerProvider());
			return $d; 
		
		}
	}

	private function cjFindoutPartner( $aid )
	{
		return \DAO\Partners::get()->findAll()->where("link like '%-" . $aid . "'")->orderBy('active', 'desc')->fetch();
	}
	
	/**
	 * @param type $partner
	 * @return Entity\PartnerFeeRate
	 */
	private function cjFindoutFeeRate( $partner )
	{
		if( !$partner instanceof \DibiRow )
		{
			return null;
		}
		$feeRate = \DAO\PartnerFeeRate::get()->findAll()->where("partner_id=%i", $partner->id_partner )->fetch();
		if( empty( $feeRate->id ))
		{
			// již neevidovaný partner
			// echo "partner již neexistuje";
			return;
		}
		$partnerfeeRate = $this->em->find('MoneyPoint\Entity\PartnerFeeRate', $feeRate->id );
		return $partnerfeeRate;
	}

	private function cjFindoutAndSetByuer( $sid )
	{
		if( empty( $sid ))
		{
			$sid = 0; 
		}
		$user = $this->em->find('MoneyPoint\Entity\Member', $sid );
		return $user;
	}
	
	
	const CJ_PARTNER_ID = 'AID';
	const CJ_USER_ID = 'SID';
	
	public function createCj( Entity\PartnerProviderFee $providerFee )
	{
		$partner = $this->cjFindoutPartner( $providerFee->getProviderInfo(self::CJ_PARTNER_ID) );
		$partnerfeeRate = $this->cjFindoutFeeRate( $partner );
		
		$user = $this->cjFindoutAndSetByuer( $providerFee->getProviderInfo(self::CJ_USER_ID) );

		$partnerProvider = $this->em->find('MoneyPoint\Entity\PartnerProvider', 3 );
	
		$partnerOrder = $providerFee->getPartnerOrder();
		if( empty( $partnerOrder ))
		{
			$partnerOrder = new \MoneyPoint\Entity\PartnerOrder();
			$partnerOrder->setOrderCj( 
					$providerFee->getProviderInfo("SALE-AMOUNT"),
					$providerFee->getProviderInfo("COMMISSION-AMOUNT"),
					$providerFee->getProviderInfo("EURO"),
					$partnerProvider
				);
			$partnerOrder->setUser( $user );
			$partnerOrder->setInsertTimestamp( $providerFee->getProviderInfo("EVENT-DATE") );
			$this->em->persist( $partnerOrder );
			$this->em->flush();
		}
		$providerFee->setPartnerOrder( $partnerOrder );
		
		// TODO
		#$this->_emailCreateOrder( $partnerOrder, $this->em );
	}
	
	/**
	 * Dont forget persist return value
	 * @param \MoneyPoint\Entity\PartnerOrder $partnerOrder
	 * @return \MoneyPoint\Entity\Transaction
	 */
	private function makeTransaction( Entity\PartnerOrder $partnerOrder )
	{
		$transaction = new Entity\Transaction();
		$transaction->setPartnerOrder( $partnerOrder );
		$transactionKey = $this->transactionFacade->getNextTransactionId( $transaction );
		$transaction->setMpTransactionId( $transactionKey );
		return $transaction;
	}
	
	public function create( $values )
	{
		if( empty( $values->idu ))
			throw new \LogicException('Vyberte uživatele');
		
		$partnerOrder = new \MoneyPoint\Entity\PartnerOrder();
		$partnerOrder->setUser( $this->em->getReference('\MoneyPoint\Entity\Member', $values->idu ));
		//\Doctrine\Common\Util\Debug::dump($partnerOrder); exit();
		$partnerOrder->setUtrata( $values->utrata );
		$partnerOrder->setRewardManual( $values->reward );
		$values->provider=0;
		
		//print_r($values);exit();
		$partnerOrder->setPartnerProvider( $this->em->getReference("\MoneyPoint\Entity\PartnerProvider", $values->provider) );
//		$partnerOrder->		$par
		
		if (!empty($partnerOrder->getExpSettleTimestamp())) {
			$partnerOrder->setExpSettleTimestamp(new \Datetime($values->expdate));
		}
		
		
		$partnerProviderFee = new \MoneyPoint\Entity\PartnerProviderFee();
		$partnerProviderFee->setShop($values->shop);
		$partnerProviderFee->setPartnerOrder($partnerOrder);
		$partnerProviderFee->setFee_original(0.1);
		
		$transaction = new Entity\Transaction();
		
		// wtf ratio 10/10
		$transaction->setPartnerOrder( $partnerOrder );
		$partnerOrder->setTransation($transaction);
		
		$id = $this->transactionFacade->getNextTransactionId( $transaction );
		$transaction->setMpTransactionId( $id );

		$this->em->persist( $partnerOrder );
		$this->em->persist( $partnerProviderFee);
		$this->em->persist( $transaction );
		$this->em->flush();
		
		return $partnerOrder;
	}
	
	

	public function _emailCreateOrder( Entity\PartnerOrder $partnerOrder )
	{
		$transaction = $partnerOrder->getTransaction();
		$user = $partnerOrder->getUser();
		$partner = $partnerOrder->getPartner();


		$emailKey = 'mailer-partnerorder';
		$mail = Configs::get()->byContent($emailKey.'.email');
		$mailCountingCj = Configs::get()->byContent($emailKey.'.email_counting_cj');
		$mailCountingClassics = Configs::get()->byContent($emailKey.'.email_counting_classic');
		$subject = Configs::get()->byContent($emailKey.'.subject');
		$subject = preg_replace('/\[\[partner_name\]\]/', \Nette\Utils\Strings::upper($partner), $subject);
		$from = Configs::get()->byContent($emailKey.'.from');
		
		
		// zaslání emailu - novy klient
		$m=new \MoneyPoint\Mailer();

		$m->setFrom( $from );
		$m->addTo( $user->getEmail() );
		$m->setSubject( $subject );
		$m->setHTMLBody( $mail );
		
		
		
		$providerFee = $partnerOrder->getPartnerProviderFee();
		
		if( empty($ppi_id))
		{
			// starý výpočet pomocí útraty a procenta
			$m->addReplacement('counting_info', $mailCountingClassics );
			$m->addReplacement('partner_spent', $partnerOrder->getUtrata() );
			$m->addReplacement('partner_reward', $partnerOrder->getFeeRate()->getFeeRatio() );
			$m->addReplacement('partner_mmo', $partnerOrder->getZakladniOdmena() );
		} else {
			$m->addReplacement('counting_info', $mailCountingCj );
			$m->addReplacement('partner_spent', $providerFee->getProviderInfo('SALE-AMOUNT') );
			$m->addReplacement('partner_provider_fee', $partnerOrder->getUtrata() );
			$m->addReplacement('partner_provider_euro', $providerFee->getProviderInfo('EURO')->getValue() );
			$m->addReplacement('partner_mmo', $partnerOrder->getZakladniOdmena() );
		}
		$m->addReplacement('partner_name', $partner );
		$m->addReplacement('partner_settle_ts', $partnerOrder->getExpSettleTimestamp() );
		// $m->addReplacement('transaction_id', $transaction->getMpTransactionId() );
		
		$m->send();
	}
	
	public static function _emailStornoOrder()
	{
		
	}
	
	
	
	public function setSettled( $idPartnerOrder )
	{
		
		$this->em->flush();
	}
	
	public function setAdminCjAgreed( Entity\PartnerOrder $partnerOrder )
	{
		$hasTransaction = $partnerOrder->getTransaction();
		if( empty($hasTransaction ))
		{
			$transaction = $this->makeTransaction( $partnerOrder );
			$this->em->persist( $transaction );
			$partnerOrder->setCjAgreedPrice();
		}
	}

	private function changeFormDetails( $formValues, Entity\PartnerOrder &$partnerOrder )
	{
		$partnerOrder->setUtrata( $formValues->utrata );
		$partnerOrder->setRewardManual( $formValues->reward );
		return $partnerOrder;
	}
	
	public function setCanceled()
	{

	}
	
	public function partnerOrderAutomaticSettle()
	{
		$this->partnerOrderStateChangeService->partnerOrderAutomaticSettle();
		$this->em->flush();
	}
	
	public function changeState( Nette\ArrayHash $formValues )
	{
		$this->partnerOrderAutomaticSettle();
		
		
		$partnerOrder  = null;
		// creation is on other place
		if( !empty($formValues->id_order) ) {
			$partnerOrder = $this->em->find( 'MoneyPoint\Entity\PartnerOrder', $formValues->id_order );
		}

		$partnerOrder = $this->partnerOrderStateChangeService->process( $formValues, $partnerOrder );
		$partnerOrder = $this->changeFormDetails($formValues, $partnerOrder);
		
		
		$this->em->flush();
		return $partnerOrder;
	}
}
