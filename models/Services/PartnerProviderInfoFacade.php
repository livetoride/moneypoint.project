<?php

namespace MoneyPoint;

class PartnerProviderInfoFacade
{
    private $em;
	/** @var PartnerProviderInfoRepository */
	private $partnerProviderInfoRepository;
	public function __construct(
			\MoneyPoint\Container $c,
			\MoneyPoint\PartnerProviderInfoRepository $partnerProviderInfoRepository
		) {
		$this->em = $c->getEm();
		$this->partnerProviderInfoRepository=$partnerProviderInfoRepository;
	}
	
	/**
	 * get partner order provider storage
	 * @param type $id
	 * @return \MoneyPoint\PartnerProviderInfoStorage
	 */
	public function getStorage( $id )
	{
		return new PartnerProviderInfoStorage( $this->em, $id );
	}
	
//	public function getFeesInfo(  ) {
//		
//		$this->partnerProviderInfoRepository->findByOriginalId($origId);
//		
//		$pp = \DAO\PartnerProviderInfo::get()->findAll(array('ppi_id'))->fetchAssoc('ppi_id', 'ppi_id');
//		print_r( $pp );exit;
//		$this->em = $em;
//		
//
//		$q = $this->em->createQuery('SELECT a FROM MoneyPoint\Entity\PartnerProviderInfo a INDEX BY a.key WHERE a.ppiId = :ppiid');
//
//		$this->data = $q->getResult();
//
//	}
	
	/**
	 * create new partner order provider storage
	 * @return \MoneyPoint\PartnerProviderInfoStorage
	 */
	public function newStorage()
	{
		$last = \DAO\PartnerProviderInfo::get()->findAll()->orderBy('id', 'desc')->fetch();
		if( empty( $last->id ))
			$next = 1;
		else
			$next = $last->ppi_id + 1;
		
		return new PartnerProviderInfoStorage( $this->em, $next );
	}
	
	public function secureFindByProviderFee( $id )
	{
		$object = $this->em->find( 'MoneyPoint\Entity\PartnerProviderFee', $id );
		if( empty( $object ))
			throw new \Nette\Application\BadRequestException('Hledané omezení neexistuje');
		
		return $object;
	}


	
}
