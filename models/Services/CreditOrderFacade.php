<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class CreditOrderFacade
{
	private $em;
	/** @var TransactionFacade */
	private $transactionFacade;
	/** @var PosibleRewardsService */
	private $posibleRewardsService;
	/** @var \Markette\Gopay\Service */
	protected $gopayService;
	/** @var CreditOrderService */
	private $creditOrderService;
	/** @var CreditOrderRepository */
	private $creditOrderRepository;
	
	/** @var \AntoninRykalsky\OrderHistoryService */
	private $orderHistoryService;
	
	/** @var DirefentialCommisionService */
	private $direfentialCommisionService;
	
	/** @var FirstBuyBonus\FirstBuyBonusService */
	protected $firstBuyBonusService;
	
	/** @var PeriodRepository */
	protected $periodRepository;
	
	/** @var \AntoninRykalsky\TimeService */
	protected $timeService;

	public function __construct( 
		\MoneyPoint\Container $c,
		\MoneyPoint\TransactionFacade $transactionFacade,
		\MoneyPoint\PosibleRewardsService $posibleRewardsService,
		\AntoninRykalsky\Gopay\GopayService $gopayService,
		DirefentialCommisionService $direfentialCommisionService,
		CreditOrderService $creditOrderService,
		\AntoninRykalsky\OrderHistoryService $orderHistoryService,
		FirstBuyBonus\FirstBuyBonusService $firstBuyBonusService,
		\AntoninRykalsky\TimeService $timeService
	) {
		$this->em = $c->getEm();
		$this->transactionFacade = $transactionFacade;
		$this->posibleRewardsService = $posibleRewardsService;
		$this->gopayService = $gopayService;
		$this->creditOrderService = $creditOrderService;
		
		$this->orderHistoryService = $orderHistoryService;
		$this->direfentialCommisionService = $direfentialCommisionService;
		$this->firstBuyBonusService = $firstBuyBonusService;
		$this->timeService = $timeService;
		
		$this->creditOrderRepository = $this->em->getRepository('MoneyPoint\Entity\CreditOrder');
		$this->periodRepository = $this->em->getRepository('MoneyPoint\Entity\Period');
		
	}
	
	public function getRepo()
	{
		return $this->creditOrderRepository;
	}
	
	private $fuckOffPdf = false;
	public function dontPrintPdf()
	{
		$this->fuckOffPdf = true;
	}
	
	public function processing( $idOrder, array $formValues )
	{

		$creditsOrder = $this->getRepo()->getOrder( $idOrder );
		$newStatus = $this->em->find('\OrderStatus', $formValues['idStatus'] );
		
		
		
		$whatToDo = $this->creditOrderService->process( $creditsOrder, $newStatus );
		
		if( $whatToDo['setPaid'] ) $this->creditOrderService->setPaid( $creditsOrder, $this->fuckOffPdf );
		elseif( $whatToDo['setRewarded'] ) $this->creditOrderService->setRewarded( $creditsOrder );
		elseif( $whatToDo['stornujPredUhradou'] ) $this->creditOrderService->setStorno( $creditsOrder );
		elseif( $whatToDo['vratUhraduNaUcet'] ) {
			$periodCreditsOrders = $this->em->getRepository('MoneyPoint\Entity\CreditOrder')->findSamePeriodOrders( $creditsOrder );
			$nextCreditsOrders = $this->em->getRepository('MoneyPoint\Entity\CreditOrder')->findNewerOrders( $creditsOrder );
			#$nextCreditsOrders = array();#$this->em->getRepository('MoneyPoint\Entity\CreditOrder')->findNewerOrders( $creditsOrder );
						
			$this->creditOrderService->setStornoAfterPay( $creditsOrder, $periodCreditsOrders, $nextCreditsOrders );
		}
			
		
		else {
			$creditsOrder->setIdStatus( $formValues['idStatus'] );
		}
		
		$this->em->flush();
	
	}
	
	
	
	public function getCreditOrder( $id )
	{
		return $this->em->find('\MoneyPoint\Entity\CreditOrder', $id );
	}
	
	private function creditOrderFactory( Entity\Member $buyer, \OrderPayment $payment, Basket $goods, OrderDetails $details, $period, $time, $transactionId )
	{
		if( $goods->getPriceSum() == 0 ){
			throw new \LogicException("Objednávka zdarma?");
		}
		
		$creditOrder = new Entity\CreditOrder();
		$creditOrder->setUser( $buyer );
		$creditOrder->setPayment( $payment );
		$creditOrder->setPriceSum( $goods->getPriceSum() );
		$creditOrder->setCreditsSum( $goods->getCreditsSum() );
		$creditOrder->setIdEndCustomer( $details->getEndCustomerId() );
		$creditOrder->setPeriodPayment( $period );
		$creditOrder->setOrderTimestamp( $time );
		
		foreach( $goods->getGoods() as $good )
		{
			/* @var $good BasketItem */
			$item = new Entity\CreditOrderItem();
			$item->setCreditOrder( $creditOrder );
			$item->setCenaSdph( $goods->getPriceSum() );
			$item->setCredits( $goods->getCreditsSum() );
			$item->setDph(0);
			$item->setIdProduct( $good->getProductId() );
			$item->setProduct('kredit');
			$item->setQuantity( $good->getQuantity() );
			$this->em->persist( $item );
		}
		
		$transaction = new Entity\Transaction();
		$transaction->setCreditOrder( $creditOrder );
		$transaction->setMpTransactionId( $transactionId );
		$creditOrder->setTransaction($transaction);
		
		return $creditOrder;
	}
	
	public function buyCredits( $idu, Basket $goods, OrderDetails $details )
	{
		/* @var $buyer \MoneyPoint\Entity\Member */
		$buyer = $this->em->find('\MoneyPoint\Entity\Member', $idu );
		$payment = $this->em->find('OrderPayment', $details->zpusob_platby );
		$creditDiscount = $this->em->getRepository('\MoneyPoint\Entity\CreditDiscount')->findDiscountByCredits( $goods->getCreditsSum() );
		$time = $this->timeService->getDateTime();
		$period = $this->periodRepository->getActualPeriod( $time );
		$transactionId = $this->em->getRepository('\MoneyPoint\Entity\Transaction')->getNextTransactionIdSolid( Entity\Transaction::CREDIT_ORDER, $buyer );
		
		
		$creditOrder = $this->creditOrderFactory( $buyer, $payment, $goods, $details, $period, $time, $transactionId );
		$creditOrder->setIdMpOrder( $this->createMpIdOrder() );
		$creditOrder->setRelatedDiscount( $creditDiscount );
		
		$this->em->persist( $creditOrder );
		$this->em->persist( $creditOrder->getTransaction() );
		
		
		// 1. setting first buy bonus settings
		$bonus = 0;
		if( 0 )
		{
			$bonus += $this->firstBuyBonusService->useBonus( $buyer, $creditOrder );
		}
			
		// 2. adding possible rewards
		$this->posibleRewardsService->addPosibleRewards( $buyer, $creditOrder, $bonus );
		
		$this->posibleRewardsService->addPosibleFirstLineRewards( $buyer, $creditOrder, $bonus );

		// 3. přidá body pro výpočet objemových odměn
		$this->direfentialCommisionService->addDifferentialPoints( $buyer, $creditOrder->getCreditsSum() );
		
		$this->orderHistoryService->create( $creditOrder );
		
		// UHRADA Z B-ÚČTU
		if( $details->zpusob_platby == 4 || $details->zpusob_platby == 5 )
		{
			// over pocet bodů
			$points = @\AntoninRykalsky\PointsAccounts::get()->actualState($idu, true)->points_left;
			$cenaCelkem = $goods->getPriceSum();
			if( $points < $cenaCelkem && $details->zpusob_platby == 4 )
			{
				throw new \LogicException('Nedostatek bodů pro úhradu objednávky!');
			}

			// nastavíme objedávku jako uhrazenou
			$this->creditOrderService->setPaid( $creditOrder );

			// odečteme body z uctu klienta
			\AntoninRykalsky\PointsAccounts::get()->uhradaBUcet( $idu , "Body čerpané za nákup kreditu", -$cenaCelkem,  5, $creditOrder->getIdOrder() );
			if( $result == 'FAIL' ) throw new \LogicException('Chyba při přičítání krediutů.');

		}

		$this->em->flush();
		
	//	$mailer = new \MoneyPoint\Emails();
	//	$mailer->newOrder( $creditOrder->getIdOrder() );
		
		
		// UHRADA GOPAY
		$gopayPayments = array(6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27);
		if( in_array( $details->zpusob_platby, $gopayPayments  ))
		{
			$this->gopayService->addPayment( $creditOrder );
			
		}
		
		
		$this->em->flush();
		return $creditOrder;
	}
	
	/**
	 * If order should be paid by gopay, redirect there
	 * @param \MoneyPoint\Entity\CreditOrder $creditsOrder
	 * @return type
	 */
	public function goPay( Entity\CreditOrder $creditsOrder )
	{
		$paymentSid = $creditsOrder->getGopayPaymentSid();
		if(!empty( $paymentSid ))
			return $this->gopayService->redirectToGopay( $creditsOrder );
	}
	
	
	
	private function createMpIdOrder()
	{
		$lastOrder = \DAO\Orders::get()->findAll()->orderBy('id_order')->desc()->fetch();
		return \AntoninRykalsky\Handy::nextYearId(@$lastOrder->id_mporder, 6);
	}
	
	/**
	 * serve to setpaid by id signal in admin
	 * @param type $idOrder
	 * @return int
	 */
	public function setPaidByIdOrder( $idOrder, $fuckOffPdf = false )
	{
		$creditsOrder = $this->em->find('\MoneyPoint\Entity\CreditOrder', $idOrder );
		$this->creditOrderService->setPaid($creditsOrder, $fuckOffPdf );
		$this->em->flush();
		return $creditsOrder;
	}
	
	public function getAvailableStates( $creditOrder )
	{
		$stateMachine = new \MoneyPoint\CreditOrderStateMachine();
		return $stateMachine->getAvailableStatesUserTexts( $creditOrder->getIdStatus() );
	}
	
	public function cronSetRewared()
	{
		$safetyDeadline = \AntoninRykalsky\Configs::get()->byContent('general.safety_deadline');
		$safetyInterval = new \DateInterval("P".$safetyDeadline."D");
		
		$minimalOrderDate = new \DateTime();
		$minimalOrderDate->sub($safetyInterval);
		
		$q = $this->em->createQuery(
				'select co.idOrder from \MoneyPoint\Entity\CreditOrder co '
				. 'where co.idStatus = :status AND co.orderTimestamp < :date '
				);
		$q->setParameter('status', \MoneyPoint\Entity\CreditOrder::STATE_PAID );
		$q->setParameter('date', $minimalOrderDate );
		$waiting = $q->getResult();
		
		foreach( $waiting as $creditOrder )
		{
			$this->processing( $creditOrder['idOrder'], array('idStatus' => Entity\CreditOrder::STATE_REWARDED ));
		}
	}
	
	public function generateInvoice( $id )
	{
		$creditsOrder = $this->em->find("MoneyPoint\Entity\CreditOrder", $id );
		$this->creditOrderService->generateInvoice( $creditsOrder );
	}
	
}
