<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;


class CreditOrderSimpleService
{
	public function __construct() {
		
	}
	
	public function createOrderEntity( $creditOrder, $buyer ) { #, $payment, $goods, $orderIdentificator
		$creditOrder->setUser( $buyer );
//		$creditOrder->setPayment( $payment );
//		$creditOrder->setPriceSum( $goods->getPriceSum() );
//		$creditOrder->setCreditsSum( $goods->getCreditsSum() );
//		$creditOrder->setIdMpOrder( $this->createMpIdOrder() );
		
		return $creditOrder;
	}
}
