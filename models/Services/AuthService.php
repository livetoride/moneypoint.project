<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use AntoninRykalsky\Flashes;
use Nette\Environment;
use AntoninRykalsky as AR;

class AuthService
{
	/** @var \AntoninRykalsky\EntityManager */
	protected $em;

	public function __construct( 
		\AntoninRykalsky\EntityManager $em	
	){
		$this->em = $em->getEm();
	}

}
