<?php

namespace MoneyPoint\FirstBuyBonus;
use MoneyPoint\Entity;
use Nette;

class FirstBuyBonusService extends Nette\Object
{
	private $em;
	protected $firstCreditsBonus;
	protected $newFlag=0;
	protected $allowFirstCreditsBonus;
		
	public function __construct(
			\AntoninRykalsky\EntityManager $em
		) {
		$this->em = $em->getEm();
		$this->allowFirstCreditsBonus = \AntoninRykalsky\Configs::get()->byContent('multilevel.allowFirstCreditsBonus');
	}
	/**
	 * Spočítá výši BPO. Uživatel smí dostat BPO pouze jednou. Dvakrát v případě, že poprvé koupí nejlevnější kredit a pak jakékoli draždí.
	 * @param type $creditsSum nakupovane kredity
	 * @param type $hadCreditFirstBuy priznak zda dostal BPO (1/2)
	 * @param type $predciLevel level kam bonus patri
	 * @return type
	 */
	public function countBonus( $creditsSum, $hadCreditFirstBuy, $predciLevel )
	{
		if( $predciLevel != 1 )
			return $this->firstCreditsBonus=0;
		
		if( $this->allowFirstCreditsBonus  )
		{
			if( $hadCreditFirstBuy != 2 )
			{
				$discount = \DAO\MpCreditsDiscount::get()->findAll()->where('credits=%i', $creditsSum )->fetch();
				if( $hadCreditFirstBuy != 2 && $discount->flag == "for-business-status" )
				{
					$this->firstCreditsBonus = $discount->bpo_coef;
					$this->newFlag = 2;
				} elseif( $hadCreditFirstBuy == 0 && $discount->flag != "for-business-status" ) {
					$this->firstCreditsBonus = $discount->bpo_coef;
					$this->newFlag = 1;
				}
				
			}
		}
		
		return $this->firstCreditsBonus;
	}
	
	public function getBonusPrice( Entity\CreditOrder $creditsOrder )
	{
		$firstCreditsBonus = 0;
		$firstCreditsBonusEntity = $creditsOrder->getFirstBuyBonus();
		if( $firstCreditsBonusEntity instanceof Entity\FirstBuyBonus )
		{
			$firstCreditsBonus = $firstCreditsBonusEntity->getBonus();
		}
		return $firstCreditsBonus;
	}
	
	/**
	 * Informace, kterou uložit uživatelovi, zda již dostal první kredity (a v jaké verzi)
	 * @return int new first credits bonus flag
	 */
	public function getNewFlag() {
		return $this->newFlag;
	}
	
	public function useBonus( Entity\Member $member, Entity\CreditOrder $order )
	{
		
		/** @var $parentMember \MoneyPoint\Entity\Member */
		$parentMember = $member->getParentMember();
		
		if(!$parentMember instanceof Entity\Member ) {
			return;
		}
		
		$countedBonus = $this->countBonus( $order->getCreditsSum(), $parentMember->hadCreditFirstBuy(), 1 );
		$bonus = new \MoneyPoint\Entity\FirstBuyBonus();
		$this->em->persist( $bonus );
		$bonus->setBonus($order, $this->getNewFlag(), $countedBonus );
		
		$parentMember->setCreditFirstBuy( $this->getNewFlag() );
		
		return $bonus;
	}
}

?>