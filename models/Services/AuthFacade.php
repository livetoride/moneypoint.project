<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use AntoninRykalsky\Flashes;
use Nette\Environment;
use AntoninRykalsky as AR;

class AuthFacade extends \AntoninRykalsky\Common\Auth
{
	/** @var \AntoninRykalsky\EntityManager */
	protected $em;
	
	/** @var AuthService */
	protected $authService;

	public function __construct( 
		\AntoninRykalsky\EntityManager $em	,
		AuthService $authService
	){
		$this->em = $em->getEm();
		$this->authService = $authService;
	}
	
	public function logout() {
		$user = Environment::getUser();

        $s = Environment::getSession('user');
        unset( $s->idu );
        unset( $s->role );
        unset( $s->imgdir );
		unset( $s->surname );
		unset( $s->name );
		unset( $s->ID );
        $user->logout(TRUE);
	}
	
	public function logoutLink()
	{
		$dp = static::$defaultPages;
        $role = 'guest';
        return $dp[$role]['presenter'].':'.$dp[$role]['action'];
	}

	public function newRegistration( $values )
	{
		
		if( !empty($values['vs_sponzor_h']))
		{
			$inv = \DAO\UserInvitation::get()->findAll()->where('referrerlink=%s', $values['vs_sponzor_h'])->fetch();
			
			if( !empty( $inv->idu ))
			{
				$user = \DAO\Uzivatele::get()->find( $inv->idu )->fetch();
				$values['vs_sponzor'] = $user->variable_symbol;
			}
			#unset( $values['vs_sponzor_h'] );
		}
		
			// opravdu je nutno odsouhlasit obchodní podmínky
			$vop = \DAO\Terms::get()->findAll()->where('[active]=1')->fetch();
			if( !empty($vop->id_term) && !$values['agreement_conditions'] )
			{
				throw new \LogicException('Registrace bez souhlasu s obchodními podmínkami není možná');
			}

			// heslo musí být stejné - ověření
			if($values['passwd']!=$values['passwd2'])
			{
				throw new \LogicException('Vámi zadné hesla se neshodují');
			}
			
			// delka hesla
			$pasLen = strlen($values['passwd']);
			if( $pasLen > 12 || $pasLen < 6 )
			{
				throw new \LogicException('Podmínky pro heslo jsou 6-12 znaků');
			}

			// overeni zda email jiz neexistuje
			$u = AR\Uzivatele::get()->findAll()->where("email=%s", $values['email'] )->fetchAll();
			if( count($u) && $values['email'] != 'antonin.rykalsky@gmail.com')
			{
				throw new \LogicException('Zadaný email již v systému existuje a nelze se pod ním registrovat vícekrát');

				$response = $this->getHttpResponse();
				$response->setCookie('email', $values['email'], time()+10*60 );

				$text='Tip: požádejte si o znovuzaslání hesla '.Html::el('a')->href($this->link(':Front:Auth:recoveryNew'))->setText('zde');
				$message = Html::el('span')->setHtml($text);
				$this->presenter->flashMessage($message, AR\Flashes::$info );

				return;
			}

			// overeni zda email jiz neexistuje
			$uw = AR\UsersWaiting::get()->findAll()->where("email=%s", $values['email'] )->fetchAll();
			if(count($uw) && $values['email'] != 'antonin.rykalsky@gmail.com')
			{
				throw new \LogicException('Pod tímto emailem již registrace proběhla, zkontrolujte prosím spamovou složku na vašem emailu, zda vám zde nedošel potvrzovací email.');
			}

			if( AR\Configs::get()->byContent('settings.allowRefererLink') )
			if(!empty($this->referrer))
			{
				$values['vs_sponzor'] = $this->referrer;
			}

		/*	$vyhledavac= 0;
			if( $vyhledavac )
			{
				$city = AR\MestoDao::get()->findAll()->where('[nazev_o]=%s', $values['mesto'] )->fetchAll();
				if( !count( $city ))
				{
					throw new \LogicException("Město ${values['mesto']} není korektně zadáno, vyberte prosím z našeptávače.");
					return;
				}
			}
			/*********************/ 
		
		
		/*	if(AR\Configs::get()->byContent('auth.country'))
				$values['country'] = AR\CiselnikStaty::get()->find( $values['country'] )->fetch()->stat; */

			if(empty($values['ic'])) unset( $values['ic']);
			$values['telefon'] = str_replace(' ', '', $values['telefon'] );

			// heslo musí být stejné - ověření
			if($values['passwd']!=$values['passwd2'])
				throw new \LogicException("Vámi zadné hesla se neshodují");

			$values['login']=$values['email'];
			
	
			// overeni zda telefon jiz neexistuje
			$u = AR\Uzivatele::get()->findAll()->where("idu>0 and telefon=%s", $values['telefon'] )->fetchAll();
			$uw = AR\UsersWaiting::get()->findAll()->where("telefon=%s", $values['telefon'] )->fetchAll();
			if((count($u)||count($uw)) )
			{
				throw new \LogicException('Zadaný telefon již v systému existuje.');
			}
			
//			if( !empty( $values['vs_sponzor']) )
//			{
//				$countU = AR\Uzivatele::get()->findAll()->where("variable_symbol=%s", $values['vs_sponzor'] )->count();
//				$countUW = 0;#AR\UsersWaiting::get()->findAll()->where("vs_sponzor=%s", $values['vs_sponzor'] )->count();
//				if( $countU == 0 && $countUW == 0)
//				{
//					exit;
//					throw new \LogicException('Zadané ID sponzora '.$values['vs_sponzor'].' neexistuje!');
//				}
//			}
			

			// info o čase registrace
			$values['pass'] = md5( $values['passwd'] );
			$values['pass_orig'] = base64_encode( $values['passwd'] );

			$now = strtotime("now");
			$values['reg_date']=date("Y-n-j", $now );
			$values['reg_time']=date("H:i:s", $now );
			$values['variable_symbol']=001100;#AR\UsersWaiting::get()->getVariableSymbol();

			if( $values['vs_sponzor']=='' )	 unset( $values['vs_sponzor'] );
			else $values['login_sponzor'] = $values['vs_sponzor'];
			
			// <editor-fold defaultstate="collapsed" desc=" Pozvánky">
			$this->context = Environment::getContext();
			#print_r($values);exit;
			#print_r($values);
			if ($this->context->parameters['modulesSettings']['referrerlink.allow'] && !empty($values['vs_sponzor_h'])) 			{
				$countUI = \DAO\UserInvitation::get()->findAll()->where('[referrerlink]=%s', $values['vs_sponzor_h'])->count();
				#print_r( $countUI );exit;
				if ($countUI == 0)
					throw new \LogicException('Referenční odkaz není platný!');


				$referrerLink = \DAO\UserInvitation::get()->findAll()->where('[referrerlink]=%s', $values['vs_sponzor_h'])->fetch();
				\DAO\UserInvitation::get()->update($referrerLink->id, array('used' => 1));
				$referrerUser = \DAO\Uzivatele::get()->find($referrerLink->idu)->fetch();
				$values['vs_sponzor'] = $referrerUser->variable_symbol;
			}
			
			// </editor-fold>

			AR\UsersWaiting::get()->insert( $values );

			#exit;

			//
			// ODESLANI EMAILU
			// potvrzení emailu
			//
			$regUri = Environment::getHttpRequest()->getUrl()->baseUrl;
			# http://localhost/beta.moneypoint.cz/auth/confirm-mail?confirmEmail-login=asdf@asdf.cz&key=asdf&do=confirmEmail-confirm
			$regUri .="auth/confirm-mail?confirmEmail-login=".$values['email']."&key=".md5( $values['email'].'salt' ).'&do=confirmEmail-confirm';
			$reglink = "<a href='".$regUri."'>".$regUri."</a>";

			$regmail = AR\Configs::get()->byContent('mailer-registration.email_client');
			$regmail = preg_replace('/\[\[reglink\]\]/', $reglink, $regmail);
			$regmail = preg_replace('/\[\[jmeno]\]/', $values['jmeno'], $regmail);
			$regmail = preg_replace('/\[\[prijmeni]\]/', $values['prijmeni'], $regmail);
			$regmail = preg_replace('/\[\[datum]\]/', date('j.n.Y', strtotime($now) ), $regmail);
			$regmail = preg_replace('/\[\[cas]\]/', date('H:i', strtotime($now) ), $regmail);

			$from = AR\Configs::get()->byContent('general.mailer_from');
			$to = $values['email'];
			$cc = AR\Configs::get()->byContent('mailer-registration.cc');
			$subject = AR\Configs::get()->byContent('mailer-registration.subject');

			// zaslání emailu - novy klient
			$m = new \MoneyPoint\Mailer();
			
			$m->setFrom( $from );
			$m->addTo( $to );
			$m->addCc( $cc );
			$m->setSubject( $subject );
			$m->setHTMLBody( $regmail );
			$m->send();
			Flashes::success( 'Registrace byla přijata, vyčkejte na potvrzovací email' );

		
		return 1;
	}
}
