<?php

namespace MoneyPoint;

class CommisionsFacade
{
	private $em;
	private $commisions = array(
			1 => 'Objemové odměny',
			2 => 'Fixní odměny',
		);
	
		
	/** @var DirefentialCommisionService */
	private $direfentialCommisionService;
	
	/** @var RanksService */
	private $ranksService;
		
	/** @var PeriodRepository */
	protected $periodRepository;
		
	private $repository;
	public function __construct(
			\AntoninRykalsky\EntityManager $em,
			DirefentialCommisionService $direfentialCommisionService,
			RanksService $ranksService
		) {
		$this->em = $em->getEm();
		$this->direfentialCommisionService = $direfentialCommisionService;
		$this->ranksService = $ranksService;
		$this->periodRepository = $this->em->getRepository('MoneyPoint\Entity\Period');
		
	}

	public function getDatasource()
	{
		$r = array();
		foreach( $this->commisions as $k=>$v)
			$r[ $k ] = array( 'id' => $k, 'commision' => $v );
		return $r;
	}
	
	public function getCommisionKey( $id )
	{
		$key = array(1=>'differential', 2=>'fixed');
		return $key[$id];
	}

	
	public function getRankTable( $baseCommisionId = null)
	{
		if( $baseCommisionId === null ) {
			return $this->periodRepository->getRankTable();
		} else {
			$baseCommision = $this->em->getReference("\AntoninRykalsky\Entity\CommisionEntity", $baseCommisionId );
			return $this->em->getRepository('MoneyPoint\Entity\CarrierGroup')->findOneBy(array('baseCommision' => $baseCommision ));
		}
	}
	
	/**
	 * Vrací klíče ranků pro nastavení formuláře
	 * @return type
	 */
	public function getRanksKeys()
	{
		return array(0,1,2,3,4,5,6,7,8);
	}
	
	public function storeRankTable( $versionId,  $arrayOfRanks, $name )
	{
		/* @var $rankGroup Entity\CarrierGroup */
		$baseCommision = $this->em->find("\AntoninRykalsky\Entity\CommisionEntity", $versionId );
		$rankGroup = $this->em->getRepository('MoneyPoint\Entity\CarrierGroup')->findOneBy( array('baseCommision'=> $baseCommision ) );

		$baseCommision->rename( $name );
		
		if( $baseCommision->getIsUsed() )
		{
			return;
		}
		
		$this->ranksService->store( $arrayOfRanks, $rankGroup );
		$this->em->flush();
	}
	
	public function storeActive( $versionId, $useThis )
	{
		$bc = $this->em->find('AntoninRykalsky\Entity\CommisionEntity', $versionId );
	
		$this->em->getRepository('AntoninRykalsky\Entity\CommisionEntity')->setActive( $bc, $useThis );

		$this->em->flush();
	}
	
	public function createNewDifferentialSet()
	{
		$date =  new \DateTime;
		$bc = new \AntoninRykalsky\Entity\CommisionEntity;
		$bc->setBaseCommision( \AntoninRykalsky\Entity\Commision::DIFFERENTIAL, "Verze z ".$date->format("j.n H:i:s") );
		$this->em->persist( $bc );
		
		$group = new Entity\CarrierGroup;
		$group->setGroup("Verze", $date, $bc );
		$this->em->persist( $group );
		
		$this->em->flush();
		return $bc->getId();
	}

}
