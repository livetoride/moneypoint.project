<?php

namespace MoneyPoint;
use Nette;

class TransactionFacade extends Nette\Object
{
	private $em;
	
	/** @var \MoneyPoint\TransactionRepository */
	protected $_transactionRepository;

	/** @var \MoneyPoint\PeriodNetworkRepository */
	protected $_periodNetworkRepository;
	
	/** @var TransactionService */
	protected $service;

	
	public function __construct(
			\MoneyPoint\Container $c
		) {
		$this->em = $c->getEm();
		$this->service = new TransactionService($this->em);
	}
	
	public function getTransactionRepository() 	{
		if (empty($this->_transactionRepository)) 		{
			$this->_transactionRepository = $this->em->getRepository('MoneyPoint\Entity\Transaction');
		}
		return $this->_transactionRepository;
	}
	
	public function getPeriodNetworkRepository() 	{
		if (empty($this->_periodNetworkRepository)) 		{
			$this->_periodNetworkRepository = $this->em->getRepository('MoneyPoint\Entity\PeriodNetwork');
		}
		return $this->_periodNetworkRepository;
	}
	
	public function getDatasource()
	{
		return $this->getTransactionRepository()->getDatasource();
	}
	
	public function secureFind( $id )
	{
		$object = $this->getTransactionRepository()->find( $id );
		if( empty( $object ))
			throw new \Nette\Application\BadRequestException('Hledané omezení neexistuje');
		
		return $object;
	}
	
	public function store( $v )
	{
		$this->service->store( $v );
		$this->em->flush();
	}

	
	
	/** @deprecated since version 1 */
	public function getNextTransactionId( $transaction )
	{
		if( !in_array( $transaction->getType(), array(1,2,5))  )
			throw new \Exception ('Nee, podporuje pouze partner order a credits order ');
		
		$user = $transaction->getUser();
		$y=date('y', strtotime('now'));
		$nextTransactionId = $user->getVariableSymbol().'-'.$transaction->getType().'-'.$y;
		
		// dohledání konečného pořadového čísla
		$a = \DAO\MpTransaction::get()->findAll()->where("mp_trans_id LIKE %like~", $nextTransactionId)->orderBy('mp_trans_id')->desc()->fetch();
		if( !empty($a->mp_trans_id))
		{
			preg_match('#-([^-$]+)$#', $a->mp_trans_id, $matches );
			if( empty($matches[1])) throw new \Exception('Neco se pokazilo pri zpracovani regularniho vyrazu v MP transaction id'.$a->mp_trans_id );
			$nextTransactionId .= '-'.sprintf('%1$03d', (int)$matches[1]+1);
		} else {
			$nextTransactionId .= '-001';
		}
		return $nextTransactionId;
	}
	
	public function getDifferentialCommisionDetails( $differentialCommision )
	{
		return $this->getPeriodNetworkRepository()->findPartialItems( $differentialCommision );
	}
	
	
	public function findVolumeCommision( $id )
	{
		// TONIK-TODO tohle tady nepatri
		$entity = '\MoneyPoint\Entity\PeriodNetwork';
		$q = $this->em->find("$entity", $id );
		return $q;
	}
	
	public function getCreditReward( $reward_id )
	{
		if(!empty( $reward_id ))
		return $this->em->find('\MoneyPoint\Entity\CreditReward', $reward_id );
	}
	
}
