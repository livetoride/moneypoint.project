<?php
namespace MoneyPoint;


class CjService
{
	private $czKeys =  array(
			"ACTION-STATUS",
			"ACTION-TYPE",
			"AID",
			"COMMISSION-ID",
			"COUNTRY",
			"EVENT-DATE",
			"LOCKING-DATE",
			"ORDER-ID",
			"ORIGINAL",
			"ORIGINAL-ACTION-ID",
			"POSTING-DATE",
			"WEBSITE-ID",
			"ACTION-TRACKER-ID",
			"ACTION-TRACKER-NAME",
			"CID",
			"ADVERTISER-NAME",
			"COMMISSION-AMOUNT",
			"ORDER-DISCOUNT",
			"SID",
			"SALE-AMOUNT",
			"COMMISSION",
			"EURO"
		);
	
	private $keys = array(
			"ACTION-STATUS",
			"ACTION-TYPE",
			"AID",
			"COMMISSION-ID",
			"COUNTRY",
			"EVENT-DATE",
			"LOCKING-DATE",
			"ORDER-ID",
			"ORIGINAL",
			"ORIGINAL-ACTION-ID",
			"POSTING-DATE",
			"WEBSITE-ID",
			"ACTION-TRACKER-ID",
			"ACTION-TRACKER-NAME",
			"CID",
			"ADVERTISER-NAME",
			"COMMISSION-AMOUNT",
			"ORDER-DISCOUNT",
			"SID",
			"SALE-AMOUNT",
			"COMMISSION",
			"EURO"
		);
	
	public function getCzKey( $key )
	{
		$k = array_search ( $key, $this->keys );
		return $this->czKeys[ $k ];
	}
	
	public function getKeys()
	{
		return $this->keys;
	}

}
