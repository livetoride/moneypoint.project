<?php

namespace MoneyPoint;

class Container /*extends \Nette\DI\Container */{

	private $em;
	private $container;
	
	public function __construct(
	\AntoninRykalsky\EntityManager $em, \Nette\DI\Container $c
	) {
		//parent::$em = $em->getEm();
		$this->em = $em->getEm();
		$this->emLight = $em->getEm();
		$this->container = $c;
	}

	public function getBaseUrl() {
		$baseUrl = 'http://';
		$baseUrl .= $this->container->httpRequest->url->host;
		$baseUrl .= $this->container->httpRequest->url->basePath;
		return $baseUrl;
	}
	
	public function getGopaySettings() {
		$p = $this->container->parameters;
		return $p['gopay'];
	}
	
	public function getEm() {
		return $this->em;
	}


	

	
}
