<?php

namespace MoneyPoint;
use Doctrine;

class CreditDiscountService
{
	private $em;
		
	private $repository;
	public function __construct(
			Doctrine\ORM\EntityManager $em
		) {
		$this->em = $em;
	}

	/** @deprecated  */
	public function findDiscountByCredits( $creditsSum )
	{
		$q = $this->em->createQuery('SELECT a FROM MoneyPoint\Entity\CreditDiscount a WHERE a.credits = :credits');
		$q->setParameter('credits', $creditsSum );
		foreach( $q->getResult() as $r )
			return $r;
	}
}
