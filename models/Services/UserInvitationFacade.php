<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class UserInvitationFacade
{
	private $em;
	public function __construct(
			\MoneyPoint\Container $c
		) {
		$this->em = $c->getEm();
	}
	
	public function createInvitation( $data, $isPaid = false )
	{
		return \MoneyPoint\Invitation::create( $data, $isPaid );
	}
	
	public function getInvitationByHash( $hash )
	{
		/** @var \MoneyPoint\Entity\UserInvitation */
		$q = $this->em->getRepository('MoneyPoint\Entity\UserInvitation')->findBy(
             array('referrerlink'=> $hash)
           );
		foreach( $q as $q1 )
			return $q1;
	}
}
