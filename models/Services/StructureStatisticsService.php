<?php

namespace MoneyPoint;

class StructureStatisticsService
{
	private $em;
	
	public function __construct( 
		\AntoninRykalsky\EntityManager $em )
	{
		$this->em = $em->getEm();
	}
	
	/**
	 * After rewarded
	 * @param type $level
	 * @param type $idu
	 * @param type $reward
	 * @param type $creditsSum
	 */
	public function actualizeStructureFirstLine( $level, $idu, $reward, $creditsSum )
	{
		if( $level === 1 )
		{
			$t = \DAO\MpStructureOverview::get()->table;
			\dibi::query('UPDATE '.$t.' SET rewards_parent = rewards_parent + %i WHERE idu=%i', $reward, $idu );
			\dibi::query('UPDATE '.$t.' SET credits_parent = credits_parent + %i WHERE idu=%i', $creditsSum, $idu );
		}
	}
	
	/**
	 * After rewarded
	 * @param type $predciLevel
	 * @param type $reward
	 * @param type $rewardedMemberId
	 * @param type $parentId
	 */
	public function actualizeStructureSmallNetwork( $predciLevel, $reward, $rewardedMemberId, $parentId=null )
	{
		$t = \DAO\MpStructureOverview::get()->table;
		\dibi::query('UPDATE '.$t.' SET rewards_%i = rewards_%i + %i WHERE idu=%i', $predciLevel, $predciLevel, $reward, $rewardedMemberId );

		$t2 = \DAO\MpFirstLine::get()->table;
		if( !empty($parentId)) {
			\dibi::query('UPDATE '.$t2.' SET rewards = rewards + %i WHERE idu=%i', $reward, $parentId );
		}
	}
	
	public function afterPaymentAddTo( $memberIdu, $predciLevel, $creditsOrder, $parentIdu=null )
	{
		if( $predciLevel <= 8 )
		{
			\AntoninRykalsky\CreditsStructure::get()->modifyCredits( $memberIdu , "Klient s id $memberIdu má nárok na zvyseni celkoveho poctu kreditu v siti o ".$creditsOrder->getCreditsSum()." kreditu z nákupu kreditu v $predciLevel. úrovni klientské sítě.", $creditsOrder->getCreditsSum(), $predciLevel, $creditsOrder->getIdOrder() );

			$t = \DAO\MpStructureOverview::get()->table;
			\dibi::query('UPDATE '.$t.' SET credits_%i = credits_%i + %i WHERE idu=%i', $predciLevel, $predciLevel, $creditsOrder->getCreditsSum(), $memberIdu );

			$t = \DAO\MpFirstLine::get()->table;
			if( !empty($parentIdu)) {
				\dibi::query('UPDATE '.$t.' SET credits = credits + %i WHERE idu=%i', $creditsOrder->getCreditsSum(), $parentIdu );
			}
		}
	}
	
	
}
