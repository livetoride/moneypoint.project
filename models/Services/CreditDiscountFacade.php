<?php

namespace MoneyPoint;

class CreditDiscountFacade
{
	private $em;
		
	private $repository;
	public function __construct(
			\AntoninRykalsky\EntityManager $em
		) {
		$this->em = $em->getEm();
	}

	/**
	 * Vrací klíče ranků pro nastavení formuláře
	 * @return type
	 */
	public function getDiscountKeys()
	{
		return array(1,2,3,4,5,6,7,8);
	}
	
	public function storeCreditDiscountTable( $creditDiscountId, $arrayOfDiscounts, $useThis )
	{
		
		$newDiscounts = $this->rewriteDiscounts( $creditDiscountId, $arrayOfDiscounts, $useThis );
		$this->em->flush();
	}
	
	public function rewriteDiscounts( $creditDiscountId, $arrayOfDiscounts, $useThis )
	{
		$group = $this->em->find("MoneyPoint\Entity\CreditDiscountGroup", $creditDiscountId );
		
		$discounts = $group->getDiscounts();
		
		if( $useThis )
		{
			$this->em->createQuery("UPDATE MoneyPoint\Entity\CreditDiscountGroup g SET g.active=0")->getResult();
			$group->setIsUsed( $useThis );
		}


		/*$q = $this->em->createQuery('SELECT a FROM MoneyPoint\Entity\CreditDiscount a WHERE a.idProduct='.$creditDiscountId );
		$discounts = $q->getResult();

		foreach( $discounts as $discount )
		{
			$this->em->remove( $discount );
		}*/
		
		$newDiscounts = array();
		$i=0;
		foreach ( $arrayOfDiscounts as $discoutDetails )
		{
			$discoutDetails = (object)$discoutDetails;
			$i++;
			if( empty( $discoutDetails->credits ) || empty( $discoutDetails->price ) )
				continue;
			
			// create new or edit old
			if( empty( $discounts[$i] ))
			{
				$discount = new Entity\CreditDiscount();
			} else {
				$discount = $discounts[$i];
			}
			
			$discount->setCredits( $discoutDetails->credits );
			$discount->setPrice( $discoutDetails->price );
			$discount->setLevelDistributionCoef( $discoutDetails->rewardratio );
			$discount->setGroup( $group );
			

			// business activation
			if( empty( $discoutDetails->business ))
				$discount->setBusinessActivation( 0 );
			else
				$discount->setBusinessActivation( $discoutDetails->business );
			
			// rank activation
			if( empty( $discoutDetails->activaterank ))
				$discount->setActivateRank( 0 );
			else
				$discount->setActivateRank( $discoutDetails->activaterank );
			
			// rank activation
			if( empty( $discoutDetails->bpocoef ))
				$discount->setBpoCoef( 0 );
			else
				$discount->setBpoCoef( $discoutDetails->bpocoef );
			
			
			$this->em->persist( $discount );
			$newDiscounts[] = $discount;
		}
		return $newDiscounts;
	}
	
	public function getDiscountGroup( $id = 1 )
	{
		return $this->em->find("MoneyPoint\Entity\CreditDiscountGroup", $id );
//		$q = $this->em->createQuery('SELECT a FROM MoneyPoint\Entity\CreditDiscount a  WHERE a.idProduct='.$id .'ORDER BY a.id');
//		return $q->getResult();
	}
	
	public function getActiveGroup()
	{
		return $this->em->getRepository("MoneyPoint\Entity\CreditDiscountGroup")->findOneByActive(1);
	}
	
	public function createNewSet()
	{
				
		$discountGroup = new Entity\CreditDiscountGroup;
		$this->em->persist( $discountGroup );
		$this->em->flush();
		
//		$produkty = array(
//			'produkt_nazev' => 'Kredit',
//			'body' => 0,
//			'products_flag_id' => 2,
//		);
//		\DAO\Products::get()->insert( $produkty );
		return $discountGroup->getId();
	}
}
