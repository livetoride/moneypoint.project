<?php

namespace MoneyPoint;

class PartnerWhateverService
{
	private $em;
	public function __construct( \AntoninRykalsky\EntityManager $c2, \MoneyPoint\Container $c ) {
		$this->em = $c2->getEm();
		$this->baseUrl = $c->getBaseUrl();
	}
	
	/**
	 * findout partner by id
	 * @param type $id
	 * @return type \MoneyPoint\Entity\Eshop
	 */
	public function getPartner( $id )
	{
		$partner = $this->em->getRepository("MoneyPoint\Entity\Partner")->find( $id );
		$partner->setBasePath( $this->baseUrl );
		return $partner;
	}
	
	public function getEshop( $id )
	{
		$partner = $this->em->getRepository("MoneyPoint\Entity\Eshop")->find( $id );
		

		$partner->setBasePath( $this->baseUrl );
		return $partner;
	}
	
	public function getPartnerFeesNew( $partner )
	{
//		$partner = $this->em->find('\MoneyPoint\Entity\PartnerFeeRate', 54 );
//		print_r( $partner );exit;
		$qb = $this->em->createQueryBuilder();
		
		return $qb->select('pfr.feeText')
			->from('\MoneyPoint\Entity\PartnerFeeRate', 'pfr')
			->where("pfr.partner=".$partner->getIdPartner());
	}
	
	public function getEshops( $tagId=null, $partnerName=null, $country )
	{
		
			
		
		if( $partnerName == null ) {
			if( $tagId === null || $tagId == 0  )
			{
				$def = \DAO\Partners::get()->findAll("id_partner")
						->join(\DAO\PartnerCountry::get()->getTable())->on('partners.id_partner=partner_country.partner_id')
						->join(\DAO\BCountry::get()->getTable())->on('partner_country.country_id=b_country.id')
						->where("type=1 AND active=1 AND country = %s", $country )
						->orderBy('name')
						->fetchPairs("id_partner", "id_partner");
				
				//		throw new \Exception("Tady");
				# SELECT "id_partner" FROM "partners" JOIN "partner_country" ON partners.id_partner=partner_country.partner_id WHERE type=1 AND active=1 AND country_id = 1 ORDER BY "name"
				# SELECT "id_partner"
				# FROM "partners"
				# JOIN "partner_country" ON partners.id_partner=partner_country.partner_id
				# JOIN "b_country" ON partner_country.country_id=b_country.id
				# WHERE type=1 AND active=1 AND b_country.country = 'ČR' ORDER BY "name"  


				
			//	print_r( $def->__toString() );exit;
				$q = $this->em->createQuery("SELECT p FROM \MoneyPoint\Entity\Eshop p WHERE p.active=1 AND p.idPartner IN (:partnersIds)");
				$q->setParameter('partnersIds', array_values($def));
				// \Doctrine\Common\Util\Debug::dump($q->getResult());exit(); 
			} else {
				$def = \DAO\Partners::get()->findAll("id_partner")
						->join(\DAO\PartnerTags::get()->getTable())->on('partners.id_partner=partner_tags.partner_id')
						->join(\DAO\PartnerCountry::get()->getTable())->on('partners.id_partner=partner_country.partner_id')
						->join(\DAO\BCountry::get()->getTable())->on('partner_country.country_id=b_country.id')
						->where("type=1 AND active=1 AND tag_id = %i AND country = %s" , $tagId, $country )
						->orderBy('name')
						->fetchPairs("id_partner", "id_partner");
				#print_r( $def );exit;
				$q = $this->em->createQuery("SELECT p FROM \MoneyPoint\Entity\Eshop p WHERE p.active=1 AND p.idPartner IN (:partnersIds)");
				$q->setParameter('partnersIds', array_values($def));
			}
		
		#\Doctrine\Common\Util\Debug::dump($q->getResult());exit;
		} else {
			$def = \DAO\Partners::get()->findAll("id_partner")
					->where("type=1 AND active=1 AND name ~* %s", $partnerName )
					->orderBy('name')
					->fetchPairs("id_partner", "id_partner");
			$q = $this->em->createQuery("SELECT p FROM \MoneyPoint\Entity\Eshop p WHERE p.active=1 AND p.idPartner IN (:partnersIds)");
			$q->setParameter('partnersIds', array_values($def));
		//	\Doctrine\Common\Util\Debug::dump($q->getResult());exit(); 
		}
		
		return $q->getResult();
	}
	
	public function getPartners( $tagId=null, $partnerName=null, $country )
	
	{	
		if( $partnerName == null ) {
			if( $tagId === null || $tagId == 0  )
			{
				$def = \DAO\Partners::get()->findAll("id_partner")
						->join(\DAO\PartnerCountry::get()->getTable())->on('partners.id_partner=partner_country.partner_id')
						->join(\DAO\BCountry::get()->getTable())->on('partner_country.country_id=b_country.id')
						->where("type=2 AND active=1 AND country = %s", $country )
						->orderBy('name')
						->fetchPairs("id_partner", "id_partner");
				#print_r( $def );exit;
				$q = $this->em->createQuery("SELECT p FROM \MoneyPoint\Entity\Partner p WHERE p.active=1 AND p.idPartner IN (:partnersIds)");
				$q->setParameter('partnersIds', array_values($def));
				// \Doctrine\Common\Util\Debug::dump($q->getResult());exit(); 
			} else {
				$def = \DAO\Partners::get()->findAll("id_partner")
						->join(\DAO\PartnerTags::get()->getTable())->on('partners.id_partner=partner_tags.partner_id')
						->join(\DAO\PartnerCountry::get()->getTable())->on('partners.id_partner=partner_country.partner_id')
						->join(\DAO\BCountry::get()->getTable())->on('partner_country.country_id=b_country.id')
						->where("type=2 AND active=1 AND tag_id = %i AND country = %s" , $tagId, $country )
						->orderBy('name')
						->fetchPairs("id_partner", "id_partner");
					$q = $this->em->createQuery("SELECT p FROM \MoneyPoint\Entity\Partner p WHERE p.active=1 AND p.idPartner IN (:partnersIds)");
				$q->setParameter('partnersIds', array_values($def));
			}
		
		#\Doctrine\Common\Util\Debug::dump($q->getResult());exit;
		} else {
			$def = \DAO\Partners::get()->findAll("id_partner")
					->where("type=2 AND active=1 AND name ~* %s", $partnerName )
					->orderBy('name')
					->fetchPairs("id_partner", "id_partner");
			$q = $this->em->createQuery("SELECT p FROM \MoneyPoint\Entity\Partner p WHERE p.active=1 AND p.idPartner IN (:partnersIds)");
			$q->setParameter('partnersIds', array_values($def));
		//	\Doctrine\Common\Util\Debug::dump($q->getResult());exit(); 
		}
		
		return $q->getResult();
	}
	
	public function getShops( $tagId=null, $partnerName=null, $country )
	{
		
		
		if( $partnerName == null ) {
			if( $tagId === null || $tagId == 0  )
			{
				$def = \DAO\Partners::get()->findAll("id_partner")
						->join(\DAO\PartnerCountry::get()->getTable())->on('partners.id_partner=partner_country.partner_id')
						->join(\DAO\BCountry::get()->getTable())->on('partner_country.country_id=b_country.id')
						->where("type=3 AND active=1 AND country= %s", $country )
						->orderBy('name')
						->fetchPairs("id_partner", "id_partner");
				#print_r( $def );exit;
				$q = $this->em->createQuery("SELECT p FROM \MoneyPoint\Entity\Shop p WHERE p.active=1 AND p.idPartner IN (:partnersIds)");
				$q->setParameter('partnersIds', array_values($def));
				// \Doctrine\Common\Util\Debug::dump($q->getResult());exit(); 
			} else {
				$def = \DAO\Partners::get()->findAll("id_partner")
						->join(\DAO\PartnerTags::get()->getTable())->on('partners.id_partner=partner_tags.partner_id')
						->join(\DAO\PartnerCountry::get()->getTable())->on('partners.id_partner=partner_country.partner_id')
						->join(\DAO\BCountry::get()->getTable())->on('partner_country.country_id=b_country.id')
						->where("type=3 AND active=1 AND tag_id = %i AND country = %s" , $tagId, $country )
						->orderBy('name')
						->fetchPairs("id_partner", "id_partner");
					$q = $this->em->createQuery("SELECT p FROM \MoneyPoint\Entity\Shop p WHERE p.active=1 AND p.idPartner IN (:partnersIds)");
				$q->setParameter('partnersIds', array_values($def));
			}
		#\Doctrine\Common\Util\Debug::dump($q->getResult());exit;
		} else {
			$def = \DAO\Partners::get()->findAll("id_partner")
					->where("type=3 AND active=1 AND name ~* %s", $partnerName )
					->orderBy('name')
					->fetchPairs("id_partner", "id_partner");
			$q = $this->em->createQuery("SELECT p FROM \MoneyPoint\Entity\Shop p WHERE p.active=1 AND p.idPartner IN (:partnersIds)");
			$q->setParameter('partnersIds', array_values($def));
		//	\Doctrine\Common\Util\Debug::dump($q->getResult());exit(); 
		}
		
		return $q->getResult();
				//\Doctrine\Common\Util\Debug::dump($q->getResult());exit();  
	}
	
	
	public function getPartnerNames(  ) {
		
		$q = \DAO\Partners::get()->findAll()->where("type=2 AND active=1")->fetchPairs('id_partner', 'name');
		$dd = '"' . implode('","', $q) . '"';
		return $dd;
	}
	public function getShopNames(  ) {
		
		$q = \DAO\Partners::get()->findAll()->where("type=3 AND active=1")->fetchPairs('id_partner', 'name');
		$dd = '"' . implode('","', $q) . '"';
		return $dd;
	}
	public function getEshopNames(  ) {
		
		$q = \DAO\Partners::get()->findAll()->where("type=1 AND active=1")->fetchPairs('id_partner', 'name');
		$dd = '"' . implode('","', $q) . '"';
		return $dd;
	}
	
	
	public function getPartnerFees( $partner )
	{
//		$partner = $this->em->find('\MoneyPoint\Entity\PartnerFeeRate', 54 );
//		print_r( $partner );exit;
		
		$q = $this->em->createQuery("SELECT pfr FROM \MoneyPoint\Entity\PartnerFeeRate pfr WHERE pfr.partner=".$partner->getIdPartner() );
		/** @var \MoneyPoint\Entity\PartnerFeeRate */
		return $q->getResult();
	}
	
	/**
	 * returns items into partner fee selector
	 * @return array
	 */
	public function getPartnerFeesSelect( $withCategories = 1 )
	{
		$q = $this->em->createQuery("SELECT pfr, p FROM \MoneyPoint\Entity\PartnerFeeRate pfr JOIN pfr.partner p INDEX BY p.idPartner" );
		/** @var \MoneyPoint\Entity\PartnerFeeRate */
		$return=array();
		foreach( $q->getResult() as $p )
		{
			if( $withCategories )
				$return[ $p->getId() ] = $p->getPartner()->getName() .' - '. $p->getRateName().' ('. $p->getFeeText().')';
			else
				$return[ $p->getId() ] = $p->getPartner()->getName();
		}
		return $return;
	}
	
	public function getPartnerFee( $id )
	{
		$fee = $this->em->find('\MoneyPoint\Entity\PartnerFeeRate', $id );
		return $fee;
	}
	
	public function saveRate( $v )
	{
		if(empty( $v->id ))
		{
			$partnerFee = new \MoneyPoint\Entity\PartnerFeeRate();
			$partner = $this->em->find("\MoneyPoint\Entity\Eshop", $v->id_partner );
			$partnerFee->setPartner( $partner );
			$this->em->persist( $partnerFee );
			
		} else {
			$partnerFee = $this->em->find('\MoneyPoint\Entity\PartnerFeeRate', $v->id );
		}
		$partnerFee->setFeeRatio( $v->feeType, $v->fee, $v->rateName );
		$this->em->flush();
	}
	
	public function removePartnerFee( $id_rate )
	{
		$fee = $this->em->find('\MoneyPoint\Entity\PartnerFeeRate', $id_rate );
		$this->em->remove( $fee );
		$this->em->flush();
		return 1;
	}
	
	public function deletePartner( $id_partner )
	{
		$partner = $this->em->find("\MoneyPoint\Entity\Eshop", $id_partner );
		$partner->setDeleted();
		$this->em->flush();
		return 1;
	}

    
	/** @return \AntoninRykalsky\StateMachineService */
	public function getStateMachine()
	{
		return new \MoneyPoint\PartnerOrderStateMachine();
		
	}
	
	public function getEshopDataSource( $action = null )
	{
		$model = \DAO\Partners::get()->getDataSource();
		switch ($action) {
			case 'default':
				$model->where('type=1 AND active=1');
				break;
			case 'archived':
				$model->where('type=1 AND active=0');
				break;

			default:
				break;
		}
		return $model;
	}
	public function getPartnerDataSource( $action = null )
	{
		$model = \DAO\Partners::get()->getDataSource();
		switch ($action) {
			case 'default':
				$model->where('type=2 AND active=1');
				break;
			case 'archived':
				$model->where('type=2 AND active=0');
				break;

			default:
				break;
		}
		return $model;
	}
	public function getShopDataSource( $action = null )
	{
		$model = \DAO\Partners::get()->getDataSource();
		switch ($action) {
			case 'default':
				$model->where('type=3 AND active=1');
				break;
			case 'archived':
				$model->where('type=3 AND active=0');
				break;

			default:
				break;
		}
		return $model;
	}

	
	public function deactivatePartnerWhatever( $items )
	{
		$count = 0;
//		print_r( $items );exit;
		foreach( $items as $id => $deactivate )
		{
			if( $deactivate ) $count++;
			\DAO\Partners::get()->update($id, array('active'=>!$deactivate));
		}
		return $count;
	}
	
	public function activatePartnerWhatever( $items )
	{
		$count = 0;
//		print_r( $items );exit;
		foreach( $items as $id => $activate )
		{
			if( $activate ) $count++;
			\DAO\Partners::get()->update($id, array('active'=>$activate));
		}
		return $count;
	}
	
}
