<?php

namespace MoneyPoint;
use AntoninRykalsky\SystemUser;

class RankFacade
{
	private $em;
	
	/** @var PeriodRepository */
	protected $periodRepository;
	
	/** @var PeriodNetworkRepository */
	protected $periodNetworkRepository;

	/** @var $rankRepository */
	protected $rankRepository;
	
	/** @var \AntoninRykalsky\EventLogFacade */
	protected $eventLogFacade;

	public function __construct(
			\AntoninRykalsky\EntityManager $em,
			\AntoninRykalsky\EventLogFacade $eventLogFacade
		) {
		$this->em = $em->getEm();
		$this->periodRepository = $this->em->getRepository('MoneyPoint\Entity\Period');
		$this->periodNetworkRepository = $this->em->getRepository('MoneyPoint\Entity\PeriodNetwork');
		$this->eventLogFacade = $eventLogFacade;
	}

	public function getUserCarrier( $idu )
	{
		$member = $this->em->findAll('MoneyPoint\Entity\PeriodNetwork', $idu );
		
//		$member->
	}
			
	/**
	 * Ruční nastavenení ranku
	 * @param type $idu
	 * @param type $actCarrierKey
	 * @param type $lastCarrierKey
	 * @return int
	 */
	public function setRankChanges( $idu, $actCarrierKey, $lastCarrierKey )
	{
		//\Doctrine\Common\Util\Debug::dump($actCarrierKey);exit();

		$ranks = $this->getRankChangesDefaults( $idu );
		$ranksKeys = $ranks->ranks->getCarriers();
		
		$oldActual = $ranks->actual->getCarrier()->getKey();
		//\Doctrine\Common\Util\Debug::dump($ranks->actual->getCarrier());exit();
		$newRankGroup = $ranks->actual->getCarrier()->getGroup()->getCarriers();
		$newRank = $newRankGroup[$actCarrierKey];
		

		$ranks->actual->setCarrier( $ranksKeys[$actCarrierKey] );
		$this->eventLogFacade->logRankChange( null, $oldActual, $actCarrierKey, SystemUser::get()->idu );
		
		

		/* @var MoneyPoint\Entity\UserCarrier */
		$usrcarrep = $this->em->getRepository('MoneyPoint\Entity\UserCarrier');
		$userCarrier = new Entity\UserCarrier;
		$userCarrier->setManual(
			$newRank,
			$this->periodRepository->getActualPeriod(), 
			$this->em->getReference('MoneyPoint\Entity\Member', $idu)
		); 
		$this->em->persist($userCarrier);
				
				
		
		if( !empty( $ranks->last ))
		{
			$oldLast = $ranks->last->getCarrier()->getKey();
			$ranks->last->setCarrier( $ranksKeys[$lastCarrierKey] );
			$this->eventLogFacade->logRankChange( null, $oldLast, $lastCarrierKey, SystemUser::get()->idu );
		}
		
		$this->em->flush();
		return 1;
	}
	
	public function setCorrectRank( $idu )
	{
		$ranks = array();
		
		$ranks['network-actual'] = 1;
		$ranks['network-last'] = 1;
		$ranks['manual'] = 2;
		$ranks['order-related'] = 0;
		
		echo max($ranks);
		exit;
	}
	
	
	public function getRankChangesDefaults( $idu )
	{
		$actualRank = null;
		/* @var $actualPN \MoneyPoint\Entity\PeriodNetwork */
		$actual = $this->periodRepository->getActualPeriod();
		$actualPN = $this->periodNetworkRepository->getUserRank( $idu, $actual->getId() );
		if( $actualPN !== null )
		{
			$actualRank = $actualPN->getCarrier();
		}
		
		$lastRank = null;
		$lastPN = null;
		$last = $this->periodRepository->getLastPeriod( $actual );
		if( $last instanceof Entity\PeriodNetwork )
		{
			$lastPN = $this->periodNetworkRepository->getUserRank( $idu, $last->getId() );
		}
		if( $lastPN !== null )
		{
			$lastRank = $lastPN->getCarrier();
		}
		return (object)array( 
			'actual' => $actualPN,
			'last' => $lastPN,
			'ranks' => $this->periodRepository->getRankTable()
		);
	}
    
}
