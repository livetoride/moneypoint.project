<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;
use MoneyPoint\Entity\CreditOrder;
use AntoninRykalsky as AR;

class CreditOrderService
{
	// only const, dont change
	const FUCK_OFF_PDF = true;
	
	/** @var \MoneyPoint\PosibleRewardsService */
	private $posibleRewardsService;
	
	/** @var \MoneyPoint\RankIncreaseService */
	private $rankIncreaseService;
	
	/** @var MemberRepository */
	protected $memberRepository;
	
	
	/** @var DirefentialCommisionService */
	protected $direfentialCommisionService;
	
	/** @var FirstBuyBonus\FirstBuyBonusService */
	protected $firstBuyBonusService;

	/** @var \AntoninRykalsky\TimeService */
	protected $timeService;
	
	/** @var PremiumStatusService */
	protected $premiumStatusService;
	
	/** @var \MoneyPoint\FixedCommisionService */
	protected $fixedCommisionService;
	
	/** @var \MoneyPoint\StructureStatisticsService */
	protected $structureStatisticsService;

	
	
	private $em;
	public function __construct( 
		\AntoninRykalsky\EntityManager $em,
		PosibleRewardsService $posibleRewardsService,
		RankIncreaseService $rankIncreaseService,
		MemberRepository $memberRepository,
		DirefentialCommisionService $direfentialCommisionService,
		FirstBuyBonus\FirstBuyBonusService $firstBuyBonusService,
		\AntoninRykalsky\TimeService $timeService,
		PremiumStatusService $premiumStatusService,
		\MoneyPoint\FixedCommisionService $fixedCommisionService,
		\MoneyPoint\StructureStatisticsService $structureStatisticsService
	) {
		$this->em = $em->getEm();
		$this->posibleRewardsService = $posibleRewardsService;
		$this->rankIncreaseService = $rankIncreaseService;
		$this->memberRepository = $memberRepository;
		$this->direfentialCommisionService = $direfentialCommisionService;
		$this->firstBuyBonusService = $firstBuyBonusService;
		$this->timeService = $timeService;
		$this->premiumStatusService = $premiumStatusService;
		$this->fixedCommisionService = $fixedCommisionService;
		$this->structureStatisticsService = $structureStatisticsService;
	}
	
	/**
	 * Make state changes of credit orders
	 */
	public function process( $creditsOrder, $newState )
	{
		$whatTodo = array(
			'setPaid' => false,
			'setRewarded' => false,
			'vratUhraduNaUcet' => false,
			'stornujPredUhradou' => false,
		);

		// uhrada objednávky
		// úhrada pomoci bodů
		if(
			($creditsOrder->getIdStatus()==1 && $newState->getIdStatus() == CreditOrder::STATE_PAID ) ||
			( $creditsOrder->getPayment()->getIdPayment()==4 && $newState->getIdStatus() == CreditOrder::STATE_PAID )
		)
		{
			$whatTodo['setPaid'] = true;

			$deadline = (int)AR\Configs::get()->byContent('general.safety_deadline');
			if( $deadline == 0 )
				$whatTodo['setRewarded']= true;
		}


		// storno nezaplacené
		if( $creditsOrder->getIdStatus()!=CreditOrder::STATE_PAID &&
			$newState->getIdStatus() == CreditOrder::STATE_STORNO_BEFORE
		) {
			$whatTodo['stornujPredUhradou'] = true;
		}
		
		// storno po zaplacení
		if( ($creditsOrder->getIdStatus()==CreditOrder::STATE_PAID ||
				$creditsOrder->getIdStatus()==CreditOrder::STATE_REWARDED )
			&& $newState->getIdStatus() == CreditOrder::STATE_STORNO_AFTER
		) {
			$whatTodo['vratUhraduNaUcet'] = true;
		}

		// nastav přidělení odměn
		if( $creditsOrder->getIdStatus()==CreditOrder::STATE_PAID &&
			$newState->getIdStatus() == CreditOrder::STATE_REWARDED
		) {
			$whatTodo['setRewarded']=true;
		}
			
		return $whatTodo;
	}
	
	
	/**
	 * Finds all orders to be rewarded and rewards them.
	 */
	public function setRewarded( Entity\CreditOrder $creditsOrder )
	{
		$_creditRewardRepository = $this->em->getRepository('MoneyPoint\Entity\CreditReward');
				
		/** @var Entity\Member */
		$user = $creditsOrder->getUser();
		if( $creditsOrder->getIdStatus() === 8 ) return;
		
		$transactionSameId = $creditsOrder->getTransaction()->getMpTransactionId();
		
		// ziskej potřebné data
		$structure = $this->memberRepository->getStructure( $user->getIdu() );

		$this->posibleRewardsService->removePosibleRewards( $creditsOrder );
		

		// drop future
		foreach( $creditsOrder->getCreditRewards() as $r )
		{
			$this->em->remove( $r );
		}
		
		// clear reward - without all bonuses
		$rewards = $this->fixedCommisionService->countRewards( $creditsOrder, $structure );
		/* @var $rewardItem \MoneyPoint\Entity\CreditReward */
		foreach( $rewards as $rewardItem )
		{
			$this->em->persist( $rewardItem );
			
			$rewardedMemberId = $rewardItem->getIdu();
			$claimRespectingPremium = $rewardItem->getClaim();
			$level = $rewardItem->getLine();
			
			/* @var $memberToRewardEntity Entity\Member */
			$memberToRewardEntity = $this->em->find('MoneyPoint\Entity\Member', $rewardedMemberId );
			
			$creditsState = $memberToRewardEntity->_getCreditState();
			
			
			$counting = new RewardCounting();
			/* @var $reward \RewardCounting */
			$reward = $counting->extraRewardsWithReduction($creditsState, $claimRespectingPremium);
			
			
			$canGetPoints = $memberToRewardEntity->canIGetPoints();
			if( $canGetPoints )
			{
				// store transaction
				$_creditRewardRepository->storeCreditRewardTransaction(
					$rewardItem,
					$reward,
					$creditsOrder
				);
				
				$this->structureStatisticsService->actualizeStructureFirstLine( $level, $creditsOrder->getUser()->getIdu(), $reward->reward, $creditsOrder->getCreditsSum() );
				$this->structureStatisticsService->actualizeStructureSmallNetwork( $level, $reward->reward, $rewardedMemberId, @$structure[$level-1] );
			}
		}

		$creditsOrder->setRewarded();
		$this->em->persist( $creditsOrder );
		$this->em->flush();
	}
	
	public function setStorno( Entity\CreditOrder $creditsOrder )
	{
		// odebere budoucí odměny
		$this->posibleRewardsService->removePosibleRewards( $creditsOrder );
		
		// přidá body pro výpočet objemových odměn
		$this->direfentialCommisionService->rmDifferentialPoints( $creditsOrder );
		
		$time = $this->timeService->getDateTime();
		$creditsOrder->setStorno( $time );
		$this->em->persist( $creditsOrder );
		$this->em->flush();
	}
	

	
	const INVOICE_STORNO = 1;
	
	/**
	 * Provádí operace spojené se stornováním objednávky kreditu po úhradě
	 * @param \MoneyPoint\Entity\CreditOrder $creditsOrder
	 */
	public function setStornoAfterPay( Entity\CreditOrder $creditsOrder, array $periodCreditsOrders, array $nextCreditsOrders )
	{
		// odebere budoucí odměny
		$this->posibleRewardsService->removePosibleRewards( $creditsOrder );
		
		// přidělení bodů
		\AntoninRykalsky\PointsAccounts::get()->manualChange( $creditsOrder->getUser()->getIdu(), "Storno nákupu kreditu", $creditsOrder->getPriceSum() );
		
		// odebere kredit
		\AntoninRykalsky\CreditsAccounts::get()->modifyCredits( $creditsOrder->getUser()->getIdu(), "Storno nákupu kreditu", -$creditsOrder->getCreditsSum(), $creditsOrder->getId() );
		
		// přidá body pro výpočet objemových odměn
		$this->direfentialCommisionService->rmDifferentialPoints( $creditsOrder );

		// snižování ranků po automatickém zvýšení
		$this->rankIncreaseService->decreaseRank( $periodCreditsOrders, $creditsOrder);
		
		
		$discount = $creditsOrder->getRelatedDiscount();
		if( $discount->isBusinessActivation() ) {
			
			$this->premiumStatusService->stornoPremiumOrder( $creditsOrder, $nextCreditsOrders );
		}
		
		
		// generuje fa
		$this->generateInvoice( $creditsOrder, self::INVOICE_STORNO );
		
		$time = $this->timeService->getDateTime();
		$creditsOrder->setStornoAfterPay( $time );
		$this->em->persist( $creditsOrder );
		$this->em->flush();
	}

	
	public function createOrder()
	{
		
	}


	public function setPaid( Entity\CreditOrder $creditsOrder, $fuckOffPdf = false )
	{
		
		$user = $creditsOrder->getUser();
		
		$time = $this->timeService->getDateTime();
		$creditsOrder->setPaid( $time );
		
		
		// turn user into business user
		$allowBusiness = \AntoninRykalsky\Configs::get()->byContent('multilevel.allowBusiness');
		$allowBusiness = 0;
		if( $allowBusiness  )
		{
			$discount = $creditsOrder->getRelatedDiscount();
			if( $discount->isBusinessActivation() )
				if( !$user->hasBusiness() )
				{
					
					$d = array('has_bussiness'=>1, 'ts_business'=> $creditsOrder->getTsPaid() );
					\DAO\Uzivatele::get()->update( $user->getIdu(), $d );
				}
		}

		$reason = \FormatingHelpers::oddeltisice( $creditsOrder->getCreditsSum() ) . ' kr';

		if( $creditsOrder->getPriceSum() >= 4000 )
		{
			$user->setHasActivatedDifferencials(1);
		}
		
		$allowRankActivation = 1;
		if( $allowRankActivation ){
			$discount = $creditsOrder->getRelatedDiscount();
			try {
				$rank = $discount->getActivateRank();
			} catch( \Exception $e )
			{
				$rank = null;
			}
			if( !empty( $rank ))
			{
				$rankRepository = $this->em->getRepository('Moneypoint\Entity\Carrier');
				$rankRepository->increaseRanks( $user->getIdu(), $rank, $creditsOrder );
				
				$volumeCommision = $this->em->getRepository("MoneyPoint\Entity\PeriodNetwork")->getUserRank( $user->getIdu() );
				$this->em->getRepository("MoneyPoint\Entity\Carrier")->fixActualCarrier( $volumeCommision );
				$this->direfentialCommisionService->countIntermediateResults( $volumeCommision );
			}
		}

		// structure overview & firstline
		$structure = $this->memberRepository->getStructure( $user->getIdu() );
		foreach( $structure as $predciLevel => $iduPredek )
		{
			$this->structureStatisticsService->afterPaymentAddTo( $iduPredek, $predciLevel, $creditsOrder, @$structure[$predciLevel-1] );
		}

		// uloz nakoupené kredit na kreditový účet
		\AntoninRykalsky\CreditsAccounts::get()->modifyCredits( $user->getIdu() ,$reason , $creditsOrder->getCreditsSum(), $creditsOrder->getIdOrder() );
		$credits_account_id = \dibi::insertId();

		// uprav moneypoint transakci pro nakupciho
		/** @var Entity\Transaction */
		$transaction = $creditsOrder->getTransaction();
		$transaction->setCreditsAccountId( $credits_account_id );
		$this->em->persist( $transaction );


		// zapnutí AD pokud kredity presahnou hrenici ( původně 5000 )
		if( Settings::get()->autodokup )
		{
			$ad = AutodokupPackagesDao::get()->findAll()->where('active=1')->fetch();
			if( CreditsAccounts::get()->actualState( $user->getIdu() )->credits_left >= $ad->credits_activation_level )
				if(!Autodokup::get()->isSwitched( $user->getIdu() ))
					Autodokup::get()->switchOn( $user->getIdu() );
		}

		
		$this->em->persist( $creditsOrder );

		$this->em->flush();
		if( !$fuckOffPdf )
		{
			$this->generateInvoice( $creditsOrder );
		}
		
		
		$u = \DAO\Uzivatele::get()->find( $user->getIdu() )->fetch();
		Emails::creditsOrderConfirmPaid( $creditsOrder, $u);
		
		$now = \AntoninRykalsky\Configs::get()->byContent('general.safety_deadline');
		if( $now == 0 )
		{
			$this->setRewarded( $creditsOrder );
		}
		
		return 1;
	}
	
	public function generateInvoice( Entity\CreditOrder $creditsOrder, $storno = 0 )
	{
		$presenter = new \FrontModule\InvoicePresenter(\Nette\Environment::getContext());
        $request = new \Nette\Application\Request('Invoice', 'GET', array());

		$g = $presenter->getMyComponent();
		$o = \DAO\Orders::get()->find( $creditsOrder->getIdOrder() )->fetch();
		$g->setObjednavka($o);

		if( $storno ) {
			$g->setStorno();
		}

		$response = $presenter->run($request);
		echo $response->getSource();
	}
}
