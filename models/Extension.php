<?php

namespace MoneyPoint;



class Extension extends \Nette\Config\CompilerExtension
{
    public $defaults = array(
        'postsPerPage' => 5,
        'comments' => TRUE
    );

    public function loadConfiguration()
    {
		$config = $this->getConfig($this->defaults);
		$builder = $this->getContainerBuilder();
		
		

		// na�ten� konfigura�n�ho souboru pro roz���en�
		$this->compiler->parseServices($builder, $this->loadFromFile(__DIR__ . '/configs/config.neon'));

		
		#$container = \Nette\Environment::getContext();
		#$p = $container->getParameters();
		##$p['webloader2'] = 'asdf';
	}
	
	public function registerParameters( $configurator )
	{
		$configurator->addConfig(__DIR__ . '/configs/parameters.neon', false);
	}
	
}
		