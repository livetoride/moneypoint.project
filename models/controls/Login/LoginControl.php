<?php

namespace AntoninRykalsky;

use Nette\Application\UI;
use Nette\Application\UI\Form;
use Nette\Environment;

class LoginControl extends UI\Control
{
	
	/** @var \MoneyPoint\AuthFacade */
	protected $authFacade;
	
	/** @var MoneyPoint\DiferentialCommisionFacade */
	protected $direfentialCommisionFacade;
	
	public function __construct(
		\MoneyPoint\AuthFacade $authFacade,
		\MoneyPoint\DiferentialCommisionFacade $direfentialCommisionFacade
			) {
		$this->authFacade = $authFacade;
		$this->direfentialCommisionFacade = $direfentialCommisionFacade;
	}
	
    public function render()
    {
		$d = array('user'=>'@');
		$this['loginForm']->setDefaults($d);

		
		$this->template->setFile(dirname(__FILE__) .'/login.latte' );
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
    }
	
	
    protected function createComponentLoginForm()
	{
        $form = new Form($this, 'loginForm');
        #$form->setMethod('post');

        $inp_id = $form->addText('user', 'login/email')
        ->addRule(Form::FILLED, 'Zadejte svůj login/email' );
		$form['user']->getControlPrototype()->tabindex(1);

        $form->addPassword('pass', 'heslo')
        ->addRule(Form::FILLED, 'Zadejte své heslo' );
		$form['pass']->getControlPrototype()->tabindex(2);

        $form->addSubmit('login', 'Přihlásit')->getControlPrototype()->class('mybutton');
		$form['login']->getControlPrototype()->tabindex(3);
        $form->onSuccess[] = array($this, 'loginFormSubmitted');

		$a = $form->getElementPrototype();
		$a->id='loginform';
	}
	
	public function loginFormSubmitted( Form $form )
	{
		if ($form['login']->isSubmittedBy()) {
			$values = $form->getValues();
			$this->login( $values['user'], $values['pass']  );
			
		 }
	}
	
	public function login( $username, $pass, $withoutMD = false )
    {
        try {
            $user = $this->presenter->user;
			$user->getStorage()->setNamespace('mlm');
            #$user->setAuthenticationHandler( new MyAuth );
			$credentials = array(
				'username' =>  $username,
				'password' => $pass,
				'extra' => null
			);


			$user->login( $credentials );


            $role = $user->getRoles();
            $role = $role[0];

			if( $role == 'admin' || $role == 'site-admin' )
			{
				$user = Environment::getUser();
				$user->logout(TRUE);
				$s = Environment::getSession('user');
				unset( $s->idu );
				unset( $s->role );
				unset( $s->imgdir );
				throw new AuthenticationException('Požadovaný účet neexistuje');
			}

			if( $withoutMD ) return 'success';

            $this->presenter->restoreRequest( $this->presenter->backlink );

			$dp = \MoneyPoint\AuthFacade::$defaultPages;
			
			$idu = \AntoninRykalsky\SystemUser::get()->idu;
			$this->direfentialCommisionFacade->installDefaultAccount($idu);
			
            $this->presenter->redirect( $dp[$role]['presenter'].':'.$dp[$role]['action'] );
        } catch ( \Nette\Security\AuthenticationException $e ) {


			$u = UsersWaiting::get()->find_ByLogin($username)->fetchAll();
			if( !$withoutMD ) $pass = md5($pass);
			if( count($u) && ($u[0]['pass']==$pass))
			{
				$this->presenter->flashMessage( 'Konto bude aktivní po potvrzení administátorem. O aktivaci se dozvíte emailem.', Flashes::$info );
			} else {
				$this->presenter->flashMessage( 'Špatný login nebo heslo.', Flashes::$info );
			}

			if( $withoutMD ) return 'fail: '.$e->getMessage();

            $role = 'guest';
            $this->presenter->redirect( ":Front:Auth:login" );
        }
    }
}
?>
