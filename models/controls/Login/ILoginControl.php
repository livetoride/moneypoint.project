<?php
namespace AntoninRykalsky;

interface ILoginControlFactory
{
    /** @return LoginControl */
    function create();
}

class LoginControlFactory implements ILoginControlFactory
{
	public function __construct( \MoneyPoint\AuthFacade $authFacade, \MoneyPoint\DiferentialCommisionFacade $direfentialCommisionFacade )
    {
        $this->authFacade = $authFacade;
        $this->direfentialCommisionFacade = $direfentialCommisionFacade;
    }

    public function create()
    {
        return new LoginControl( $this->authFacade, $this->direfentialCommisionFacade );
    }
}