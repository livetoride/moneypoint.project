<?php

use Nette\Application\UI;


/**
 * MenuControl
 *
 * @author Antonin Rykalsky @ 2oo9
 */
class MPAccountState extends UI\Control
{
    /** @var <array> drží seznam položek pro Control */
    public $logged = 0;
    public $credits = 0;
    public $points = 0;




    /*
     * vrací popis aktuální stránky,
     * pro zlepšení jednoznačnosti stránky je dobré zde vést,
     * doplnkové parametry .. např. NewsPresenter .. News:default item
     */
    public function render() {
		if( !$this->logged )return;

    	$this->template->setFile(dirname(__FILE__) . '/template.latte');
    	$this->template->registerHelperLoader('FormatingHelpers::loader');
    	$this->template->credits = $this->credits;
    	$this->template->points = $this->points;
    	$this->template->render();
    }

}
