<?php

/**
 * Nette Framework Extras
 *
 * This source file is subject to the New BSD License.
 *
 * For more information please see http://extras.nettephp.com
 *
 * @copyright  Copyright (c) 2009 David Grudl
 * @license    New BSD License
 * @link       http://extras.nettephp.com
 * @package    Nette Extras
 * @version    $Id: VisualPaginator.php 4 2009-07-14 15:22:02Z david@grudl.com $
 */

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use AntoninRykalsky as AR;

class MpPartnerDetail extends Control
{


	public function handleDetail( $path )
	{
		$path = $this->getParam('path');
		$p = AR\PartnersDao::get()->findAll()->where('[path]=%s', $path )->fetch();
		$this->template->partner = $p;

		$this->template->credits = 0;
		try {
			$idu = \AntoninRykalsky\SystemUser::get()->getIdu();
			$this->template->idu = $idu;
			if( !empty(\AntoninRykalsky\CreditsAccounts::get()->actualState( $idu )->credits_left))
					$this->template->credits = \AntoninRykalsky\CreditsAccounts::get()->actualState( $idu )->credits_left;
		} catch ( \Exception $e ) {}

		if($this->presenter->isAjax())
			$this->invalidateControl();
	}

	public function render()
	{
		$this->template->setFile(dirname(__FILE__) . '/template.phtml');
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
	}




}
