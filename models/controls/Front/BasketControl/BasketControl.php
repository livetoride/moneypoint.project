<?php

use AntoninRykalsky as AR,
	Nette\Application\UI,
	Nette\Utils\Html;
use Nette\Environment;

use AntoninRykalsky\CreditsDiscount;

/**
 * MenuControl
 *
 * @author Antonin Rykalsky @ 2oo9
 */
class BasketControl extends UI\Control
{
	var $admin;
	var $changeQuantity = 0;

	public function setAdmin( $admin )
	{
		if( $admin )
			$this->admin = $admin;
	}
	public function allowChangeQuantity( $allow )
	{
		if( $allow )
			$this->changeQuantity = 1;
	}

	public function handleQuantityChange($operation, $productid)
	{
		// odeber 1 ks
		$basket = Environment::getSession($this->admin.'basket');
		if( $operation == 'minus' )
			$basket->zbozi[$productid]->count--;
		elseif( $operation == 'plus' )
			$basket->zbozi[$productid]->count++;

		if( $basket->zbozi[$productid]->count < 0 )
				$basket->zbozi[$productid]->count = 0;

		$this->template->quantity = $basket->zbozi;

		if ($this->presenter->isAjax()) {
			$this->presenter->invalidateControl('basketControl');
		} else {
			$this->presenter->redirect('this');
		}
	}

	var $renderHistory = 0;
	var $id_order = null;
	public function setRenderHistory( $history = 1, $id_order = null )
	{
		$this->renderHistory = $history;
		$this->id_order = $id_order;
	}

	public function renderHistory()
	{
		$o = AntoninRykalsky\Orders::get()->find($this->id_order)->fetch();
		$products = AntoninRykalsky\OrdersGoods::get()->findAll()->where('id_order=%i', $o->id_order )->fetchAll();
		foreach( $products as $k=>$p ) {
			$products[$k]['produkt_nazev'] = $products[$k]['product'];
			$products[$k]['creditsSum'] = $o->credits_sum;

		}
		//$products = AR\Produkty::get()->findAll('id_product IN ('.implode(',',$product_ids).')')->fetchAll();


		$this->template->configs = AR\Configs::get();
		$this->template->changeQuantity = 0;
		$this->template->products = $products;
		$this->template->creditsOrder = 1;
		$this->template->cenaCelkem = $o->price_sum;


        $this->template->setFile(dirname(__FILE__) .'/moneypoint-basket.latte');
        $this->template->render();
	}

	public function render()
    {
		if( $this->renderHistory && $this->id_order != null )
		{
			$this->renderHistory();
			return;
		}

		$basket = Environment::getSession($this->admin.'basket');



		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->basket = $basket->goods;

		$this->template->configs = AR\Configs::get();
		$this->template->changeQuantity = $this->changeQuantity;
		$this->template->creditsOrder = true;

        $this->template->setFile(dirname(__FILE__) .'/moneypoint-basket.latte');
        $this->template->render();
	}
}
