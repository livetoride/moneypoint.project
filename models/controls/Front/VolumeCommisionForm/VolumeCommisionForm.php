<?php

namespace MoneyPoint;

use Nette\Application\UI\Form;

class VolumeCommisionForm
{
	/** @var MoneyPoint\VolumeCommisionFacade */
	protected $volumeCommisionFacade;
	
	function __construct( VolumeCommisionFacade $VolumeCommisionFacade, $presenter ) {
		$this->volumeCommisionFacade = $volumeCommisionFacade;
		$this->presenter = $presenter;
	}
	
	public function getForm() {
		$form = new Form();
		$renderer = $form->getRenderer();

		$form->addHidden('id');
		$form->addText('code', 'kód diagnózy');

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedVolumeCommisionForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		$this->form = $form;
		return $form;
	}

	function translate( $volumeCommision )
	{
		return array(
			'id' => $volumeCommision->getId(),
			'code' => $volumeCommision->getCode()
		);
	}

	function submitedVolumeCommisionForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			
			try {
				$this->volumeCommisionFacade->store( $v );
			} catch ( \Exception $e )
			{
				$this->presenter->flashMessage('Uloženo', 'SUCCESS');
				$this->presenter->flashMessage('Není uloženo', 'SUCCESS');
			}
			$this->presenter->redirect('default');
		}
	}
}
?>
