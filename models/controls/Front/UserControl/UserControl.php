<?php
use AntoninRykalsky as AR;
use Nette\Application\UI;
use Nette\Environment;
use Nette\Application\UI\Form;

class UserControl extends UI\Control
{
	
	/** \MoneyPoint\MemberFacade */
	private $memberFacade;
	public function __construct( \MoneyPoint\MemberFacade $memberFacade ) {
		$this->memberFacade = $memberFacade;
		
	}
	
	var $a;

    public function render() {

       # $flashes = $this->getTemplate()->getFlashes;
		$template = $this->template;

		$template->setTranslator( $this->presenter->getProjectTranslator() );


		$basket = Environment::getSession('basket');
		$pocet = 0;
		foreach( (array)$basket['zbozi'] as $v )
			$pocet+=$v->count;
		$template->pocet = $pocet;

		 $templateName['loginDiv'] = AR\Configs::get()->byContent('settings.template_logindiv');
		 $templateName['memberDiv'] = AR\Configs::get()->byContent('settings.template_membermenu');

        // ověření, zda je uživatel přihlášen
		$idu = Environment::getSession('user');
        $user = Environment::getUser();
		$user->getStorage()->setNamespace('mlm');
		$idu = \AntoninRykalsky\SystemUser::get()->getIdu();
        if ( empty( $idu )) {
            $template->flashes = $this->getPresenter()->getFlashSession()->flash;
            $template->setFile(dirname(__FILE__) . '/' . $templateName['loginDiv'] );
        } else {
			$this->template->rank = $this->memberFacade->getMemberRank( $idu );
			
			
            $form = $this->getComponent('logoutForm');

            $userInfo = array(
                'user' => $idu,
                'role' => 'member' // implode(', ', $user->getIdentity()->roles)
            );

			$this->template->bussiness = \MoneyPoint\LoggedMember::get()->statuses()->bussiness;

			$form->setDefaults( $userInfo );

            $template->userInfo = $userInfo;
            $template->flashes = $this->getPresenter()->getFlashSession()->flash;

//			print_r( $userInfo['role'] );exit;

			if ( $idu > 0)
			{
				$member = MoneyPoint\LoggedMember::get();
				$idu = $member->idu;
				$u = $this->template->userDetail = $member->user();

				// muzu zapnout autodokup
				if( AR\Configs::get()->byContent('settings.autodokup') )
				{
					$canSwitch =  AR\Autodokup::get()->canSwitchOn( $idu  );
					$isSwitched =  AR\Autodokup::get()->isSwitched( $idu  );
					$template->autodokup = count($canSwitch) || $isSwitched;
				}
				
				$this->template->allowBusiness = $allowBusiness = AR\Configs::get()->byContent('multilevel.allowBusiness');

				$template->setFile(dirname(__FILE__) . '/' . $templateName['memberDiv'] );

			} else {
				$template->setFile(dirname(__FILE__) . '/userInfo.phtml');
			}


        }

		$template->nakupPovolen = $user->isAllowed('nakup-zbozi', 'show');
		$template->registerHelperLoader('FormatingHelpers::loader');

        $template->render();
    }

    /**
	 * Component factory.
	 * @param  string  component name
	 * @return void
	 */
    protected function createComponentLoginForm()
	{
        $form = new Form($this, 'loginForm');
        #$form->setMethod('post');

        $inp_id = $form->addText('user', 'Id:')
        ->addRule(Form::FILLED, 'Zadejte své ID' );

        $form->addPassword('pass', 'heslo:')
        ->addRule(Form::FILLED, 'Zadejte své heslo' );

        $form->addSubmit('login', 'Přihlásit')->getControlPrototype()->class('mybutton');
        $form->onSuccess[] = array($this, 'loginFormSubmitted');

		$a = $form->getElementPrototype();
		$a->id='loginform';


        $template = $this->createTemplate();
        $form->addProtection('Please submit this form again (security token has expired).');

	}

	protected function createComponentLogoutForm()
    {
        $form = new Form($this, 'logoutForm');
        #$form->setMethod('post');

        $form->addText('user', '')->setDisabled();
        $form->addText('role', '')->setDisabled();

        $form->addSubmit('logout', 'Odhlásit')->setHtmlId('searchsubmit');
        $form->onSuccess[] = array($this, 'logoutFormSubmitted');

        $template = $this->createTemplate();

        $form->addProtection('Please submit this form again (security token has expired).');
        return $form;
    }


     public function loginFormSubmitted( Form $form )
     {
        if ($form['login']->isSubmittedBy()) {
            $values = $form->getValues();
            $this->presenter->forward( "Auth:login", $values['user'], $values['pass']  );
         }
    }

    public function logoutFormSubmitted( Form $form )
     {
        if ($form['logout']->isSubmittedBy()) {
            $this->presenter->forward( "Auth:logout" );
         }
    }
}
