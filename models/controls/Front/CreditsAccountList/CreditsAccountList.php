<?php
namespace MoneyPoint;

use DataGrid\DataGrid;
use DataGrid\DataSources\Dibi\DataSource;
use dibi;
use FormatingHelpers;
use MpTransactionDetail;
use Nette\Application\UI\Control;
use Nette\Utils\Html;

class CreditsAccountList extends Control
{
	/** @var \MoneyPoint\PartnerOrderFacade */
	protected $partnerOrderFacade;
	/** @var \MoneyPoint\TransactionFacade */
	protected $transactionFacade;
	protected $commonTranslator;
	
	public function __construct( 
		\MoneyPoint\PartnerOrderFacade $partnerOrderFacade,
		\MoneyPoint\TransactionFacade $transactionFacade ,
		$commonTranslator
	) {
		$this->partnerOrderFacade = $partnerOrderFacade;
		$this->transactionFacade = $transactionFacade;
		$this->commonTranslator = $commonTranslator;
	}
	
	public $idu;
	public function render()
	{
		$this->template->setFile(dirname(__FILE__) . '/template.phtml' );
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
	}
	
	public function gridOnCellRendered(Html $cell, $column, $value)
	{
		if ($column === 'credits_change') {
			if ($value < 30000) $cell->addClass('money-low');
			elseif ($value >= 100000) $cell->addClass('money-high');
		}
		return $cell;
	}
	protected function createComponentMpTransactionDetail()
    {
		return new MpTransactionDetail( $this->partnerOrderFacade, $this->transactionFacade );
	}
	public function creditsAccountDS( $datagrid = null )
	{
		$model = \AntoninRykalsky\CreditsAccounts::dsCreditsAccountsList();
		$model->orderBy('id', dibi::DESC);
		$model->where('idu=%i', $this->idu );
		$model->where('credits_change<>0' );

		if( $datagrid )
			return $model;

		return $model3;
	}
	
	protected function createComponentCreditsAccount()
    {
		$model = new DataSource( $this->creditsAccountDS( 1 ) );
		$grid = new DataGrid($this, 'creditsAccount');
		
		$renderer = \MoneyPoint\DatagridRenderer::get()->getRenderer();
//		$renderer->onCellRender[] = array($this, 'gridOnCellRendered');
//		$renderer->wrappers['datagrid']['container'] .= 'table class="credits-account-list-control datagrid"';

		$grid->setTranslator($this->commonTranslator);

		$grid->setDataSource( $model );
		$grid->setRenderer($renderer);
		$grid->itemsPerPage = 20;
		$grid->multiOrder = FALSE;
//		$translator = new \GettextTranslator( APP_DIR . '/locale/datagrid.mo' );
//		$grid->setTranslator($translator);


		$operations = array(); // define operations
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, \DAO\MpCreditsAccounts::get()->getPrimary()); // allows checkboxes to do operations with more rows

		// if no columns are defined, takes all cols from given data source
		$grid->addActionColumn('akce')->getHeaderPrototype()->class('action-column');
		$icon = Html::el('span');
		// <a href="{$detailComponent->link('detail!', array(path => $p->path))}" onclick="showtable()" class="ajax">

		$action = $grid->addAction('Detail', 'mpTransactionDetail:creditTransaction!', clone $icon->class('icon mp-info'))->getHtml();
		$action->class('ajax');
		$action->class('show-table');
		#$action->onClick('showtable()');


		$grid->addColumn('change_timestamp', 'Datum');
		$grid['change_timestamp']->formatCallback[] = 'FormatingHelpers::czechDateShort';
//		$grid['change_timestamp']->addDefaultSorting('desc');
		$grid['change_timestamp']->getCellPrototype()->class('change_timestamp');
		$grid['change_timestamp']->getHeaderPrototype()->class('change_timestamp');

		$grid->addColumn('reason', 'Číslo akce / popis');
		$grid['reason']->replacement[''] = '-';
		//$grid['variable_symbol']->formatCallback[] = 'FormatingHelpers::MP_id';
		$grid['reason']->getCellPrototype()->class('mpid');
		$grid['reason']->getCellPrototype()->class('cislo_akce');


//
//		$grid->addColumn('objem', 'Objem');
//		$grid['objem']->formatCallback[] = 'FormatingHelpers::currency2';
//		$grid['objem']->getCellPrototype()->class('objem');

		$grid->addColumn('credits_change', 'Změna');
		$grid['credits_change']->formatCallback[] = 'MpHelpers::kredityUAkciAOdmen';
		$grid['credits_change']->getCellPrototype()->class('credits_change');
		$grid['credits_change']->getHeaderPrototype()->class('credits_change');
		
		$grid->addColumn('credits_left', 'Zůstatek');
		$grid['credits_left']->formatCallback[] = 'MpHelpers::kredity';
		$grid['credits_left']->getCellPrototype()->class('credits_left');
		$grid['credits_left']->getHeaderPrototype()->class('credits_left');


		return $grid;
    }


}
