<?php

namespace MoneyPoint;

use DataGrid;
use Nette\Utils\Html;

class VolumeCommisionList
{
	public function getList( DiferentialCommisionFacade $direfentialCommisionFacade, $commonTranslator = null )
	{
		
		$idu = \AntoninRykalsky\SystemUser::get()->idu;
		$ds = $direfentialCommisionFacade->getDatasource( $idu );
		$model = new \DataGrid\DataSources\Doctrine\QueryBuilder($ds);
		$model->mapping['id'] = 'a_id';

		$grid = new DataGrid\DataGrid;
		$grid->setTranslator($commonTranslator);
		$renderer = new DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setRenderer($renderer);

		$grid->itemsPerPage = 10;
		$grid->setDataSource($model);
		$grid->multiOrder = FALSE;
		
		$operations = array();
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, 'id' );

		
		// if no columns are defined, takes all cols from given data source
		$grid->addActionColumn('akce')->getHeaderPrototype()->style('width: 20px;');
		$icon = Html::el('span');
		$action = $grid->addAction('Detail', 'mpTransactionDetail:volumeDetail!', clone $icon->class('icon mp-info'))->getHtml();
		$action->class('show-table');
		
		
		$grid->addColumn('p_period', 'účetní období');
//		$grid['p_period']->addFilter();
//		$grid['a_code']->addDefaultSorting();
		

		return $grid;
	}

}
?>
