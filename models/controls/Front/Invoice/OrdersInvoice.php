<?php
/**
 * Model zacházající s tabulkou kariéra
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class OrdersInvoice extends \AntoninRykalsky\OrdersInvoice
{

	public function setStorno()
	{
		parent::setStorno();
		$this->templateInvoice = dirname(__FILE__)."/makeInvoiceStorno.phtml";
	}

	public function __construct(IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);
		$this->templateInvoice = dirname(__FILE__)."/makeInvoice.phtml";
    }


}
