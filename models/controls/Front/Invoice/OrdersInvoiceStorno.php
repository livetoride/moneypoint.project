<?php
/**
 * Model zacházající s tabulkou kariéra
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class OrdersInvoiceStorno extends \AntoninRykalsky\OrdersInvoice
{
	public function __construct() {
		parent::__construct();
		$this->templateInvoice = dirname(__FILE__)."/makeInvoiceStorno.phtml";
	}
}
