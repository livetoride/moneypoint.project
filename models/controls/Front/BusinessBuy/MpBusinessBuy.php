<?php

/**
 * Nette Framework Extras
 *
 * This source file is subject to the New BSD License.
 *
 * For more information please see http://extras.nettephp.com
 *
 * @copyright  Copyright (c) 2009 David Grudl
 * @license    New BSD License
 * @link       http://extras.nettephp.com
 * @package    Nette Extras
 * @version    $Id: VisualPaginator.php 4 2009-07-14 15:22:02Z david@grudl.com $
 */

namespace MoneyPoint;

use Nette\Application\UI\Control;
use AntoninRykalsky as AR;


class BusinessBuy extends Control
{


	public function handleShow()
	{
		// vygenerovano
		$this->template->detailTemplate = '_show.phtml';
		$this->template->title = 'Aktivace PREMIUM účtu';
//		$this->template->transaction = $t;
		
		$result = \DAO\MpCreditsDiscount::get()->findAll()->where("flag='for-business-status'")->fetchAll();
		$this->template->discount = (object)$result[0];
		

		if($this->presenter->isAjax())
		{
			$this->invalidateControl();
		}
	}

	private $templateName = '/template.phtml';

	public function render()
	{
		$this->template->setFile(dirname(__FILE__) . $this->templateName );
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
	}




}
