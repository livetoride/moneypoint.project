<?php

use Nette\Application\UI\Control;
use AntoninRykalsky as AR;

class MpTransactionDetail extends Control
{
	/** @var \MoneyPoint\PartnerOrderFacade */
	protected $partnerOrderFacade;
	
	/** @var \MoneyPoint\TransactionFacade */
	protected $transactionFacade;
	
	public function __construct( 
		\MoneyPoint\PartnerOrderFacade $partnerOrderFacade,
		\MoneyPoint\TransactionFacade $transactionFacade
	) {
		parent::__construct();
		$this->partnerOrderFacade = $partnerOrderFacade;
		$this->transactionFacade = $transactionFacade;
		
	}
	

	// scrypt for backup and restore postgres database
	function dl_file($file){
		$folder = WWW_DIR.'/files/generated-invoices/';
		$file = $folder . $file;
	   if (!is_file($file)) { die("<b>404 File not found!</b>"); }
	   $len = filesize($file);
	   $filename = basename($file);
	   $filename = preg_replace('/invoice/', 'faktura', $filename);

	   $file_extension = strtolower(substr(strrchr($filename,"."),1));
	   $ctype="application/force-download";
	   header("Pragma: public");
	   header("Expires: 0");
	   header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	   header("Cache-Control: public");
	   header("Content-Description: File Transfer");
	   header("Content-Type: $ctype");
	   $header="Content-Disposition: attachment; filename=".$filename.";";
	   header($header );
	   header("Content-Transfer-Encoding: binary");
	   header("Content-Length: ".$len);
	   @readfile($file);
	   exit;
	}

	public function handleDownload( $type, $id_order )
	{
		$o = \DAO\Orders::get()->find( $id_order )->fetch();
		if( $o->idu != \MoneyPoint\LoggedMember::get()->idu )
		{echo "forbidden"; exit;}
		switch ($type) {
			case 'invoice':
				$file = $o->invoice_filename;
				break;
			case 'storno':
				$file = $o->storno_filename;
				break;

			default:
				echo "forbidden"; exit;
				break;
		}

		$this->dl_file($file);
	}

	private function _d_creditsOrder()
	{
		$t = $this->transactionDAO;
		$this->template->transaction = $t;

		// objednavka kreditu
		$order = new \MoneyPoint\CreditOrder( $t->id_order );
		$this->template->creditsOrder = $order;
		$this->template->bankaccount = \AntoninRykalsky\Configs::get()->byContent('general.bank_account').' / '.\AntoninRykalsky\Configs::get()->byContent('general.bank_code');

		// vygenerovano
		$this->template->now = new \Nette\DateTime('now');
		$this->template->user = \MoneyPoint\LoggedMember::get()->user();
		$this->template->detailTemplate = '_creditsOrder.phtml';
		$this->template->title = 'Detail akce – vlastní nákup kreditu';

	}
	private function _d_baseAction( $id )
	{
		$ca =  \DAO\MpCreditsAccounts::get()->find($id)->fetch();
		$this->template->creditChange = $ca;

		// vygenerovano
		$this->template->now = new \Nette\DateTime('now');
		$this->template->user = \MoneyPoint\LoggedMember::get()->user();
		$this->template->detailTemplate = '_baseAction.phtml';
		$this->template->title = 'Detail – '.$ca->reason;
		$this->template->baseAction = 1;

	}
	private function _d_creditsOrderReward()
	{
		$t = $this->transactionDAO;
		$this->template->transaction = $t;

		
		// odměna za objednavku
		$creditOrderReward = new \MoneyPoint\CreditOrdersReward( $t->id_transaction );
		$this->template->creditsOrder = $creditOrderReward;
		
		/* @var $reward \MoneyPoint\Entity\CreditReward */
		$reward = $this->transactionFacade->getCreditReward( $t->reward_id );
		$this->template->reward = $reward;
		
		$deadlineMomentCredits = $creditOrderReward->credits->credits_left-$creditOrderReward->credits->credits_change;
		$counting = new \MoneyPoint\RewardCounting();
		
		$claim = 0;
		$firstCreditBonus = 0;
		if( !empty( $reward ))
		{
			$claim+=$reward->getClaim();
			$firstCreditBonus+=$reward->getFirstCreditBonus();
		}
		$rewardD = $counting->extraRewardsWithReduction($deadlineMomentCredits, $claim+$firstCreditBonus );
		$this->template->rewardReductionDetails = $rewardD;
		
		// vygenerovano
		$this->template->now = new \Nette\DateTime('now');
		$this->template->user = \MoneyPoint\LoggedMember::get()->user();
		$this->template->detailTemplate = '_creditsOrderReward.phtml';
		$this->template->title = 'Detail akce – nákup kreditu v síti';
	}
	private function _d_withdraw()
	{
		$transaction = new \Moneypoint\TransactionDetails( $this->transactionDAO );
		$this->template->detailTemplate = '_withdraw.phtml';
		$this->template->transaction = $transaction;
		$this->template->user = \MoneyPoint\LoggedMember::get()->user();
		$this->template->now = new \Nette\DateTime('now');
//		AR\PointsWithdraw::get()->find($v->id_withdraw)->fetch();
		$this->template->title = 'Detail akce - výplacení odměny';
	}
	private function _d_partnerOrder()
	{
		$this->template->detailTemplate = '_partnerOrder.phtml';

		$this->template->transaction = $t = $this->transactionDAO;
		
		$this->template->order = $order = $this->partnerOrderFacade->details( $t->id_pio_order );

		$p = $order->getPartner();
		
		// load before render
		$order->getPartner();

		$creditsLeft = 0;
		$creditsChange = 0;
		if( !empty( $t->credits_account_id )) {
			$c = AR\CreditsAccounts::get()->find( $t->credits_account_id )->fetch();
			$creditsLeft += $c->credits_left;
			$creditsChange += $c->credits_change;
			$this->template->credits = $c;
		}
		if( !empty( $t->points_account_id ))
			$this->template->points = $p = AR\PointsAccounts::get()->find( $t->points_account_id )->fetch();

		$counting = new \MoneyPoint\RewardCounting(  );
		$rewardD = $counting->extraRewardsWithReduction($creditsLeft-$creditsChange, $order->getZakladniOdmena() );
		$this->template->rewardReductionDetails = $rewardD;
		
		$this->template->user = \MoneyPoint\LoggedMember::get()->user();
		$this->template->now = new \Nette\DateTime('now');
		$this->template->title = 'Detail akce - nákup v obchodě';
	}
	private function _d_differentialTransaction( $notTransactionId = null )
	{
		$this->template->detailTemplate = '_differential.phtml';

		$transactionDao = $this->transactionDAO;
		if( !empty( $this->transactionDAO->id_transaction ))
		{
			$transactionEntity = $this->template->transaction = $this->transactionFacade->secureFind( $this->transactionDAO->id_transaction );
			$this->template->details = $this->transactionFacade->getDifferentialCommisionDetails( $transactionEntity->getDifferentialCommision() );
			$this->template->transactionCommon = $this->transactionDAO;
		} else {
			$this->template->details = $this->transactionFacade->findVolumeCommision( $notTransactionId  );
		}
		
		
		if( !empty( $transactionDao->credits_account_id ))
			$this->template->credits = $c = AR\CreditsAccounts::get()->find( $transactionDao->credits_account_id )->fetch();
		if( !empty( $transactionDao->points_account_id ))
			$this->template->points = $p = AR\PointsAccounts::get()->find( $transactionDao->points_account_id )->fetch();
		
		
		$counting = new \MoneyPoint\RewardCounting();
		$rewardD = $counting->fromTransaction($transactionEntity);
		$this->template->rewardReductionDetails = $rewardD;


		$this->template->user = \MoneyPoint\LoggedMember::get()->user();
		$this->template->now = new \Nette\DateTime('now');
		$this->template->title = 'Detail akce - objemové odměny';

	}
	private function _d_invitation( $p )
	{
		$this->template->detailTemplate = '_invitation.phtml';
		$this->template->pozvanka = \MoneyPoint\Invitation::load($p);

		$this->template->user = \MoneyPoint\LoggedMember::get()->user();
		$this->template->now = new \Nette\DateTime('now');
		$this->template->title = 'Detail pozvánky';
	}

	var $transactionDAO;
	var $transaction;

	public function handleVolumeDetail( $id )
	{
		
		$this->_d_differentialTransaction( $id );
		
//
//		if($this->presenter->isAjax())
//			$this->invalidateControl();
	}
	
	public function handleDetail( $id_transaction )
	{
		// zabezpec aby zobrazil pouze prihlaseny uzivatel
		$this->transactionDAO = $t = \DAO\MpTransaction::get()->find( $id_transaction )->fetch();
		$this->template->detailTemplate = '_empty.phtml';

		switch( $t->id_type )
		{
		  case 2:
			$this->_d_partnerOrder();
			break;
		  case 3:
			$this->_d_withdraw();
			break;
		  case 5:
			$this->_d_differentialTransaction();
			break;
		  default:
			$this->template->detailTemplate = '_empty.phtml';
			if( !empty($t->id_order) )
			{
				$this->_d_creditsOrder();
			} elseif( !empty($t->id_order_reward )) {
				$this->_d_creditsOrderReward();
			}
			
			break;
		}

//		$this->template->transaction = $t;

		if($this->presenter->isAjax())
			$this->invalidateControl();
	}

	public function handleInvitationDetail( $id )
	{
			$ca = \DAO\MpCreditsAccounts::get();
			$pozvanka = \DAO\UserInvitation::get()->findAll()->as('ui')
					->leftJoin( $ca->table )->as('ca')->on('ui.credits_account_id=ca.id')
					->where('ui.id= %i',  $id )->fetch();
	
			$this->_d_invitation( $pozvanka );
		
		if($this->presenter->isAjax())
			$this->invalidateControl();
	}
	
	public function handleCreditTransaction( $id )
	{
		$t = \DAO\MpTransaction::get()->findAll()->where("credits_account_id=%i",$id )->fetch();
		if( !empty( $t->id_type ))
			// standart transakce
			return $this->handleDetail( $t->id_transaction );
		else {
			
			// pozvanka
			$ui = \DAO\UserInvitation::get();
			$pozvanka = \DAO\MpCreditsAccounts::get()->findAll()->as('ca')
					->leftJoin( $ui->table )->as('ui')->on('ui.credits_account_id=ca.id')
					->where('ca.id= %i AND ui.id IS NOT NULL',  $id )->fetch();
			
			if( !empty( $pozvanka ))
			{
				$this->_d_invitation( $pozvanka );
			} else {
				$this->_d_baseAction( $id );
			}
			
			
		}
		
		if($this->presenter->isAjax())
			$this->invalidateControl();
	}
	
	private $templateName = '/template.phtml';
	private $isAdmin = 0;
	public function setAdminMode()
	{
		$this->templateName = '/templateAdmin.phtml';
		$this->isAdmin = 1;
	}

	public function render()
	{
		$do = $this->presenter->getParameter('do');
		$this->template->notAjaxShowTable = !$this->presenter->isAjax() && $do == 'mpTransactionDetail-detail';
		
		$this->template->setTranslator( $this->presenter->getProjectTranslator() );
		$this->template->isAdmin = $this->isAdmin;
		$this->template->setFile(dirname(__FILE__) . $this->templateName );
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
	}




}
