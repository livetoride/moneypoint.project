<?php

/**
 * Nette Framework Extras
 *
 * This source file is subject to the New BSD License.
 *
 * For more information please see http://extras.nettephp.com
 *
 * @copyright  Copyright (c) 2009 David Grudl
 * @license    New BSD License
 * @link       http://extras.nettephp.com
 * @package    Nette Extras
 * @version    $Id: VisualPaginator.php 4 2009-07-14 15:22:02Z david@grudl.com $
 */

namespace Moneypoint;

use AntoninRykalsky as AR;
use AntoninRykalsky\PointsWithdraw as DAOPointsWithdraw;
use ConfirmationDialog;
use DataGrid\DataGrid;
use DataGrid\DataSources\Dibi\DataSource;
use DataGrid\Renderers\Conventional;
use MoneyPoint\BankAccount;
use MoneyPoint\LoggedMember;
use MoneyPoint\PointsWithdrawCreator;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\Html;

class MpWithdrawControl extends Control
{

	public function render()
	{
		$idu = LoggedMember::get()->idu;

		// has bankaccount
		$ba = new BankAccount();
		if( !$ba->hasBankAccount( $idu ) )
		{
			$this->template->setBankAccount = 1;
		} else {
			$this->template->bankAccount = $ba->getUserActiveBankAccount( $idu );
		}

		$this->template->menukey = 2;

		$pwc = new PointsWithdrawCreator();
		$this->template->upperLimit = $uL = $pwc->getUpperLimit();
		$dokupReserved = $pwc->getDokupReserved();
		$this->template->dokupReserved = (int)@$dokupReserved->cena_sdph*@$dokupReserved->product_quantity;
		$this->template->waitingSum = $pwc->getWaitingSum();

		$this->template->waiting = $pwc->getWaitingWithdraw( $idu );

		$points = $pwc->getPoints();
		$this->template->points = @$points->points_left;

		// minimal withdraw
		$minimalWithdraw = $pwc->getMinimalWithdraw();
		$this->template->minWithdraw = $mW = $minimalWithdraw;

		$this->template->cannotWithdraw = $mW > $uL;
		$this->template->withdrawForm = $this['withdraw'];

		$this->template->setFile(dirname(__FILE__) . '/template.phtml' );
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
	}

	protected function createComponentWithdraw()
    {
        $form = new Form($this, 'withdraw');

		$form->addText('price', 'Kolik bodů chcete vybrat?');
		$p =  $form['price']->getControlPrototype();
		$p->setId("ui-tabs-panel");;
		$form->addHidden('id_order', 'ID objednávky');

		$form->addSubmit('save', 'Odeslat požadavek');
		$form->onSuccess[] = array($this, 'submitedWithdraw');
		return $form;
    }

	function submitedWithdraw( Form $form )
    {
		if(isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			
			$v['price'] = preg_replace('# #', '', $v['price']);
			if( !is_numeric( $v['price'] ))
			{
				$this->presenter->flashMessage('Zadejte hodnotu pro výběr odměny pouze jako celé kladné číslo (např. 1000)', \Flashes::$error );
				$this->redirect('this');
				return;
			}
			
			$pwc = new PointsWithdrawCreator();
			$upper = $pwc->getUpperLimit();
			$minimalWithdraw = $pwc->getMinimalWithdraw();
			$limit = $upper;
			if( $v['price']>$limit )
			{
				$this->presenter->flashMessage('Požadavek přesahuje celkovou hodnotu nevyplacených odměn!', \Flashes::$error);
				$this->redirect('this');
				return;
			}
			if( $v['price']<$minimalWithdraw )
			{
				$this->presenter->flashMessage('Minimální částka pro vyplacení je '.$minimalWithdraw.' Kč!', \Flashes::$error);
				$this->redirect('this');
				return;
			}

			LoggedMember::get()->withdraw()->createWithdraw( $v['price'] );

			$this->presenter->flashMessage('Požadavek na vyplacení odměny byl odeslán', AR\Flashes::$success );
			if( !$this->presenter->isAjax() )
				$this->redirect('this');
			else
				$this->invalidateControl();
		}
	}

	/** ConfirmationDialog factory */
	public function createComponentConfirmForm()
	{
		
		ConfirmationDialog::$_strings = array(
			'yes' => 'Ano',
			'no' => 'Ne',
			'expired' => 'Confirmation token has expired. Please try action again.',
		);
		$form = new ConfirmationDialog();

		$form->dialogClass = 'static_dialog';

		$form->addConfirmer(
			'cancelWithdraw',
			array($this, 'confirmedCancelWithdraw'),
			sprintf('Opravdu chcete zrušit tento požadavek?')
		);

		return $form;
	}

	public function confirmedCancelWithdraw( $id_withdraw )
	{
		$w = new \MoneyPoint\PointsWithdraw( $id_withdraw );
		$w->checkUser();
		$result = $w->setStorno();

		if( $result==1 )
			$this->presenter->flashMessage('Výběr byl stornován', \AntoninRykalsky\Flashes::$success );

		if (!$this->presenter->isAjax())
			$this->redirect('this');
	}

	protected function createComponentWaitingWithdraw()
    {
		$model = DAOPointsWithdraw::get()->getDataSource();
		$idu = LoggedMember::get()->idu;
		$model->where("[idu]=%i AND [accepted]=0 AND [deleted]=%i", $idu, 0 );

		//datagrid
		$model = new DataSource($model);
		$grid = new DataGrid;
		$renderer = new Conventional;
		$renderer->paginatorFormat = '';
		$renderer->footerFormat = '';
		$grid->setDataSource( $model );
		$grid->setRenderer($renderer);
		$grid->itemsPerPage = 10;
		$grid->multiOrder = FALSE;

		$operations = array();
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, DAOPointsWithdraw::get()->getPrimary());

		$grid->addColumn('request_timestamp', 'Datum vystavení požadavku');
		$grid['request_timestamp']->formatCallback[] = 'FormatingHelpers::czechDate';
		$grid->addColumn('points', 'výše odměny k vyplacení');
		$grid['points']->formatCallback[] = 'FormatingHelpers::body';

		$grid->addActionColumn('Akce')->getHeaderPrototype()->style('width: 10px');
		$grid['Akce']->getHeaderPrototype()->class('hideaction');
		$icon = Html::el('span');
		$grid->addAction('Zrušit výběr', 'confirmForm:confirmCancelWithdraw!', clone $icon->class('icon icon-del'));

		return $grid;
    }



}
