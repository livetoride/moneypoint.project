<?php

/**
 * Nette Framework Extras
 *
 * This source file is subject to the New BSD License.
 *
 * For more information please see http://extras.nettephp.com
 *
 * @copyright  Copyright (c) 2009 David Grudl
 * @license    New BSD License
 * @link       http://extras.nettephp.com
 * @package    Nette Extras
 * @version    $Id: VisualPaginator.php 4 2009-07-14 15:22:02Z david@grudl.com $
 */

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class MpFilters extends Control
{

	public function setEmptyFilters()
	{
		$data = \AntoninRykalsky\SystemUser::get()->getData();
//		if( date('Y-m-d', strtotime('now')) >= date('Y-m-d', strtotime('1.3.2014')))
//			$d['date_since'] = '1.3.2014';
//		else
			$d['date_since'] = '1.3.2014';
		
		$d['date_to'] = date('d.m.Y', strtotime('now'));
		$d['id_type'] = 0;
		$data->filters = $d;
	}

	public function filterSubmitted( Form $form )
	{
		if ($form['reset']->isSubmittedBy()) {
			$this->setEmptyFilters();
		
			if( $this->presenter->isAjax() )
				$this->presenter->invalidateControl();
			else
				$this->presenter->redirect('this');
		}

		if ($form['filtr']->isSubmittedBy()) {
			$v =(array) $form->getValues();
			$data = \AntoninRykalsky\SystemUser::get()->getData();
			$d['date_since'] = $v['date_since'];
			$d['date_to'] = $v['date_to'];
			$d['id_type'] = $v['id_type'];
			$data->filters = $d;

			if( $this->presenter->isAjax() )
				$this->presenter->invalidateControl();
			else
				$this->presenter->redirect('this');
		}
	}

	protected function createComponentFilterForm()
	{
		$this->template->accountPrehledy = $this->presenter->creditsAccountDS();

        $form = new Form($this, 'filterForm');
        #$form->setMethod('post');

		/*
		 <input type="text" name="year1" id="year1" class="datepicker" value="1.1.2012" />
		 <input type="text" name="year2" id="year2" class="datepicker"  />
		 */
        $date_since = $form->addText('date_since', 'dateSince');
		$date_since->getControlPrototype()->class('datepicker');

        $date_to = $form->addText('date_to', 'date_to');
		$date_to->getControlPrototype()->class('datepicker');

		$items=array(
			0=>'vše',
//			4=>'vlastní nákupy kreditu',
			2=>'nákupy v obchodech',
			1=>'základní odměny',
			5=>"objemové odměny",
			3=>'výplaty odměn'
		);
        $id_type = $form->addSelect('id_type', 'id_type', $items);
		$id_type->getControlPrototype()->class('jquery-selectbox');


//					<select class="mp-select" name="filter-type">
//						<option value="0">všechny akce</option>
//						<option value="2">nákupy v obchodech</option>
//						<option value="1">nákupy kreditu v síti</option>
//						<option value="4">vlastní nákupy kreditu</option>
//						<option value="3">výběry odměn</option>
//					</select>

		if( $this->isAdmin )
		{
			$form->addSubmit('reset', 'reset');
			$form->addSubmit('filtr', 'filtr');
		} else {
			$form->addSubmit('reset', '');
			$form['reset']->getControlPrototype()->class('ajax');
			$form->addSubmit('filtr', '');
			$form['filtr']->getControlPrototype()->class('ajax');
		}


		$form->onSuccess[] = array($this, 'filterSubmitted');


        $template = $this->createTemplate();
        $form->addProtection('Please submit this form again (security token has expired).');

		return $form;
	}

	private $templateFile = '/template.phtml';
	private $isAdmin = 0;

	public function setAdminMode()
	{
		$this->templateFile = '/templateAdmin.phtml';
		$this->isAdmin = 1;
	}

	/**
	 * Renders paginator.
	 * @return void
	 */
	public function render()
	{
		$data = \AntoninRykalsky\SystemUser::get()->getData();
		if(empty($data->filters['date_since']) || empty($data->filters['date_to']) )
			$this->setEmptyFilters();
		
		$this->template->form = $f = $this['filterForm'];
		
		$f->setDefaults( (array)$data->filters );
		$f->setValues( (array)$data->filters );

		$this->template->setFile(dirname(__FILE__) . $this->templateFile );
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->render();
	}
}
