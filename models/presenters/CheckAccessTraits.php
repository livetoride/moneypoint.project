<?php

use Nette\Environment;

trait checkAccessTraits {



    public function checkAccess() {
		
		
		if( $this->backlink() != ':Front:Auth:logout')
		{
			if ( $this->user->isLoggedIn() &&
					!is_numeric( \AntoninRykalsky\SystemUser::get()->getIdu())) {

				$this->forward(':Front:Auth:logout');
			}
		}

        // if the user is not allowed access to the ACL, then redirect him
		$this->user->getStorage()->setNamespace('mlm');
		$s = Environment::getSession('user');
		$this->template->userPanelName = $s->login;
		$this->template->clientName = $s->name;
		$this->template->clientSurname = $s->surname;
		$this->template->clientID = $s->ID;


		// sets referer
		$referrer = $this->getParam('r');
 		if(!empty($referrer) && is_numeric($referrer))
		{
			$u = AR\Uzivatele::get()->find( $referrer )->fetch();
			if(is_numeric($u->idu) && $u->idrole=2 && $u->idu>0 )
			{
				$response = $this->getHttpResponse();
				$response->setCookie('referrer', $referrer, time()+30*60*60*24 );
			} else unset( $_COOKIE['referrer']);
			$this->redirect( $this->backlink() );
		}



		// gets referer
		$request = Environment::getHttpRequest();
		$referrerCookie = $request->getCookie('referrer');
		if( !empty($referrerCookie))
			$this->referrer = $referrerCookie;


		$allowed = array();
		$allowed[]=':Front:Oops:forbidden';
		$allowed[]=':Front:Error:default';
		$allowed[]=':Front:News:show';
		$allowed[]=':Front:Auth:recovery';
		$allowed[]=':Front:Cron:default';
		$allowed[]=':Front:Auth:recoveryNew';
		$allowed[]=':Front:Auth:confirmMail';
		$allowed[]=':Front:Auth:registration';
		$allowed[]=':Front:Captcha:registration';
		$allowed[]=':Front:Auth:cities';
		$allowed[]=':Front:Auth:login';


		if ( !$this->user->isAllowed($this->resource, 'show')
		) {
			$this->flashMessage('Byl jste odhlášen z Vašeho účtu, prosím přihlaste se', Flashes::$info );
			$this->redirect(':Front:Auth:login');
        }


		if(	empty($s->idu) && !in_array($this->backlink(), $allowed ))
		{
			//$this->forward('Oops:forbidden');
		}
    }
	
	
	public function MlmAdmin_checkAccess() {
        // if the user is not allowed access to the ACL, then redirect him
		$s = Environment::getSession('admin-user');
		
		if(  \AntoninRykalsky\SystemUser::get()->isLogged() )
		{
			$idu = \AntoninRykalsky\SystemUser::get()->getIdu();
			$userPanel = \DAO\Uzivatele::get()->find( $idu )->fetch();
			#print_r( $userPanel );exit;
			$this->template->userPanel = $userPanel;
		} else {
			#$this->forward(':Admin:Login:logout');
		}
		#print_r($member->user());exit;

		$this->adminStuff();
		
		$a = $this->user->isAllowed('secretary_resource', MLMADMIN_PRIVILEGE);

        if (	$this->backlink() != ':Admin:Login:default' &&
				$this->backlink() != ':Admin:Captcha:show' &&
				!Environment::isConsole()
				&& (
					!$this->user->isAllowed($this->resource, MLMADMIN_PRIVILEGE)
				)
				) {
			#echo !$this->user->isAllowed($this->resource, MLMADMIN_PRIVILEGE);
			#exit;
			$this->forward(':Admin:Login:default');
        }



		// pokud můžeš do backendu ale ne do tohoto resource, blokni to
		// může do backendu - pouze odstřihni od resource
		 if (	$this->backlink() != ':Admin:Login:default' &&
				$this->backlink() != ':Admin:Captcha:show' &&
				!Environment::isConsole() &&
				!$this->user->isAllowed($this->resource, MLMADMIN_PRIVILEGE)) {
            // @todo change redirect to login page
//			echo 'redirect forbiden';exit;
//			$this->forward('Oops:forbidden');
        }

    }


}
