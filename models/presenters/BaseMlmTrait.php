<?php

use AntoninRykalsky as AR;

trait baseMlmTrait {
    
	/** @var MoneyPoint\MemberFacade */
	protected $memberFacade;

	function injectMember(MoneyPoint\MemberFacade $memberFacade) {
		$this->memberFacade = $memberFacade;
	}


	protected function startup()
	{
		// rule no1
		parent::startup();

		$vop = $this->template->vop = \DAO\Terms::get()->findAll()->where('[active]=1')->fetch();
		
		$this->projectTranslator = new \MoneyPointTranslator();
		$this->template->setTranslator( $this->projectTranslator );
//		echo "inc";exit;
	}
	
	protected $projectTranslator;
	
	public function getProjectTranslator()
	{
		return $this->projectTranslator;
	}

	public function createComponentAccountState() {
		$komponenta = new \MPAccountState($this, 'accountState');
		$komponenta->logged = $this->user->isInRole('member');
		$this->idu = AR\SystemUser::get()->getIdu();
		if( !empty( $this->idu ))
		{
			if( !empty(\AntoninRykalsky\CreditsAccounts::get()->actualState( $this->idu )->credits_left))
				$komponenta->credits = \AntoninRykalsky\CreditsAccounts::get()->actualState( $this->idu )->credits_left;
			else
				$komponenta->credits = 0;

			if( !empty(\AntoninRykalsky\PointsAccounts::get()->actualState( $this->idu )->points_left))
				$komponenta->points = \AntoninRykalsky\PointsAccounts::get()->actualState( $this->idu )->points_left;
			else
				$komponenta->points = 0;
		}
		return $komponenta;
	}




	// <editor-fold defaultstate="collapsed" desc="components">
    public function createComponentMenuControl() {
            $komponenta = new UberMenuControl($this, 'menuControl');
            return $komponenta;
    }

	
    public function createComponentUserControl() {
            $komponenta = new UserControl( $this->memberFacade );
            return $komponenta;
    }
	
	public function createComponentBusinessBuy() {
            $komponenta = new \MoneyPoint\BusinessBuy($this, 'businessBuy');
            return $komponenta;
    }

	public function createComponentMlmadminUserControl() {
            $komponenta = new Mlmadmin_UserControl($this, 'mlmadminUserControl');
            return $komponenta;
    }

	public function createComponentMlmadminMenuControl() {
            $komponenta = new Mlmadmin_MenuControl($this, 'mlmadminMenuControl');
            return $komponenta;
    }

	public function createComponentProducersControl() {
            $komponenta = new ProducersControl($this, 'producersControl');
            return $komponenta;
    }

    public function createComponentCategoryControl() {
            $komponenta = new CategoryControl($this, 'categoryControl');
            return $komponenta;
    }

    protected function createComponentNavigation($name) {

		//$eshop = MlmConfig::get()->getConfig('eshop');
		$eshop = AR\Configs::get()->byContent('general.eshop_title');
		$nav = new Navigation($this, $name);
		$nav->setupHomepage( $eshop , $this->template->baseUri );
    }

	// </editor-fold>




}
