<?php

namespace MoneyPoint\FutureReward;

class FirstLineFutureReward
{
	private $user;
	private $creditsOrder;
	private $creditsOrderReward;
	
	public function setReward( 
			\MoneyPoint\Entity\Member $from, 
			\MoneyPoint\Entity\CreditOrder $creditsOrder, 
			\MoneyPoint\Entity\CreditReward $creditsOrderReward
	) {
		$this->user = $from;
		$this->creditsOrder = $creditsOrder;
		$this->creditsOrderReward = $creditsOrderReward;
	}
	
	public function getUser() {
		return $this->user;
	}

	public function getCreditOrder() {
		return $this->creditsOrder;
	}

	public function getCreditOrderReward() {
		return $this->creditsOrderReward;
	}





}

?>