<?php

namespace MoneyPoint\FutureReward;

class FirstLineFutureRewards
{
	private $rewards;
	public function __construct() {
		$this->rewards = array();
	}
	
	public function getRewards() {
		return $this->rewards;
	}
	
	public function addReward( FirstLineFutureReward $reward )
	{
		$this->rewards[] = $reward;
	}



}

?>