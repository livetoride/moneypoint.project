<?php

namespace MoneyPoint\FutureReward;

class FutureRewards
{
	private $rewards;
	private $rewardsCount=0;
	private $rewardsSum=0;
	private $days;
	
	/**
	 * 
	 * @param type $start
	 * @param type $days
	 */
	public function __construct( $start=null, $days = 21 )
	{
		
		$this->days = $days++;;
		$this->rewards = array();
		for( $i=0; $i<$days; $i++ )
		{
			$date = new \DateTime();
			$oDateIntervalValue = new \DateInterval('P'.$i.'D');
			$date->add($oDateIntervalValue);
			$this->rewards[ $date->format('Y-m-d') ] = new Entity( $date );
		}
		#print_r( $this->rewards );exit;
	}
	
	/**
	 * 
	 * @param type $date
	 * @param type $posibleReward
	 * @param type $rewardsSum
	 * @throws \LogicException
	 */
	public function addReward( $date,  $posibleReward, $rewardsSum )
	{
		if(!isset( $this->rewards[ $date ] )) {
			#throw new \LogicException('Tato odměna nepatří do úseku '.$this->days.' dní.');
			return;
		}
					
		$this->rewards[ $date ]->addPosibleReward( $posibleReward, $rewardsSum );
		$this->rewardsSum+=$posibleReward;
		$this->rewardsCount+=$rewardsSum;
	}
	
	public function getRewards() {
		return $this->rewards;
	}

	/** vrací část (třetinu) dní s odměnami */
	public function getRewardsPart( $partNo, $fragmentLength ) {
		$offset = ( $this->days / $fragmentLength ) * ($partNo-1);
		$limit = ( $this->days / $fragmentLength ) * $partNo;
		
		if( $partNo == $fragmentLength )
			$limit = 100;
		
		$return = array();
		$i=0;
		foreach( $this->rewards as $k => $v )
		{
			
			if( $i >= $offset && $i<$limit )
				$return[ $k ]=$v;
			$i++;
		}
		return $return;
	}

	public function getRewardsSum() {
		return $this->rewardsSum;
	}
	
	public function getRewardsCount() {
		return $this->rewardsCount;
	}
}

?>