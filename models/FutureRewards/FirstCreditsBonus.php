<?php

namespace MoneyPoint\FirstCreditsBonus;

class FirstCreditsBonus
{
	protected $firstCreditsBonus;
	protected $newFlag;
	protected $allowFirstCreditsBonus;
	
	public function __construct() {
		$this->allowFirstCreditsBonus = \AntoninRykalsky\Configs::get()->byContent('multilevel.allowFirstCreditsBonus');
	}
	
	/**
	 * spočítá výši BPO
	 * @param type $creditsSum nakupovane kredity
	 * @param type $hadCreditFirstBuy priznak zda dostal BPO (1/2)
	 * @param type $predciLevel level kam bonus patri
	 * @return type
	 */
	public function countBonus( $creditsSum, $hadCreditFirstBuy, $predciLevel )
	{
		if( $predciLevel != 1 )
			return $this->firstCreditsBonus=0;
		
		if( $this->allowFirstCreditsBonus  )
		{
			if( $hadCreditFirstBuy != 2 )
			{
				$discount = \DAO\MpCreditsDiscount::get()->findAll()->where('credits=%i', $creditsSum )->fetch();
				if( $hadCreditFirstBuy != 2 && $discount->flag == "for-business-status" )
				{
					$this->firstCreditsBonus = $discount->bpo_coef;
					$this->newFlag = 2;
				} elseif( $hadCreditFirstBuy == 0 && $discount->flag != "for-business-status" ) {
					$this->firstCreditsBonus = $discount->bpo_coef;
					$this->newFlag = 1;
				}
				
			}
		}
		
		return $this->firstCreditsBonus;
	}
}

?>