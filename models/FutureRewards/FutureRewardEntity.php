<?php

namespace MoneyPoint\FutureReward;

/** 
 * @entity
 * @table(name="`mpCreditReward`")
 */

class Entity
{
	private $tsResolution;
	private $rewardCount;
	private $maxPosibleRewardSum;
	
	public function getTsResolution() {
		return $this->tsResolution;
	}

	public function getRewardCount() {
		return $this->rewardCount;
	}

	public function getMaxPosibleRewardSum() {
		return $this->maxPosibleRewardSum;
	}

	public function __construct( \DateTime $tsResolution ) {
		$this->tsResolution = $tsResolution->format('Y-m-d');
		$this->rewardCount = 0;
		$this->maxPosibleRewardSum = 0;
	}
	
	public function addPosibleReward( $reward, $rewardCount ){
		$this->rewardCount = $rewardCount;
		$this->maxPosibleRewardSum+=$reward;
	}
}

?>