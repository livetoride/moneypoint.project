<?php
namespace MoneyPoint\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="partners")
 * @ORM\Entity
 * 
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap({1 = "Eshop", 2 = "Partner", 3 = "Shop" })
 */
class Partner extends PartnerCommon
{
	public static $PROVIDERS = array (0=>'Bez providera', 3=>'CJ.com', 2=>'espoluprace.cz', 1=>'simplia.cz');	
	
	/**
     * @var integer
     *
     * @ORM\Column(name="id_partner", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="partners_id_partner_seq", allocationSize=1, initialValue=1)
     */
    private $idPartner;
	
	/**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

	 /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="full_link", type="string", length=511, nullable=true)
     */
    private $fullLink;

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="string", length=255, nullable=true)
     */
    private $img;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=64, nullable=true)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="desc", type="text", nullable=true)
     */
    private $desc;

    /**
     * @var string
     *
     * @ORM\Column(name="instructions", type="text", nullable=true)
     */
    private $instructions;

    /**
     * @var string
     *
     * @ORM\Column(name="allow_uid", type="decimal", precision=1, scale=0, nullable=true)
     */
    private $allowUid = '0';
	
	/**
     * @ORM\oneToMany(targetEntity="MoneyPoint\Entity\PartnerFeeRate", mappedBy="partner")
     */
    private $feeRates;
	
	/**
     * @var string
     *
     * @ORM\Column(name="reward", type="string", length=128, nullable=true)
     */
    private $reward;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", length=1)
     */
    private $active;

	private $basePath = '';
	
	/**
     * @ORM\Column(name="provider_id", type="integer")
     */
    private $providerId;

	// <editor-fold defaultstate="collapsed" desc=" getters ">
	
	public function getId() {
		return $this->idPartner;
	}
	public function getIdPartner() {
		return $this->idPartner;
	}


	public function getLink() {
		return $this->link;
	}

	public function getName() {
		return $this->name;
	}

	public function getFullLink() {
		return $this->fullLink;
	}

	public function getImg() {
		$r = str_replace("[+baseUri+]", $this->basePath, $this->img);
		return $r;
	}

	public function getPath() {
		return $this->path;
	}

	public function getDesc() {
		return $this->desc;
	}

	public function getInstructions() {
		return $this->instructions;
	}

	public function getAllowUid() {
		return $this->allowUid;
	}

	public function getReward() {
		return $this->reward;
	}


// </editor-fold>
	
	public function setBasePath( $path )
	{
		$this->basePath = $path;
	}
	
	public function setDeleted( $deleted = 1 )
	{
		$this->active = !$deleted;
	}
	
	private $localImg;
	public function setLocalImg( $localImg )
	{
		$this->localImg = $localImg;
	}
	public function getLocalImg()
	{
		return $this->basePath . $this->localImg;
	}

	public function __toArray() {
        $reflection = new \ReflectionClass($this);
        $details = array();
		foreach ($reflection->getMethods() as $method ) {
			preg_match('#^get(.+)#', $method->name, $matches );
			if( !empty( $matches[1] ))
			{
				$methodName = lcfirst( $matches[1] );
				eval('$details[ $methodName ] = $this->'.$method->name . '();' );
			}
		}
		$details['provider_id'] = $details['providerId'];
		unset ($details['providerId']); 
//		\Doctrine\Common\Util\Debug::dump( $details );exit;
		
        return $details;
    }
	


	public function getProviderId() {
		return $this->providerId;
}

	public function setProviderId($providerId) {
		$this->providerId = $providerId;
	}




}


