<?php
/*
<span class="jquery-selectbox-item value-0 item-0">všechny akce</span>
<span class="jquery-selectbox-item value-4 item-1">vlastní nákupy kreditu</span>
<span class="jquery-selectbox-item value-2 item-2">nákupy v obchodech</span>
<span class="jquery-selectbox-item value-1 item-3">nákupy kreditu v síti</span>
<span class="jquery-selectbox-item value-3 item-4">výběry odměn</span>
5 objemové odměny
 * 
 *  */
namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nette;

/**
 * MpTransaction
 *
 * @ORM\Table(name="mp_transaction", indexes={@ORM\Index(name="mp_transaction_credits_account_id_index", columns={"credits_account_id"})})
 * @ORM\Entity(repositoryClass="MoneyPoint\TransactionRepository")
 */
class Transaction
{
	const CREDIT_ORDER = 1;
	const PARTNER_ORDER = 2;
	const DIFFERENTIAL_COMMISION = 5;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id_transaction", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_transaction_id_transaction_seq", allocationSize=1, initialValue=1)
     */
    private $idTransaction;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_type", type="integer", nullable=true)
     */
    private $idType;

    /**
	 * @ORM\ManyToOne(targetEntity="PointAccount")
	 * @ORM\JoinColumn(name="points_account_id", referencedColumnName="id")
	 */
    private $pointsAccount;

   
	/**
	 * @ORM\ManyToOne(targetEntity="CreditAccount")
	 * @ORM\JoinColumn(name="credits_account_id", referencedColumnName="id")
	 */
    private $creditsAccount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insert_timestamp", type="datetime", nullable=true)
     */
    private $insertTimestamp;

	/**
     * @var MoneyPoint\Entity\Member
     *
     * @ORM\ManyToOne(targetEntity="MoneyPoint\Entity\Member")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idu", referencedColumnName="idu")
     * })
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="mp_trans_id", type="string", nullable=true)
     */
    private $mpTransId;

	/**
	 * @var MoneyPoint\Entity\CreditOrder
	 * 
     * @ORM\OneToOne(targetEntity="MoneyPoint\Entity\CreditOrder", inversedBy="transaction")
     * @ORM\JoinColumn(name="id_order", referencedColumnName="id_order")
     * })
     */
    private $creditOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_order_reward", type="integer", nullable=true)
     */
    private $idOrderReward;


	/**
	 * @var MoneyPoint\Entity\PartnerOrder
	 * 
     * @ORM\OneToOne(targetEntity="MoneyPoint\Entity\PartnerOrder", inversedBy="transaction")
     * @ORM\JoinColumn(name="id_pio_order", referencedColumnName="id_order")
     * })
     */
    private $partnerOrder;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reward_timestamp", type="datetimetz", nullable=true)
     */
    private $rewardTimestamp;

	
	/**
	 * @var MoneyPoint\Entity\CreditReward
	 * 
     * @ORM\OneToOne(targetEntity="MoneyPoint\Entity\CreditReward", inversedBy="transaction")
     * @ORM\JoinColumn(name="reward_id", referencedColumnName="id_reward")
     * })
     */
	private $reward;
	
	/**
	 * @var MoneyPoint\Entity\PeriodNetwork
     * @ORM\OneToOne(targetEntity="PeriodNetwork", mappedBy="transaction")
     */
	private $differentialCommision;
	
	

	public function __construct() {
		$this->insertTimestamp = new \DateTime();
	}
	
	public function getId() {
		return $this->idTransaction;
	}

		
	public function setPartnerOrder( \MoneyPoint\Entity\PartnerOrder $partnerOrder) {
		$this->partnerOrder = $partnerOrder;
		$this->idType = self::PARTNER_ORDER;
		$this->user = $partnerOrder->getUser();
		
		// just in case
		$partnerOrder->setTransation($this);
	}
	
	public function setCreditOrder( \MoneyPoint\Entity\CreditOrder $creditsOrder) {
		$this->creditOrder = $creditsOrder;
		$this->idType = self::CREDIT_ORDER;
		$this->user = $creditsOrder->getUser();
		$this->insertTimestamp = $creditsOrder->getOrderTimestamp();
	}
	
	
	public function getDifferentialCommision() {
		return $this->differentialCommision;
	}

		
	public function setDifferentialCommision( $diffCommision )
	{
//		$this->creditOrder = $diffCommision;
		$this->idType = self::DIFFERENTIAL_COMMISION;
		$this->user = $diffCommision->getUser();
	}
	
	public function getPartnerOrder() {
		return $this->partnerOrder;
	}

		
	public function getUser() {
		return $this->user;
	}

	public function getType() {
		return $this->idType;
	}
	
	public function getReward() {
		foreach( $this->reward as $r )
		{
			return $r;
		}
	}
	
	public function setReward( \MoneyPoint\Entity\CreditReward $reward, $points_account_id, $credits_account_id, $user, $creditOrder ) {
		$this->reward = $reward;
		$this->idType = self::CREDIT_ORDER;
		$this->creditsAccountId = $credits_account_id;
		$this->pointsAccount = $points_account_id;
		$this->user = $user;
		$this->rewardTimestamp = new \DateTime;
		// TODO drop line
		$this->idOrderReward = $creditOrder->getIdOrder();
		$this->mpTransId = $creditOrder->getTransaction()->getMpTransactionId();
		
	}

	
	public function setMpTransactionId($mpTransId) {
		$this->mpTransId = $mpTransId;
	}
	public function getMpTransactionId() {
		return $this->mpTransId;
	}
	
	/** @deprecated */
	public function setPointsAccountId($pointsAccountId) {
		$this->pointsAccount = $pointsAccountId;
	}
	public function setPointsAccount($pointsAccount) {
		$this->pointsAccount = $pointsAccount;
	}

	/** @deprecated */
	public function setCreditsAccountId($creditsAccountId) {
		$this->creditsAccountId = $creditsAccountId;
	}
	
	public function setCreditsAccount($creditsAccount) {
		$this->creditsAccount = $creditsAccount;
	}
	
	public function getPointsAccount() {
		return $this->pointsAccount;
	}

	/** @deprecated */
	public function getCreditsAccountId() {
		return $this->creditsAccountId;
	}
	
	public function getCreditsAccount() {
		return $this->creditsAccount;
	}

	public function fixInsertTimestamp( $ts )
	{
		$this->insertTimestamp = $ts;
	}




}
