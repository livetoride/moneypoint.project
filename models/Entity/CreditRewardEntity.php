<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
 * Vzniká po přidělení odměny
 * @ORM\Entity(repositoryClass="MoneyPoint\CreditRewardRepository")
 * @ORM\Table(name="`mpCreditReward`")
 */
class CreditReward
{
    /**
     * @ORM\Column(name="id_reward", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="`mpCreditReward_id_reward_seq`", allocationSize=1, initialValue=1)
     */
	private $id;
	
	/** @ORM\Column(type="integer") */
	private $line;
	
	/** @ORM\Column(type="decimal") */
	private $claim;
	
	/** @ORM\Column(name="`firstCreditsBonus`", type="decimal") */
	private $firstCreditsBonus;
	
	/** @ORM\Column(name="`possibleClaim`",type="decimal") */
	private $possibleClaim;

	/**
	 * Just temorary holder
	 */
	private $idu;
	
	/**
	 * @ORM\ManyToOne(targetEntity="CreditOrder", inversedBy="creditRewards")
	 * @ORM\JoinColumn(name="order_id", referencedColumnName="id_order")
	 */
	private $creditOrder;
	
	/**
	 * 
	 * @param type $line
	 * @param type $claim
	 * @param type $firstCreditsBonus
	 * @param type $possibleClaim
	 */
	public function setReward($line, $claim, $firstCreditsBonus = 0, $possibleClaim = null ) {
		$this->line = $line;
		$this->claim = $claim;
		$this->firstCreditsBonus = $firstCreditsBonus;
		$this->possibleClaim = $possibleClaim;
	}
	
	public function setCountedReward($line, $claim, $idu )
	{
		$this->line = $line;
		$this->claim = $claim;
		$this->idu = $idu;
	}
	
	public function setCreditOrder($creditOrder) {
		$this->creditOrder = $creditOrder;
	}

	
		
	// <editor-fold defaultstate="collapsed" desc="getters">
	public function getId_reward() {
		return $this->id;
	}
	public function getId() {
		return $this->id;
	}

	public function getLine() {
		return $this->line;
	}

	public function getClaim() {
		return $this->claim;
	}

	public function getFirstCreditBonus() {
		return $this->firstCreditsBonus;
	}
	
	public function getFirstCreditBonusBpo() {
		return $this->firstCreditsBonus*2;
	}
	
	public function getTotalReward() {
		return $this->firstCreditsBonus + $this->claim;
	}
	
	/**
	 * Claim if user had premium account
	 * @return double
	 */
	public function getPossibleClaim() {
		return $this->possibleClaim;
	}

	public function getIdu() {
		return $this->idu;
	}
	
	public function getCreditOrder() {
		return $this->creditOrder;
	}

			
	// </editor-fold>




}

?>