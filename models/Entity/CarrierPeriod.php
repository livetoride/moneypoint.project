<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MpCarrierPeriod
 *
 * @ORM\Table(name="mp_carrier_period")
 * @ORM\Entity
 */
class CarrierPeriod
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_carrier_period_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	/**
     * @var MoneyPoint\Entity\CarrierGroup
     *
     * @ORM\ManyToOne(targetEntity="MoneyPoint\Entity\CarrierGroup")
     * @ORM\joinColumns({
     *   @ORM\JoinColumn(name="carrier_version_id", referencedColumnName="id")
     * })
     */
    private $carrierGroup;

   
	/**
     * @var MoneyPoint\Entity\Period
     *
     * @ORM\ManyToOne(targetEntity="MoneyPoint\Entity\Period")
     * @ORM\joinColumns({
     *   @ORM\JoinColumn(name="period_id", referencedColumnName="id")
     * })
     */
    private $period;

	public function getCarrierGroup() {
		return $this->carrierGroup;
	}

	public function getPeriod() {
		return $this->period;
	}

	public function setCarrierGroup($carrierGroup) {
		$this->carrierGroup = $carrierGroup;
	}

	public function setPeriod($period) {
		$this->period = $period;
	}



}
