<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**/
// remove
use AntoninRykalsky\Handy;
use Exception;
/**/

/**
 * @ORM\Entity
 */
class PartnerOrderManual
{
	const STATE_CJ_NEW = 0;
	const STATE_WAITING = 1;
	const STATE_REWARDED = 3;
	const STATE_STORNO = 4;
	
	static $states = array(
		1=> 'čekací lhůta',
//		2=> 'vyrazeno-ke kontrole',
		3=> 'připsaná',
		4=> 'storno'
	);
	
	
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id_order", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $idOrder;

	/**
     * @var MoneyPoint\Entity\PartnerFeeRate
     *
     * @ORM\ManyToOne(targetEntity="MoneyPoint\Entity\PartnerFeeRate")
     * @ORM\joinColumns({
     *   @ORM\JoinColumn(name="fee_rate_id", referencedColumnName="id")
     * })
     */
    private $partnerFeeRate;


	
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insert_timestamp", type="datetime", nullable=true)
     */
    private $insertTimestamp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="settle_timestamp", type="datetime", nullable=true)
     */
    private $settleTimestamp;

    /**
     * @var string
     *
     * @ORM\Column(name="settled", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $settled;

    /**
     * @var float
     *
     * @ORM\Column(name="utrata", type="float", precision=10, scale=0, nullable=true)
     */
    private $utrata= 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="zakladni_odmena", type="integer", nullable=true)
     */
    private $zakladniOdmena;

    /**
     * @var integer
     *
     * @ORM\Column(name="extra_odmena", type="integer", nullable=true)
     */
    private $extraOdmena;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="exp_settle_timestamp", type="datetime", nullable=true)
     */
    private $expSettleTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_status", type="integer", nullable=true)
     */
    private $idStatus;

	/**
     * @ORM\OneToMany(targetEntity="PartnerProviderFee", mappedBy="partnerOrder")
     */
	private $partnerProviderFee;
	
	 /**
     * @var MoneyPoint\Entity\Member
     *
     * @ORM\ManyToOne(targetEntity="MoneyPoint\Entity\Member")
     * @ORM\joinColumns({
     *   @ORM\JoinColumn(name="idu", referencedColumnName="idu")
     * })
     */
    private $user;
	
	
	/**
     * @ORM\OneToOne(targetEntity="MoneyPoint\Entity\Transaction", mappedBy="partnerOrder")
     */
	private $transaction;
	
	public function __construct() {
		$this->idStatus = self::STATE_WAITING;
		$this->settled = 0;
		$this->setInsertTimestamp( new \DateTime() );
	}
	
	public function getPartnerProviderFee() {
		return $this->partnerProviderFee;
	}
	
	public function getPartnerProviderFeeOne() {
		foreach( $this->partnerProviderFee as $p )
		{
			return $p;
		}
	}

	
	
	public function setPartnerProviderFee( PartnerProviderFee $partnerProviderFee) {
		throw new \Exception("DEPRECATED");
		$this->partnerProviderFee = $partnerProviderFee;
//		$partnerProviderFee->setPartnerOrder( $this );
	}


		
	public function setOrderBySpend( $utrata, $partnerFeeRate ) {
		$this->utrata = Handy::toDouble( $utrata );
		$this->zakladniOdmena = round( $utrata/100 * $partnerFeeRate->getFeeRatio() );
		$this->partnerFeeRate = $partnerFeeRate;
	}
	
	public function setOrderByPoints( $partnerFeeRate ) {
		$this->zakladniOdmena = $partnerFeeRate->getFeePoints();
		$this->partnerFeeRate = $partnerFeeRate;
	}
	
	/**
	 * 
	 * @param type $utrata
	 * @param type $provize
	 * @param type $euro
	 * @param PartnerFeeRate $partnerFeeRate
	 */
	public function setOrderCj( $utrata, $provize, $euro, $partnerProvider ) {
		$this->idStatus = self::STATE_CJ_NEW;
		$this->utrata = round( $utrata * $euro );
		$this->zakladniOdmena = round( $provize * $euro );
		$this->partnerProvider = $partnerProvider;
	}
	
	public function setRewardManual( $odmena )
	{
		$this->zakladniOdmena = $odmena;
	}

	public function getPartnerProvider() {
		return $this->partnerProvider;
	}

	public function setPartnerProvider( PartnerProvider $partnerProvider) {
		$this->partnerProvider = $partnerProvider;
	}

		
	public function setInsertTimestamp( $ts )
	{
		if( !$ts instanceof \DateTime ) {
			$ts = new \DateTime( $ts ); 
		}
		
		$this->insertTimestamp = $ts;
		$deadline = (int)  \AntoninRykalsky\Configs::get()->byContent('general.safety_deadline_partner');
		
		$this->expSettleTimestamp = clone $this->insertTimestamp;
		$interval = new \DateInterval('P'.$deadline.'D');
		$this->expSettleTimestamp->add($interval);
	}

	public function setUser( \MoneyPoint\Entity\Member $user) {
		$this->user = $user;
	}

	
	
	public function getIdOrder() {
		return $this->idOrder;
	}


	public function getInsertTimestamp() {
		return $this->insertTimestamp;
	}

	public function getSettleTimestamp() {
		return $this->settleTimestamp;
	}

	public function getSettled() {
		return $this->settled;
	}

	public function getUtrata() {
		return $this->utrata;
	}

	public function getZakladniOdmena() {
		return $this->zakladniOdmena;
	}

	public function getExtraOdmena() {
		return $this->extraOdmena;
	}

	public function getExpSettleTimestamp() {
		return $this->expSettleTimestamp;
	}

	public function getIdStatus() {
		return $this->idStatus;
	}
	
	public function getStatusName() {
		return static::$states[ $this->idStatus ];
		
	}

	public function getUser() {
		return $this->user;
	}

	public function getData() {
		return $this->data;
	}

	public function getPartner() {
		// $r = $this->feeRateId;
		\Doctrine\Common\Util\Debug::dump( $this->partnerFeeRate );
		exit;
		return $this->partnerFeeRate->getPartner()->getName();
	}
	public function getFeeRate() {
		return $this->partnerFeeRate;
	}

	public function getPartnerFeeRate() {
		return $this->partnerFeeRate;
	}

	public function getMaximalniOdmena() {
		return $this->maximalniOdmena;
	}
	
	public function getTransaction() {
		return $this->transaction;
	}
	public function setTransation( $transaction )
	{
		$this->transaction = $transaction;
	}

	public function setSettled() {
		$this->settled = true;
		$this->settleTimestamp = new \DateTime();
		$this->idStatus = 3;
	}

	public function setUtrata($utrata) 	{
		#$utrata = preg_replace('#[\D\.\,]#', '', $utrata);
		if (!is_numeric($utrata))
			throw new Exception('Utrata musí být číslo');
		$utrata = Handy::toDouble($utrata);
		$this->utrata = $utrata;
	}
		
	public $maximalniOdmena = -50;		

	// <editor-fold defaultstate="collapsed" desc=" TO REMOVE">
	public $data;
	public $partner;
	

	
	public function getTransaction123() 	{
		return \DAO\MpTransaction::get()->findAll()->where('id_pio_order=%i', $this->data['id_order'])->fetch();
	}

	public function setTsBuy($ts) 	{
		$this->data['insert_timestamp'] = $ts;
	}


	


	public function setCommision($commision) 	{
		#$commision = preg_replace('#[\D\.\,]#', '', $commision);
		if (!is_numeric($commision))
			throw new Exception('Utrata musí být číslo');
		$commision = Handy::toDouble($commision);
		$this->data['zakladni_odmena'] = $commision;
	}
	
	public function setCjAgreedPrice()
	{
		$this->idStatus = self::STATE_WAITING;
	}


	

	public function setPartnerFee($partnerFee) 	{
		if (!is_numeric($partnerFee))
			throw new Exception('Partner fee musí být předán jako číselný údaj');
		$this->data['fee_rate_id'] = $partnerFee;
	}

	public function setIdu($idu) 	{
		if (!is_numeric($idu))
			throw new Exception('Uživatel musí být předán jako číselný údaj');
		$this->data['idu'] = $idu;
	}

	public function getRozhodnyOkamzik() 	{
		$ro = new \DateTime($this->data['insert_timestamp']);
		$days = \AntoninRykalsky\Configs::get()->byContent('general.safety_deadline_partner');
		$interval = new \DateInterval('P' . (int) $days . 'D');
		return $ro->add($interval);
	}


	// </editor-fold>

}
