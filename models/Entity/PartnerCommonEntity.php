<?php
 
namespace MoneyPoint\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
 
 /** @ORM\MappedSuperclass */
 class PartnerCommon
 {

 	/**
      * @ORM\ManyToMany(targetEntity="AntoninRykalsky\Entity\Tags")
      * @ORM\JoinTable(
      *     name="partner_tags",
      *     joinColumns={
      *         @ORM\JoinColumn(name="partner_id", referencedColumnName="id_partner")
      *     },
      *     inverseJoinColumns={
      *         @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
      *     }
      * )
      */
     private $tags;
 	
 	/**
      * @ORM\ManyToMany(targetEntity="AntoninRykalsky\Entity\Country")
      * @ORM\JoinTable(
      *     name="partner_country",
      *     joinColumns={
      *         @ORM\JoinColumn(name="partner_id", referencedColumnName="id_partner")
      *     },
      *     inverseJoinColumns={
      *         @ORM\JoinColumn(name="country_id", referencedColumnName="id")
      *     }
      * )
      */
 	private $country;
 	
 	public function __construct()
     {
         $this->tags = new ArrayCollection;
         $this->country = new ArrayCollection;
     }
 	
 	public function getTags() {
		
 		return $this->tags;
 	}
 	public function getTagNames() {
 		$return = array();
 		foreach ($this->tags as $value) {
 			/* @var $value \AntoninRykalsky\Entity\Tags */
 			$return[] = $value->getTagName();
 			
 		}
 		return $return;
 	}
 	public function removeTag($tag) {
 		$this->tags->removeElement( $tag );
 	}
 	public function addTag($tag) {
 		$this->tags[] = $tag;
 	}
 	
 	
 	
 		public function getCountry() {
 		return $this->country;
 	}
 	public function getCountryNames() {
 		$return = array();
 		foreach ($this->country as $value) {
 			/* @var $value \AntoninRykalsky\Entity\Country */
 			$return[] = $value->getCountryName();
 			
 		}
 		return $return;
 	}
 	public function addCountry($country) {
 		$this->country[] = $country;
 	}
	public function removeCountry($country) {
 		$this->country->removeElement( $country );
 	}
 }

