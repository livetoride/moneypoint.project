<?php


namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Slouží pro uložení stavu nakoupených kreditu v síti uživatele
 *
 * @ORM\Table(name="mp_period_network")
 * @ORM\Entity
 */
class PeriodCreditState #implements \AntoninRykalsky\MLM\ISingleAccountState
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_period_network_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idu", type="integer", nullable=true)
     */
    private $idu;

    /**
     * @var integer
     *
     * @ORM\Column(name="period_id", type="integer", nullable=true)
     */
    private $periodId;

	/**
     * @var MoneyPoint\Entity\Period
     *
     * @ORM\ManyToOne(targetEntity="MoneyPoint\Entity\Period")
     * @ORM\joinColumns({
     *   @ORM\JoinColumn(name="period_id", referencedColumnName="id")
     * })
     */
    private $period;
	
    /**
     * @var float
     *
     * @ORM\Column(name="parent_network_period", type="float", precision=10, scale=0, nullable=true)
     */
    private $parentNetworkPeriod = 0;

	/**
     * @var float
     *
     * @ORM\Column(name="parent_direct_period", type="float", precision=10, scale=0, nullable=true)
     */
    private $parentDirectPeriod = 0;
	

	public function init($idu, $period ) {
		$this->idu = $idu;
		$this->period = $period;
	}
	
	public function getCreditsState() {
		return $this->parentNetworkPeriod;
	}
	public function getDirectCreditsState() {
		return $this->parentDirectPeriod;
	}

	public function setCreditsState($creditsState) {
		$this->parentNetworkPeriod = $creditsState;
	}
	
	public function addToState($state, $level ) {
		if( $level == 0 )
			$this->parentDirectPeriod += $state;
		else
			$this->parentNetworkPeriod += $state;
	}


	
	

}
