<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MpFirstBuyBonus
 *
 * @ORM\Table(name="mp_first_buy_bonus")
 * @ORM\Entity
 */
class FirstBuyBonus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_first_buy_bonus_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="bonus_flag", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $bonusFlag = '1';

    /**
     * @var CreditOrder
     *
     * @ORM\ManyToOne(targetEntity="CreditOrder")
     * @ORM\joinColumns({
     *   @ORM\JoinColumn(name="order_id", referencedColumnName="id_order")
     * })
     */
    private $order;
	
	/**
     * @var float
     *
     * @ORM\Column(name="bonus", type="float", precision=10, scale=0, nullable=true)
     */
    private $bonus;

	
	public function getId() {
		return $this->id;
	}

	public function getBonusFlag() {
		return $this->bonusFlag;
	}


	public function getOrder() {
		return $this->order;
	}


	public function getBonus() {
		return $this->bonus;
	}

	
	public function setBonus(CreditOrder $order, $bonusFlag, $bonus ) {
		$this->bonus = $bonus;
		$this->bonusFlag = $bonusFlag;
		$this->order = $order;
	}
	
}
