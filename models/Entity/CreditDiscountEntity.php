<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MpCreditsDiscount
 *
 * @ORM\Table(name="mp_credits_discount")
 * @ORM\Entity(repositoryClass="MoneyPoint\CreditDiscountRepository")
 */
class CreditDiscount
{
	const ACTIVATE_BUSINESS = 'for-business-status';
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_credits_discount_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="CreditDiscountGroup", inversedBy="discounts")
	 * @ORM\JoinColumn(name="id_product", referencedColumnName="id")
	 */
	private $group;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="credits", type="integer", nullable=true)
     */
    private $credits;

    /**
     * @var integer
     *
     * @ORM\Column(name="bonus", type="integer", nullable=true)
     */
    private $bonus = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="flag", type="string", length=32, nullable=true)
     */
    private $flag;

	/**
     * @var integer
     *
     * @ORM\Column(name="activate_rank", type="integer", nullable=true)
     */
    private $activateRank;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="bpo_coef", type="integer", nullable=true)
     */
    private $bpoCoef;
	
	public function getId() {
		return $this->id;
	}

	public function getCredits() {
		return $this->credits;
	}

	public function getPrice() {
		return $this->price;
	}

	public function isBusinessActivation() {
		return $this->flag == self::ACTIVATE_BUSINESS;
	}
	
	
	
	public function setId($id) {
		$this->id = $id;
	}

	
	public function setCredits($credits) {
		$this->credits = $credits;
		$this->quantity = $credits;
	}

	public function setPrice($price) {
		$this->price = $price;
	}

	public function setBusinessActivation( $activate = 1 ) {
		if( $activate )
			$this->flag = self::ACTIVATE_BUSINESS;
		else
			$this->flag = null;
	}
	
	/**
	 * Rank - key, který získává uživatel nákupem kreditu
	 * @return type
	 */
	public function getActivateRank() {
		return $this->activateRank;
	}

	public function setActivateRank($activateRank) {
		$this->activateRank = $activateRank;
	}


	public function getBpoCoef() {
		return $this->bpoCoef;
	}

	public function setBpoCoef($bpoCoef) {
		$this->bpoCoef = $bpoCoef;
	}
	
	public function getLevelDistributionCoef() {
		return 3;
		return $this->levelDistributionCoef;
	}

	public function setLevelDistributionCoef($levelDistributionCoef) {
		$this->levelDistributionCoef = $levelDistributionCoef;
	}

	public function getIdProduct() {
		return $this->idProduct;
	}

	public function setGroup($group) {
		$this->group = $group;
	}




}
