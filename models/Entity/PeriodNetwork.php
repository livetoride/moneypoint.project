<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MpPeriodNetwork
 *
 * @ORM\Table(name="mp_period_network")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="MoneyPoint\PeriodNetworkRepository")
 */
class PeriodNetwork
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_period_network_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idu", type="integer", nullable=true)
     */
    private $idu;
	
	/**
     * @var MoneyPoint\Entity\Member
     *
     * @ORM\ManyToOne(targetEntity="MoneyPoint\Entity\Member")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idu", referencedColumnName="idu")
     * })
     */
    private $user;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="parent_idu", type="integer", nullable=true)
     */
    private $parentIdu;

	/**
     * @var MoneyPoint\Entity\Period
     *
     * @ORM\ManyToOne(targetEntity="MoneyPoint\Entity\Period")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="period_id", referencedColumnName="id")
     * })
     */
    private $period;
	
	
    /**
     * Počet síťových(nepřímých) bodů náležející přímému předku
     *
     * @ORM\Column(name="parent_network_period", type="float", precision=10, scale=0, nullable=true)
     */
    private $parentNetworkPeriod;



    /**
     * Počet přímých bodů náležející přímému předku
     *
     * @ORM\Column(name="parent_direct_period", type="float", precision=10, scale=0, nullable=true)
     */
    private $parentDirectPeriod;

    /**
     * Nevyužito .. pro načítací systém
     *
     * @ORM\Column(name="parent_network_total", type="float", precision=10, scale=0, nullable=true)
     */
    private $parentNetworkTotal;

    /**
     * Nevyužito .. pro načítací systém
     *
     * @ORM\Column(name="parent_direct_total", type="float", precision=10, scale=0, nullable=true)
     */
    private $parentDirectTotal;

    /**
     * Nepřímá odměna pro přímého předka ve struktuře v účetním období
     *
     * @ORM\Column(name="parent_network_reward", type="float", precision=10, scale=0, nullable=true)
     */
    private $parentNetworkReward;

    /**
     * Celková odměna pro přímého předka ve struktuře v účetním období
     *
     * @ORM\Column(name="parent_total_reward", type="float", precision=10, scale=0, nullable=true)
     */
    private $parentTotalReward;
	
	/**
     * Rank rozdílových provizí v účetním období
     *
     * @ORM\OneToOne(targetEntity="\MoneyPoint\Entity\Carrier")
     * @ORM\JoinColumn(name="carrier_id", referencedColumnName="id")
     */
    private $carrier;
	
	/**
     * @var \MoneyPoint\Entity\Carrier
     *
     * @ORM\OneToOne(targetEntity="\MoneyPoint\Entity\Carrier")
     * @ORM\JoinColumn(name="parent_carrier_id", referencedColumnName="id")
     */
    private $parentCarrier;
	
	/**
     * Nepřímá rozdílová odměna pro vlastníka této entity
     *
     * @ORM\Column(name="network_reward", type="float", precision=10, scale=0, nullable=true)
     */
    private $networkReward;
	
	/**
     * Celková odměna pro vlastníka této entity
     *
     * @ORM\Column(name="total_reward", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalReward;

	
	/**
     * @ORM\OneToOne(targetEntity="Transaction", inversedBy="differentialCommision")
     * @ORM\JoinColumn(name="transaction_id", referencedColumnName="id_transaction")
     */
    private $transaction;
	
	/**
     * @ORM\ManyToOne(targetEntity="PeriodNetwork", inversedBy="childrenCommision")
     * @ORM\JoinColumn(name="commision_parent_id", referencedColumnName="id")
     */
	private $parentCommision;
	
	/**
     * @ORM\OneToMany(targetEntity="PeriodNetwork", mappedBy="parentCommision")
	 * @ORM\OrderBy({"idu" = "ASC"})
     */
	private $childrenCommision;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="is_transaction_generated", type="integer", length=1)
	 */
	private $isTransactionGenerated = 0;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="is_closed", type="integer", length=1)
	 */
	private $isClosed = 0;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="is_grouped_reward", type="integer", length=1)
	 */
	private $isGroupedReward = 0;
	
	public function getChildrenCommision() {
		return $this->childrenCommision;
	}
	
	/**
	 * Only for testing
	 * @param type $commision
	 */
	public function _addChildrenCommision( $commision )
	{
		$this->childrenCommision[] = $commision;
	}
	public function _setParentPoints( $networkPoints, $directPoints )
	{
		$this->parentNetworkPeriod = $networkPoints;
		$this->parentDirectPeriod = $directPoints;
	}
	

		
	/**
	 * provize nadřazeného ve struktuře
	 * @return PeriodNetwork
	 */
	public function getParentCommision() {
		return $this->parentCommision;
	}

	/**
	 * provize nadřazeného ve struktuře
	 * @param PeriodNetwork $parentCommision
	 */
	public function setParentCommision($parentCommision) {
		$this->parentCommision = $parentCommision;
	}

	
	
	
	
	public function getId() {
		return $this->id;
	}

		
	public function init( Member $member, Period $period, Carrier $carrier ) {
		$this->user = $member;
		$this->idu = $member->getIdu();
		$this->period = $period;
		$this->carrier = $carrier;
	}
	
	public function setParentIdu($parentIdu) {
		if(is_null($parentIdu)) return;
		
		$this->parentIdu = $parentIdu;
		
		$this->parentNetworkPeriod = 0;
		$this->parentDirectPeriod = 0;
		
		$this->parentNetworkTotal = 0;
		$this->parentDirectTotal = 0;
		
		$this->parentNetworkReward = 0;
		$this->parentTotalReward = 0;
	}
	
	public function getDirectPointsPeriod() {
		return $this->parentDirectPeriod;
	}
	
	public function getNetworkPointsPeriod() {
		return $this->parentNetworkPeriod;
	}

		
	public function getIdu() {
		return $this->idu;
	}
	
	public function getParentIdu() {
		return $this->parentIdu;
	}


	
	public function getCarrier() {
		return $this->carrier;
	}

	public function setCarrier(\MoneyPoint\Entity\Carrier $carrier) {
		$this->carrier = $carrier;
	}
	
	public function getParentCarrier() {
		return $this->parentCarrier;
	}

	public function setParentCarrier(\MoneyPoint\Entity\Carrier $parentCarrier) {
		$this->parentCarrier = $parentCarrier;
	}
	
	
	// dílčí odměny od přímých potomků
	public function getParentNetworkReward() {
		return $this->parentNetworkReward;
	}
	
	

	public function getParentTotalReward() {
		return $this->parentTotalReward;
	}
	public function getParentDirectReward() {
		return $this->parentTotalReward - $this->parentNetworkReward;
	}

	public function setParentNetworkReward($parentNetworkReward) {
		$this->parentNetworkReward = $parentNetworkReward;
	}

	public function setParentTotalReward($parentTotalReward) {
		$this->parentTotalReward = $parentTotalReward;
	}

	// odmeny pro získatele provize
	public function getTotalReward() {
		return $this->totalReward;
	}
	
	public function getDirectReward() {
		return $this->totalReward - $this->networkReward;
	}
	
	public function getNetworkReward() {
		return $this->networkReward;
	}

	public function setMyReward($directReward, $networkReward) {
		$this->isGroupedReward = 1;

		$this->totalReward = $directReward + $networkReward;
		$this->networkReward = $networkReward;
	}
	
	

	public function getPeriod() {
		return $this->period;
	}

	/**
	 * Vrací jednotlivé složky této provize
	 * @return type
	 */
	public function getPartialCommision() {
		return $this->partialCommision;
	}





	public function getTransaction() {
		return $this->transaction;
	}

	public function setTransaction( Transaction $transaction) {
		$this->isTransactionGenerated = 1;
		$this->transaction = $transaction;
	}


	public function getUser() {
		return $this->user;
	}




}
