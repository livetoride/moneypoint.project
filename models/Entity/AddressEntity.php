<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Uzivatele
 *
 * @ORM\Table(name="uzivatele")
 * @ORM\Entity
 */
class Address implements \AntoninRykalsky\IAddress
{
	/**
     * @var integer
     *
     * @ORM\Column(name="idu", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="uzivatele_idu_seq", allocationSize=1, initialValue=1)
     */
    private $idu;
	
    /**
     * @var string
     *
     * @ORM\Column(name="ulice", type="string", length=64, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="mesto", type="string", length=64, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="psc", type="string", length=5, nullable=true)
     */
    private $zipCode;


	public function getIdu() {
		return $this->idu;
	}

	public function getStreet() {
		return $this->street;
	}

	public function getCity() {
		return $this->city;
	}

	public function getZipCode() {
		return $this->zipCode;
	}

	public function setIdu($idu) {
		$this->idu = $idu;
	}

	public function setStreet($street) {
		$this->street = $street;
	}

	public function setCity($city) {
		$this->city = $city;
	}

	public function setZipCode($zipCode) {
		$this->zipCode = $zipCode;
	}




}

