<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="mp_credits_discount_group")
 * @ORM\Entity
 */
class CreditDiscountGroup
{
	
	public function __construct()
	{
		$this->tsInsert = new \DateTime();
	}
	
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_credit_discount_group_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	/**
	 * @ORM\Column(name="groupname", type="string", length=64, nullable=true)
	 */
	private $groupname;
	
	/**
	 * @ORM\Column(name="ts_insert", type="datetimetz", nullable=true)
	 */
	private $tsInsert;

	/**
	 * @ORM\Column(name="active", type="integer", length=1)
	 */
	private $active;
	
	/**
	 * @ORM\oneToMany(targetEntity="CreditDiscount", mappedBy="group")
	 * @ORM\OrderBy({"id" = "ASC"})
	 */
	private $discounts;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="is_used", type="integer", length=1)
	 */
	private $isUsed;


	
	
	public function getId() {
		return $this->id;
	}

	public function getGroupname() {
		return $this->groupname;
	}

	public function getTsInsert() {
		return $this->tsInsert;
	}

	public function getActive() {
		return $this->active;
	}

	public function getDiscounts() {
		return $this->discounts;
	}
	
	public function isUsed() {
		return $this->isUsed;
	}
	
	public function setIsUsed($isUsed) {
		$this->isUsed = $isUsed;
		$this->active = $isUsed;
	}




	
	





}
