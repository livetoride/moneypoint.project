<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Seskupení entit popisujících distribuci odměn
 *
 * @ORM\Table(name="mp_produkty_level_distribution_group")
 * @ORM\Entity(repositoryClass="MoneyPoint\LevelDistributionGroupRepository")
 */
class LevelDistributionGroup
{
	const UNIT_POINTS = 1;
	const UNIT_PERCENT = 2;
	
	public function __construct() {
		$this->distributions = new ArrayCollection();
	}
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_produkty_level_distribution_group_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="groupname", type="string", length=32, nullable=true)
     */
    private $groupname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_insert", type="datetime", nullable=true)
     */
    private $tsInsert;
	
	
	/**
     * @ORM\OneToMany(targetEntity="\MoneyPoint\Entity\LevelDistribution", mappedBy="group")
	 * @ORM\OrderBy({"level"="ASC"})
     */
    private $distributions;
	
	/**
	 * @ORM\Column(name="unit_id", type="integer", nullable=true)
	 */
	private $unit;
	

	/**
	  * @ORM\ManyToOne(targetEntity="\AntoninRykalsky\Entity\CommisionEntity")
	  * @ORM\JoinColumn(name="bcommision_id", referencedColumnName="id")
	  */
    private $baseCommision;
	
	
	public function getId() {
		return $this->id;
	}

	public function getGroupname() {
		return $this->groupname;
	}

	public function getTsInsert() {
		return $this->tsInsert;
	}

		/**
	 * Vrací kolekci jednotlivých distribučních nastavení
	 * @return type
	 */
	public function getDistributions() {
		return $this->distributions;
	}
	
	public function getUnit() {
		return $this->unit;
	}

	public function setUnit($unit) {
		$this->unit = $unit;
	}
	
	public function getBaseCommision() {
		return $this->baseCommision;
	}

	/**
	 * 
	 * @param type $groupname
	 * @param type $baseCommision
	 */
	public function setGroup($groupname, $baseCommision) {
		$this->groupname = $groupname;
		$this->tsInsert = new \DateTime();
		$this->baseCommision = $baseCommision;
	}









}
