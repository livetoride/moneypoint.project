<?php

namespace MoneyPoint\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Popis distribuce odměn do jednotlivých úrovní
 *
 * @ORM\Table(name="mp_produkty_level_distribution")
 * @ORM\Entity
 */
class LevelDistribution
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_produkty_level_distribution_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;


    /**
     * @var integer
     *
     * @ORM\Column(name="level_depth", type="integer", nullable=true)
     */
    private $level;

	/**
     * @var float
     *
     * @ORM\Column(name="points", type="float", precision=10, scale=0, nullable=true)
     */
    private $points;

	
	/**
     * @var MoneyPoint\Entity\LevelDistributionGroup
     *
     * @ORM\ManyToOne(targetEntity="\MoneyPoint\Entity\LevelDistributionGroup", inversedBy="distributions")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private $group;
	
	public function getLevel() {
		return $this->level;
	}

	public function getPoints( $phpType = true ) {
		if( $phpType ) {
			return $this->points;
		} else {
			return preg_replace('#\.#', ',', $this->points);
		}
	}

	public function setLevel($level, $points, $group ) {
		$this->level = $level;
		$this->points = $points;
		$this->group = $group;
	}
	
	private $points2=null;
	public function setTemporaryCoefPoints( $points )
	{
		$this->points2 = $points;
	}
	public function getTemporatyCoefPoints() {
		return $this->points;
	}
}
