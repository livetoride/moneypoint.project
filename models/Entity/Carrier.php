<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MpCarrier
 *
 * @ORM\Table(name="mp_carrier")
 * @ORM\Entity(repositoryClass="MoneyPoint\RanksRepository")
 */
class Carrier
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_carrier_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="key", type="string", length=16, nullable=true)
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(name="carrier", type="string", length=32, nullable=true)
     */
    private $carrier;

    /**
     * @var integer
     *
     * @ORM\Column(name="credits_min", type="integer", nullable=true)
     */
    private $creditsMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="credits_max", type="integer", nullable=true)
     */
    private $creditsMax;

    /**
     * @var float
     *
     * @ORM\Column(name="price_coef", type="float", precision=10, scale=0, nullable=true)
     */
    private $priceCoef;
	
	/**
     * @var	\MoneyPoint\Entity\CarrierGroup
     *
     * @ORM\ManyToOne(targetEntity="\MoneyPoint\Entity\CarrierGroup", inversedBy="carriers")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     */
    private $group;

	public function getId() {
		return $this->id;
	}

	public function getKey() {
		return $this->key;
	}

	public function getCarrier() {
		return $this->carrier;
	}

	public function getCreditsMin() {
		return $this->creditsMin;
	}

	public function getCreditsMax() {
		return $this->creditsMax;
	}

	public function getPriceCoef() {
		return $this->priceCoef;
	}

	public function getVersionId() {
		return $this->versionId;
	}

		
	public function setCarrier( $key, $carrier, $creditsMin, $creditsMax, $priceCoef ) {
		$this->key = $key;
		$this->carrier = $carrier;
		$this->creditsMin = $creditsMin;
		$this->creditsMax = $creditsMax;
		$this->priceCoef = $priceCoef;
	}

	
	public function getGroup() {
		return $this->group;
	}

		
	public function setGroup( $group ) {
		$this->group = $group;
	}


	

}
