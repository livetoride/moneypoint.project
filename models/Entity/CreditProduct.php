<?php
namespace MoneyPoint\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="products")
 * @ORM\Entity
 */
class CreditProduct {
	
	/**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="products_id_product_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="produkt_nazev", type="text", nullable=true)
     */
    private $productName;
	
    /**
     * @var string
     *
     * @ORM\Column(name="produkt_path", type="text", nullable=true)
     */
    private $productPath;

	/**
	 * @ORM\Column(name="insert_timestamp", type="datetime", nullable=true)
	 */
	private $tsInsert;
	
	/**
     * @ORM\Column(name="active", type="integer", length=1)
     */
    private $isActive;
	
	/**
     * @ORM\Column(name="products_flags_id", type="integer", length=1)
     */
    private $productFlagId;
	
	/**
     * @ORM\Column(name="used", type="integer", length=1)
     */
    private $isUsed;
	
	public function getId() {
		return $this->id;
	}

	public function getProductName() {
		return $this->productName;
	}

	public function getProductPath() {
		return $this->productPath;
	}

	public function getTsInsert() {
		return $this->tsInsert;
	}

	public function getIsActive() {
		return $this->isActive;
	}

	public function getProductFlagId() {
		return $this->productFlagId;
	}

	public function getIsUsed() {
		return $this->isUsed;
	}


}

?>