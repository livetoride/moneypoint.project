<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MpPeriods
 *
 * @ORM\Table(name="mp_periods")
 * @ORM\Entity(repositoryClass="MoneyPoint\PeriodRepository")
 */
class Period
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_periods_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="starts", type="date", nullable=false)
     */
    private $starts;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ends", type="date", nullable=false)
     */
    private $ends;

    /**
     * @var boolean
     *
     * @ORM\Column(name="closed", type="boolean", nullable=false)
     */
    private $closed = 'false';

    /**
     * @var string
     *
     * @ORM\Column(name="period", type="string", length=32, nullable=true)
     */
    private $period;

	
	/**
     * @ORM\ManyToMany(targetEntity="MoneyPoint\Entity\CarrierGroup")
     * @ORM\JoinTable(
     *     name="mp_carrier_period",
     *     joinColumns={
     *         @ORM\JoinColumn(name="period_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="carrier_version_id", referencedColumnName="id")
     *     }
     * )
     */
    private $carrierGroup;


	

	public function getId() {
		return $this->id;
	}

	public function getStarts() {
		return $this->starts;
	}

	public function getEnds() {
		return $this->ends;
	}

	public function getClosed() {
		return $this->closed;
	}

	public function getPeriod() {
		return $this->period;
	}
	
	public function getUnlockDate() {
		$days = (int)\AntoninRykalsky\Configs::get()->byContent('general.safety_deadline');
		$unlock = clone $this->ends;
		$unlock->add( new \DateInterval("P{$days}D"));
		return $unlock;
	}

	/**
	 * 
	 * @param DateTime $starts počátek období
	 * @param DateTime $ends konec období včetně
	 * @param string $period název období
	 * @throws \LogicException
	 */
	public function setPeriod( $starts, $ends, $period ) {
		try {
			$this->starts = new \DateTime( $starts );
			$this->ends = new \DateTime( $ends );
		} catch( \Exception $e ) {
			throw new \LogicException('Datum je v chybném tvaru. Zadejte jej ve tvaru například 21.2.2014');
		}
		$this->period = $period;
	}
	
	public function getCarrierGroup() {
		if( !empty( $this->carrierGroup[0] ))
		return $this->carrierGroup[0];
	}





}
