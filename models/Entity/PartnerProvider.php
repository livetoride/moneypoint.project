<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PartnerProvider
 *
 * @ORM\Table(name="partner_provider")
 * @ORM\Entity
 */
class PartnerProvider
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="partner_provider_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="key", type="string", length=32, nullable=true)
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(name="partner_provider", type="string", length=64, nullable=true)
     */
    private $partnerProvider;

	// <editor-fold defaultstate="collapsed" desc=" getters ">
	public function getId() {
		return $this->id;
	}

	public function getKey() {
		return $this->key;
	}

	public function getPartnerProvider() {
		return $this->partnerProvider;
	}

	// </editor-fold>

	public function isSpentType()
	{
		return $this->key === 'manual-spent';
	}
	
	public function isCjType()
	{
		return $this->key === 'cj';
	}


}
