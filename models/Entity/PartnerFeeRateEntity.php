<?php
namespace MoneyPoint\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * PartnerFeeRate
 *
 * @ORM\Table(name="partner_fee_rate", indexes={@ORM\Index(name="IDX_D53BB57B9393F8FE", columns={"partner_id"})})
 * @ORM\Entity
 */
class PartnerFeeRate extends \Nette\Object
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="partner_fee_rate_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="fee_ratio", type="float", precision=10, scale=0, nullable=true)
     */
    private $feeRatio;

    /**
     * @var float
     *
     * @ORM\Column(name="fee_points", type="float", precision=10, scale=0, nullable=true)
     */
    private $feePoints;

    /**
     * @var string
     *
     * @ORM\Column(name="fee_type", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $feeType = '1';

    /**
     * @var \MoneyPoint\Entity\Eshop
     *
     * @ORM\ManyToOne(targetEntity="\MoneyPoint\Entity\Eshop")
     * @ORM\joinColumns({
     *   @ORM\JoinColumn(name="partner_id", referencedColumnName="id_partner")
     * })
     */
    private $partner;
	
	 /**
     * @var string
     *
     * @ORM\Column(name="rate_name", type="string", length=128, nullable=true)
     */
    private $rateName;
	
	
	// <editor-fold defaultstate="collapsed" desc=" getters">
	public function getId() {
		return $this->id;
	}

	public function getFeeRatio() {
		return $this->feeRatio;
	}

	public function getFeePoints() {
		return $this->feePoints;
	}

	public function getFeeType() {
		return $this->feeType;
	}
	
	public function getFee() {
		if( $this->feeType == 1)
			$r = str_replace(".", ',', $this->feeRatio );
		else 
			$r = $this->feePoints;
		return $r;
	}
	
	public function getFeeText() {
		if( $this->feeType == 1)
			$r = str_replace(".", ',', $this->feeRatio ).'%';
		else
			$r = str_replace(".", ',', $this->feePoints ).' b';
		return $r;
	}

	public function getPartner() {
		return $this->partner;
	}
	
	public function getRateName() {
		return $this->rateName;
	}



// </editor-fold>

	public function __toArray() {
        $reflection = new \ReflectionClass($this);
        $details = array();
		foreach ($reflection->getMethods() as $method ) {
			preg_match('#^get(.+)#', $method->name, $matches );
			if( !empty( $matches[1] ))
			{
				$methodName = lcfirst( $matches[1] );
				eval('$details[ $methodName ] = $this->'.$method->name . '();' );
			}
		}
        return $details;
    }

	
	
	public function setFeeRatio( $type, $fee, $name ) {
		
		if( $type == 1 )
		{
			$this->feeRatio = $fee;
			$this->feePoints = 0;
		} else {
			$this->feeRatio = 0;
			$this->feePoints = $fee;
		}
		$this->feeType = $type;
		$this->rateName = $name;
	}

	public function setPartner(\MoneyPoint\Entity\Eshop $partner) {
		$this->partner = $partner;
	}



	
}
