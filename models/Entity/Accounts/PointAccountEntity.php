<?php
namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="mp_points_accounts")
 * @ORM\Entity(repositoryClass="MoneyPoint\PointAccountRepository")
 */
class PointAccount
{
	const TYPE_FIXED_COMMISSION = 1;
	const TYPE_DIFFERENTIAL_COMMISSION = 5;
	
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_points_accounts_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	/**
     * @ORM\OneToOne(targetEntity="MoneyPoint\Entity\Member")
     * @ORM\JoinColumn(name="idu", referencedColumnName="idu")
     */
    private $owner;
	
	/**
	 * @ORM\Column(name="points_change", type="float", precision=10, scale=0, nullable=true)
	 */
	private $change;
	
	/**
	 * @ORM\Column(name="reason", type="string", length=255, nullable=true)
	 */
	private $reason;
	
	/**
	 * @ORM\Column(name="change_timestamp", type="datetime", nullable=true)
	 */
	private $tsChange;
	
	/**
	 * @ORM\Column(name="points_left", type="float", precision=10, scale=0, nullable=true)
	 */
	private $left;
	
	public function setPointChange( $owner, $change, $reason, $left) {
		$this->owner = $owner;
		$this->change = $change;
		$this->reason = $reason;
		$this->tsChange = new \DateTime();
		$this->left = $left;
	}
	
	public function fixLeft( $left )
	{
		$this->left = $left;
	}
	public function fixChange( $change )
	{
		$this->change = $change;
	}
		
	public function getLeft() {
		return $this->left;
	}
	
	public function getIdu()
	{
		return $this->owner->getIdu();
	}
	
	public function getId() {
		return $this->id;
	}

	public function getOwner() {
		return $this->owner;
	}

	public function getChange() {
		return $this->change;
	}

	public function getReason() {
		return $this->reason;
	}

	public function getTsChange() {
		return $this->tsChange;
	}




}
