<?php
namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;
use AntoninRykalsky as AR;

/**
 * @ORM\Table(name="mp_credits_accounts")
 * @ORM\Entity(repositoryClass="MoneyPoint\CreditAccountRepository")
 */
class CreditAccount implements AR\IAccountable
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="credits_accounts_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	/**
     * @ORM\OneToOne(targetEntity="MoneyPoint\Entity\Member")
     * @ORM\JoinColumn(name="idu", referencedColumnName="idu")
     */
    private $owner;
	
	/**
	 * @ORM\Column(name="credits_change", type="float", precision=10, scale=0, nullable=true)
	 */
	private $change;
	
	/**
	 * @ORM\Column(name="reason", type="string", length=255, nullable=true)
	 */
	private $reason;
	
	/**
	 * @ORM\Column(name="change_timestamp", type="datetime", nullable=true)
	 */
	private $tsChange;
	
	/**
	 * @ORM\Column(name="credits_left", type="float", precision=10, scale=0, nullable=true)
	 */
	private $left;
	
	/**
	 * @ORM\Column(name="credits_sum", type="float", precision=10, scale=0, nullable=true)
	 */
	private $incomeSum;
	
	public function setCreditChange( $owner, $change, $reason, $tsChange=null ) {
		$this->owner = $owner;
		$this->change = $change;
		$this->reason = $reason;
		if( empty( $tsChange ))
		{
			$tsChange = new \DateTime();
		}
		$this->tsChange = $tsChange;
	}

	public function setLeft($left) {
		$this->left = $left;
	}
	
	public function fixChange( $change )
	{
		$this->change = $change;
	}
	public function fixLeft( $left )
	{
		$this->left = $left;
	}
	
	public function setIncomeSum($incomeSum) {
		$this->incomeSum = $incomeSum;
	}
	
	public function getId() {
		return $this->id;
	}

	public function getOwner() {
		return $this->owner;
	}

	public function getChange() {
		return $this->change;
	}

	public function getReason() {
		return $this->reason;
	}

	public function getTsChange() {
		return $this->tsChange;
	}

	public function getLeft() {
		return $this->left;
	}

	public function getIncomeSum() {
		return $this->incomeSum;
	}





}
