<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * MpCarrierGroup
 *
 * @ORM\Table(name="mp_carrier_group")
 * @ORM\Entity
 */
class CarrierGroup
{
	public function __construct() {
		$this->carriers = new ArrayCollection();
	}
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_carrier_group_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="groupname", type="string", length=32, nullable=true)
     */
    private $groupname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_insert", type="datetime", nullable=true)
     */
    private $tsInsert;

	/**
     * @ORM\OneToMany(targetEntity="\MoneyPoint\Entity\Carrier", mappedBy="group")
	 * @ORM\OrderBy({"id" = "ASC"})
     */
    private $carriers;
	
	/**
	  * @ORM\ManyToOne(targetEntity="\AntoninRykalsky\Entity\CommisionEntity")
	  * @ORM\JoinColumn(name="bcommision_id", referencedColumnName="id")
	  */
    private $baseCommision;
	
	public function getId() {
		return $this->id;
	}

	public function getGroupname() {
		return $this->groupname;
	}

	public function getTsInsert() {
		return $this->tsInsert;
	}

	public function setGroup($groupname, $tsInsert, $baseCommision ) {
		$this->groupname = $groupname;
		$this->tsInsert = $tsInsert;
		$this->baseCommision = $baseCommision;
	}

	public function getCarriers( $notZero = false ) {
		$r = $this->carriers;
		if( $notZero )
		{
			unset( $r[0] );
		}
		
		return $r;
	}
	
	public function setCarriers( $carriers ) {
		foreach( $carriers as $c )
			$c->setGroup( $this );
		$this->carriers = $carriers;
	}
	
	const INFINITY = -1;
	
	public function getCarrier( $credits )
	{
		foreach( $this->carriers as $carrier )
		{
			if( $carrier->getCreditsMin() <= $credits && $carrier->getCreditsMax() >= $credits  )
				return $carrier;
			if( $carrier->getCreditsMin() <= $credits && $carrier->getCreditsMax() == self::INFINITY  )
				return $carrier;
		}
		\Doctrine\Common\Util\Debug::dump( $this->carriers );
		throw new \Exception( "Neexistuje hledaný rank pro $credits kreditu" );
	}
	
	public function getBasicCarrier()
	{
		return $this->getCarrierById( 0 );
	}
	
	public function getCarrierById( $id )
	{
		foreach( $this->carriers as $carrier )
		{
			if( $carrier->getKey() == $id )
				return $carrier;
		}
	}
	
	/** @deprecated */
	public function getBaseCommisionId() {
		return $this->baseCommisionId;
	}

	/** @deprecated */
	public function setBaseCommisionId($baseCommisionId) {
		$this->baseCommisionId = $baseCommisionId;
	}
	
	public function getBaseCommision() {
		return $this->baseCommision;
	}








}
