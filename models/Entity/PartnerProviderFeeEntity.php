<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PartnerProviderInfo
 *
 * @ORM\Table(name="partner_provider_fee")
 * @ORM\Entity
 */
class PartnerProviderFee extends \Nette\Object
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="partner_provider_fee_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	/**
     * @var string
     *
     * @ORM\Column(name="provider", type="string", length=64, nullable=true)
     */
    private $provider;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="info_id", type="integer", nullable=true)
     */
    private $infoId;
	
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_event", type="datetime", nullable=true)
     */
    private $tsEvent;
	
	/**
     * @var float
     *
     * @ORM\Column(name="fee", type="float", precision=10, scale=0, nullable=true)
     */
    private $fee;
	
	/**
     * @var float
     *
     * @ORM\Column(name="fee_original", type="float", precision=10, scale=0, nullable=true)
     */
    private $fee_original;
	
	/**
     * @var float
     *
     * @ORM\Column(name="fee_currency", type="float", precision=10, scale=0, nullable=true)
     */
    private $fee_currency;
	
	/**
     * @var string
     *
     * @ORM\Column(name="original_id", type="string", length=64, nullable=true)
     */
    private $originalId;
	
	/**
     * @var string
     *
     * @ORM\Column(name="shop", type="string", length=64, nullable=true)
     */
    private $shop;

	/**
     * @ORM\ManyToOne(targetEntity="PartnerOrder", inversedBy="partnerProviderFee")
	 * @ORM\JoinColumn(name="partner_order_id", referencedColumnName="id_order")
     */
	private $partnerOrder;
	
	
	/**
     * @ORM\OneToMany(targetEntity="PartnerProviderInfo", mappedBy="partnerProviderFee", indexBy="key")
	 * @var PartnerProviderInfo[]
     */
	private $providerInfo;
	
	public function getProvider() {
		return $this->provider;
	}

	public function getInfoId() {
		return $this->infoId;
	}

	public function getTsEvent() {
		return $this->tsEvent;
	}

	public function getFee() {
		return $this->fee;
	}

	public function getFee_original() {
		return $this->fee_original;
	}

	public function getFee_currency() {
		return $this->fee_currency;
	}

	public function getOriginalId() {
		return $this->originalId;
	}
	
	public function setCjFee( $commision, $euro )
	{
		$this->fee_currency = $euro;
		$this->fee_original = $commision["COMMISSION-AMOUNT"];
		$this->fee = $euro*$commision["COMMISSION-AMOUNT"];
		
		$this->originalId = $commision["COMMISSION-ID"];
		$this->provider = 'cj.com';
		$this->tsEvent = new \DateTime($commision["EVENT-DATE"]);
		$this->shop = $commision["ADVERTISER-NAME"];
	}

	public function setPartnerOrder($partnerOrder) {
		$this->partnerOrder = $partnerOrder;
	}

		
	public function getShop() {
		return $this->shop;
	}

	public function setShop($shop) {
		$this->shop = $shop;
	}

	
	public function getId() {
		return $this->id;
	}

	public function getPartnerOrder() {
		return $this->partnerOrder;
	}

	public function getProviderInfos(  ) {
		return $this->providerInfo;
	}
	public function getProviderInfo( $key ) {
		if( $this->providerInfo[$key] instanceof PartnerProviderInfo )
			return $this->providerInfo[$key]->getValue();
		else return '';
	}

	public function addProviderInfo($providerInfo) {
		$this->providerInfo[ $providerInfo->getKey() ] = $providerInfo;
	}


	public function setFee_original($fee_original) {
		$this->fee_original = $fee_original;
	}

}
