<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PartnerProviderInfo
 *
 * @ORM\Table(name="partner_provider_info")
 * @ORM\Entity
 */
class PartnerProviderInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="partner_provider_info_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;


	/**
     * @ORM\ManyToOne(targetEntity="PartnerProviderFee", inversedBy="providerInfo")
     * @ORM\JoinColumn(name="ppi_id", referencedColumnName="id")
     */
    private $partnerProviderFee;

    /**
     * @var string
     *
     * @ORM\Column(name="key", type="string", length=64, nullable=true)
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;
	
	public function getKey() {
		return $this->key;
	}

	public function getValue() {
		return $this->value;
	}

	public function setInfo($key, $value) {
		$this->key = $key;
		$this->value = $value;
	}

	public function getPartnerProviderFee() {
		return $this->partnerProviderFee;
	}

		
	public function setProviderFee( PartnerProviderFee $providerFee )
	{
		$this->partnerProviderFee = $providerFee;
		$providerFee->addProviderInfo( $this );
	}


}
