<?php
namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="mp_credit_transfer")
 * @ORM\Entity(repositoryClass="MoneyPoint\CreditTransferRepository")
 */
class CreditTransfer
{
	const S_NEW = 1;
	const S_RECEIVED = 2;
	const S_STORNO = 3;
			
	const TRANSFER_MIN = 100;
	const TRANSFER_MAX = 400;
		
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_credit_transfer_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;
	
	/**
     * @ORM\OneToOne(targetEntity="MoneyPoint\Entity\Member")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="idu")
     */
    private $sender;
	
	/**
     * @ORM\OneToOne(targetEntity="MoneyPoint\Entity\Member")
     * @ORM\JoinColumn(name="recipient_id", referencedColumnName="idu")
     */
    private $recipient;
	
	/**
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=true)
     */
    private $price;
	
	/**
	 * @ORM\Column(name="state_id", type="integer", nullable=true)
	 */
	private $stateId;

    /**
     * @ORM\Column(name="ts_insert", type="datetime", nullable=false)
     */
    private $tsInsert;

    /**
     * @ORM\Column(name="ts_answer", type="datetime", nullable=false)
     */
    private $tsAnswer;
	
	/**
     * @ORM\OneToOne(targetEntity="MoneyPoint\Entity\CreditAccount")
     * @ORM\JoinColumn(name="ca_recipient", referencedColumnName="id")
     */
    private $caRecipient;
	
	/**
     * @ORM\OneToOne(targetEntity="MoneyPoint\Entity\CreditAccount")
     * @ORM\JoinColumn(name="ca_sender", referencedColumnName="id")
     */
    private $caSender;
	
	/**
     * @ORM\OneToOne(targetEntity="MoneyPoint\Entity\CreditAccount")
     * @ORM\JoinColumn(name="ca_giveback", referencedColumnName="id")
     */
    private $caGiveback;
	
	

	/* -- */
	
	public function setTransfer( $sender, $recipient, $price, $caSender ) {
		$this->sender = $sender;
		$this->recipient = $recipient;
		$this->price = $price;
		$this->stateId = self::S_NEW;
		if( empty( $this->tsInsert )) {
			$this->tsInsert = new \DateTime();
		}
		$this->caSender = $caSender;
	}
	
	private function checkAllowChange()
	{
		if( $this->stateId != self::S_NEW ) {
			throw new \LogicException("Nemůžu přijmout stornovaný požadavek");
		}
	}
	
	public function setReceived( $caRecipient )
	{
		$this->checkAllowChange();
		$this->stateId = self::S_RECEIVED;
		if( empty( $this->tsAnswer )) {
			$this->tsAnswer = new \DateTime;
		}
		$this->caRecipient = $caRecipient;
	}
	
	public function setStorno( $caGiveback )
	{
		$this->checkAllowChange();
		$this->stateId = self::S_STORNO;
		if( empty( $this->tsAnswer )) {
			$this->tsAnswer = new \DateTime;
		}
		$this->caGiveback = $caGiveback;
	}


	// <editor-fold defaultstate="collapsed" desc=" getters ">
	public function getId() {
		return $this->id;
	}

	public function getSender() {
		return $this->sender;
	}

	public function getRecipient() {
		return $this->recipient;
	}

	public function getPrice() {
		return $this->price;
	}

	public function getStateId() {
		return $this->stateId;
	}

	public function getTsInsert() {
		return $this->tsInsert;
	}

	public function getTsAnswer() {
		return $this->tsAnswer;
	}

	public function getCaRecipient() {
		return $this->caRecipient;
	}

	public function getCaSender() {
		return $this->caSender;
	}

	public function getCaGiveback() {
		return $this->caGiveback;
	}

		// </editor-fold>


}
