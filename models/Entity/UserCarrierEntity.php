<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MpUserCarrier
 *
 * @ORM\Table(name="mp_user_carrier")
 * @ORM\Entity(repositoryClass="MoneyPoint\UserCarrierRepository")
 */
class UserCarrier
{
	const TYPE_MANUAL = 2;
	const TYPE_ORDER_RELATED = 1;
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="mp_user_carrier_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var MoneyPoint\Entity\Member
     * @ORM\ManyToOne(targetEntity="\MoneyPoint\Entity\Member")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idu", referencedColumnName="idu")
     * })
     */
    private $user;

    /**
     * @var integer
     * @ORM\Column(name="rank_flag", type="integer", nullable=true)
     */
    private $rankFlag;
    
	/**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;
    
	/**
     *	@var  MoneyPoint\Entity\Carrier
	 * 
     *	@ORM\OneToOne(targetEntity="\MoneyPoint\Entity\Carrier")
     *	@ORM\JoinColumns({
     *	  @ORM\JoinColumn(name="rank_id", referencedColumnName="id")
     * })
     */
    private $carrier;
   
	/**
     * @var MoneyPoint\Entity\Period
     *
     * @ORM\OneToOne(targetEntity="\MoneyPoint\Entity\Period")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="period_id", referencedColumnName="id")
     * })
     */
    private $period;
	
	/**
     * @var MoneyPoint\Entity\CreditOrder
     *
     * @ORM\OneToOne(targetEntity="\MoneyPoint\Entity\CreditOrder")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_id", referencedColumnName="id_order")
     * })
     */
    private $order;
	
	/**
     * @var datetime
     *
     * @ORM\Column(name="ts_insert", type="datetime", nullable=true)
     */
    private $tsInsert;
	
	public function getId() {
		return $this->id;
	}

	public function getRankFlag() {
		return $this->rankFlag;
	}

	public function getType() {
		return $this->type;
	}

	public function getCarrier() {
		return $this->carrier;
	}

	public function getPeriod() {
		return $this->period;
	}

	public function getTsInsert() {
		return $this->tsInsert;
	}
	
	public function getUser() {
		return $this->user;
	}
	public function getOrder() {
		return $this->order;
	}

	
	
	public function setManual($carrier, $period, $user) {
		$this->rankFlag = $carrier->getKey();
		$this->type = self::TYPE_MANUAL;
		$this->carrier = $carrier;
		$this->period = $period;
		$this->tsInsert = new \DateTime;
		$this->user = $user;
	}
	

	public function setOrderRelated($carrier, $period, $user, CreditOrder $order) {
		$this->rankFlag = $carrier->getKey();
		$this->type = self::TYPE_ORDER_RELATED;
		$this->carrier = $carrier;
		$this->period = $period;
		$this->tsInsert = new \DateTime;
		$this->user = $user;
		$this->order = $order;
	}
}
