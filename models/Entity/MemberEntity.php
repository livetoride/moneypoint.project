<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Uzivatele
 *
 * @ORM\Table(name="uzivatele")
 * @ORM\Entity(repositoryClass="\MoneyPoint\MemberRepository2")
 */
class Member implements \AntoninRykalsky\IUser
{
	public function __construct() {
		$this->parentMember = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
    /**
     * @var integer
     *
     * @ORM\Column(name="idu", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="uzivatele_idu_seq", allocationSize=1, initialValue=1)
     */
    private $idu;

    /**
     * @var integer
     *
     * @ORM\Column(name="idusponzor", type="integer", nullable=false)
     */
    private $idusponzor;

    /**
     * @var integer
     *
     * @ORM\Column(name="idrole", type="integer", nullable=false)
     */
    private $idrole = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", length=32, nullable=false)
     */
    private $pass;

    /**
     * @var string
     *
     * @ORM\Column(name="jmeno", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="prijmeni", type="string", length=64, nullable=false)
     */
    private $surname;

	/**
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="idu", referencedColumnName="idu")
     */
	private $address;
	
    /**
     * @var string
     *
     * @ORM\Column(name="telefon", type="string", length=15, nullable=true)
     */
    private $telefon;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=64, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="nick", type="string", length=64, nullable=true)
     */
    private $nick;

    /**
     * @var string
     *
     * @ORM\Column(name="role_key", type="string", length=30, nullable=true)
     */
    private $roleKey;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reg_time", type="time", nullable=true)
     */
    private $regTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reg_date", type="date", nullable=true)
     */
    private $regDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="act_time", type="time", nullable=true)
     */
    private $actTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="act_date", type="date", nullable=true)
     */
    private $actDate;

    /**
     * @var string
     *
     * @ORM\Column(name="variable_symbol", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $variableSymbol;

    /**
     * @var string
     *
     * @ORM\Column(name="reg_mail", type="string", length=64, nullable=true)
     */
    private $regMail;

    /**
     * @var string
     *
     * @ORM\Column(name="reg_name", type="string", length=64, nullable=true)
     */
    private $regName;

    /**
     * @var string
     *
     * @ORM\Column(name="reg_surname", type="string", length=64, nullable=true)
     */
    private $regSurname;

    /**
     * @var string
     *
     * @ORM\Column(name="reg_street", type="string", length=64, nullable=true)
     */
    private $regStreet;

    /**
     * @var string
     *
     * @ORM\Column(name="reg_city", type="string", length=64, nullable=true)
     */
    private $regCity;

    /**
     * @var string
     *
     * @ORM\Column(name="reg_zip", type="string", length=5, nullable=true)
     */
    private $regZip;

    /**
     * @var string
     *
     * @ORM\Column(name="reg_phone", type="string", length=15, nullable=true)
     */
    private $regPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="pohlavi", type="string", length=1, nullable=true)
     */
    private $pohlavi;

    /**
     * @var integer
     *
     * @ORM\Column(name="vip_coef", type="integer", nullable=true)
     */
    private $vipCoef;

    /**
     * @var integer
     *
     * @ORM\Column(name="pin", type="integer", nullable=true)
     */
    private $pin;

    /**
     * @var string
     *
     * @ORM\Column(name="narozeni", type="string", length=32, nullable=true)
     */
    private $narozeni;

    /**
     * @var int
     *
     * @ORM\Column(name="has_bussiness", type="decimal", precision=1, scale=0, nullable=true)
     */
    private $hasBussiness = 0;
	
    /**
     * @var int
     *
     * @ORM\Column(name="has_activated_differencials", type="decimal", precision=1, scale=0, nullable=true)
     */
    private $hasActivatedDifferencials = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="invitation_send", type="integer", nullable=false)
     */
    private $invitationSend = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_business", type="datetime", nullable=true)
     */
    private $tsBusiness;

    /**
     * @var string
     *
     * @ORM\Column(name="`hadCreditFirstBuy`", type="decimal", precision=1, scale=0, nullable=true)
     */
    private $hadcreditfirstbuy = '0';

	
	/**
     * @ORM\OneToOne(targetEntity="\MoneyPoint\Entity\UserCarrier", mappedBy="user")
     */
	private $carrierLevel;
	
	/**
     * @ORM\ManyToOne(targetEntity="Member", inversedBy="childrenMember")
     * @ORM\JoinColumn(name="idusponzor", referencedColumnName="idu")
     */
	private $parentMember;
	
	/**
     * @ORM\OneToMany(targetEntity="Member", mappedBy="parentMember")
	 * @ORM\OrderBy({"idu" = "ASC"})
     */
	private $childrenMember;
	
	public function getParentMember() {
		return $this->parentMember;
	}

	public function getChildrenMember() {
		return $this->childrenMember;
	}

		
	public function getCarrierLevel() {
		return $this->carrierLevel;
	}

	
	
	public function getIdu() {
		return $this->idu;
	}

	public function getIdusponzor() {
		return $this->idusponzor;
	}

	public function getIdrole() {
		return $this->idrole;
	}

	public function getPass() {
		return $this->pass;
	}

	public function getName() {
		return $this->name;
	}

	public function getSurname() {
		return $this->surname;
	}

	public function getTelefon() {
		return $this->telefon;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getNick() {
		return $this->nick;
	}

	public function getRoleKey() {
		return $this->roleKey;
	}

	public function getRegTime() {
		return $this->regTime;
	}

	public function getRegDate() {
		return $this->regDate;
	}

	public function getActTime() {
		return $this->actTime;
	}

	public function getActDate() {
		return $this->actDate;
	}
	public function getMpId() {
		return $this->variableSymbol;
	}
	public function getVariableSymbol() {
		return $this->variableSymbol;
	}

	public function getRegMail() {
		return $this->regMail;
	}

	public function getRegName() {
		return $this->regName;
	}

	public function getRegSurname() {
		return $this->regSurname;
	}

	public function getRegStreet() {
		return $this->regStreet;
	}

	public function getRegCity() {
		return $this->regCity;
	}

	public function getRegZip() {
		return $this->regZip;
	}

	public function getRegPhone() {
		return $this->regPhone;
	}

	public function getPohlavi() {
		return $this->pohlavi;
	}

	public function isVip() {
		return !empty($this->vipCoef);
	}
	public function getVipCoef() {
		return $this->vipCoef;
	}

	public function getPin() {
		return $this->pin;
	}

	public function getNarozeni() {
		return $this->narozeni;
	}

	public function hasBusiness() {
		return $this->hasBussiness;
	}

	public function getInvitationSend() {
		return $this->invitationSend;
	}

	public function getTsBusiness() {
		return $this->tsBusiness;
	}

	public function hadCreditFirstBuy() {
		return $this->hadcreditfirstbuy;
	}
	
	/**
	 * 
	 * @param int $newFlag 0=none; 1=lowest credits; 2=anything else credits
	 */
	public function setCreditFirstBuy( $newFlag ) {
		if( $this->hadcreditfirstbuy < $newFlag )
		{
			echo "setting BPO for {$this->idu} to {$newFlag} \n";
			$this->hadcreditfirstbuy = $newFlag;
		}
	}
	
	
	public function setHasBussiness($hasBussiness, $date = null) {
		$this->hasBussiness = $hasBussiness;
		if( !$hasBussiness )
		{
			$this->tsBusiness = null;
		} else {
			if( empty( $date )) $date = new \DateTime();
			$this->tsBusiness = $date;
		}
	}

	public function getAddress() {
		return $this->address;
	}


	public function hasActivatedDifferencials() {
		return $this->hasActivatedDifferencials;
	}

	public function setHasActivatedDifferencials($hasActivatedDifferencials) {
		$this->hasActivatedDifferencials = $hasActivatedDifferencials;
	}

	public function _getCreditState()
	{
		$lastTransaction = \AntoninRykalsky\CreditsAccounts::get()->actualState( $this->idu );
		return (int)@$lastTransaction->credits_left;
	}
	
	public function canIGetPoints()
	{
		return ($this->idrole == 2) && $this->idu!=0;
	}


}
