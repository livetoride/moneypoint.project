<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrdersGoods
 *
 * @ORM\Table(name="orders_goods")
 * @ORM\Entity
 */
class CreditOrderItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="orders_goods_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \MoneyPoint\Entity\CreditOrder
     *
     * @ORM\OneToOne(targetEntity="\MoneyPoint\Entity\CreditOrder")
     * @ORM\joinColumns({
     *   @ORM\JoinColumn(name="id_order", referencedColumnName="id_order")
     * })
     */
    private $creditOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="integer", nullable=false)
     */
    private $idProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="product", type="string", length=64, nullable=true)
     */
    private $product;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="cena_sdph", type="float", precision=10, scale=0, nullable=true)
     */
    private $cenaSdph;

    /**
     * @var integer
     *
     * @ORM\Column(name="dph", type="integer", nullable=true)
     */
    private $dph;


    /**
     * @var integer
     *
     * @ORM\Column(name="credits", type="integer", nullable=true)
     */
    private $credits = '0';

  
	public function getId() {
		return $this->id;
	}

	public function getCreditOrder() {
		return $this->creditOrder;
	}

	public function getIdProduct() {
		return $this->idProduct;
	}

	public function getProduct() {
		return $this->product;
	}

	public function getQuantity() {
		return $this->quantity;
	}

	public function getCenaSdph() {
		return $this->cenaSdph;
	}

	public function getDph() {
		return $this->dph;
	}

	public function getCredits() {
		return $this->credits;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setCreditOrder($creditOrder) {
		$this->creditOrder = $creditOrder;
	}

	public function setIdProduct($idProduct) {
		$this->idProduct = $idProduct;
	}

	public function setProduct($product) {
		$this->product = $product;
	}

	public function setQuantity($quantity) {
		$this->quantity = $quantity;
	}

	public function setCenaSdph($cenaSdph) {
		$this->cenaSdph = $cenaSdph;
	}

	public function setDph($dph) {
		$this->dph = $dph;
	}

	public function setCredits($credits) {
		$this->credits = $credits;
	}



	
}
