<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Uzivatele
 *
 * @ORM\Table(name="uzivatele")
 * @ORM\Entity
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idu", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="uzivatele_idu_seq", allocationSize=1, initialValue=1)
     */
    private $idu;

    /**
     * @var integer
     *
     * @ORM\Column(name="idusponzor", type="integer", nullable=false)
     */
    private $idusponzor;

    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", length=32, nullable=false)
     */
    private $pass;

    /**
     * @var string
     *
     * @ORM\Column(name="jmeno", type="string", length=64, nullable=false)
     */
    private $jmeno;

    /**
     * @var string
     *
     * @ORM\Column(name="prijmeni", type="string", length=64, nullable=false)
     */
    private $prijmeni;


	public function getIdu() {
		return $this->idu;
	}

	public function getIdusponzor() {
		return $this->idusponzor;
	}

	public function getIdrole() {
		return $this->idrole;
	}

	public function getPass() {
		return $this->pass;
	}

	public function getJmeno() {
		return $this->jmeno;
	}

	public function getPrijmeni() {
		return $this->prijmeni;
	}




}

