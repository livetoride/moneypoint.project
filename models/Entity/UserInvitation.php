<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserInvitation
 *
 * @ORM\Table(name="user_invitation")
 * @ORM\Entity
 */
class UserInvitation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="user_invitation_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	/**
     * @var MoneyPoint\Entity\Member
     *
     * @ORM\ManyToOne(targetEntity="MoneyPoint\Entity\Member")
     * @ORM\joinColumns({
     *   @ORM\JoinColumn(name="idu", referencedColumnName="idu")
     * })
     */
    private $sponsor;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=128, nullable=true)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="referrerlink", type="string", length=64, nullable=true)
     */
    private $referrerlink;

    /**
     * @var string
     *
     * @ORM\Column(name="jmeno", type="string", length=64, nullable=true)
     */
    private $jmeno;

    /**
     * @var string
     *
     * @ORM\Column(name="prijmeni", type="string", length=64, nullable=true)
     */
    private $prijmeni;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_expires", type="datetime", nullable=true)
     */
    private $tsExpires;

    /**
     * @var string
     *
     * @ORM\Column(name="used", type="decimal", precision=1, scale=0, nullable=true)
     */
    private $used = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="credits_account_id", type="integer", nullable=true)
     */
    private $creditsAccountId;

    /**
     * @var string
     *
     * @ORM\Column(name="invitation_id", type="string", length=64, nullable=true)
     */
    private $invitationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type = '0';
	
	
	public function getId() {
		return $this->id;
	}

	public function getSponsor() {
		return $this->sponsor;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getTimestamp() {
		return $this->timestamp;
	}

	public function getText() {
		return $this->text;
	}

	public function getReferrerlink() {
		return $this->referrerlink;
	}

	public function getJmeno() {
		return $this->jmeno;
	}

	public function getPrijmeni() {
		return $this->prijmeni;
	}

	public function getTsExpires() {
		return $this->tsExpires;
	}

	public function getUsed() {
		return $this->used;
	}

	public function getCreditsAccountId() {
		return $this->creditsAccountId;
	}

	public function getInvitationId() {
		return $this->invitationId;
	}

	public function getType() {
		return $this->type;
	}
	
	public function isAdmin() {
		return $this->type == 2;
	}




}
