<?php
namespace MoneyPoint\Entity;





use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 *
 * @ORM\Table(name="orders", indexes={@ORM\Index(name="IDX_E52FFDEE99B902AD", columns={"idu"}), @ORM\Index(name="IDX_E52FFDEEB3B52D7D", columns={"id_payment"})})
 * @ORM\Entity(repositoryClass="\MoneyPoint\CreditOrderRepository")
 */
class CreditOrder implements \AntoninRykalsky\IOrder {
	
	const STATE_NEW = 1;
	const STATE_PAID = 3;
	const STATE_STORNO_BEFORE = 10;
	const STATE_REWARDED = 8;
	const STATE_STORNO_AFTER = 4;
	
	public function canHaveNetworkPoints()
	{
		return in_array( 
			$this->idStatus, 
			array(
				self::STATE_NEW,
				self::STATE_PAID,
				self::STATE_REWARDED
			)
		);
	}
	
	/**
     * @var integer
     *
     * @ORM\Column(name="id_order", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="orders_id_order_seq", allocationSize=1, initialValue=1)
     */
    private $idOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_end_customer", type="integer", nullable=true)
     */
    private $idEndCustomer;

    /**
     * @var string
     *
     * @ORM\Column(name="client_comment", type="text", nullable=true)
     */
    private $clientComment;

    /**
     * @var string
     *
     * @ORM\Column(name="employee_comment", type="text", nullable=true)
     */
    private $employeeComment;

    

    /**
     * @var boolean
     *
     * @ORM\Column(name="paid", type="boolean", nullable=false)
     */
    private $paid = 'false';

   

    /**
     * @var integer
     *
     * @ORM\Column(name="body_sum", type="integer", nullable=true)
     */
    private $bodySum;

    /**
     * @var integer
     *
     * @ORM\Column(name="ido", type="integer", nullable=true)
     */
    private $ido;

    /**
     * @var integer
     *
     * @ORM\Column(name="credits_sum", type="integer", nullable=true)
     */
    private $creditsSum;


    /**
     * @var string
     *
     * @ORM\Column(name="invoice_filename", type="string", length=100, nullable=true)
     */
    private $invoiceFilename;

  
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="invoice_timestamp", type="datetime", nullable=true)
     */
    private $invoiceTimestamp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pointed_timestamp", type="datetime", nullable=true)
     */
    private $pointedTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_mporder", type="integer", nullable=true)
     */
    private $idMpOrder;

    
    /**
     * @var MoneyPoint\Entity\Member
     *
     * @ORM\ManyToOne(targetEntity="MoneyPoint\Entity\Member")
     * @ORM\joinColumns({
     *   @ORM\JoinColumn(name="idu", referencedColumnName="idu")
     * })
     */
    private $user;

    /**
     * @var \OrderPayment
     *
     * @ORM\ManyToOne(targetEntity="\OrderPayment")
     * @ORM\joinColumns({
     *   @ORM\JoinColumn(name="id_payment", referencedColumnName="id_payment")
     * })
     */
    private $payment;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="id_status", type="integer", nullable=false)
     */
    private $idStatus;
	
	 /**
     * @var float
     *
     * @ORM\Column(name="price_sum", type="float", precision=10, scale=0, nullable=true)
     */
    private $priceSum;

	
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_timestamp", type="datetime", nullable=true)
     */
    private $orderTimestamp;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="setpaid_timestamp", type="datetime", nullable=true)
     */
    private $setpaidTimestamp;

	/**
     * @var \DateTime
     *
     * @ORM\Column(name="storno_timestamp", type="datetime", nullable=true)
     */
    private $stornoTimestamp;
	
	/**
     * @var string
     *
     * @ORM\Column(name="storno_filename", type="string", length=100, nullable=true)
     */
    private $stornoFilename;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="order_type", type="integer", nullable=true)
     */
    private $orderType;
	
	/**
     * @ORM\OneToOne(targetEntity="MoneyPoint\Entity\Transaction", mappedBy="creditOrder")
     */
	private $transaction;
	
	/**
     * @var string
     *
     * @ORM\Column(name="`gopayPaymentSid`", type="string", length=100, nullable=true)
     */
    private $gopayPaymentSid;
	
	/**
     * @ORM\OneToOne(targetEntity="FirstBuyBonus", mappedBy="order")
     */
    private $firstBuyBonus;
	

	/**
     * @var Period
     *
     * @ORM\ManyToOne(targetEntity="Period")
     * @ORM\JoinColumn(name="payment_period_id", referencedColumnName="id")
     */
    private $periodPayment;
	
	/**
	 * @ORM\oneToMany(targetEntity="CreditReward", mappedBy="creditOrder")
	 */
	private $creditRewards;
	
	public function getPeriodPayment() {
		return $this->periodPayment;
	}

	public function setPeriodPayment( Period $periodPayment) {
		$this->periodPayment = $periodPayment;
	}

	
	
	public function __construct() {
		$this->idStatus = self::STATE_NEW;
		$this->priceSum = 0;
		$this->creditsSum = 0;
		$this->orderType = 2;
		$this->setOrderTimestamp( new \DateTime() );
	}
	
	public function getOrderTimestamp() {
		return $this->orderTimestamp;
	}

	public function getIdStatus()
	{
		return $this->idStatus;
	}
		
	public function getTransaction() {
		return $this->transaction;
	}

	public function setTransaction($transaction) {
		$this->transaction = $transaction;
	}

	
	public function setUser( \MoneyPoint\Entity\Member $user) {
		$this->user = $user;
	}

	public function setPayment(\OrderPayment $payment) {
		$this->payment = $payment;
	}

	public function setCreditsSum($creditsSum) {
		$this->creditsSum = $creditsSum;
	}
	
	public function setPriceSum($priceSum) {
		$this->priceSum = $priceSum;
	}

	
	public function setIdMpOrder($idMpOrder) {
		$this->idMpOrder = $idMpOrder;
	}
	
	
	
	public function setIdStatus( $idStatus )
	{
		$ok = array(
			self::STATE_NEW,
			self::STATE_PAID,
			self::STATE_REWARDED,
			self::STATE_STORNO_AFTER,
			self::STATE_STORNO_BEFORE
		);
		if(!in_array($idStatus, $ok )) {
			throw new \Exception('Weird status');
		}
		$this->idStatus = $idStatus;
	}
	
	public function setPaid( $setpaidTimestamp = null )
	{
		if(empty($setpaidTimestamp))
			$setpaidTimestamp = new \DateTime();
		
		$this->setpaidTimestamp = $setpaidTimestamp;
		$this->paid = 1;
		$this->idStatus = self::STATE_PAID;
	}
	
	public function setStorno( $stornoTimestamp = null )
	{
		if(empty($stornoTimestamp))
			$stornoTimestamp = new \DateTime();
		
		$this->stornoTimestamp = $stornoTimestamp;
		$this->idStatus = self::STATE_STORNO_BEFORE;
	}
	
	public function setStornoAfterPay( $stornoTimestamp = null )
	{
		if(empty($stornoTimestamp))
			$stornoTimestamp = new \DateTime();
		
		$this->stornoTimestamp = $stornoTimestamp;
		$this->idStatus = self::STATE_STORNO_AFTER;
	}
	
	public function setRewarded( $rewardTimestamp = null )
	{
		$this->idStatus = self::STATE_REWARDED;
		
	}

	public function setGopayPaymentSid($gopayPaymentSid) {
		$this->gopayPaymentSid = $gopayPaymentSid;
	}

	public function getGopayPaymentSid() {
		return $this->gopayPaymentSid;
	}

	
	public function getPriceSum() {
		return $this->priceSum;
	}

						
	public function getIdOrder() {
		return $this->idOrder;
	}

	public function getIdEndCustomer() {
		return $this->idEndCustomer;
	}
	
	public function setIdEndCustomer($idEndCustomer) {
		$this->idEndCustomer = $idEndCustomer;
	}

	
	public function getClientComment() {
		return $this->clientComment;
	}

	public function getEmployeeComment() {
		return $this->employeeComment;
	}

	public function getPaid() {
		return $this->paid;
	}

	public function getBodySum() {
		return $this->bodySum;
	}

	public function getIdo() {
		return $this->ido;
	}

	public function getCreditsSum() {
		return $this->creditsSum;
	}

	public function getInvoiceFilename() {
		return $this->invoiceFilename;
	}

	public function getInvoiceTimestamp() {
		return $this->invoiceTimestamp;
	}
	
	public function getStornoFilename() {
		return $this->stornoFilename;
	}

	
	
	public function getPointedTimestamp() {
		return $this->pointedTimestamp;
	}

	public function getIdMporder() {
		return $this->idMpOrder;
	}

	public function getUser() {
		return $this->user;
	}

	public function getPayment() {
		return $this->payment;
	}

	public function getStatuses() {
		return $this->statuses;
	}

	public function getCreditRewards() {
		return $this->creditRewards;
	}
		
	/**
     * @var \MoneyPoint\Entity\CreditDiscount
     *
     * @ORM\ManyToOne(targetEntity="MoneyPoint\Entity\CreditDiscount")
     * @ORM\joinColumns({
     *   @ORM\JoinColumn(name="discount_id", referencedColumnName="id")
     * })
     */
    private $relatedDiscount;

	public function getRelatedDiscount() {
		return $this->relatedDiscount;
	}

	public function setRelatedDiscount( CreditDiscount $relatedDiscount) {
		$this->relatedDiscount = $relatedDiscount;
	}

	
	public function getFirstBuyBonus() {
		return $this->firstBuyBonus;
	}

					
	// <editor-fold defaultstate="collapsed" desc=" OLD ">

	private $tsResolution;
	
	public $statuses = array(
		1 => "neuhrazená",
		3 => "uhrazená",
		8 => "odměny přiděleny",
		10 => "storno",
		4 => "storno uživatelem"
	);

	public function setOrderTimestamp($order_timestamp) {
		$this->orderTimestamp = $order_timestamp;

		if( !$order_timestamp instanceof \DateTime )
			$resolution = new \DateTime($order_timestamp);
		else
			$resolution = clone $order_timestamp;
		
		$now = \AntoninRykalsky\Configs::get()->byContent('general.safety_deadline');
		$add = new \DateInterval('P' . $now . 'D');
		$resolution->add($add);
		$this->tsResolution = $resolution;
	}


	public function getStatusText() 	{
		return $this->statuses[$this->idStatus ];
	}


	public function getTsOrder() 	{
		return $this->orderTimestamp;
	}
	public function getTsResolution() 	{
		if( empty( $this->tsResolution ))
			$this->setOrderTimestamp ( $this->orderTimestamp );
		
		return $this->tsResolution;
	}

// </editor-fold>


	public function getId() {
		return $this->idOrder;
	}
	public function getPid() {
		return $this->idMpOrder;
	}
	
	public function getStatus() {
		return $this->idStatus;
	}
	public function getTsPaid() {
		return $this->setpaidTimestamp;
	}
	
	
}

?>