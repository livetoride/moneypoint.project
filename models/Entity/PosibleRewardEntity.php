<?php

namespace MoneyPoint\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mpcreditfuturerewards
 *
 * @ORM\Table(name="`mpCreditFutureRewards`", indexes={@ORM\Index(name="IDX_C8DF93B599B902AD", columns={"idu"})})
 * @ORM\Entity
 */
class PosibleRewardEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="`mpCreditFutureRewards_id_seq`", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="`tsResolution`", type="date", nullable=true)
     */
    private $tsResolution;

    /**
     * @var float
     *
     * @ORM\Column(name="claim", type="float", precision=10, scale=0, nullable=true)
     */
    private $claim;

    /**
     * @var float
     *
     * @ORM\Column(name="`firstCreditsBonus`", type="float", precision=10, scale=0, nullable=true)
     */
    private $firstcreditsbonus;

	/**
     * @var float
     *
     * @ORM\Column(name="`idu`", type="integer")
     */
    private $idu;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer")
     */
    private $count;
	
	public function __construct() {
		$this->claim = 0;
		$this->count = 0;
		$this->firstcreditsbonus = 0;
	}
	
	public function getTsresolution() {
		return $this->tsResolution;
	}

	public function getClaim() {
		return $this->claim;
	}

	public function getFirstcreditsbonus() {
		return $this->firstcreditsbonus;
	}

	public function getIdu() {
		return $this->idu;
	}

	public function setTsresolution(\DateTime $tsresolution) {
		$this->tsResolution = $tsresolution;
	}

	public function setClaim($claim) {
		$this->claim = $claim;
	}

	public function setFirstcreditsbonus($firstcreditsbonus) {
		$this->firstcreditsbonus = $firstcreditsbonus;
	}

	public function setIdu( $idu) {
		$this->idu = $idu;
	}
	
	/**
	 * Vloží detaily o budoucí odměně
	 * @param type $claim
	 * @param type $firstCreditsBonus
	 */
	public function addReward($claim, $firstCreditsBonus)
	{
		if( $claim < 0 ) {
			$this->removeReward($claim, $firstCreditsBonus);
			return;
		}
		
		$this->firstcreditsbonus += $firstCreditsBonus;
		$this->claim += $claim;
		$this->count++;
	}
	private function removeReward($claim, $firstCreditsBonus)
	{
		$this->firstcreditsbonus += $firstCreditsBonus;
		$this->claim += $claim;
		$this->count--;
	}

	public function getCount() {
		return $this->count;
	}




}
