<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint\Auth;

use AntoninRykalsky\Configs;
use AntoninRykalsky\MyMailer;


class EmailConfirmationUwStorage extends \AntoninRykalsky\Auth\EmailConfirmationUwStorageSeqId
{
	
	public function confirmEmailAccount( $uw ) {
		$idu = parent::confirmEmailAccount( $uw );
		
		// vytvoříme default věci
		$container = \Nette\Environment::getContext();
		$diferentialCommisionFacade = $container->getByType('\MoneyPoint\DiferentialCommisionFacade');
		$diferentialCommisionFacade->_initialization( $idu );
	}

	public function sendMailToClient( $uw )
	{
		parent::sendMailToClient($uw);
		
		$this->sendMailToParent($uw);
//		return;
//		$from = AR\Configs::get()->byContent('general.mailer_from');
//				$to = $uw['email'];
//				$cc = AR\Configs::get()->byContent('mailer-registration-done.cc');
//				$mail = AR\Configs::get()->byContent('mailer-registration-done.email_client');
//				$subject = AR\Configs::get()->byContent('mailer-registration-done.subject');
//
//				// naplneni hodnot v emailu
//				$passOrig =  base64_decode( $uw['pass_orig'] );
//				$doporucitel = '-';
//				if( (int)@$uw->idusponzor > 0 ) {
//					$doporucitel = AR\Uzivatele::get()->find($uw->idusponzor)->fetch();
//					$doporucitel = $doporucitel->jmeno . ' ' . $doporucitel->prijmeni;
//				}

	}
	
	public function sendMailToParent($uw) {
		if( !empty($uw[0]['email'] ))
			$uw=$uw[0];
		
		$act = \DAO\Uzivatele::get()->findAll()->where('email=%s', $uw['email'] )->fetch();
		if( empty( $act->idusponzor ))
			return false;

		$parent = \DAO\Uzivatele::get()->find( $act->idusponzor )->fetch();
		
		$emailKey = 'mailer-first-line-reg';
		$from = Configs::get()->byContent($emailKey.'.from');
		$subject = Configs::get()->byContent($emailKey.'.subject');
		
		$mail = Configs::get()->byContent($emailKey.'.email');
		$mail = preg_replace('#\[\[client_name\]\]#', $act->jmeno, $mail);
		$mail = preg_replace('#\[\[client_surname\]\]#', $act->prijmeni, $mail);
		$mail = preg_replace('#\[\[client_email\]\]#', $act->email, $mail);
		$mail = preg_replace('#\[\[client_id\]\]#', $act->variable_symbol, $mail);

		if( !empty( $parent->email ))
		{
			$m = new \MoneyPoint\Mailer();
			// zaslání emailu - novy klient
			$m->setFrom( $from );
			$m->addTo( $parent->email );
			$m->setSubject( $subject );
			$m->setHTMLBody( $mail  );
			$m->send();
			
		}
	}
}
?>
