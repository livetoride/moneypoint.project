<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class AutodokupHistoryDao extends BaseModel
{

	/** @var string  table name */
	protected $table = 'mp_autodokup_history';

	/** @var string|array  primary key column name */
	protected $primary = 'id_history';

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new AutodokupHistoryDao();
		return $me;
	}


	protected $vals = array(
		'id_history' => 'int',
		'idu' => 'varchar',
		'vop_agree' => 'boolean',
		'activation_timestamp' => 'timestamp',
		'deactivation_timestamp' => 'timestamp',
		'active' => 'boolean',
		'id_autodokup' => 'int'
	);

}
