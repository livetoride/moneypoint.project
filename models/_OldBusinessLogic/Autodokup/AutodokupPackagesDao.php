<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class AutodokupPackagesDao extends BaseModel
{

	/** @var string  table name */
	protected $table = 'mp_autodokup_packages';

	/** @var string|array  primary key column name */
	protected $primary = 'id_autodokup';

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new AutodokupPackagesDao();
		return $me;
	}


	protected $vals = array(
		'id_autodokup' => 'int',				// -
		'name' => 'varchar',					// -
		'credits_activation_level' => 'int',	// nutné pro aktivaci; musí být vetší nebo rovno
		'credits_dokup_level' => 'int',			// uroveň kdy má systém dokupovat; musí být menší nebo rovno
		'points_withdraw_level' => 'int',		// kdy je povolen výběr; větší nebo rovno
		'points_withdraw_balance' => 'int',		// nucený zůstatek po výběru; větší nebo rovno
		'id_produkt' => 'int',					// -
		'product_quantity' => 'int',			// počet balíčků pro nákup
		'vop_statement' => 'text',				// souhlasil s podmínkami
		'active' => 'boolean'					// tento záznam je aktivní

	);

	/** vrati aktivní autodokupy s podminkami credits_activation_level a cena_sdph*/
	public function findPackages( $points, $credits )
	{
		$attr = '*';

		$a = $this->connection->select($attr)->from($this->table)->as('t')
				->where('[active]=%i AND [credits_activation_level]<=%i', 1, $credits );

		$balik = $a->fetch();
		$product = Produkty::get()->findAll()->where('active=1')->fetch();
//		if( empty($balik->product_quantity))
//		{ throw new LogicException('Neznámý počet balíků'); }

//		if( !empty($balik->product_quantity))
//		if( $points < $product->cena_sdph*@$balik->product_quantity )
//			$a->where("0=0");

		return $a;
	}

	public function find( $id = null )
	{
		if( $id == null ) throw new Exception( 'Zadej hledanou hodnotu' );
		$attr = '*';

		$pT = Produkty::get()->table;
		$pP = Produkty::get()->primary;

		$product = Produkty::get()->findAll()->where('active=1')->fetch();
		$this->updateWhere('active=1', array('id_produkt'=>$product->id_product));

		return $this->connection->select($attr)->from($this->table)->as('t')
				->leftJoin($pT)->as('p')->on('p.id_product=t.id_produkt')
				->where(
				't.[active]=%i AND id_autodokup=%i', 1, $id
				);


	}


}
