<?php
/**
 * Model zacházající s tabulkou kariéra
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use DAO\BankAccounts;


class BankAccount
{
	public function hasBankAccount( $idu )
	{
		$bankAccount = BankAccounts::get()->findAll()->where('[idu]=%i', $idu )->orderBy('[id_account]')->desc()->count();
		return (bool)$bankAccount;
	}

	public function getUserActiveBankAccount( $idu )
	{
		$bankAccount = BankAccounts::get()->findAll()->where('[idu]=%i', $idu )->orderBy('[id_account]')->desc()->fetchAll();
		return $bankAccount[0];

	}
}
