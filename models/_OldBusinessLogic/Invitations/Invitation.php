<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use AntoninRykalsky as AR;
use AntoninRykalsky\Handy;
use MoneyPoint\Mailer;
use Nette\Environment;
use Nette\Object;

class Invitation extends Object
{
	public $id;
	public $idu;
	public $email;
	public $timestamp;
	public $text;
	public $referrerlink;
	public $jmeno;
	public $prijmeni;
	public $ts_expires;
	public $used;
	public $agree_nonstandard;
	public $credits_account_id;
	public $invitation_id;
	public $type;
	
	const FREE_INVITATION_LIMIT = 9999999;
	const ENABLE_INVITATION_LIMIT = false;
	
	public function __construct( $data )
	{
		if( is_array($data ))
		{
			foreach( $data as $k => $v )
				$this->$k = $v;
		}
	}
	
	public static function load( $pozvanka )
	{
		// stav
		$tsLimit = new \DateTime('-1day');
		if( $pozvanka['used']==1 )
			$pozvanka['status'] = 'využitá';
		elseif( $pozvanka['ts_expires'] < $tsLimit )
			$pozvanka['status'] = 'nevyužitá';
		else {
			$pozvanka['status'] = 'odeslaná';
		}
		return $pozvanka;
	}
	
	public static $default_text = 'Dobrý den.

Na základě Vaší žádosti Vám zasíláme registrační link pro registraci do systému moneypoint.

Moneypoint nabízí nejvyšší odměny za nákupy v ČR a SR a spoustu dalších služeb v oblasti nákupů, úspor nebo investic.

';
			
	public static function _invitationID( $user )
	{
		$id = 'POZ'.$user->variable_symbol.'/';
		$myInvitation = \DAO\UserInvitation::get()->findAll()->where('idu=%i', $user->idu )->orderBy('id', 'desc')->fetch();
		$length = 3;
		if( empty( $myInvitation->invitation_id ))
		{
			$id .= sprintf('%1$0'.$length.'d', 1 );
		} else {
			preg_match('#(\d{'.$length.'})$#', $myInvitation->invitation_id, $matches );
			$next = (int)$matches[1] + 1;
			if( $next > 999 )
				$id .= $next;
			else
				$id .= sprintf('%1$0'.$length.'d', $next );
		}
		return $id;
	}
	
	public static function _refererLink( $jmeno, $prijmeni )
	{
		$ok = 0;
		while( $ok != 1)
		{
			$l = md5($jmeno.$prijmeni.'salt'.time());
			$ui = \DAO\UserInvitation::get()->findAll()->where('[referrerlink]=%s', $l);
			if( count($ui) == 0 ) $ok = 1;
		}
		return $l;
	}
	
	const STANDARD = false;
	const PAID = true;
	const ADMIN = 2;
	
	public static function create( $data, $isPaid = false )
	{
		$referrer = \DAO\Uzivatele::get()->find( $data['idu'] )->fetch();
		
		$count1 = \DAO\UserInvitation::get()->findAll()->where('lower(email) = lower(%s) AND used=1', $data['email'] )->fetchAll();
		if( count( $count1 ))
		{
			throw new \AntoninRykalsky\LogicException("Uživatel s tímto emailem již využil jiné pozvánky");
		}
		
		
		$count2 = \DAO\UserInvitation::get()->findAll()->where('lower(email) = lower(%s) AND used<>1 AND ts_expires>now()', $data['email']  )->fetchAll();
		if( count( $count2 ))
		{
			throw new \AntoninRykalsky\LogicException("Uživateli s tímto emailem již byla pozvánka odeslána a ještě neexpirovala");
		}
		
		// overime limit volnych pozvanek
		if( !$isPaid && $referrer->invitation_send >= self::FREE_INVITATION_LIMIT )
		{
			throw new \AntoninRykalsky\LogicException("Nemáte k dispozici žádnou pozvánku! Pro vytvoření nové pozvánky klikněte na Vytvořit pozvánku");
		}
		// pridame free pozvanku na ucet
		if( !$isPaid )
			\dibi::query('UPDATE uzivatele SET invitation_send=invitation_send+1 WHERE idu='.$referrer->idu);
		$invitationID = static::_invitationID($referrer);
		if( $isPaid && $isPaid !== static::ADMIN )
		{
			// odebereme kredit
			\AntoninRykalsky\CreditsAccounts::get()->modifyCredits( $data['idu'],$invitationID , -100 );
			$data['credits_account_id'] = \dibi::getInsertId();
//			print_r( $data );exit;
		}
		
		$data['type'] = (int)$isPaid;
		$data['timestamp'] = Handy::getTimestamp();
		$data['referrerlink'] = static::_refererLink( $data['jmeno'], $data['prijmeni']);
		$data['ts_expires'] = new \DateTime('+5 day');
		$data['invitation_id'] = $invitationID;

		\DAO\UserInvitation::get()->insert( $data );
		$data['id'] = \dibi::insertId();

		return new Invitation( $data );
	}
	
	public function send( $isAdmin = false )
	{
		$mailerKey = 'mailer-invitation';
		//
		// ODESLANI EMAILU
		// potvrzení emailu
		//
		$oldReg = 0;

		if( $oldReg )
			$regUri = Environment::getHttpRequest()->getUrl()->baseUrl."auth/registration?d=".$u->variable_symbol;
		else 
			$regUri = Environment::getHttpRequest()->getUrl()->baseUrl."auth/registration?r=".$this->referrerlink;

		$reglink = "<a href='".$regUri."'>".$regUri."</a>";
		//echo $reglink;exit;

		if( $isAdmin == self::ADMIN )
		{
			$mail = "[[client-vzkaz]]";
			$mail = preg_replace('#\[\[client\-vzkaz\]\]#', $this->text, $mail);
			$mail = preg_replace('#\[\[invitation_link\]\]#', $reglink, $mail);
			$mail = preg_replace('#\[\[invitation_expire\]\]#', $this->ts_expires->format("j.m.Y, H:m"), $mail);
		} else {
			$mail = "[[client-vzkaz]]<br />" . AR\Configs::get()->byContent($mailerKey.'.email_padding');
			$mail = preg_replace('#\[\[client\-vzkaz\]\]#', $this->text, $mail);
			$mail = preg_replace('#\[\[invitation_link\]\]#', $reglink, $mail);
			$mail = preg_replace('#\[\[invitation_expire\]\]#', $this->ts_expires->format("j.m.Y, H:m"), $mail);
		}
		
		$from = AR\Configs::get()->byContent($mailerKey.'.from');
		
		if( $this->type != static::ADMIN )
		{
			$user = LoggedMember::get()->user();
			$subject = AR\Configs::get()->byContent($mailerKey.'.subject');
		} else {
			$subject = "Pozvánka do systému moneypoint";
		}
		if( !empty( $user->prijmeni ))
			$subject = preg_replace('#\[\[client_from\]\]#', $user->jmeno.' '.$user->prijmeni, $subject);
		
		
		$m=new Mailer();
		
		// zaslání emailu - novy klient
		$m->setFrom( $from );
		$m->addTo( $this->email );
		$m->setSubject( $subject );
		$m->setHTMLBody( $mail );
		$m->send();
	}
	
	
}
