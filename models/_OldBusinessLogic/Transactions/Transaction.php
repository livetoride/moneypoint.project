<?php
/**
 * Model zacházající s tabulkou kariéra
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace Moneypoint;

class Transaction
{
	protected $idTransaction;
	protected $transaction;


	public function __construct( $id_transaction = null ) {

		if( is_numeric( $id_transaction ))
		{
			$this->idTransaction = $id_transaction;
			$this->transaction = Dao\Transactions::get()->find( $id_transaction )->fetch();
		}

	}

	public function getWithdraw()
	{
		if(empty($this->transaction->id_withdraw)) throw new \Exception('Tato transakce není výběr');
		return new PointsWithdraw( $this->transaction->id_withdraw );
	}

	public function createTransaction( $data )
	{
		if( empty( $data['idu']) ) throw new \Exception('Neni predano ID uzivatele');
		$y=date('y', strtotime('now'));

		if(!empty($data['idu_from'] ))
		{
			// pokud odmeny ze site, zobrazuj id nakupciho
			$u = \AntoninRykalsky\Uzivatele::get()->find($data['idu_from'])->fetch();
			$data['mp_trans_id'] = $u->variable_symbol.'-'.$data['id_type'].'-'.$y;
		} elseif( $data['id_type'] == 4 && empty( $data['mp_trans_id'] ) ) {
			// nakup kreditu zaznac  v mp_trans_id s kodem 1
			$u = \AntoninRykalsky\Uzivatele::get()->find($data['idu'])->fetch();
			$data['mp_trans_id'] = $u->variable_symbol.'-1-'.$y;
		} elseif( !empty( $data['mp_trans_id'] ) ) {
			// jiz zadano
		} else {
			// ostatni zaznac id koho se transakce tyka a klasicky kod transakce
			$u = \AntoninRykalsky\Uzivatele::get()->find($data['idu'])->fetch();
			$data['mp_trans_id'] = $u->variable_symbol.'-'.$data['id_type'].'-'.$y;
		}


		$a = \DAO\MpTransaction::get()->findAll()->where("mp_trans_id LIKE %like~", $data['mp_trans_id'])->orderBy('mp_trans_id')->desc()->fetch();
		if( !empty($a->mp_trans_id))
		{
			preg_match('#-([^-$]+)$#', $a->mp_trans_id, $matches );
			if( empty($matches[1])) throw new \Exception('Neco se pokazilo pri zpracovani regularniho vyrazu v MP transaction id'.$a->mp_trans_id );
			$data['mp_trans_id'] .= '-'.sprintf('%1$03d', (int)$matches[1]+1);
		} else {
			$data['mp_trans_id'] .= '-001';
		}

		return $data;
	}
}
