<?php
/**
 * Model zacházající s tabulkou kariéra
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace Moneypoint\Dao;

class Transactions extends \AntoninRykalsky\BaseModel
{

	/** @var string  table name */
	protected $table = 'mp_transaction';

	/** @var string|array  primary key column name */
	protected $primary = 'id_transaction';

	protected $vals = array(
		'id_transaction' => 'int',
		'id_type' => 'int',
		'points_account_id' => 'integer',
		'credits_account_id' => 'integer',
		'insert_timestamp' => 'timestamp',
		'idu' => 'int',
		'mp_trans_id' => 'varchar',
		'id_order' => 'integer', // v pripade id_type = 4
		'id_order_reward' => 'integer', // v pripade id_type = 1
		'line' => 'integer', // zanoreni nakupu, v pripade id_type = 4
		'reward_timestamp' => 'timestamp', // zanoreni nakupu, v pripade id_type = 4
		'id_pio_order' => 'integer',
		'id_withdraw' => 'integer',
	);

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new Transactions();
		return $me;
	}


	public function getDataSource()
    {
		$this->connection = \dibi::getConnection();
		return $this->connection->dataSource(
			"SELECT t.*,
				(CASE WHEN t.id_type=4 THEN oc.price_sum
					WHEN t.id_type=1 THEN orr.price_sum
					WHEN t.id_type=3 THEN pw.points
					WHEN t.id_type=2 THEN porders.utrata
					WHEN t.id_type=5 THEN 0
					ELSE -1
				END) as objem,
				(CASE WHEN t.id_type=4 THEN 0
					WHEN t.id_type=1 THEN pa.points_change
					WHEN t.id_type=3 THEN pa.points_change
					WHEN t.id_type=2 THEN pa.points_change
					WHEN t.id_type=5 THEN pa.points_change
					ELSE -1
				END) as points_change,
				(CASE WHEN t.id_type=4 THEN to_char(oc.credits_sum, '999 999') || ' kr'
					WHEN t.id_type=1 THEN 'základní odměna' /*to_char(t.line, '99') || '. linie'*/
					WHEN t.id_type=3 THEN 'vyplacení odměny'
					WHEN t.id_type=2 THEN ppf.shop
					WHEN t.id_type=5 THEN 'objemová odměna'
					ELSE 'dopln reason'
				END) as reason

			FROM mp_transaction t
			LEFT JOIN mp_points_accounts pa ON t.points_account_id = pa.id
				LEFT JOIN partner_orders porders ON porders.id_order=t.id_pio_order
					LEFT JOIN partner_fee_rate pfr ON porders.fee_rate_id=pfr.id
						LEFT JOIN partner_provider_fee ppf ON ppf.partner_order_id=porders.id_order
						LEFT JOIN partners p ON pfr.partner_id=p.id_partner
			LEFT JOIN mp_points_withdraw pw ON pw.id_withdraw = t.id_withdraw
			LEFT JOIN mp_credits_accounts ca ON t.credits_account_id = ca.id
				LEFT JOIN orders oc ON oc.id_order = t.id_order
			LEFT JOIN orders orr ON orr.id_order = t.id_order_reward"
		);
    }

	public function insert(array $data)
	{
		$t = new \Moneypoint\Transaction();
		$data = $t->createTransaction( $data );

		parent::insert($data);
		return $data['mp_trans_id'];
	}

}
