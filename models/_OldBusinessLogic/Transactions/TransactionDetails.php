<?php
/**
 * Model zacházající s tabulkou kariéra
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace Moneypoint;

class TransactionDetails extends \DibiRow2Object
{
	public function withdraw()
	{
		if(empty($this->id_withdraw)) throw new \Exception('There is no withdraw');
		$r = \AntoninRykalsky\PointsWithdraw::get()->find($this->id_withdraw)->fetch();
		return new PointsWithdrawDetail( $r );
	}

	public function credits()
	{
		if(empty($this->credits_account_id)) throw new \Exception('There is no credits transaction');
		return \AntoninRykalsky\CreditsAccounts::get()->find($this->credits_account_id)->fetch();
	}

	public function points()
	{
		if(empty($this->points_account_id)) throw new \Exception('There is no points transaction');
		return \AntoninRykalsky\PointsAccounts::get()->find($this->points_account_id)->fetch();
	}

	public function user()
	{
		if(empty($this->idu)) throw new \Exception('Transaction has no user');
		return \AntoninRykalsky\Uzivatele::get()->find($this->idu)->fetch();
	}
}
