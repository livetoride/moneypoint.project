<?php
/**
 * Model zacházající s tabulkou kariéra
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use AntoninRykalsky\GetAscendant;
use dibi;


class StructureOverview
{
	public static function changeUserStats( $idu, $change = 1 )
    {
		if( $change >= 0 )
			$changeText = '+'.$change;
		else 
			$changeText = (string)$change;
		
		
		$asc = GetAscendant::get()->find( $idu );
		$i=0;
		foreach( $asc as $idP )
		{
			$i++;
			if ( $idP <> 0 && $i<=10 )
				dibi::query ('UPDATE [mp_structure_overview] SET users_%i=users_%i %sql WHERE idu=%i', $i, $i, $changeText, $idP );
		}
    }
}
