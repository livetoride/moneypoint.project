<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class Settings
{
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new Settings();
		return $me;
	}

	public $autodokup = 0;


}
