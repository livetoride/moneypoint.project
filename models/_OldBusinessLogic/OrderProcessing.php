<?php
/**
 * Model pro manipulaci s objednávkami
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;
use AntoninRykalsky\OrdersInvoice;
use FormatingHelpers;
use AntoninRykalsky\ProduktyLevelDistribution;
use Nette\Utils\Strings;
use LogicException;
use dibi;

class OrderProcessing
{
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new OrderProcessing();
		return $me;
	}


	public function SPECsetPaid( $id_order )
	{
		$debug = 0;
		$order = SpecialOrdersDao::get()->find($id_order)->fetch();

		$service = PartnersServiceDao::get()->find( $order->id_service )->fetch();
		$partner = PartnersDao::get()->findAll()->where('id_partner=%i', $service->id_partner )->fetch();

		$bodyKNarokovani = $order->provize_points;
		if( empty($order->id_order ))			throw new LogicException('Unknown ID of partners order');
		if( empty($partner->name ))			throw new LogicException('Unknown partner');


		if( $debug )
			echo "<br />Klient s id ".$order->idu." ma narok ZO $bodyKNarokovani z partnerskeho eshopu ".$partner['name'];

		// připočti Body
		PointsAccounts::get()->specPartnerPoints( $order->idu , "Nákup ".$partner['name']." - ".$service['name']." - ZO.", $bodyKNarokovani, $id_order );

			// má nárok na extra odměnu?
			Autodokup::get()->dokup( $order->idu );
			$stavKreditu = CreditsAccounts::get()->actualState( $order->idu );
			$extraOdmena = 0;
			if( @$stavKreditu->credits_left >= $bodyKNarokovani)
			{
				// nárok na plnou extra odměnu
				$extraOdmena = $bodyKNarokovani;
			} elseif( @$stavKreditu->credits_left == 0 )
			{
				// nemá nárok
				$extraOdmena = 0;
			} else {
				// částečná extra odměna
				$extraOdmena = @$stavKreditu->credits_left;
			}
			if( $extraOdmena != 0 )
			{
				PointsAccounts::get()->pioPoints( $order->idu , "Nákup ".$partner['name']." - ".$service['name']." - EO.", $extraOdmena, $id_order );
				CreditsAccounts::get()->modifyCredits( $order->idu , "kredit čerpané za EO z partnerskeho obchodu ".$partner['name'].".", -$extraOdmena,  $id_order );
			}
			// má nárok na extra odměnu?
			Autodokup::get()->dokup( $order->idu );
	}

	public function getInvoiceFilename()
	{

	}

	/**
	 *Vystaví fakturu
	 *
	 * ! očekává transakci - reaguj na vyjímky
	 *
	 * @param type $idOrder
	 * @param type $debug
	 */
	public function makeInvoice( $idOrder, $debug = 0 )
	{
		try {

			$o = Orders::get()->find($idOrder)->fetch();
			$u = Uzivatele::get()->find( $o->idu )->fetch();
			$og = OrdersGoods::get()->findAll()->where('[id_order]=%i', $idOrder)->fetchAll();

			$template = $this->createTemplate();
			$template->objednatel = $u;
			$template->objednavka = $o;
			$template->datVystaveni = '6.8.2011';
			$template->datSplatnosti = '12.8.2011';
			$template->bankAccount = '431588280297';
			$vs = $template->variableSymbol = $template->invoiceId = '20110123456';


			$template->bankCode = '0100';

			$template->zbozi = $og;
			$template->setFile(APP_DIR."/modules/MlmadminModule/templates/Orders/makeInvoice.phtml");
			if( $debug ) {
				$template->render();exit;
			}



			$pdf = new PdfResponse($template);

			// Všechny tyto konfigurace jsou nepovinné:

		    // Orientace stránky
	        $pdf->pageOrientaion = PdfResponse::ORIENTATION_PORTRAIT;
		    // Okraje stránky
	        $pdf->pageMargins = "5,15,16,15,9,9";

			// Způsob zobrazení PDF
			$pdf->displayLayout = "continuous";
			// Velikost zobrazení
			$pdf->displayZoom = "fullwidth";

			// Název dokumentu
			$pdf->documentTitle = "Faktura MoneyPoint.cz";
		    // Dokument vytvořil:
	        $pdf->documentAuthor = "MoneyPoint.cz";

			$path = WWW_DIR.'/files/generated-invoices/';
			$filename = 'invoice_'.$vs.'.pdf';
			// Orders::get()->update($idOrder, array('invoice_filename'=>$filename));


			$pdf->addOutput($path.$filename, 'F');
			$pdf->send();
			$this->flashMessage('Faktura vygenerována', Flashes::$success );

		} catch ( Exception $e ) {
			dibi::rollback();
			$this->flashMessage("Fakturu se nepodařilo vygenerovat (".$e->getMessage().")", Flashes::$error );
		}

	}


	var $invoiceGenerator;
	public function setInvoiceGenerator( $invoiceGenerator )
	{
		$this->invoiceGenerator = $invoiceGenerator;
	}
	public function getInvoiceGenerator(  )
	{
		return $this->invoiceGenerator;
	}


	private function getTimestamp()
	{
		return date("Y-n-j H:i:s", strtotime("now"));
	}

	// otevře aktuální účetní období
	public function openPeriods()
	{
		$uo = UcetniObdobi::get()->actualUo();
		$what = array('uzavreno' => 't');
		UcetniObdobi::get()->update( $uo->ido , $what );
	}

	var $processing_orders;
	var $processing_period;
	var $presenter;

	/*
	 * ZDE JE TRANSAKCE
	 */
	public function process( $idOrder, $v, $presenter = null )
	{

		// $this->openPeriods();

		if( !empty( $presenter ))
		{
			$this->presenter = $presenter;
		}

//		try {

			$this->processing_orders = $o = Orders::get()->find($idOrder)->fetch();
//			$this->processing_period = $uo = UcetniObdobi::get()->find($o->ido)->fetch();




			$pripoctiBody = 0;
			if(
					// uhrazení  obednavky
					($o->id_status==1 && $v->id_status == 3) ||
					// uhrada z buctu
					( $o->id_payment==4 && $v->id_status == 3)
			)
			{
				$generator = new OrdersInvoice();
				$generator->setPresenter( $presenter );
				$generator->setObjednavka( $o );
				$generator->setBankAccount( Configs::get()->byContent('general.bank_account') );
				$generator->setBankCode( Configs::get()->byContent('general.bank_code') );

//				$result = $generator->generate();

//				if( empty($result) ) throw new LogicException('Generování faktury selhalo');

				@$v->setpaid_timestamp = $this->getTimestamp();
				$mlmProcessAdd = 1;

				// odesli email
			}

			// storno fa
			if( $o->id_status==3 && $v->id_status == 4)
			{
				$generator = new OrdersInvoiceStorno();
				$generator->setPresenter( $this );
				$generator->setObjednavka( $o );

				$result = $generator->generate( );
				if( !$result ) throw new LogicException('Generování faktury selhalo');
			}

			// nezaplaceno => odečti
			if( $o->id_status==3 && $v->id_status == 1) $mlmProcessRemove = 1;
			//if( $o->id_status==3 && $v->id_status == 5) $mlmAddPoints = 1;

			if( $o->id_status==3 && $v->id_status == 8) $pripoctiBody = 1;



//			dibi::begin();
//			if( $uo->uzavreno == 't' ) throw new \Exception('Účetní období je již uzavřeno');

			// nastav jmeno vygenerovane faktury
			if( $o->id_status==1 && $v->id_status == 3)
			{
				$filename = $generator->getFilename();
				Orders::get()->update($idOrder, array('invoice_filename'=>$filename));
			}

			/*
			 * PRIDAVA kredit na konto objednatele
			 */
			if( !empty($mlmProcessAdd)&&$mlmProcessAdd ) $this->P90A_processAdd();

			/*
			 * ODEBIRA kredit z konta objednatele
			 */
			if( !empty($mlmProcessRemove)&&$mlmProcessRemove ) $this->P90A_processRemove();

			/*
			 * PRIDAVA BODY na konta predku ve strukture
			 */
			if( $pripoctiBody ) $this->addMoneyPoints();


			// změnit data
			($v->id_status==3?$v->paid ='t':$v->paid ='f');

			//echo 'nastavuji ajko '.$v->id_status;exit;
			Orders::get()->update($idOrder, get_object_vars($v) );

//			dibi::commit();

//		} catch (Exception $e) {
//
//			dibi::rollback();
//            $this->feedback = $e->getMessage();
//			return 'FAIL' ;
//        }

			// uhrazení  obednavky
			if ($o->id_status==1 && $v->id_status == 3) {
				$u =  Uzivatele::get()->find( $o->idu )->fetch();
				\MoneyPoint\Emails::creditsOrderConfirmPaid( $o, $u );
			}
				
			// uhrada z buctu
			if ($o->id_payment==4 && $v->id_status == 3) {
				$u =  Uzivatele::get()->find( $o->idu )->fetch();
				\MoneyPoint\Emails::creditsOrderPaidPoints( $o, $u );
			}
			

		#dibi::commit();

		$toReward = new \MoneyPoint\CreditOrdersToReward();
		$toReward->reward();

			return 1;
	}

	private function addMoneyPoints()
	{
		$debug = 0; $br = "<br />\n";
		if( $debug )
			echo Strings::webalize('Připočti Body ').$br;

		// ziskej potřebné data
		$o = $this->processing_orders;
		//$uo = $this->processing_period;
		$og = OrdersGoods::get()->findAll()->where('[id_order]=%i', $o->id_order)->fetchAll();
		if( count($og)>1 || count($og)<1 )			throw new LogicException('Předpokládám jeden produkt');
		$predci = \AntoninRykalsky\GetAscendant::get()->find($o->idu);

		$quantity = $og[0]->quantity;

		if( $debug ) {
			echo "ID objednatele: ". $o->idu . $br;
			print_r( $predci );
			echo $br;
		}

		foreach( $predci as $pLevel => $iduPredek )
		{
			// kvuli offsetu
			$predciLevel=$pLevel+1;

			// pokud není určena bodová odměna - nebude keš!
			$business = Uzivatele::get()->canGetBusiness( $iduPredek );
			$bodyKNarokovani = ProduktyLevelDistribution::get()->getKoeficient($o->price_sum, $predciLevel, $business);
			$canGetPoints = Uzivatele::get()->canGetPoints( $iduPredek );
			$reason = "Nákup kreditu v ceně " . FormatingHelpers::currency2($o->price_sum);
			if( $debug )
				echo "<br />Predek s id $iduPredek ma narok základní odměnu $bodyKNarokovani Bodu - level $predciLevel ".
					" kredit $quantity, level $predciLevel"
					;

//			// připočti Body - základní odměna
//			if( $canGetPoints )
//			{
//				if( $debug ) echo 'prirazuji: '.$reason;
//				//PointsAccounts::get()->creditsOrder( $iduPredek , "Nákup kreditu v LKS".(int)$predciLevel." (ZO)", $bodyKNarokovani, $predciLevel, $o->id_order );
//				PointsAccounts::get()->creditsOrder( $iduPredek , $reason." (ZO)", $bodyKNarokovani, $predciLevel, $o->id_order );
//			}

			Autodokup::get()->dokup( $iduPredek );

			$points_account_id = null;
			$credits_account_id = null;

			// má nárok na extra odměnu? - EO je teď jedina odměna
			$stavKreditu = CreditsAccounts::get()->actualState( $iduPredek );
			$extraOdmena = 0;
			if( @$stavKreditu->credits_left >= $bodyKNarokovani)
			{
				// nárok na plnou extra odměnu
				$extraOdmena = $bodyKNarokovani;
			} elseif( @$stavKreditu->credits_left == 0 )
			{
				// nemá nárok
				$extraOdmena = 0;
			} else {
				// částečná extra odměna
				$extraOdmena = @$stavKreditu->credits_left;
			}
			// pouze role prodejce
			if( $canGetPoints )
			{
				if( $extraOdmena != 0 )
				{
					// extra odměna
					PointsAccounts::get()->creditsOrder( $iduPredek , $reason, $extraOdmena, $predciLevel, $o->id_order );
					$points_account_id = \dibi::getInsertId();
					CreditsAccounts::get()->modifyCredits( $iduPredek , $reason, -$extraOdmena,  $o->id_order );
					$credits_account_id = \dibi::getInsertId();
				}
				$transaction=array(
					'id_type' => 1,
					'points_account_id' => $points_account_id,
					'credits_account_id' => $credits_account_id,
					'insert_timestamp' => Handy::get()->getTimestamp(),
					'idu' => $iduPredek
				);
				\Moneypoint\DAO\Transactions::get()->insert($transaction);
			}

			// pokud je v MT připočti body dle koeficientu
			$class = MPKoef::get()->getCoef( $iduPredek );
			if( $canGetPoints )
			if( $class->bonus > 0 )
			{
				//
				// ODMENY MANAGERSKYCH TRID A VIP
				//
				$mtZo = round( ($bodyKNarokovani)/100*$class->bonus );
				// základ pro výpočet mt odměny - kontrétně části EO
				$mtEoBase = round( ($extraOdmena)/100*$class->bonus );

				$stavKreditu = CreditsAccounts::get()->actualState( $iduPredek );
				$mtEo = 0;
				if( @$stavKreditu->credits_left >= $mtEoBase )
				{
					// nárok na plnou extra odměnu
					$mtEo = $mtEoBase;
				} elseif( @$stavKreditu->credits_left == 0 )
				{
					// nemá nárok
					$mtEo = 0;
				} else {
					// částečná extra odměna
					$mtEo = @$stavKreditu->credits_left;
				}
				// VYSE MT BONUSU
				$mtBonus = $mtZo + $mtEo;

				if(!empty($class->vip))
				{
					$text = $reason . " (VIP".$class->bonus."%)";
					$text2 = $reason ." (VIP".$class->bonus."%)";
				} else {
					$text = $reason . " (".$class->trida.")";
					$text2 = $reason . " (".$class->trida.")";
				}

				PointsAccounts::get()->creditsOrder( $iduPredek ,$text , $mtBonus, $predciLevel, $o->id_order );
				if( $mtEo != 0)
				CreditsAccounts::get()->modifyCredits( $iduPredek , $text2, -$mtEo, $o->id_order );
			}

			// má nárok na extra odměnu?
			Autodokup::get()->dokup( $iduPredek );


		}
	}



	/**
	 * Předpokládá transakci
	 *
	 */
	private function P90A_processAdd()
	{

	}

	/**
	 * Předpokládá transakci
	 *
	 */
	private function P90A_processRemove()
	{
		$o = $this->processing_orders;
		$uo = $this->processing_period;

		echo 'Odečti body <br />';
		// přímé
		$pointsActuals['verze'] = $uo['id_verze'];
		$pointsActuals['ido'] = $o['ido'];
		$pointsActuals['idu'] = array($o['idu']);
		$pointsActuals['obrat_remove'] = -(int)$o['body_sum'];

		print_r( $pointsActuals );
//
//		// odečti získané body
//		ZiskaneBodyActuals::get()->appendPoints(
//			$pointsActuals['idu'],
//			$pointsActuals['ido'],
//			$pointsActuals['obrat_remove'], 0, 0
//		);
//		ZiskaneBody::get()->appendPoints(
//			$pointsActuals['idu'],
//			$pointsActuals['ido'],
//			$pointsActuals['obrat_remove'], 0, 0
//		);
//
//		// nepřímé
//		$undirect = GetAscendant::get()->find($o['idu']);
//		ZiskaneBodyActuals::get()->appendPoints(
//			$undirect,
//			$pointsActuals['ido'],
//			0, $pointsActuals['obrat_remove'], 0
//		);
//		// pricti/odecti ziskane body - strukture
//		ZiskaneBody::get()->appendStructurePoints(
//			$undirect,
//			$pointsActuals['ido'],
//			$pointsActuals['obrat_remove'], 0, 0
//		);


	}


	/**
	 *
	 * @param object $v - hodnoty z formulare - koncovy zakaznik, poznamka
	 * @param <type> $userId - id uživatele - objednatele
	 * @param <type> $orderGoods - pole id product => quantity
	 * @param <type> $priceSum - celková cena, spočítat mimo
	 * @return <type> FAIL / null
	 */
	public function NewOrder( $v, $userId, $orderGoods, $priceSum, $creditsSum )
	{
		$v = (object)$v;
		#
        # kontroly
        #


        try {

			if( $v->adminOrder )
				if( !preg_match("/^\d\d.\d\d.\d\d\d\d+$/i", $v->order_date ) ){ throw new \Exception("Špatně zadán datum objednávky", 1); }


			if( empty($priceSum) ){ throw new \Exception("Objednávka zdarma?"); }

            if( !preg_match("/\d+/i", $v->zpusob_platby ) ){ throw new \Exception("Musí být zvolen způsob platby"); }
			if( !preg_match("/\d+/i", $userId ) ){ throw new \Exception("Musí být zadáno ID uživatele"); }

			# započneme transakci
			dibi::begin();

			if($v->allowKoncovy)
			{
				$endCustomer['jmeno'] = $v->koncovy_jmeno;
				$endCustomer['prijmeni'] = $v->koncovy_prijmeni;
				$endCustomer['telefon'] = $v->koncovy_telefon;
				$endCustomer['firma'] = $v->koncovy_firma;
				$endCustomer['ulice'] = $v->koncovy_ulice;
				$endCustomer['mesto'] = $v->koncovy_mesto;
				$endCustomer['psc'] = $v->koncovy_psc;
				// vložime + ziskame id
				EndCustomers::get()->insert( $endCustomer );
				$idEndCustomer = dibi::getInsertId();
				$order['id_end_customer'] = (int)$idEndCustomer;
			}




			#id_order
			$order['idu'] = (int)$userId;
			$order['id_payment'] = (int)$v->zpusob_platby;

			if($v->allowPoznamka)
			{
				$order['client_comment'] = $v->poznamka;
			}
			$order['id_status'] = 1;
			$order['order_date']=date("Y-n-j", strtotime("now"));
			if( $v->adminOrder )
			{
				$order['order_date']=date("Y-n-j", strtotime($v->order_date));
				$order['employee_comment'] = "[ objednáno administrátorem ]";
			}

			$order['order_time']=date("H:i:s", strtotime("now"));
			$order['price_sum'] = $priceSum;
			$order['credits_sum'] = $creditsSum;


//			$ido = UcetniObdobi::get()->findByDate( $order['order_date'] )->fetch()->ido;
//
//			if( !((int)$ido>0) ) throw new \Exception('K zadaném datu objednávky neexistuje účetní období', 2);
//			$order['ido'] = $ido;

			// vložime + ziskame id
			Orders::get()->insert( $order );
			$idOrder = dibi::getInsertId();


//			$bodySum = 0;
//			foreach( $orderGoods->goods as $productId => $count)
//			{
//				$p = Produkty::get()->find($productId)->fetch();
//				$g = &$ordersGoods[];
//				$g['id_order'] = $idOrder;
//				$g['id_product'] = $productId;
//				$g['product'] = $p['produkt_nazev'];
//				$g['quantity'] = $count;
//				$g['cena_sdph'] = $p['cena_sdph'];
//				$g['dph'] = $p['dph'];
//				$g['body'] = $p['body'];
//				$g['credits'] = $p['body'];
//				$bodySum += $p['body']*$g['quantity'];
//
//				$cd = CreditsDiscount::get()->findAll()->
//							where('[id_product]=%i AND [quantity]=%i',
//									$productId,
//									$count )->fetch();
//				$g['credits'] = $cd->credits;
//				$p = null;
//				OrdersGoods::get()->insert( $g );
//			}
//			Orders::get()->update( $idOrder, array('body_sum'=>$bodySum));


		} catch (Exception $e) {
            $this->feedback = $e->getMessage();
			$this->code = $e->getCode();
			return 'FAIL' ;
        }

		dibi::commit();
		return $idOrder;
	}
}
