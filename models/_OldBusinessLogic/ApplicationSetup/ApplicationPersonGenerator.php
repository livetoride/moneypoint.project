<?php
/**
 * Model pro manipulaci s objednávkami
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

use DAO\ArTestNames;
use Nette\Utils\Strings;



class ApplicationPersonGenerator
{
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new ApplicationPersonGenerator();
		return $me;
	}


	public function getPhone( $id )
	{
		$pfix = $this->phonesPrefix[ $id % count( $this->phonesPrefix ) ];
		return "+420".$pfix.sprintf("%1$06d", $id+1 );
	}
	
	/**
	 * Počítá ID sponzora při, stejném počtu potomků pro zadané ID
	 * @param int $branches počet potomků v každé větvi
	 * @param int $orderOfActual id klienta ktereho chceme zaradit
	 * @return int ID klienta kam jej zaradit
	 */
	public function getSponzor( $branches = 1, $orderOfActual, $startAt = 1 )
	{
		if( $startAt == 1 )
			$rootDisplacementCoef = -2;
		elseif( $startAt == 0 )
			$rootDisplacementCoef = -3;
		
		$preSolved = $orderOfActual+$branches+$rootDisplacementCoef;
		return floor( $preSolved/$branches );
	}
	
	private $users;
	private function getUsers( $id )
	{
		if( empty( $this->users ))
		{
			$n  = new \AntoninRykalsky\Setup\TestNames();
			$this->users = $n->getUsers();
		}
		$r = $this->users[$id];
		return (object) array(
			'id' => $r[0],
			'name' => $r[1],
			'surname' => $r[2],
			'street' => $r[3],
			'no' => $r[4],
			'city' => $r[5],
			'zip' => $r[6],
		);
		/*

		 *  [1] => Array
        (
            [0] => 2
            [1] => Lucie
            [2] => NovĂˇ
            [3] => Na BesedÄ›
            [4] => 2479
            [5] => BaÄŤice
            [6] => 65456
        )
		 * 
		 * DibiRow Object
(
    [id] => 1
    [name] => Jana
    [surname] => NÄ›mcovĂˇ
    [street] => Na BÄ›lidle
    [no] => 661
    [city] => BaÄŤetĂ­n
    [zip] => 66984
)
		 * 		 */
	}

	public function getPerson( $id, $branches = 2 )
	{
		if( $id < 72)
		{
			$d = ArTestNames::get()->find( $id + 1 )->fetch();
		} else {
			$d = $this->getUsers( $id );
		}
//		echo $id  . "\n";
//		print_r( $d );
//		echo "\n\n";
		
		
		$r=array();
		$r['jmeno'] = $d->name;
		$r['prijmeni'] = $d->surname;
		$r['email'] = Strings::webalize( $d->name ).'.'.Strings::webalize( $d->surname ).'@seznam.cz';
		$r['telefon'] = $this->getPhone($id);
		$r['ulice'] = $d->street . ' ' .$d->no;
		$r['mesto'] = $d->city;
		$r['psc'] = $d->zip;
		$r['passwd'] = $r['passwd2'] = 'traktor';
		if( $id == 0 )
			$r['vs_sponzor'] = null;
		else
			$r['vs_sponzor'] = $this->getSponzor($branches, $id, 0);
			$r['btw_id'] = $id;
		$r['agreement_conditions'] = 1;

		return $r;
	}

	public $phonesPrefix = array(
		'601',
		'602',
		'603',
		'604',
		'605',
		'606',
		'607',
		'608',
		'720',
		'721',
		'722',
		'723',
		'724',
		'725',
		'726',
		'727',
		'728',
		'729'
	);

}
