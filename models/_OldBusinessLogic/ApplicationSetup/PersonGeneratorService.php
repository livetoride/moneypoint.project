<?php
/**
 * Model pro manipulaci s objednávkami
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;


class PersonGeneratorService
{
	/**
	 * 
	 * @param type $count
	 * @param type $branches
	 * @return type array of array
	 */
	public function getUserTestingData( $count = 20, $branches = 1 )
	{
		$users = array();
		for( $i=0; $i<$count; $i++ )
		{
			$users[$i] = ApplicationPersonGenerator::get()->getPerson( $i, $branches );
		}
		return $users;
	}
}
