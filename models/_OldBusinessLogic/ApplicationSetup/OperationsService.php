<?php
/**
 * Model pro manipulaci s objednávkami
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class OperationsService
{
	private $em;
	
	/** @var \MoneyPoint\CreditOrderFacade */
	protected $creditOrderFacade;
	
	/** @var \MoneyPoint\PosibleRewardsFacade */
	protected $posibleRewardsFacade;
	
	/** @var PeriodRepository */
	protected $periodRepository;
	
	/** @var PeriodNetworkRepository */
	protected $periodNetworkRepository;
	
	/** @var DirefentialCommisionService */
	private $direfentialCommisionService;
	
	private $repository;
	public function __construct(
			\AntoninRykalsky\EntityManager $em,
			\MoneyPoint\CreditOrderFacade $creditOrderFacade,
			\MoneyPoint\PosibleRewardsFacade $posibleRewardsFacade,
			DirefentialCommisionService $direfentialCommisionService
	) {
		$this->em = $em->getEm();
		$this->creditOrderFacade = $creditOrderFacade;
		$this->posibleRewardsFacade = $posibleRewardsFacade;
		$this->periodRepository = $this->em->getRepository('MoneyPoint\Entity\Period');
		$this->periodNetworkRepository = $this->em->getRepository('MoneyPoint\Entity\PeriodNetwork');
		$this->direfentialCommisionService = $direfentialCommisionService;
	}
	
	
	private $posibleRewardsStorage;
	private function storePosibleRewards( $forEntity )
	{
		
		$rewards = $this->posibleRewardsFacade->getRewards( $forEntity->getIdu() );
		$this->posibleRewardsStorage = $rewards;
	}
	private function checkPosibleRewards( $phpunit, $creditsOrder,  $forEntity )
	{
		$rewards = $this->posibleRewardsFacade->getRewards( $forEntity->getIdu() );
		
		$key = $creditsOrder->getTsResolution()->format('Y-m-d');

		$new = $rewards;
		$old = $this->posibleRewardsStorage;
		
//		echo $old->getRewardsSum() . "\n".  $new->getRewardsSum() . "\n";
		$phpunit->assertTrue( $old->getRewardsSum() < $new->getRewardsSum() );
	}
	
	public function buyCreditsFor( $phpunit, $line, $num )
	{
		/* @var $first MoneyPoint\Entity\Member */
		$first = $this->em->find("MoneyPoint\Entity\Member", 45 );
		$childrens = $first->getChildrenMember();
		
		$children = @$childrens[$num];
		if( $children === null )
		{
			throw new \Exception("Neexistuje potomek");
		}
		
		
		$this->storePosibleRewards( $first );

		$goods = new \MoneyPoint\Basket();
		$goods->addToBasket(1, 4000 );

		$details = new \MoneyPoint\OrderDetails();
		$details->setPayment(3);
		
		$creditsOrder = $this->creditOrderFacade->buyCredits( $children->getIdu(), $goods, $details );
		
		$this->checkPosibleRewards( $phpunit, $creditsOrder, $first );
		
		return $creditsOrder;
	}
	
	public function setPaid( $creditsOrder )
	{
		$this->creditOrderFacade->dontPrintPdf();
		$this->creditOrderFacade->processing( $creditsOrder->getId(), array('id_status'=>3) );
	}
	
	public function findoutOrders()
	{
		$orders = $this->em->getRepository("MoneyPoint\Entity\CreditOrder")->findBy(array());
		$periodId = 4;
		
		$this->periodNetworkRepository->resetNetworkPoints('iddqd', $this->em->getReference('MoneyPoint\Entity\Period', $periodId ));
		
		$periodOrders=array();
		/* @var $creditOrder \MoneyPoint\Entity\CreditOrder */
		foreach( $orders as $creditOrder )
		{
			$period = $this->periodRepository->getActualPeriod( $creditOrder->getTsOrder() );
			
			if( !is_array( $periodOrders[ $period->getId() ] )) $periodOrders[ $period->getId() ]=array();
			$periodOrders[ $period->getId() ][] = $creditOrder->getTsOrder()->format('Y-m-d');
			
			if($period->getId() === 4 && $creditOrder->canHaveNetworkPoints() )
			{
				$buyer = $creditOrder->getUser();
				$this->direfentialCommisionService->addDifferentialPoints( $buyer, $creditOrder->getCreditsSum() );
			}
		}
		$this->em->flush();
		print_r( $periodOrders );
		exit;
	}
	
}
