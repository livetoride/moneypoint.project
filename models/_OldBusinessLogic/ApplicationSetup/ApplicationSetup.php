<?php
/**
 * Model pro manipulaci s objednávkami
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;
use dibi;
use AntoninRykalsky as AR;
use AntoninRykalsky\ApplicationPersonGenerator;
use Nette;

class ApplicationSetup extends Nette\Object
{
	/** @var MoneyPoint\DiferentialCommisionFacade */
	var $direfentialCommisionFacade;
	
	public function __construct()
	{
		$container = \Nette\Environment::getContext();
		
		$this->sequenceFixer = new \AntoninRykalsky\SequenceFixer();
//		$this->sequenceFixer->fixSeq();

		$this->users = $this->getUserTestingData( 20 );
		$this->creditsDistribution = $this->getCreditsData();
		
		$this->direfentialCommisionFacade = $container->getByType('\MoneyPoint\DiferentialCommisionFacade');
	}
	
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new ApplicationSetup();
		return $me;
	}
	
	/** @var \AntoninRykalsky\SequenceFixer */
	protected $sequenceFixer;

	public $users;
	public $creditsDistribution;

	private function getUserTestingData( $count = 20, $branches = 1 )
	{
		$users = array();
		for( $i=0; $i<$count; $i++ )
		{
			$users[$i] = ApplicationPersonGenerator::get()->getPerson( $i, $branches );
		}
		return $users;
	}
	
	private function getCreditsData()
	{
		return \DAO\MpCreditsDiscount::get()->findAll()->fetchAll();
	}



	public function clearUsers()
	{
		\DAO\Uzivatele::get()->deleteWhere('idu>0');
		\DAO\Uzivatele::get()->__reset();
		\DAO\UsersWaiting::get()->deleteWhere('1=1');
		\DAO\UsersWaiting::get()->__reset();

		\DAO\MpStructureOverview::get()->deleteWhere('1=1');
		\DAO\MpStructureOverview::get()->__reset();
		\DAO\MpFirstLine::get()->deleteWhere('1=1');
		\DAO\MpFirstLine::get()->__reset();
		
		\DAO\UserPassRecovery::get()->deleteWhere('1=1');
		\DAO\UserPassRecovery::get()->__reset();
		
		\DAO\UzivateleAbadon::get()->deleteWhere('1=1');
		\DAO\UzivateleAbadon::get()->__reset();
		
		\DAO\UserInvitation::get()->deleteWhere('1=1');
		\DAO\UserInvitation::get()->__reset();
		
		$data = array(
			'has_bussiness' => 0,
			'invitation_send' => 0,
			'ts_business' => null,
			'hadCreditFirstBuy' => 0,
		);
		\DAO\Uzivatele::get()->updateWhere('idu>0', $data );
	}

	public function resetStructureOverview()
	{
		$count = 10;
		$rem='';
		$rem.='credits_parent=0,';
		for( $i=1; $i<$count; $i++ )
			$rem.='credits_'.$i.'=0,';
		
		$rem.='rewards_parent=0,';
		for( $i=1; $i<$count; $i++ )
			$rem.='rewards_'.$i.'=0,';
		
		$table = \DAO\MpStructureOverview::get()->table;
		\dibi::query('UPDATE '.$table.' SET '.trim($rem, ','));
		
		
		$data =array(
			'users_first' => 0,
			'users_other' => 0,
			'credits' => 0,
			'rewards' => 0
		);
		$table = \DAO\MpFirstLine::get()->table;
		\dibi::query('UPDATE '.$table.' SET ', $data);
		
		$data =array(
			'vip_coef' => null,
			'has_bussiness' => 0,
			'invitation_send' => 0,
			'ts_business' => null,
			'hadCreditFirstBuy' => 0
		);
		$table = \DAO\Uzivatele::get()->table;
		\dibi::query('UPDATE '.$table.' SET ', $data);
	}


	public function clearPeriods()
	{
		\DAO\MpPeriods::get()->deleteWhere('1=1');
		\DAO\MpPeriods::get()->__reset();
	}
	
	public function clearOrders()
	{
		\DAO\BankAccounts::get()->deleteWhere('1=1');
		\DAO\BankAccounts::get()->__reset();
		\DAO\EndCustomers::get()->deleteWhere('1=1');
		\DAO\EndCustomers::get()->__reset();
		\DAO\MpCreditsAccounts::get()->deleteWhere('1=1');
		\DAO\MpCreditsAccounts::get()->__reset();
		\DAO\MpCreditFutureRewards::get()->deleteWhere('1=1');
		\DAO\MpCreditFutureRewards::get()->__reset();
		\DAO\MpCreditReward::get()->deleteWhere('1=1');
		\DAO\MpCreditReward::get()->__reset();
		\DAO\MpPointsAccounts::get()->deleteWhere('1=1');
		\DAO\MpPointsAccounts::get()->__reset();
		\DAO\MpCreditsStructure::get()->deleteWhere('1=1');
		\DAO\MpCreditsStructure::get()->__reset();
		\DAO\MpPointsWithdraw::get()->deleteWhere('1=1');
		\DAO\MpPointsWithdraw::get()->__reset();
		\DAO\Orders::get()->deleteWhere('1=1');
		\DAO\Orders::get()->__reset();
		\DAO\OrdersGoods::get()->deleteWhere('1=1');
		\DAO\OrdersGoods::get()->__reset();
		\DAO\PartnerOrders::get()->deleteWhere('1=1');
		\DAO\PartnerOrders::get()->__reset();
		\DAO\UserInvitation::get()->deleteWhere('1=1');
		\DAO\UserInvitation::get()->__reset();
		
		$this->resetStructureOverview();
		
		\DAO\PartnerProviderInfo::get()->deleteWhere('1=1');
		\DAO\PartnerProviderInfo::get()->__reset();
		
		\DAO\PartnerProviderFee::get()->deleteWhere('1=1');
		\DAO\PartnerProviderFee::get()->__reset();

		\DAO\MpPeriodNetwork::get()->deleteWhere('1=1');
		\DAO\MpPeriodNetwork::get()->__reset();
		
		\DAO\MpFirstBuyBonus::get()->deleteWhere('1=1');
		\DAO\MpFirstBuyBonus::get()->__reset();
		
		\DAO\MpCreditTransfer::get()->deleteWhere('1=1');
		\DAO\MpCreditTransfer::get()->__reset();
		
		\DAO\MpPeriodNetwork::get()->deleteWhere('1=1');
		\DAO\MpPeriodNetwork::get()->__reset();
		
		$users = \DAO\Uzivatele::get()->findAll()->where('idu>0')->orderBy('idu', 'ASC')->fetchAll();
		// reálně se volají postupně od prvního k pozdějším
		foreach( $users as $user )
		{
			#$this->direfentialCommisionFacade->_initialization( $user['idu'] );
		}

		$invoiceDir = WWW_DIR.'/files/generated-invoices';
		foreach (\Nette\Utils\Finder::findFiles('*.pdf')->in($invoiceDir) as $key => $file)
			unlink( $key );

		\MoneyPoint\DAO\Transactions::get()->deleteWhere('1=1');
		dibi::query("ALTER SEQUENCE mp_transaction_id_transaction_seq RESTART WITH 1;");

		$this->sequenceFixer->fixSeq();
	}

	public function clearProducts()
	{
		AR\ProductsDao::get()->deleteWhere('1=1');
			AR\CreditsDiscount::get()->deleteWhere('1=1');
	}

	public function setup( $destroy = 'none', $type = 1 )
	{
		$this->setupUsers();
	}

	public function setPaidCreditsOrder( $idOrder )
	{
		AR\OrderProcessing::get()->process( $idOrder, (object)array('id_status' => 3), $this );
	}

	public function buyCredits( $idu, $credits, $setPaid=true )
	{
		throw new \Exception("Deprecated!");
	}


	var $idus=array();
	public function setUsers()
	{
		// <editor-fold defaultstate="collapsed" desc=" nastaveni stromove struktury">
		$p = new TestAuthPresenter( \Nette\Environment::getContext() );
		$auth = new \MoneyPoint\AuthFacade();
		$confirmer = new \MoneyPoint\Auth\EmailConfirmationUwStorage();
		foreach( $this->users as $k => $v )
		{
			if(is_numeric($v['vs_sponzor']))
			{
				// nastav vs
				$v['vs_sponzor'] = $this->users[ $v['vs_sponzor'] ]['variable_symbol'];
				$v['vs_sponzor_h'] = $this->users[ $v['vs_sponzor'] ]['variable_symbol'];

			}
			

			$auth->newRegistration( $v );
			$user = AR\UsersWaiting::get()->findAll()->where('email=%s', $v['email'] )->fetch();
			$this->users[$k]['variable_symbol'] = $user->variable_symbol;

			$uw = \DAO\UsersWaiting::get()->findAll()->where("email=%s", $v['email'])->fetchAll();
			$confirmer->confirmEmailAccount( $uw );
			$user = AR\Uzivatele::get()->findAll()->where('email=%s', $v['email'] )->fetch();
			$this->idus[] = $user->idu;
		}
		// </editor-fold>
	}

	public function getUsers()
	{
		return $this->users;
	}
	
	public function getUserCompex( $count = 20, $branches = 2 )
	{
		return $this->getUserTestingData( $count, $branches );
	}
}
