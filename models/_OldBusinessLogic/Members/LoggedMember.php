<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class LoggedMember
{
	/**
	 * @return \MoneyPoint\Member
	 */
	public static function get()
	{
		$idu = \AntoninRykalsky\SystemUser::get()->getIdu();
		static $me = null;
		if ( $me == null )
		$me = new Member( $idu );
		return $me;
	}
	
	public static function reset()
	{
		static $me = null;
	}

}
