<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class Member
{
	
	// <editor-fold defaultstate="collapsed" desc=" entity stuff ">

	public $terms;
	public $clientStructure;
	public $credits;
	public $points;
	private $user;

	public function __construct($idu) {
		$this->user = \DAO\Uzivatele::get()->find($idu)->fetch();
		$this->terms = new Terms($this);
		$this->clientStructure = new ClientStructure($this);
		$this->credits = new Credits($this);
		$this->points = new Points($this);

		$this->idu = $idu;
	}

// </editor-fold>

	public function isLogged()
	{
		return \AntoninRykalsky\SystemUser::get()->getIdu() > 0;
	}

	public function vip()
	{
		return new Vip( $this );
	}

	public function isVip()
	{
		$this->user();
		return (int)$this->user->vip_coef;
	}

	/**
	 *
	 * @return ClientStructure
	 */
	public function clientStructure()
	{
		return $this->clientStructure;
	}

	public function credits()
	{
		return $this->credits;
	}

	public function user()
	{
		if( empty( $this->user ))
			$this->user = \DAO\Uzivatele::get()->find( $this->idu )->fetch();
		return $this->user;
	}

	public function points()
	{
		return $this->points;
	}

	public function transactions()
	{
		return new \Moneypoint\Transaction();
	}

	public function statuses()
	{
		return new MemberStatuses( $this );
	}

	public function partnerOrder()
	{
		return new PartnerOrderFacade( $this );
	}

	public function withdraw()
	{
		return new \Moneypoint\PointsWithdraw( $this );
	}
	
	private $invitation;

	/**
	 * 
	 * @return \MoneyPoint\MemberInvitation
	 */
	public function invitation() {
		if (empty($this->invitation))
			$this->invitation = new \MoneyPoint\MemberInvitation($this);
		return $this->invitation;
	}

}
