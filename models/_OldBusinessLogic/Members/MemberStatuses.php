<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class MemberStatuses
{
	/**
	 * @var bool has status business
	 */
	public $bussiness = false;

	public function __construct( Member $m ) {
		$this->member = $m;
		//$this->bussiness =  $m->credits()->getState() >= 50000;
		$this->bussiness =  $m->user()->has_bussiness;
	}
}
