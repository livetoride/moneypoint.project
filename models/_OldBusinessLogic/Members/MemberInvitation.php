<?php

/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */

namespace MoneyPoint;

use Nette\Object;

class MemberInvitation extends Object {

	private $member;

	public function __construct(Member $m) {
		$this->member = $m;
	}

	public function leftInvitations() {
		if( \MoneyPoint\Invitation::ENABLE_INVITATION_LIMIT == false )
		{
			return 9999999999;
		}
		
		$u = \DAO\Uzivatele::get()->find( $this->member->idu )->fetch(); // obnovime pocty
		return Invitation::FREE_INVITATION_LIMIT - (int) $u->invitation_send;
	}
	
}
