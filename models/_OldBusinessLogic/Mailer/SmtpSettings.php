<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class SmtpSettings
{
	/*
	 * In case we need to, findout which account has to be used.
	 */
	public function getSmtpMailer( $mail )
	{
		return new \Nette\Mail\SmtpMailer( $this->setSmtpServers($mail) );
	}
	/*
	 * In case we need to, findout which account has to be used.
	 */
	public function setSmtpServers( $mail )
	{
		$from = key( $mail->getFrom() );

		$configs = array();
		$configs['antonin@mlm-soft.cz'] = array(
			'host' => 'smtp.live.com',
			'username' => 'antonin@mlm-soft.cz',
			'password' => '*',
			'secure' => 'tls'
		);
		$configs['pozvanky@moneypoint.cz'] = array(
			'host' => 'smtp.gmail.com',
			'username' => 'moneypointcz@gmail.com',
			'password' => '8fD5931bbE',
			'secure' => 'ssl'
		);
		$configs['registrace@moneypoint.cz'] = array(
			'host' => 'smtp.gmail.com',
			'username' => 'moneypointcz@gmail.com',
			'password' => '8fD5931bbE',
			'secure' => 'ssl'
		);
		$configs['info@moneypoint.cz'] = array(
			'host' => 'smtp.gmail.com',
			'username' => 'moneypointcz@gmail.com',
			'password' => '8fD5931bbE',
			'secure' => 'ssl'
		);
		$configs['system@moneypoint.cz'] = array(
			'host' => 'smtp.gmail.com',
			'username' => 'moneypointcz@gmail.com',
			'password' => '8fD5931bbE',
			'secure' => 'ssl'
		);
		if( !empty( $configs[$from] ))
		{
			$mailer = new \Nette\Mail\SmtpMailer( $configs[$from] );
			$mail->setMailer( $mailer );
		} else
			$mailer = null;
		
		
		return $mailer;
	}
	

}
