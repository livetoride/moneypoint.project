<?php
/**
 * Model zacházající s tabulkou kariéra
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace Moneypoint;

class PointsWithdrawDetail extends \DibiRow2Object
{
	protected function overide()
	{
		$this->paid = $this->accepted;

		if( $this->accepted )
			$this->state = 'Vyplacená';
		elseif($this->deleted)
			$this->state = 'Storno';
		else
			$this->state = 'Čeká na vyplacení';
	}
}
