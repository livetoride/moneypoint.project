<?php
/**
 * Model zacházající s tabulkou kariéra
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use AntoninRykalsky as AR;
use AntoninRykalsky\Handy;
use AntoninRykalsky\PointsAccounts;
use DAO\BankAccounts;
use DAO\MpPointsWithdraw;
use dibi;
use Exception;
use Nette\Application\BadRequestException;

class PointsWithdraw
{
	protected $idWithdraw;
	protected $member;

	public function __construct( $id_withdraw=null, $details = false ) {
		if( is_numeric( $id_withdraw ) )
		{
			$this->idWithdraw = $id_withdraw;
		} elseif( $id_withdraw instanceof \MoneyPoint\Member )
		{
			$this->member = $id_withdraw;
		}

		if( $details )
		{
			// vypocitavej detaily
		}
	}

	public function getDSadminWithdraw()
    {
		$this->connection = dibi::getConnection();
        return $this->connection->dataSource("SELECT t.*, u.jmeno || ' ' || u.prijmeni as name, u.variable_symbol FROM $this->table t
				LEFT JOIN uzivatele u ON u.idu = t.idu
				");
    }

	public function createWithdraw( $price, $idu=null )
	{
		if( $this->member instanceof \MoneyPoint\Member )
		{
			$idu = $this->member->idu;
		}
		if( empty( $idu ))
		{
			throw new Exception('Neni znam klient, ktery chce vybirat body');
		}

		dibi::begin();

		// natahneme posledni objednavku kvuli ID 2011-000-000
		$lastWithdraw = MpPointsWithdraw::get()->findAll()->orderBy('id_withdraw')->desc()->fetch();
		$lastWithdraw = Handy::nextYearId2(@$lastWithdraw->id_mpwithdraw, 6);


		$bankAccount = BankAccounts::get()->findAll()->where('[idu]=%i', $idu )->orderBy('[id_account]')->desc()->fetchAll();
		$vals = array(
			'idu' => $idu,
			'points' => $price,
			'request_timestamp' => date( 'Y-m-d H:i:s', strtotime('now') ),
			'bank_account' => $bankAccount[0]['account'],
			'bank_code' => $bankAccount[0]['bank_code'],
			'id_mpwithdraw' => $lastWithdraw
		);

		MpPointsWithdraw::get()->insert($vals);
		$this->idWithdraw = $idWithdraw = dibi::insertId();

		// odebereme body
		PointsAccounts::get()->withdraw( $idu, 'Výběr bodů', -$price );
		$points_account_id = dibi::insertId();

		$transaction= array(
			'id_type' => 3,
			'points_account_id' => $points_account_id,
			'credits_account_id' => null,
			'insert_timestamp' => Handy::get()->getTimestamp(),
			'idu' => $idu,
			'id_withdraw' => $idWithdraw
		);
		\Moneypoint\DAO\Transactions::get()->insert( $transaction );

		dibi::commit();

		return $this;
	}

	public function setStorno()
	{
		$w = MpPointsWithdraw::get()->find( $this->idWithdraw )->fetch();
		if( $w->accepted ) {
			// nefunguje
			$this->flashMessage('Výběr byl již proplacen',AR\Flashes::$info );
			return;
		}

		// transakce
		$t = \DAO\MpTransaction::get()->findAll()->where('id_withdraw=%i', $this->idWithdraw )->fetch();

		// výsledná změna bodů
		PointsAccounts::get()->cancelWithdraw( $w->idu, 'Výběr bodů stornován', 0 );
		$points_account_id = dibi::insertId();

		// odebereme body
		PointsAccounts::get()->withdraw( $w->idu, 'Výběr bodů stornován', $w->points );
		$fix_points_account_id = dibi::insertId();

		$storno = array(
			'deleted'=>1,
			'storno_timestamp'=> Handy::getTimestamp(),
			'storno_points_id'=> $t->points_account_id, // zrušený požadavek na výběr
			'storno_fix_points_id'=> $fix_points_account_id // korekční požadavek na výběr  -  kvůli účtovému zobrazení
		);
		$stornoTransaction = array(
			'points_account_id'=> $points_account_id, // výsledný požadavek na výběr
		);

		MpPointsWithdraw::get()->update( $this->idWithdraw, $storno );
		\DAO\MpTransaction::get()->update( $t->id_transaction, $stornoTransaction );


		return 1;
	}

	public function checkUser()
	{
		$w = MpPointsWithdraw::get()->find( $this->idWithdraw )->fetch();
		$idu = LoggedMember::get()->idu;
		if( $idu != $w->idu ) throw new BadRequestException('Toto není váš výběr');

	}

	public function setProcessed( $v=null )
	{
		if( empty( $this->idWithdraw ))
		{
			throw new Exception('ID vyberu neni nastaveno');
		}
		$v->accepted = (bool)$v->accepted;

		$wOld = MpPointsWithdraw::get()->find( $this->idWithdraw )->fetch();

		if( $v->accepted && empty($wOld->accept_timestamp) )
			@$v->accept_timestamp = date('Y-m-d H:i:s', strtotime('now'));

		MpPointsWithdraw::get()->update( $this->idWithdraw, (array)$v );

//		// odebereme body
//		$withdraw = \DAO\MpPointsWithdraw::get()->find($this->idWithdraw)->fetch();
//		AR\PointsAccounts::get()->withdraw( $withdraw->idu, 'Výběr bodů', -$withdraw->points );
//		$points_account_id = dibi::insertId();

//		$transaction= array(
//			'points_account_id' => $points_account_id,
//			'credits_account_id' => null,
//		);
//		$t = \Moneypoint\DAO\Transactions::get()->findAll()->where('id_withdraw=%i', $this->idWithdraw)->fetch();
//		\Moneypoint\DAO\Transactions::get()->update( $t->id_transaction, $transaction );
	}


}
