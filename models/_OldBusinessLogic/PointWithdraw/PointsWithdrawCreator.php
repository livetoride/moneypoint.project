<?php
/**
 * Model zacházající s tabulkou kariéra
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use AntoninRykalsky\Autodokup;
use AntoninRykalsky\AutodokupPackagesDao;
use AntoninRykalsky\PointsAccounts;
use AntoninRykalsky\PointsWithdraw;

class PointsWithdrawCreator
{
	public function getDokupReserved()
	{
		return 0;
		$idu = \MoneyPoint\LoggedMember::get()->idu;
		$dokup = Autodokup::get()->findUserDokupDetail( $idu );
		if(empty($dokup->id_autodokup)) return null;

		return AutodokupPackagesDao::get()->find( $dokup->id_autodokup )->fetch();
	}

	public function getUpperLimit()
	{
		$idu = \MoneyPoint\LoggedMember::get()->idu;
		$points = PointsAccounts::get()->findAll()->where("idu = %i", $idu )->orderBy('id')->desc()->fetch();
		
		$waiting = PointsWithdraw::get()->findAll()->where("idu = %i AND [accepted]=%i", $idu, 0 )->orderBy('id_withdraw')->desc()->fetchAll();

		$waitingSum = 0;
		foreach( $waiting as $v )
			$waitingSum+=$v['points'];

		$reserved = $this->getDokupReserved();

		@$upLimit = $points->points_left;

		return floor($upLimit);
	}

	public function getPoints()
	{
		$idu = \MoneyPoint\LoggedMember::get()->idu;
		return PointsAccounts::get()->findAll()->where("idu = %i", $idu )->orderBy('id')->desc()->fetch();
	}

	public function getWaitingSum()
	{
		$idu = \MoneyPoint\LoggedMember::get()->idu;
		$waiting = PointsWithdraw::get()->findAll()->where('[idu]=%i AND accepted = 0 AND deleted = 0', $idu)->fetchAll();
		$waitingSum = 0;
		foreach( $waiting as $v )
		{
			$waitingSum += $v['points'];
		}
		return $waitingSum;
	}

	public function getWaitingWithdraw( $idu )
	{
		$waiting = PointsWithdraw::get()->findAll()->where('[idu]=%i AND [accepted]=%i AND [deleted]=%i', $idu, 0, 0 )->fetchAll();
		return $waiting;
	}

	public function getMinimalWithdraw()
	{
		$minimalWithdraw = \AntoninRykalsky\Configs::get()->byContent('general.minimalWithdraw');
		return $minimalWithdraw;
	}
}
