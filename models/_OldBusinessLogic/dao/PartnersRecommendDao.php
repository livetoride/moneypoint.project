<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;
use dibi;

class PartnersRecommendDao extends BaseModel
{

	/** @var string  table name */
	protected $table = 'partners_recommend';

	/** @var string|array  primary key column name */
	protected $primary = 'id';

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new PartnersRecommendDao();
		return $me;
	}


	protected $vals = array(
		'id' => 'int',
		'website' => 'varchar',
		'guess_money' => 'int',
		'my_orders' => 'int',
		'satisfaction' => 'int',
		'idu' => 'int',
		'insert_timestamp' => 'timestamp'
	);

	public function memberDatasource()
    {
		$this->connection = dibi::getConnection();
        return $this->connection->dataSource("SELECT t.*, uzivatele.variable_symbol as userid FROM $this->table t
				LEFT JOIN uzivatele on uzivatele.idu = t.idu");
    }

}
