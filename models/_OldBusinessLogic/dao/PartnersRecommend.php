<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class PartnersRecommend
{
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new PartnersRecommend();
		return $me;
	}

	private	function check_site_existence($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 1); //nevypisovat hlavičky
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //výsledek jako string
		curl_setopt($ch, CURLOPT_URL, $url); //nastavení url
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1); //nasleduj presmerovani
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); //nasleduj presmerovani
		$data = curl_exec($ch);
		curl_close($ch);
		return preg_match("/HTTP\/\d.\d 200 OK/", $data);
	}


	public $my_orders = array(
			'0.7' => '1',
			'0.8' => '2 - 5',
			'0.9' => '6 - 10',
			'1' => 'nad 10',
			);

	private function memberRecommendListModel( $idu )
	{
		$model = PartnersRecommendDao::get()->memberDatasource();

		if( $idu >= 0 ) // admin
		$model->where('idu=%i', $idu );
		return $model;
	}

	/**
	 * Vrací datagrid se seznamem hodnocení
	 */
	public function getRecommendList( $idu )
    {
		$u = Uzivatele::get()->find($idu)->fetch();
		if(!isset( $u->idu ) )	throw new \LogicException('Není předáno ID uživatele');
		$model = $this->memberRecommendListModel( $idu );

		$model = new \DataGrid\DataSources\Dibi\DataSource($model);
		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setDataSource( $model );
		$grid->setRenderer($renderer);
		$grid->itemsPerPage = 10;
		$grid->multiOrder = FALSE;


		$renderer->paginatorFormat = '%input%'; // customize format of paginator
		$renderer->footerFormat = '%paginator%';

		$operations = array(); // define operations
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, PartnersRecommendDao::get()->getPrimary()); // allows checkboxes to do operations with more rows

		// if no columns are defined, takes all cols from given data source
		$grid->addColumn('website', 'Obchod');
		$grid->addColumn('guess_money', 'odhad celkové ceny nákupů ročně');
		$grid['guess_money']->formatCallback[] = 'FormatingHelpers::currency2';

		$grid->addColumn('my_orders', 'počet zatím realizovaných nákupů');
		$grid['my_orders']->replacement = $this->my_orders;

		$grid->addColumn('satisfaction', 'celková spokojenost');
		$grid['satisfaction']->formatCallback[] = 'FormatingHelpers::percent';


		return $grid;
    }

	public function onCellRenderedRecommendList(Html $cell, $column, $value)
	{
		if ($column != 'website') {
			$cell->addClass('center');
		}
		return $cell;
	}

	/**
	 * Přidání hodnocení uživatelem
	 */
	public function addRecommend( $values )
	{
//		print_r( $values );exit;
		if(!is_numeric($values['idu'])) throw new \LogicException('Nebylo předáno ID uživatele');
		if(empty($values['website'])) throw new \LogicException('Zadejte název doporučovaného obchodu');
		if(!is_numeric($values['guess_money'])) throw new \LogicException('Nebyl předán odhad útraty');
		if(empty($values['my_orders'])) throw new \LogicException('Nebyl předán pocet nákupů');
		if(empty($values['satisfaction'])) throw new \LogicException('Nebyl predán údaj o spokojení s obchodem');
		$values['insert_timestamp'] = Handy::getTimestamp();





		$uri = new Uri( 'http://'.$values['website'] );
		$values['website'] = $uri->getAuthority();
		$url = $values['website'];

		if(preg_match('/^www\./', $values['website'] )){
			$withoutWww = preg_replace('/^(www\.)/', '', $values['website']);
			$existsWithoutWww = $this->check_site_existence( $withoutWww );
			if( $existsWithoutWww ) {
				// pokud existuje i bez www, preferuj ten
				$url = $withoutWww;
			}
		} else {
			// jinak testuj cely nazev
			$url = $values['website'];
		}
		$values['website'] = $url;
		$exists = $this->check_site_existence( $values['website'] );
		if(empty($exists) || !$exists ) {
			throw new \LogicException('Na zadané adrese se nic nenachází.');
		}

		$recommends = PartnersRecommendDao::get()->findAll()->where('idu=%i AND website=%s', $values['idu'], $values['website'] )->fetchAll();
		if(!empty( $recommends ))
		{
			throw new \LogicException('Tento obchod jste již doporučil.');
		}

		PartnersRecommendDao::get()->insert( $values );
	}
}
