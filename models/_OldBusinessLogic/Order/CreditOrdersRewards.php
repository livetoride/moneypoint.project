<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;
use AntoninRykalsky\Configs;

class CreditOrdersRewards
{
	private $creditsOrder;
	public $rewards;
	public $points;
	public $credits;

	public function __construct(CreditOrder $order) {
		$this->creditsOrder = $order;
		$t=\Moneypoint\DAO\Transactions::get()->table;
		$this->rewards = \Moneypoint\DAO\Transactions::get()->findAll()
				->where("$t.id_type=%i AND $t.id_order_reward=%i", 1, $this->creditsOrder->idOrder )->fetchAssoc('id_transaction');
		echo \Moneypoint\DAO\Transactions::get()->findAll()
				->where("$t.id_type=%i AND $t.id_order_reward=%i", 1, $this->creditsOrder->idOrder )->__toString();
		$this->points = \Moneypoint\DAO\Transactions::get()->findAll(null, "$t.id_transaction, mp_points_accounts.*")
				->leftJoin('mp_points_accounts')->on("mp_points_accounts.id=$t.points_account_id")
				->where("$t.id_type=%i AND $t.id_order_reward=%i", 1, $this->creditsOrder->idOrder )->fetchAssoc('id_transaction');
		$this->credits = \Moneypoint\DAO\Transactions::get()->findAll(null, "$t.id_transaction, mp_credits_accounts.*")
				->leftJoin('mp_credits_accounts')->on("mp_credits_accounts.id=$t.credits_account_id")
				->where("$t.id_type=%i AND $t.id_order_reward=%i", 1, $this->creditsOrder->idOrder )->fetchAssoc('id_transaction');

	}


}
