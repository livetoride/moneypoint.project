<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;
use AntoninRykalsky\Configs;

class CreditOrdersToReward
{
	public function reward()
	{
		$safetyDeadline = Configs::get()->byContent('general.safety_deadline');
		if(!is_numeric($safetyDeadline)) throw new LogicException( 'Není nastavena bezpečnostní prodleva pro zpracování objednávek' );

		if( $safetyDeadline == 0) $safetyDeadlineSql = ''; else $safetyDeadlineSql = "-interval '$safetyDeadline day'";
		$o = \AntoninRykalsky\Orders::get()->findAll()
				->where("[id_status]=3")
				->orderBy('id_order')
				->fetchAll();

		$state = array('id_status' => 8 /* bodováno */ );
		foreach( $o as $o1 )
		{
			$order = new CreditOrder($o1->id_order);
			$order->setRewarded();

		}
	}
}
