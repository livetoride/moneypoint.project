<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */

namespace MoneyPoint;

class BasketItem implements \AntoninRykalsky\IBasketItem {
	const CREDIT_ITEM = 2;
	
	private $productId;
	private $discountId;
	private $quantity;
	private $price;
	private $productFlag;
	private $credits;
	
	public function getProductId() {
		return $this->productId;
	}

	public function getDiscountId() {
		return $this->discountId;
	}

	public function getQuantity() {
		return $this->quantity;
	}

	public function getPrice() {
		return $this->price;
	}
	
	public function getProductFlag() {
		return $this->productFlag;
	}
	
	public function getCredits() {
		return $this->credits;
	}

		
	public function setCreditProduct($product, $discount) {
		$this->productId = $product->getId();
		$this->discountId = $discount->getId();
		$this->price = $discount->getPrice();
		
		$this->credits = $discount->getCredits();
		$this->quantity = 1;
		$this->productFlag = self::CREDIT_ITEM;
	}
}

class Basket implements \AntoninRykalsky\IBasket
{
	private $goods = array();
	private $creditsSum = 0;
	private $priceSum = 0;

	public function getPriceSum()
	{
		return $this->priceSum;
	}
	
	public function getCreditsSum()
	{
		return $this->creditsSum;
	}
	
	public function getGoods() {
		return $this->goods;
	}

		
	public function setEmpty()
	{
		$this->goods = array();
		$this->creditsSum = 0;
		$this->priceSum = 0;
	}
	
	public function addCredits( \MoneyPoint\Entity\CreditProduct $product, \MoneyPoint\Entity\CreditDiscount $discount )
	{
		$this->creditsSum += $discount->getCredits();
		$this->priceSum += $discount->getPrice();
		
		$basketItem = new BasketItem();
		$basketItem->setCreditProduct( $product, $discount );
		$this->goods[] = $basketItem;
	}
}
