<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;
use AntoninRykalsky\ProductsDao as Produkty;
use AntoninRykalsky\CreditsDiscount;

class CreditOrderDetails extends \Nette\Object
{
	private $creditOrder;
	public $clientStatusText;
	public $statusText;
	public $isPaid = false;
	public $isCanceled = false;
	public $isStorno = false;
	public $isWaitingToPaid = false;

	public $oneCreditPrice;

	public $paymentText;
	public $price_sum;
	public $credits_sum;
	public $idu;
	public $variable_symbol;

	public function __construct( CreditOrder $creditOrder ) {
		$this->creditOrder = $creditOrder;

		$order = \DAO\Orders::get()->find( $creditOrder->idOrder )->fetch();
		$this->price_sum = $order->price_sum;
		$this->credits_sum = $order->credits_sum;
		$this->idu = $order->idu;
		$this->variable_symbol = $order->id_mporder;

		$statuses=array(
			1 => "neuhrazená",
			3 => "uhrazená",
			8 => "odměny přiděleny",
			10 => "storno",
			9 => "storno",
			4 => "storno uživatelem"
		);
		$this->clientStatusText = $statuses[$this->creditOrder->order->id_status];
		if( $this->creditOrder->order->id_status == 8 )
			$this->clientStatusText = $statuses[3];
		

		$isPaid = array(
			1 => false,
			3 => true,
			8 => true,
			10 => false,
			9 => false,
			4 => true
		);
		
		$this->isPaid = $isPaid[$this->creditOrder->order->id_status];
		$this->isCanceled = in_array( $this->creditOrder->order->id_status, array(4,10) );
		$this->isStorno = $this->creditOrder->order->id_status==10;
		$this->isWaitingToPaid = $this->creditOrder->order->id_status==1;
//		print_r( $this->isWaitingToPaid );

		$this->oneCreditPrice = $this->creditOrder->order->price_sum / $this->creditOrder->order->credits_sum;

		$payments = \AntoninRykalsky\OrdersPayments::get()->findAll()->fetchPairs( 'id_payment', 'payment' );
		$payments[3]="Peníze";
		$payments[4]="Body";
		$this->paymentText = $payments[$this->creditOrder->order->id_payment];
	}


}
