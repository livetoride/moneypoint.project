<?php
namespace AntoninRykalsky;

interface IBasket {
	public function getPriceSum();
	
	public function getGoods();
	
	public function setEmpty();
}

interface IBasketItem {
	public function getProductId();

	public function getDiscountId();

	public function getQuantity();

	public function getPrice();
}
