<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class CreditOrdersReward
{
	public $reward;
	public $points;
	public $credits;
	public $order;
	public $buyer;

	// je zaplacena zakoupitelem
	public $paid;

	// vyse odmeny je 0, z duvodu nedostatku kreditu
	public $zero;

	public function __construct( $id_transaction ) {
		if(!is_int($id_transaction)) throw new \Exception('CreditsOrderRewards needs integer id transaction');

		$t=\Moneypoint\DAO\Transactions::get()->table;
		$this->reward = \Moneypoint\DAO\Transactions::get()->findAll()
				->where("$t.id_transaction=%i", $id_transaction )->fetch();
		if(!is_numeric($this->reward->id_transaction )) throw new \Exception('transakce neexistuje');

		// vyhleda prislusnou objednavku
		$this->order = \AntoninRykalsky\Orders::get()->find( $this->reward->id_order_reward )->fetch();
			$expiration = clone $this->order->setpaid_timestamp;
			$ochrannaLhuta = \AntoninRykalsky\Configs::get()->byContent('general.safety_deadline');
			$add = new \DateInterval('P'.(int)$ochrannaLhuta.'D');
			$expiration->add($add);
			@$this->order->expiration = $expiration;

		$this->buyer = \AntoninRykalsky\Uzivatele::get()->find( $this->order->idu )->fetch();

		$this->paid = in_array( $this->order->id_status, array(3,8) );

		// body souvisejici s odmenou
		$this->points = \Moneypoint\DAO\Transactions::get()->findAll(null, "$t.id_transaction, mp_points_accounts.*")
				->leftJoin('mp_points_accounts')->on("mp_points_accounts.id=$t.points_account_id")
				->where("$t.id_transaction=%i", $id_transaction )->fetch();

		// kredit souvisejici s odmenou
		$this->credits = \Moneypoint\DAO\Transactions::get()->findAll(null, "$t.id_transaction, mp_credits_accounts.*")
				->leftJoin('mp_credits_accounts')->on("mp_credits_accounts.id=$t.credits_account_id")
				->where("$t.id_transaction=%i", $id_transaction )->fetch();

	}
	
	
	public function getRozhodnyOkamzik()
	{
		$ro = new \DateTime( $this->order->order_timestamp );
		$days = \AntoninRykalsky\Configs::get()->byContent('general.safety_deadline');
		$interval = new \DateInterval( 'P'.(int)$days.'D' );
		return $ro->add($interval)->format('j.n.Y, H:i:s');
	}
	
	public function getRedukovanaOdmena()
	{
		if( $this->points->points_change == -$this->credits->credits_change*5 )
			return 0;
		// redukovana odmena je rovna odectenym kreditum
		return ($this->points->points_change + $this->credits->credits_change*5);
	}
	public function getPlnaOdmena()
	{
		if( $this->points->points_change == -$this->credits->credits_change*5 )
			return $this->points->points_change;
		// redukovana odmena je rovna odectenym kreditum
		return -$this->credits->credits_change * 5;
	}
	

	public function maximalniMoznaOdmena()
	{
		$member = new Member($this->order->idu);
		return \AntoninRykalsky\ProduktyLevelDistribution::get()
				->getKoeficient( $this->order->price_sum, $this->reward->line, $member->statuses()->bussiness );
	}


}
