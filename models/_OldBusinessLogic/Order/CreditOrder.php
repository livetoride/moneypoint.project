<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

use AntoninRykalsky\Configs;
use AntoninRykalsky\Orders;
use DateInterval;
use DateTime;
use Exception;
use Nette\Object;

class CreditOrder extends Object
{
	public $idOrder;
	public $order;
	public $transaction;
	public $credits;

	public function __construct( $id, $type = 'idOrder', $fast = 0 ) {

		
//		$this->

		switch ($type) {
			case 'idOrder':
				$this->idOrder = $id;
				if( !$fast )
					$this->order = Orders::get()->find($id)->fetch();
				break;

			case 'idTransaction':
				$this->transaction = $t = \Moneypoint\DAO\Transactions::get()->find($id)->fetch();
				$this->idOrder = $t->id_order;
				$this->order = Orders::get()->find($t->id_order)->fetch();
				break;

			default:
				throw new Exception('nevim o jakou objednavku jde');
				break;
		}
		if( empty($this->transaction))
			$this->transaction = \Moneypoint\DAO\Transactions::get()->findAll()->where('id_type=%i AND id_order=%i', 1, $this->idOrder)->fetch();

		if( !empty( $this->transaction->credits_account_id ))
			$this->credits = \AntoninRykalsky\CreditsAccounts::get()->find( $this->transaction->credits_account_id )->fetch();
	}

	public function expiration()
	{
		$expiration = clone $this->order->setpaid_timestamp;
		$ochrannaLhuta = Configs::get()->byContent('general.safety_deadline');
		$add = new DateInterval('P'.(int)$ochrannaLhuta.'D');
		$expiration->add($add);
		@$this->order->expiration = $expiration;
		return $expiration;
	}

	public function orderDetails()
	{
		return new CreditOrderDetails( $this );
	}

	public function client()
	{
		return new Member( $this->order->idu );
	}

	public function processing()
	{
		return new CreditOrdersProcessing( $this );
	}

	public function rewards()
	{
		return new CreditOrdersRewards( $this );
	}

	public function orderGoods()
	{
//		throw new \Exception();
		$goods = new Basket();

		$g = \AntoninRykalsky\OrdersGoods::get()->findAll()->where('id_order = %i', $this->idOrder )->fetchAll();
		foreach( $g as $item )
		{
			$goods->addToBasket($item['id_product'], $item['quantity'] );
		}


		return $goods;
	}

	

	public static function automaticStorno()
	{

		$days = \AntoninRykalsky\Configs::get()->byContent('general.automatic_creditorder_storno');

		if( $days == 0 ) $daysText = 'now'; else $daysText="-".$days.'days';
		$time = date('Y-m-d H:i:s', strtotime($daysText));
		$o = \DAO\Orders::get()->findAll()->where('id_status = 1 AND order_timestamp <= %t', $time )->fetchPairs('id_order', 'id_order');
		
		if( !empty(static::$debug) )
		echo "\n pocet neuhrazenych objednavek je ".count($o)."\n";

		// quick storno
		$orderDetails = array(
			'id_status' => 10,
			'storno_timestamp' => new DateTime('now')
		);
		$keys = implode(',', (array)array_keys($o) );if( empty( $keys )) $keys = 'null';
		\DAO\Orders::get()->updateWhere('id_order IN ('.$keys.')', $orderDetails);

	}


	public function setStornoAfterPaid()
	{
		throw new \Exception("Deprecated");
	}


	public function setStorno( $idStatus = 10 )
	{
		$orderDetails = array(
			'id_status' => $idStatus,
			'storno_timestamp' => new DateTime('now')
		);
		Orders::get()->update( $this->idOrder, $orderDetails );
	}

	public function generateInvoice( $storno = 0 )
	{
		$presenter = new \FrontModule\InvoicePresenter(\Nette\Environment::getContext());
        $request = new \Nette\Application\Request('Invoice', 'GET', array());

		$g = $presenter->getMyComponent();
		$o = \DAO\Orders::get()->find( $this->idOrder )->fetch();
		$g->setObjednavka($o);

		if( $storno ) {
			$g->setStorno();
		}

		$response = $presenter->run($request);
		echo $response->getSource();
	}

}
