<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class OrderDetails
{
	public $zpusob_platby;
	public $adminOrder = false;
	public $allowKoncovy = false;
	public $allowPoznamka = false;
	private $endCustomerId = false;

	public function setPayment( $idZpusob )
	{
		$this->zpusob_platby = $idZpusob;
	}
	
	public function setAddress( $v )
	{
		$idu = \AntoninRykalsky\SystemUser::get()->idu;
		$address['ulice'] = $v['koncovy_ulice'];
		$address['mesto'] = $v['koncovy_mesto'];
		$address['psc'] = $v['koncovy_psc'];
		\DAO\Uzivatele::get()->update( $idu, $address );
		$userData = (array)\DAO\Uzivatele::get()->find( $idu )->fetch();
		$address+=$userData;
		\DAO\EndCustomers::get()->insert( $address );
		$this->endCustomerId = \dibi::insertId();
	}
	
	public function getEndCustomerId() {
		return $this->endCustomerId;
	}


			
}
