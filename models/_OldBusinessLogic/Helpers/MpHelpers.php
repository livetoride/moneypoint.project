<?php
/**
 * FormatingHelpers
 *
 * @author Antonin Rykalsky @ 2oo9
 */
abstract class MpHelpers extends \FormatingHelpers {

	/**
	 *
	 * @return <type> Automatický loader helperů
	 */
	public static function loader($helper)
    {
        $callback = callback(__CLASS__, $helper);
        if ($callback->isCallable()) {
            return $callback;
        }
    }
	
	public static function vipHelper($value)
    {
        if( $value == null || $value == 0 ){
            return 'Ne';
        } else {
            return $value.'%';
        }
    }

	public static function creditsAccountPointsSign($num)
    {
		$num=(double)($num);
		$num = number_format( $num, 0, ',', ' ');
		if( $num > 0)
			$r = '+ '.$num.' Kč';
		elseif( $num == 0)
			$r = '+ 0 Kč';
		else
			$r = '- '.abs($num).' Kč';
		return $r;
    }
	
	public static function kredityUAkciAOdmen($value)
    {
		#if($id!=null && !is_string($id)) throw new LogicException('Zadaná hodnota není číselná');
		$castka = number_format( $value, 0, ',', ' ');
		
		$plus='';
		if( $value > 0 ) $plus='+';
        return $plus.$castka. ' Kč';
    }
	
	public static function kredity($value, $id = null)
    {
		#if($id!=null && !is_string($id)) throw new LogicException('Zadaná hodnota není číselná');
		$castka = number_format( $value, 0, ',', ' ');
		if(is_string($id))
		{
			$el = Html::el('span');
			$el->id = $id;
			$el->setText($castka);
			$castka = $el;
		}
        return $castka. ' Kč';
    }
	
	public static function cislo($value)
    {
		$castka = number_format( $value, 0, ',', ' ');
        return $castka;
    }

	public static function kredityLong($value, $id = null)
    {
		#if($id!=null && !is_string($id)) throw new LogicException('Zadaná hodnota není číselná');
		$castka = number_format( $value, 0, ',', ' ');
		if(is_string($id))
		{
			$el = Html::el('span');
			$el->id = $id;
			$el->setText($castka);
			$castka = $el;
		}
        return $castka. ' Kč';
    }

	public static function MP_id($value)
    {
		preg_match('/^(\d?\d?\d?\d?)(\d\d\d\d)$/', $value, $matches);
		//print_r( $matches );exit;
		if(!empty($matches[1]))
			return @$matches[1] . '' . @$matches[2];
		else {
			return '-';
		}
    }

	public static function MP_idwithdraw($value)
    {
		preg_match('/(\d\d)(\d\d\d)(\d\d\d)/', $value, $matches);
		//print_r( $matches );exit;
        return $matches[1] . ' ' . $matches[2]. ' ' . $matches[3];
    }

	public static function MP_idorder($value)
    {
		preg_match('/(\d\d\d\d)(\d\d\d)(\d\d\d)/', $value, $matches);
		// print_r( $matches );exit;
        return ($matches[1] . ' ' . $matches[2]. ' ' . $matches[3]);
    }
	
	public static function datetime($date)
    {
		if (!empty ($date))
		return $date->format('j.m.Y, H:i:s');
    }
	
	public static function datetimeshort($date)
    {
		return $date->format('j.n.Y');
    }
	
	public static function partnerReward( $r )
	{
		$decimal = \AntoninRykalsky\Configs::get()->byContent('partners.feeDecimal');

		// na alespon jedno desetinne
		if( !$decimal ) {
			if( $r - round( $r ) <> 0)
				$return = number_format($r, 1, ',', '').' %';
			else
				$return = number_format($r, 0, ',', '').' %';
		} 
		// na jedno desetinne
		else
			$return = number_format($r, 1, ',', '').' %';
		return $return;
	}
	
	public static function partnerLink( $r )
	{
		
		$append = '';
		
		$idu = 0;
		if( \AntoninRykalsky\SystemUser::get()->isLogged() ) {
			$idu = \AntoninRykalsky\SystemUser::get()->getIdu();
		}
		
		/*$type='';
		$type .= ($type==='' && preg_match('#espoluprace#', $r->link ) ? 'ESPOLUPRACE' : '');
		$type .= ($type==='' && preg_match('#click-7306242-#', $r->link ) ? 'CJ' : '');*/
		
		$type = $r->provider_id;
		
		
		$connector='';
		switch ($type) {
			case 2:
				# http://tracking.espoluprace.cz/aff_c?offer_id=98&aff_id=9188&aff_sub=123&file_id=1136
				preg_match('#\?#', $r->link ) ? $connector .='&' : $connector .='?';
				$append .= $connector.'aff_sub='.$idu;
				break;
			case 1:
				preg_match('#\?#', $r->link ) ? $connector .='&' : $connector .='?';
				$append .= $connector.'sid='.$idu;
				break;
			case 3:
				$append .= $connector.'-'.$idu;
				break;
			
			default:
				
				break;
		}
		
		$return = $r->link.$append;
		return $return;
	}
	
	public static function partnerEuro( $return )
	{
		$return = preg_replace('#\.#', ',', $return );
		return $return . ' €';
	}
	
	public static function partnerEuroCoef( $return )
	{
		$return = preg_replace('#\.#', ',', $return );
		return $return . ' Kč/€';
	}
	
	
	public static function differentialCoef( $return )
	{
		if(!is_numeric($return)) return $return;
		return number_format($return, 3, ',', ' ' );
	}
	
	public static function notNegativeDifferentialCarrierDiff( $return )
	{
		return $return < 0 ? '-': $return . ' %';
	}
	
	

}
