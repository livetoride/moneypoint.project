CREATE INDEX config_modules_module_key_idx ON config_modules ((lower(module_key)));
CREATE INDEX menu_menu_order_idx ON menu(menu_order);
DROP INDEX IF EXISTS menu_id_resource_idx;
CREATE INDEX menu_id_resource_idx ON menu((lower(id_resource)));
CREATE INDEX gui_acl_resources_key_name_idx ON gui_acl_resources((lower(key_name)));