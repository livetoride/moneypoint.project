<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

use dibi;

class PointsAccounts extends BaseModel
{

	/** @var string  table name */
	protected $table = 'mp_points_accounts';

	/** @var string|array  primary key column name */
	protected $primary = 'id';

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new PointsAccounts();
		return $me;
	}


	protected $vals = array(
		'id' => 'int',
		'idu' => 'int',
		'points_change' => 'int',
		'points_left' => 'int',
		'reason' => 'varchar',
		'change_timestamp' => 'timestamp',
		'id_type' => 'int',
		'line' => 'int',
		'id_order' => 'int',
		'id_pio_order' => 'int',
		'points_structure' => 'int'
	);

	/** vratí aktuální záznam o bodech klienta */
	var $lastActualState;
	public function actualState( $idu, $reset = 0 )
	{
		if( $reset || empty( $this->lastActualState[$idu] ))
			$this->lastActualState[$idu] = $this->findAll()->where('idu=%i', $idu)->orderBy('id')->desc()->fetch();
		return $this->lastActualState[$idu];
	}

	/**
	 * Funkce vyhrazená pro změny kreditu, měla by být obalena v transakci
	 * @param type $idu
	 * @param type $reason
	 * @param type $points
	 */
	public function creditsOrder( $idu, $reason, $points_change, $line=null, $id_order = null  )
	{
		if( $idu === null ) throw new \LogicException('Musí být zadáno pro koho body jsou!');
		if( $reason === null ) throw new \LogicException('Zadejte textovou informaci o transakci!');
		if( $points_change === null ) throw new \LogicException('Zadejte změnu na bodovém účtu!');
		if( $line === null ) throw new \LogicException('Musí být zadána linie!');
		if( $id_order === null ) throw new \LogicException('Musí být zadána objednávka!');
		$id_type = 1;

		$this->modifyPoints( $idu, $reason, $points_change, $id_type, $line, $id_order );
	}
	
	public function diferentialCommision( $idu, $reason, $points_change )
	{
		if( $idu === null ) throw new \LogicException('Musí být zadáno pro koho body jsou!');
		if( $reason === null ) throw new \LogicException('Zadejte textovou informaci o transakci!');
		if( $points_change === null ) throw new \LogicException('Zadejte změnu na bodovém účtu!');
		$id_type = 5;

		$this->modifyPoints( $idu, $reason, $points_change, $id_type );
	}


	/**
	 * Funkce vyhrazená pro změny kreditu, měla by být obalena v transakci
	 * @param type $idu
	 * @param type $reason
	 * @param type $points
	 */
	public function pioPoints( $idu, $reason, $points_change, $id_pio_order = null  )
	{
		if( $idu == null ) throw new \LogicException('Musí být zadáno pro koho body jsou!');
		if( $reason == null ) throw new \LogicException('Zadejte textovou informaci o transakci!');
		if( $points_change == null ) throw new \LogicException('Zadejte změnu na bodovém účtu!');
		if( $id_pio_order == null ) throw new \LogicException('Musí být zadáno ID objednávky z pio!');
		$id_type = 2;

		$this->modifyPoints( $idu, $reason, $points_change, $id_type, null, null, $id_pio_order );
	}

	/**
	 * Funkce vyhrazená pro změny kreditu, měla by být obalena v transakci
	 * @param type $idu
	 * @param type $reason
	 * @param type $points
	 */
	public function specPartnerPoints( $idu, $reason, $points_change, $id_pio_order = null  )
	{
		if( $idu == null ) throw new \LogicException('Musí být zadáno pro koho body jsou!');
		if( $reason == null ) throw new \LogicException('Zadejte textovou informaci o transakci!');
		if( $points_change == null ) throw new \LogicException('Zadejte změnu na bodovém účtu!');
		if( $id_pio_order == null ) throw new \LogicException('Musí být zadáno ID objednávky z pio!');
		$id_type = 2;

		$this->modifyPoints( $idu, $reason, $points_change, $id_type, null, null, $id_pio_order );
	}

	public function withdraw( $idu, $reason, $points_change  )
	{
		if( $idu == null ) throw new \LogicException('Musí být zadáno pro koho body jsou!');
		if( $reason == null ) throw new \LogicException('Zadejte textovou informaci o transakci!');
		if( $points_change == null ) throw new \LogicException('Zadejte změnu na bodovém účtu!');
		$id_type = 3;

		$this->modifyPoints( $idu, $reason, $points_change, $id_type, null, null, null );
	}

	public function cancelWithdraw( $idu, $reason, $points_change  )
	{
		if( $idu == null ) throw new \LogicException('Musí být zadáno pro koho body jsou!');
		if( $reason == null ) throw new \LogicException('Zadejte textovou informaci o transakci!');
		//if( $points_change == null ) throw new \LogicException('Zadejte změnu na bodovém účtu!');
		$id_type = 3;

		// odebereme body
		$this->modifyPoints( $idu, $reason, $points_change, $id_type, null, null, null );
		// vysledny záznam
		$this->modifyPoints( $idu, $reason, $points_change, $id_type, null, null, null );
	}


	public function manualChange( $idu, $reason, $points_change  )
	{
		if( $idu == null ) throw new \LogicException('Musí být zadáno pro koho body jsou!');
		if( $reason == null ) throw new \LogicException('Zadejte textovou informaci o transakci!');
		if( $points_change == null ) throw new \LogicException('Zadejte změnu na bodovém účtu!');
		$id_type = 4;

		$this->modifyPoints( $idu, $reason, $points_change, $id_type, null, null, null );
	}


	public function uhradaBUcet( $idu, $reason, $points_change, $id_type,  $id_order = null  )
	{
		if( $idu == null ) throw new \LogicException('Musí být zadáno pro koho body jsou!');
		if( $reason == null ) throw new \LogicException('Zadejte textovou informaci o transakci!');
		if( $points_change == null ) throw new \LogicException('Zadejte změnu na bodovém účtu!');
		if( $points_change > 0 ) throw new \LogicException('Úhrada musí být záporná.');

		if( $id_order == null ) throw new \LogicException('Musí být zadána související objednávka!');
		$id_type = 5;

		$this->modifyPoints( $idu, $reason, $points_change, $id_type, null, $id_order, null );
	}

	/**
	 * Funkce vyhrazená pro změny kreditu, měla by být obalena v transakci
	 * @param type $idu
	 * @param type $reason
	 * @param type $points
	 */
	protected function modifyPoints( $idu, $reason, $points_change, $id_type, $line=null, $id_order = null, $id_pio_order = null  )
	{
		// stary zaznam
		$points_change = round($points_change*10)/10;

		if( $id_type == null ) throw new \LogicException('O jaký typ provize jde?');

		// stary zaznam
		$points = $points_change;
		// pokud je změna kladná počítej ji do obratu
		$points_structure = ($points_change>0?$points_change:0);
		$last = $this->actualState($idu);
		if( !empty($last['id']) )
		{
			$points = $last['points_left'] + $points_change;
			// pokud je změna kladná počítej ji do obratu
			$points_structure = (int)@$last['points_structure'] + ($points_change>0?$points_change:0);
		}
		if( $points_change < 0 && $last['points_left'] < abs($points_change) )
			throw new \LogicException('Nemůžu odebírat body do mínusu');

		$pAccount = array(
			'idu' => $idu,
			'reason' => $reason,
			'points_change' => $points_change,
			'points_left' => $points,
			'change_timestamp' => Date("Y-m-d H:i:s", strtotime('now')),
			'id_type' => $id_type,
			'line' => $line,
			'id_order' => $id_order,
			'id_pio_order' => $id_pio_order,
			'points_structure' => $points_structure
		);
		return $this->insert( $pAccount );


	}

	public function getDataSource()
    {
		$this->connection = dibi::getConnection();
        return $this->connection->dataSource("SELECT t.*, u.variable_symbol || ', LKS' || t.line as variable_symbol  FROM $this->table t
				LEFT JOIN orders o ON o.id_order = t.id_order
				LEFT JOIN uzivatele u ON u.idu = o.idu
				");
    }


}
