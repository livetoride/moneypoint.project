<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;
use dibi;

class PointsWithdraw extends BaseModel
{

	/** @var string  table name */
	protected $table = 'mp_points_withdraw';

	/** @var string|array  primary key column name */
	protected $primary = 'id_withdraw';

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new PointsWithdraw();
		return $me;
	}


	protected $vals = array(
		'id_withdraw' => 'int',
		'id_mpwithdraw' => 'int',
		'idu' => 'int',
		'points' => 'int',
		'accepted' => 'bool',
		'description' => 'text',
		'request_timestamp' => 'timestamp',
		'accept_timestamp' => 'timestamp',
		'bank_account' => 'varchar',
		'bank_code' => 'varchar',
		'employee_comment' => 'varchar',
		'insert_timestamp' => 'varchar',
		'deleted' => 'boolean'
	);



	public function getDSadminWithdraw()
    {
		$this->connection = dibi::getConnection();
        return $this->connection->dataSource("SELECT t.*, u.jmeno || ' ' || u.prijmeni as name, u.variable_symbol, tr.mp_trans_id FROM $this->table t
				LEFT JOIN uzivatele u ON u.idu = t.idu
				LEFT JOIN mp_transaction tr ON tr.id_withdraw = t.id_withdraw
				");
    }

}
