<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class ClientStructure
{
	protected $member;

	public function __construct( Member $m ) {
		$this->member = $m;
	}

	public function myStructure()
	{
		$structure = \AntoninRykalsky\GetAscendant::get()->find( $this->member->idu );

		$sCorrect = array();
		foreach( $structure as $k => $v )
			if( $v != 0) $sCorrect[ $k+1 ] = $v;

		return $sCorrect;
	}



}
