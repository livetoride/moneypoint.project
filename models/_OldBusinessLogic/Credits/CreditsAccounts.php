<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;
use dibi;

class CreditsAccounts extends BaseModel
{

	/** @var string  table name */
	protected $table = 'mp_credits_accounts';

	/** @var string|array  primary key column name */
	protected $primary = 'id';

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new CreditsAccounts();
		return $me;
	}


	protected $vals = array(
		'id' => 'int',
		'idu' => 'int',
		'credits_change' => 'int',
		'credits_left' => 'int',
		'reason' => 'varchar',
		'change_timestamp' => 'timestamp',
		'id_order' => 'int',
		'id_pio_order' => 'int',
		'credits_sum' => 'bigint'	// součet nakoupených bodů
	);


	var $actualStates=array();

	/** vratí aktuální záznam o kreditech */
	var $lastActualState;
	public function actualState( $idu )
	{
		#if( empty( $this->lastActualState[$idu] ))
			$this->lastActualState[$idu] = $this->findAll()->where('idu=%i', $idu)->orderBy('id')->desc()->fetch();
		return $this->lastActualState[$idu];
	}

	
	/**
	 * Funkce vyhrazená pro změny kreditu, měla by být obalena v transakci
	 * @param type $idu
	 * @param type $reason
	 * @param type $credits
	 */
	public function modifyCredits( $idu, $reason, $credit_change, $id_order = null, $id_pio_order = null )
	{
		// stary zaznam
		$credit_change = floor($credit_change*10)/10;
//		$credit_change = round($credit_change*10)/10;
		$credits = $credit_change;
		$last = $this->actualState($idu);
		if( !empty($last['id']) )
		{
			$credits = $last['credits_left'] + $credit_change;
		}
		if( $credit_change < 0 && $last['credits_left'] < abs($credit_change) )
			throw new \LogicException('Nemůžu odebírat kredit do mínusu. Aktualni stav: '.$last['credits_left'].' odecti:'.abs($credit_change), 1 );

		// pokud je změna kladná počítej ji do obratu
		$last = $this->actualState($idu);
		$credit_sum = (int) @$last['credits_sum'];

		if( $credit_change > 0 )
		{
			// pokud je změna kladná počítej ji do obratu
			$credit_sum += $credit_change;
		}


		$cAccount = array(
			'idu' => $idu,
			'reason' => $reason,
			'credits_change' => $credit_change,
			'credits_left' => $credits,
			'change_timestamp' => Date("Y-m-d H:i:s", strtotime('now')),
			'id_order' => $id_order,
			'id_pio_order' => $id_pio_order,
			'credits_sum' => $credit_sum
		);
		// nextval('credits_accounts_sq'::regclass),
		return $this->insert( $cAccount );


	}

	public static function dsCreditsAccountsList()
	{
		
		$c = \dibi::getConnection();
		return $c->dataSource(
			"SELECT ca.id, ca.idu, ca.credits_change, ca.credits_left, ca.change_timestamp, 
				(CASE WHEN t.id_type IS not null THEN t.mp_trans_id
				ELSE ca.reason
				END) as reason
			FROM mp_credits_accounts ca
			LEFT JOIN mp_transaction t ON t.credits_account_id = ca.id"
		);

	}

	public function getDataSource()
    {
		$this->connection = dibi::getConnection();
		return $this->connection->dataSource("SELECT t.*, (u.variable_symbol || ', LKS' || pa.line) as variable_symbol,
				pa.points_change

				,(CASE WHEN o.id_order>0 THEN o.price_sum
					WHEN t.id_pio_order>0 THEN po.utrata
					ELSE 0
				END) as objem
				,(CASE WHEN o.id_order>0 THEN (u.variable_symbol || '-' || pa_classic.line || '-kredity' ) /* asdf */
					WHEN t.id_pio_order>0 THEN (u.variable_symbol || '-obchod')
					ELSE (u.variable_symbol || '-jine')
				END) as cislo_akce

				FROM $this->table t
				LEFT JOIN orders o ON o.id_order = t.id_order
				LEFT JOIN partner_orders po ON po.id_order = t.id_pio_order

				LEFT JOIN uzivatele u ON u.idu = t.idu
				LEFT JOIN mp_points_accounts pa ON pa.idu=t.idu AND pa.id IN
				(
					SELECT max(pa2.id) FROM mp_points_accounts pa2 WHERE pa2.idu=t.idu AND pa2.id_pio_order = t.id_pio_order
				)
				LEFT JOIN mp_points_accounts pa_classic ON pa.idu=t.idu AND pa.id IN
				(
					SELECT max(pa2.id) FROM mp_points_accounts pa2 WHERE pa2.idu=t.idu AND pa2.id_order = t.id_order
				)
				");
    }

}
