<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class CreditsStructure extends BaseModel
{

	/** @var string  table name */
	protected $table = 'mp_credits_structure';

	/** @var string|array  primary key column name */
	protected $primary = 'id';

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new CreditsStructure();
		return $me;
	}


	protected $vals = array(
		'id' => 'int',
		'idu' => 'int',
		'credits_change' => 'int',
		'credits_structure' => 'int',
		'reason' => 'varchar',
		'depth' => 'int',
		'id_order' => 'int'
	);


	/** vratí aktuální záznam o kreditech */
	public function actualState( $idu )
	{
		$last = $this->findAll()->where('idu=%i', $idu)->orderBy('id')->desc()->fetch();
		return $last;
	}

	/**
	 * Funkce vyhrazená pro změny kreditu, měla by být obalena v transakci
	 * @param type $idu
	 * @param type $reason
	 * @param type $credits
	 */
	public function modifyCredits( $idu, $reason, $credit_change, $depth, $id_order = null )
	{

		// stary zaznam
		$credits = $credit_change;
		$last = $this->actualState($idu);

		if( !empty($last['id']) )
		{
			$credits = $last['credits_structure'] + $credit_change;
		}
		if( $credit_change < 0 && $last['credits_left'] < abs($credit_change) )
			throw new LogicException('Nemůžu odebírat kredit do mínusu');


		$cAccount = array(
			'idu' => $idu,
			'reason' => $reason,
			'credits_change' => $credit_change,
			'credits_structure' => $credits,
			'depth' => $depth,
			'id_order' => $id_order
		);
		$this->insert( $cAccount );
	}
}
