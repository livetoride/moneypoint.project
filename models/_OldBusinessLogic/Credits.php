<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class Credits
{
	protected $member;
	private $lastTransaction;

	public function __construct( Member $m ) {
		$this->member = $m;
	}

	public function getLastTransaction()
	{
		$lastTransaction = \AntoninRykalsky\CreditsAccounts::get()->actualState( $this->member->idu );
		$this->lastTransaction = $lastTransaction;
		return $lastTransaction;
	}

	public function getState()
	{
		$this->getLastTransaction();
		return (int)@$this->lastTransaction->credits_left;
	}

	public function buy( Basket $goods, OrderDetails $details )
	{
		throw new \Exception("Deprecated!");
	}



}
