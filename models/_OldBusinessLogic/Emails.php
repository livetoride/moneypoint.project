<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;
use FormatingHelpers;
use Nette\Environment;

use AntoninRykalsky as AR;

class Emails
{
	
	// <editor-fold defaultstate="collapsed" desc=" newOrder">
	public function newOrder($id_order) {

		$order = new \MoneyPoint\CreditOrder($id_order);
		$email = \DAO\Uzivatele::get()->find($order->orderDetails()->idu)->fetch()->email;

		if (!$order->orderDetails()->isPaid) {
			$regmail = AR\Configs::get()->byContent('mailer-creditsorder-bank.email');
			$subject = AR\Configs::get()->byContent('mailer-creditsorder-bank.subject');
		} else {
			$regmail = AR\Configs::get()->byContent('mailer-creditsorder-points.email');
			$subject = AR\Configs::get()->byContent('mailer-creditsorder-points.subject');
		}

		$ba = AR\Configs::get()->byContent('general.bank_account');
		$regmail = preg_replace('/\[\[bankaccount]\]/', $ba, $regmail);

		$bc = AR\Configs::get()->byContent('general.bank_code');
		$regmail = preg_replace('/\[\[bankcode]\]/', $bc, $regmail);

		$regmail = preg_replace('/\[\[variable_symbol]\]/', $order->orderDetails()->variable_symbol, $regmail);
		$regmail = preg_replace('/\[\[price]\]/', \FormatingHelpers::currency2($order->orderDetails()->price_sum), $regmail);
		$regmail = preg_replace('/\[\[credits_sum]\]/', $order->orderDetails()->credits_sum, $regmail);
		$regmail = preg_replace('/\[\[transaction_id]\]/', $order->transaction->mp_trans_id, $regmail);

		if (!empty($order->credits->credits_change))
			$regmail = preg_replace('/\[\[credits]\]/', $order->credits->credits_change, $regmail);
		$regmail = preg_replace('/\[\[paid_points]\]/', $order->orderDetails()->price_sum, $regmail);

		$from = AR\Configs::get()->byContent('general.mailer_from');
		$to = $email;


				$mail = new \MoneyPoint\Mailer();
				$mail->setFrom($from);
				$mail->addTo($to);
				$mail->setSubject($subject);
				$mail->setHTMLBody($regmail, WWW_DIR . '/images/moneypoint');
				$mail->send();
		
	}

// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc=" creditsOrderPaid">
	public static function creditsOrderPaidPoints( $o, $u ) 	{
		$emailKey = 'mailer-creditsorder-points';
		$mail = AR\Configs::get()->byContent($emailKey.'.email');
		$subject = AR\Configs::get()->byContent($emailKey.'.subject');
		$from = AR\Configs::get()->byContent($emailKey.'.from');
		
		$mail = preg_replace('/\[\[order_date\]\]/', \FormatingHelpers::czechDateShort($o->order_timestamp), $mail);
		$mail = preg_replace('/\[\[payment]\]/', \FormatingHelpers::currency($o->price_sum), $mail);
		
		// zaslání emailu - novy klient
		$m=new \MoneyPoint\Mailer();

	}
	// </editor-fold>
	// 
	// <editor-fold defaultstate="collapsed" desc=" credits order setpaid email">
	public static function creditsOrderConfirmPaid( Entity\CreditOrder $o, $u ) 	{
		$emailKey = 'mailer-creditsorder-confirmpaid';
		$mail = AR\Configs::get()->byContent($emailKey.'.email');
		$subject = AR\Configs::get()->byContent($emailKey.'.subject');
		$from = AR\Configs::get()->byContent($emailKey.'.from');
		
		$mail = preg_replace('/\[\[order_date\]\]/', FormatingHelpers::czechDateShort( $o->getOrderTimestamp() ), $mail);
		$mail = preg_replace('/\[\[payment]\]/', FormatingHelpers::currency($o->getPriceSum() ), $mail);
		$mail = preg_replace('/\[\[order_pricesum]\]/', FormatingHelpers::currency($o->getPriceSum() ), $mail);
		$mail = preg_replace('/\[\[order_creditssum]\]/', $o->getCreditsSum(), $mail);
		$mail = preg_replace('/\[\[transaction_id]\]/', $o->getTransaction()->getMpTransactionId(), $mail);
		
		
		// zaslání emailu - novy klient
		$m=new \MoneyPoint\Mailer();

		$m->setFrom( $from );
		$m->addTo( $u->email );
		$m->setSubject( $subject );
		$m->setHTMLBody( $mail );
		$m->send();
	}
	// </editor-fold>

}
