<?php
/**
 * Model zacházející s tabulkou získaných bodů
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class Vip
{
	public function __construct( $parent ) {
		$this->parent = $parent;
	}

	public function getVipInfo( $idu=null ) {
		$idu = $this->parent->idu;
		$user = \DAO\Uzivatele::get()->find( $idu )->fetch();
		if(!empty($user->vip_coef))
		{
			$object = (object) array('trida' => null, 'bonus' => (int)@$user->vip_coef, 'vip' => 1 );
			return $object;
		}
	}

	public function set( $value = 100 )
	{
		$data = array( 'vip_coef' => $value );
		\DAO\Uzivatele::get()->update( $this->parent->idu, $data );
		return 1;
	}

	public function deactivate()
	{
		return $this->set( 0 );
	}
}
