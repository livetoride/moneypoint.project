<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace MoneyPoint;

class DatagridRenderer
{
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new DatagridRenderer();
		return $me;
	}

	public function getRenderer()
	{
		$r = new \DataGrid\Renderers\Conventional;
		//		$renderer->paginatorFormat = '%label% %input% of %count%';
		$r->footerFormat = '%paginator% %info%';
//		$r->infoFormat = 'Položky %from% - %to% z %count% | Zobraz: %selectbox% | %reset%';
		$r->infoFormat = 'Položky %from% - %to% z %count%';
		//$r->infoFormat = '';
		$r->paginatorFormat = '%input%';
		//$r->paginatorFormat = 'Strana %page% z %count%';

		return $r;
	}
}
