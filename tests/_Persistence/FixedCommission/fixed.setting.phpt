<?php
require __DIR__ . '/../bootstrap.php';
use Tester\Assert;

/**
 */

class Test  extends Tester\TestCase {
	public function setUp()
	{
		$container = \Nette\Environment::getContext(); 
		$this->em = $container->getByType("AntoninRykalsky\EntityManager")->getEm();
		$this->fixedCommisionFacade = $container->getByType("MoneyPoint\FixedCommisionFacade");
	}
	
	public function testOne()
	{
		$bc = new \AntoninRykalsky\Entity\CommisionEntity;
		$bc->setBaseCommision( \AntoninRykalsky\Entity\Commision::FIXED, "Verze" );
		$this->em->persist( $bc );

		$group = new \MoneyPoint\Entity\LevelDistributionGroup;
		$group->setGroup( "Verze", $bc  );
		$this->em->persist( $group );

		$this->em->flush();
		
		$arrayOfDistributions = array(
			1 => 
			array( 'level'=>1, 'points'=>20 ),
			array( 'level'=>2, 'points'=>"" ),
			array( 'level'=>3, 'points'=>"" ),
			array( 'level'=>4, 'points'=>"" ),
			array( 'level'=>5, 'points'=>"" ),
			array( 'level'=>6, 'points'=>"" ),
			array( 'level'=>7, 'points'=>"" ),
			array( 'level'=>8, 'points'=>"" ),
			array( 'level'=>"", 'points'=>"" ),
			array( 'level'=>"", 'points'=>"" )
		);
		$unitType = 2;
		
		$rankGroup = $this->fixedCommisionFacade->storeDistributionTable( $bc->getId(), $arrayOfDistributions, $unitType );
		$baseCommision = $rankGroup->getBaseCommision();
		Assert::type('\MoneyPoint\Entity\LevelDistributionGroup', $rankGroup);
		Assert::type('AntoninRykalsky\Entity\CommisionEntity', $baseCommision);
		
		$rankGroup = $this->fixedCommisionFacade->storeActive( $baseCommision->getId(), 1 );
		#\Doctrine\Common\Util\Debug::dump($baseCommision);
	}
}

$testCase = new Test;
$testCase->run();