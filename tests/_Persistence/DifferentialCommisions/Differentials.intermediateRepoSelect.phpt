<?php
require __DIR__ . '/../bootstrap.php';
$container = \Nette\Environment::getContext(); 


/**
 * Ověří výpočet
 */


class intermediateResult extends Tester\TestCase
{

    public function setUp() {
        $container = \Nette\Environment::getContext();
		$this->em = $container->getByType("AntoninRykalsky\EntityManager")->getEm();
    }
	
	public function testTwo( $periodId = 4 )
	{
		$q = $this->em->createQuery("select pn, c1, pn2, c2 "
				. "from \MoneyPoint\Entity\PeriodNetwork pn "
				. "join pn.carrier as c1 "
				. "join pn.childrenCommision as pn2 "
				. "join pn2.carrier as c2 "
				. "WHERE pn.period = :period AND pn.idu = 23 ORDER BY pn.idu ");
		$q->setParameter('period', $this->em->getReference('MoneyPoint\Entity\Period', $periodId ) );
		$pns= $q->getResult();
		
		/* @var $pn \MoneyPoint\Entity\PeriodNetwork */
		foreach( $pns as $pn )
		{
			$c1 = $pn->getCarrier();
			\Tester\Assert::true( $c1 instanceof \MoneyPoint\Entity\Carrier );
			
			$ch = $pn->getChildrenCommision();
			\Tester\Assert::true( count( $ch) == 3 );
			foreach( $ch as $pn2 )
			{
				$c1 = $pn->getCarrier();
				\Tester\Assert::true( $c1 instanceof \MoneyPoint\Entity\Carrier );
			}
				
		}
	}

}

$testCase = new intermediateResult;
$testCase->run();

