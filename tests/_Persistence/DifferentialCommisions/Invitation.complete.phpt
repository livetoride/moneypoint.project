<?php

require __DIR__ . '/../bootstrap.php';
use Tester\Assert;

/**
 * Slouží pouze k vygenerování pozvánky. Moc nic neověřuje.
 */
class GreetingTest extends Tester\TestCase
{

	/** @var \MoneyPoint\DirefentialCommisionService */
	protected $direfentialCommisionService;
	protected $em;
	/** @var \MoneyPoint\ApplicationSetup */
	private $applicationSetup;
	
	private $loginModel;
	
    public function setUp() {
        $container = \Nette\Environment::getContext();
		$this->direfentialCommisionService = $container->getByType('\MoneyPoint\DirefentialCommisionService');
		$this->em = $container->getByType('\AntoninRykalsky\EntityManager')->getEm();
		$this->applicationSetup = $container->getByType('\MoneyPoint\ApplicationSetup');
		$this->loginModel = $container->getByType('\AntoninRykalsky\Login');
		
		\DAO\UserInvitation::get()->deleteWhere('1=1');
    }

    public function testTwo() {
		
		\AntoninRykalsky\SystemUser::get()->logMeAs( 22 );
		
		$g = new \AntoninRykalsky\PersonGeneratorService();
		$users = $g->getUserTestingData();
		
		foreach( $users as $user )
		{

			$data = array(
				'jmeno' => $user['jmeno'],
				'prijmeni' => $user['prijmeni'],
				'email' => $user['email'],
				'text' => 'Dobrý den. Na základě Vaší žádosti Vám zasíláme registrační link pro registraci do systému moneypoint. Moneypoint nabízí nejvyšší odměny za nákupy v ČR a SR a spoustu dalších služeb v oblasti nákupů, úspor nebo investic.',
				'idu' => 22
			);
			$agree = @$data['agree_nonstandard'];

			$invitation = \MoneyPoint\Invitation::create($data, (bool)$agree );
			$invitation->send();

			break; # just one 
		}
		
		
		
	}

}

$testCase = new GreetingTest;
$testCase->run();
