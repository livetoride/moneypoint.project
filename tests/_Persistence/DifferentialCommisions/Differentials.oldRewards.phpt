<?php

require __DIR__ . '/../bootstrap.php';
use Tester\Assert;

/**

 */
class GreetingTest extends Tester\TestCase
{

	protected $em;
	
    public function setUp() {
        $container = \Nette\Environment::getContext();
		$this->em = $container->getByType('\AntoninRykalsky\EntityManager')->getEm();
    }

    public function testTwo() {
		
		$repo = $this->em->getRepository("\MoneyPoint\Entity\PeriodNetwork" );
		$toreward = $repo->getEntitiesForAssigningOneCommision( 4, 23 );

		Assert::same(1300, $toreward['direct'] );
		Assert::same(1030, $toreward['network'] );
	}

}

$testCase = new GreetingTest;
$testCase->run();
