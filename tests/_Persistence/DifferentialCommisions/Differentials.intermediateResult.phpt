<?php
require __DIR__ . '/../bootstrap.php';
$container = \Nette\Environment::getContext(); 


/**
 * Ověří výpočet
 */


class intermediateResult extends Tester\TestCase
{

    public function setUp() {
        $container = \Nette\Environment::getContext();
//		$this->em = $container->getByType("AntoninRykalsky\EntityManager")->getEm();
		$this->mockista = new \Mockista\Registry();
    }
	
	public function mockCommision( $carrierKey, $carrier, $carrierFrom, $carrierTo, $coef, $networkPoints, $directPoints )
	{
		$commisionParent = new \MoneyPoint\Entity\PeriodNetwork();
		$carrierParent = new \MoneyPoint\Entity\Carrier;
		$carrierParent->setCarrier( $carrierKey, $carrier, $carrierFrom, $carrierTo, $coef );
		$commisionParent->setCarrier($carrierParent);
		$commisionParent->_setParentPoints($networkPoints, $directPoints);
		return $commisionParent;
	}

	public function getData()
	{
		$data = array();

		$parent1 = $this->mockCommision( 1, "DIAMOND I", 10000, 20000, 0.325, 13200, 500 );
		$son11 = $this->mockCommision( 1, "BRONZE", 0, 4000, 0.1, 400, 400 );
		$parent1->_addChildrenCommision( $son11 );
		$network1=array( 90.0 );
		$direct1=array( 130.0 );
		$data[] = array( $parent1, $network1 , $direct1 );
		
		$parent2 = $this->mockCommision( 1, "DIAMOND I", 10000, 20000, 0.325, 13200, 500 );
		$son21 = $this->mockCommision( 1, "BRONZE", 0, 4000, 0.3, 800, 0 );
		$parent2->_addChildrenCommision( $son21 );
		$son22 = $this->mockCommision( 1, "BRONZE", 0, 4000, 0.3, 4400, 0 );
		$parent2->_addChildrenCommision( $son22 );
		$son23 = $this->mockCommision( 1, "BRONZE", 0, 4000, 0.1, 4000, 4000 );
		$parent2->_addChildrenCommision( $son23 );
		$network2=array( 20.0, 110.0, 900.0 );
		$direct2=array( 0.0, 0.0, 1300.0  );
		$data[] = array( $parent2, $network2, $direct2 );
		
		
		return $data;
	}

	/**
	 * @dataProvider getData
	 */
	public function testTwo( $commisionParent, $network, $direct ) {
//		Tester\Environment::setup();s
		
		// you can create mock of existing class
		$container = \Nette\Environment::getContext();
		$direfentialCommisionFacade = $container->getByType("MoneyPoint\DirefentialCommisionService");
		
		/* @var $counted \MoneyPoint\Entity\PeriodNetwork */
		$counted = $direfentialCommisionFacade->countIntermediateResults( $commisionParent );
		$childrensCommision = $counted->getChildrenCommision();
		
		foreach( $childrensCommision as $k => $comission )
		{
			Tester\Assert::same( (int)$network[$k], (int)$comission->getParentNetworkReward() ); // odmena pro parenta od sona
			Tester\Assert::same( (int)$direct[$k], (int)$comission->getParentDirectReward() ); // odměna celkem (direct+total)
			
		}
	}
}

$testCase = new intermediateResult;
$testCase->run();

