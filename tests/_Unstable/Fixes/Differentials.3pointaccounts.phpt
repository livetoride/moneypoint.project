<?php

require __DIR__ . '/../bootstrap.php';

$container = \Nette\Environment::getContext(); 
/* @var $diferentialCommisionAssignerFacade \MoneyPoint\DiferentialCommisionAssignerFacade */
$diferentialCommisionAssignerFacade = $container->getByType("MoneyPoint\DiferentialCommisionAssignerFacade");

/**
 * POZOR: Opraví změny ( ne zůstatky ) v bodových kontech podle tabulky objemových odměn
 */

$diferentialCommisionAssignerFacade->reassignCommisions();