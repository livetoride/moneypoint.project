<?php

require __DIR__ . '/../bootstrap.php';

$container = \Nette\Environment::getContext(); 
$em = $container->getByType("AntoninRykalsky\EntityManager")->getEm();

/**
 * Přepisuje špatnou historii, v bodových kontech přepočítává zůstatky po transakcích
 * v případech kdy jsou změněny změny v kontech.
 */


$creditAccounts = $em->createQuery(
		'select ca from MoneyPoint\Entity\CreditAccount ca '
		. 'join ca.owner u '
		. 'order by u.idu, ca.id ASC');
$creditAccountByUser = array();
foreach( $creditAccounts->getResult() as $ca )
{
	if( empty( $creditAccountByUser[ $ca->getOwner()->getIdu() ] ))
	{
		$creditAccountByUser[ $ca->getOwner()->getIdu() ] = array();
	}

	$creditAccountByUser[ $ca->getOwner()->getIdu() ][] = $ca;
}


foreach( $creditAccountByUser as $userId => $collection )
{
	$last = 0;
	foreach( $collection as $creditAccount )
	{
		$actual = $creditAccount->getLeft();
		$correct = $last + $creditAccount->getChange();
		
		if( $actual !== $correct )
		{
			echo "UŽIVATEL $userId MA V BOD.TRANSAKCI {$creditAccount->getId()} zustatek $actual - ma byt $correct\n";
			$creditAccount->fixLeft( $correct );
		}
		$last = $creditAccount->getLeft();
	}
}
$em->flush();
