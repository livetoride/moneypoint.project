<?php

require __DIR__ . '/../bootstrap.php';

$container = \Nette\Environment::getContext(); 
$em = $container->getByType("AntoninRykalsky\EntityManager")->getEm();

/**
 * Přepisuje špatnou historii, v bodových kontech přepočítává zůstatky po transakcích
 * v případech kdy jsou změněny změny v kontech.
 */


$pointAccounts = $em->createQuery(
		'select pa from MoneyPoint\Entity\PointAccount pa '
		. 'join pa.owner u '
		. 'order by u.idu, pa.id ASC');
$pointAccountByUser = array();
foreach( $pointAccounts->getResult() as $pa )
{
	if( empty( $pointAccountByUser[ $pa->getIdu() ] ))
	{
		$pointAccountByUser[ $pa->getIdu() ] = array();
	}

	$pointAccountByUser[ $pa->getIdu() ][] = $pa;
}


foreach( $pointAccountByUser as $userId => $collection )
{
	$last = 0;
	foreach( $collection as $pointAccount )
	{
		$actual = $pointAccount->getLeft();
		$correct = $last + $pointAccount->getChange();
		
		if( $actual !== $correct )
		{
			echo "UŽIVATEL $userId MA V BOD.TRANSAKCI {$pointAccount->getId()} zustatek $actual - ma byt $correct\n";
			$pointAccount->fixLeft( $correct );
		}
		$last = $pointAccount->getLeft();
	}
}
$em->flush();
