<?php
require __DIR__ . '/../bootstrap.php';
$container = \Nette\Environment::getContext(); 

/* @var $direfentialCommisionFacade \MoneyPoint\DiferentialCommisionFacade */
$direfentialCommisionFacade = $container->getByType("MoneyPoint\DiferentialCommisionFacade");


/**
 * POZOR: 
 * - Přepočítá objem bodů v síti pro parenta.
 * - výši provizí z objemových odměn - ale pouze v tabulce objemových odměn
 */

$periods = \dibi::query("select period_id from mp_period_network
group by period_id
order by period_id")->fetchAll();

#array_pop ( $periods ); # pop actual period - expecting before first of month

foreach( $periods as $periodDao )
{
	$direfentialCommisionFacade->fixPeriodNetworkPoints( $periodDao->period_id );
	$direfentialCommisionFacade->fixPeriodNetworkRewards( $periodDao->period_id );
}
