<?php
require __DIR__ . '/../bootstrap.php';
$container = \Nette\Environment::getContext(); 
$em = $container->getByType("AntoninRykalsky\EntityManager")->getEm();


$q = $em->createQuery("select pn from \MoneyPoint\Entity\PeriodNetwork pn ORDER BY pn.idu");
$pns= $q->getResult();

/* @var $pn \MoneyPoint\Entity\PeriodNetwork */
foreach ($pns as $pn )
{
	$parent = $pn->getUser()->getParentMember();
	if( $parent instanceof \MoneyPoint\Entity\Member )
	{
		if( $parent->getIdu() > 0 )
		{
			$pn->setParentIdu($parent->getIdu());
		}
	}
}
$em->flush();