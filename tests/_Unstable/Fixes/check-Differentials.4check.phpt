<?php

require __DIR__ . '/../bootstrap.php';
use Tester\Assert;

$periodNetwords = \DAO\MpPeriodNetwork::get()->findAll()->fetchAll();

$limit = 10;
$i=0;
foreach( $periodNetwords as $pn )
{
	if( $pn->total_reward == 0 )
		continue;
	
	echo "{$pn->period_id} user {$pn->idu} TOTAL {$pn->total_reward} \n";
	
	if( empty( $pn->transaction_id ))
	{
		echo " ^ has no transaction\n";
	} else {
		$t = \DAO\MpTransaction::get()->find( $pn->transaction_id )->fetch();
	}
	
	if( empty( $pn->transaction_id ))
	{
		echo " ^ has no point account change\n";
	} else {
		
		if( !empty( $t->points_account_id ))
		{
			$pa = \DAO\MpPointsAccounts::get()->find( $t->points_account_id )->fetch();
			#Assert::same( $pn->total_reward, $pa->points_change );
		} else {
			echo " ^ missing point account info\n";
		}
		
	}
	
	
	#if( $i<$limit ) $i++; else break;
}