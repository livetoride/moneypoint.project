<?php
require __DIR__ . '/../bootstrap.php';
$container = \Nette\Environment::getContext(); 


/**
 * POZOR:
 * - Nastaví period networks všem na 0.
 * - přepočítá je podle objednávek kreditů.
 */

/* @var $direfentialCommisionFacade \MoneyPoint\DiferentialCommisionFacade */
$direfentialCommisionFacade = $container->getByType("MoneyPoint\DiferentialCommisionFacade");
$direfentialCommisionFacade->recountPeriodNetworkPoints();