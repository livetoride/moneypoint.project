<?php

require __DIR__ . '/../bootstrap.php';

$container = \Nette\Environment::getContext(); 
$em = $container->getByType("AntoninRykalsky\EntityManager")->getEm();

/**
 * Datumy transakcí na 1.x
 */


class intermediateResult extends Tester\TestCase
{

    public function setUp() {
        $container = \Nette\Environment::getContext();
		$this->em = $container->getByType("AntoninRykalsky\EntityManager")->getEm();
    }
	
	public function testTwo( $periodId = 4 )
	{
		$q = $this->em->createQuery("select pn, t, p "
				. "from \MoneyPoint\Entity\PeriodNetwork pn "
				. "join pn.transaction t "
				. "join pn.period p "
				 );
		$pns= $q->getResult();
		
		/* @var $pn \MoneyPoint\Entity\PeriodNetwork */
		foreach( $pns as $pn )
		{
			$p = $pn->getPeriod();
			$t = $pn->getTransaction();
			$t->fixInsertTimestamp( $p->getStarts() );
		}
		$this->em->flush();
	}

}

$testCase = new intermediateResult;
$testCase->run();


