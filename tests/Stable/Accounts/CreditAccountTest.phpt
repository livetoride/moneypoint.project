<?php

require __DIR__ . '/../bootstrap.php';
use Tester\Assert;


class CreditAccountTest extends Tester\TestCase
{

    public function setUp() {
        $container = \Nette\Environment::getContext();
		$this->em = $container->getByType("AntoninRykalsky\EntityManager")->getEm();
		$this->mockista = new \Mockista\Registry();
    }
	
	public function getChanges()
	{
		return array(
			array(100, 100, 100, 200),
			array(100, -100, 100, 0),
			array(-100, -100, -100, -200)
		);
	}

	/**
	 * @dataProvider getChanges
	 */
    public function testTwo( $change1, $change2, $result1, $result2 ) {
		
		$creditAccountService = new \AntoninRykalsky\AccountableService;
		
		$owner = new \MoneyPoint\Entity\Member;
		$reason = 'Test';
		
		$oldTmp = new \MoneyPoint\Entity\CreditAccount;
		$oldTmp->setCreditChange($owner, $change1, $reason);
		$old = $creditAccountService->add( $oldTmp );
		\Doctrine\Common\Util\Debug::dump($old);
		
		$newTmp = new \MoneyPoint\Entity\CreditAccount;
		$newTmp->setCreditChange($owner, $change2, $reason);
		$new = $creditAccountService->add( $newTmp, $old );
		\Doctrine\Common\Util\Debug::dump($new);
		
		Assert::same( $result1, $old->getLeft() );
		Assert::same( $result2, $new->getLeft() );
	}
}

$testCase = new CreditAccountTest;
$testCase->run();
