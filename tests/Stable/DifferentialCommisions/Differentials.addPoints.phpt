<?php

require __DIR__ . '/../bootstrap.php';
use Tester\Assert;


class GreetingTest extends Tester\TestCase
{
	/** @var \MoneyPoint\DirefentialCommisionService */
	protected $direfentialCommisionService;

    public function setUp() {
        $container = \Nette\Environment::getContext();
		$this->direfentialCommisionService = $container->getByType('\MoneyPoint\DirefentialCommisionService');
		$this->mockista = new \Mockista\Registry();
    }

    public function testTwo() {
		
		$period = $this->mockista->create( "MoneyPoint\Entity\Period" );
		$period->expects('getId')->andReturn(5);
		$period->expects('__toString')->andReturn("5");
		
		$buyer = $this->mockista->create( "MoneyPoint\Entity\Member" );
		$buyer->expects('getIdu')->andReturn( 101 );
	
		$structure = array( 101, 45, 1 );
		
		$getCreditsSum = 4000;
		
		
		$accountStateHolder = array();
		$accountStateHolder[ $period->getId() ] = array();
		foreach( $structure as $idu )
		{
			$a = new \MoneyPoint\Entity\PeriodCreditState();
			$a->init($idu, $period);
			$accountStateHolder[ $period->getId() ][$idu] = $a;
		}
		
		$pointStates = $this->direfentialCommisionService->addDifferentialPoints( $buyer, $getCreditsSum, $period, $structure, $accountStateHolder );
		
		Assert::same( $pointStates[$period->getId()][101]->getDirectCreditsState(), $getCreditsSum );
		Assert::same( $pointStates[$period->getId()][45]->getCreditsState(), $getCreditsSum );
		Assert::same( $pointStates[$period->getId()][1]->getCreditsState(), $getCreditsSum );
		Assert::count( count($structure), $pointStates[$period->getId()] );
		
	}
}

$testCase = new GreetingTest;
$testCase->run();
