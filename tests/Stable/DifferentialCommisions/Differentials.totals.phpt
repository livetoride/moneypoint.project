<?php

require __DIR__ . '/../bootstrap.php';
use Tester\Assert;


class GreetingTest extends Tester\TestCase
{
	/** @var \MoneyPoint\DirefentialCommisionService */
	protected $direfentialCommisionService;

    public function setUp() {
        $container = \Nette\Environment::getContext();
		$this->direfentialCommisionService = $container->getByType('\MoneyPoint\DirefentialCommisionService');
		$this->mockista = new \Mockista\Registry();
    }

    public function testTwo() {
		
		$period = $this->mockista->create( "MoneyPoint\Entity\Period" );
		$period->expects('getId')->andReturn(5);
		$period->expects('__toString')->andReturn("5");

		$periodNetwork = new \MoneyPoint\Entity\PeriodNetwork;
		
		$groupedForUserId = 23;
		$values = array(
			'parentIdu' => 23,
			'network' => 1030,
			'direct' => 1300
		);
		
		$totalEntity = $this->direfentialCommisionService->assignTotalCommisionPerEntity( $groupedForUserId, $values, $period, $periodNetwork );
		
		Assert::same( $totalEntity->getNetworkReward(), 1030 );
		Assert::same( $totalEntity->getTotalReward(), 2330 );
		Assert::same( $totalEntity->getId(), null );
	}
}

$testCase = new GreetingTest;
$testCase->run();
