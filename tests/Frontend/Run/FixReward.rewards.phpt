<?php

require __DIR__ . '/../bootstrap.php';
use Tester\Assert;


class GreetingTest extends Tester\TestCase
{
    public function testCmsDocument() {
        
		\AntoninRykalsky\SystemUser::get()->logMeAs( 1 );

		$container = \Nette\Environment::getContext();
		
		
		// z DI kontejneru, který vytvořil bootstrap.php, získáme instanci PresenterFactory
		$presenterFactory = $container->getByType('Nette\Application\IPresenterFactory');

		// a vyrobíme presenter Sign
		$presenter = $presenterFactory->createPresenter('Front:FixReward');
		
		$presenter->autoCanonicalize = FALSE;
		
		// zobrazení stránky Sign:in metodou GET
		$request = new Nette\Application\Request('Front:FixReward', 'GET', array('action' => 'rewards'));
		$response = $presenter->run($request);
		
		Assert::true( $response instanceof Nette\Application\Responses\TextResponse );
		Assert::true( $response->getSource() instanceof Nette\Templating\ITemplate );
	}
	
}

Tester\Environment::setup();
$testCase = new GreetingTest;
$testCase->run();
