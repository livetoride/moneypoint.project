<?php

use Nette\Utils\Neon;

include_once( 'libs/nette/nette/Nette/loader.php' );

class MakeLinks
{
	private function getLinks()
	{
		$conf = Neon::decode( file_get_contents( $this->configurationFile ) );
		
		$links = array();
		foreach( $conf['baseLinks'] as $c )
		{
			$links[] = preg_replace('#(%projectDir%)#', $conf['projectDir'], $c );
		}
		foreach( $conf['otherLinks'] as $c )
		{
			$links[] = preg_replace('#(%projectDir%)#', $conf['projectDir'], $c );
		}
		
		# preparujeme na klic hodnota
		$links2=array();
		for( $i=0; $i<count($links); $i++ )
		{
			if( !($i % 2) && $i != count($links) )
			{
				$links2[] = array(
					$links[$i],
					$links[$i+1]
				);
			}
		}
		return $links2;
	}

	private function format($l)
	{
		return 'ln -s '.$this->dirname.'/'.$l[0].' '.$l[1];
	}
	
	private function formatWin($l)
	{
		$dir = $this->dirname; #$dir = preg_replace("#(\\\)#", '/', $this->dirname );
		$isDir = '';
		if( is_dir( $dir.'/'.$l[0] ))
		{
			$isDir .= '/D';
		}
		return 'mklink '.$isDir.' "'.$l[1].'" "'.$dir.'/'.$l[0].'"';
	}
	
	private $dirname;
	public function setDirname($d)
	{
		$this->dirname = dirname( __DIR__);
	}
	
	private $configurationFile;
	public function setConfigurationFile($configurationFile)
	{
		if( !file_exists($configurationFile))
		{
			echo "Soubor $configurationFile neexistuje";
			exit;
		}
		$this->configurationFile = $configurationFile;	
	}
	
	
	public function show( $type = 'lin' )
	{
		echo "\n";
		$links = $this->getLinks();
		foreach( $links as $l )
		{
			if( $type == 'lin')
				$command = $this->format( $l );
			else 
				$command = $this->formatWin( $l );
			echo $command."\n";
		}
		
	}
	
	public function delete( $type )
	{
		echo "\n";
		$links = $this->getLinks();
		foreach( $links as $l )
		{
			$command = "rm {$l[1]}\n";
			echo $command;
			if( $type == 'really' )
			{
				system( $command );
			}
			
			
		}
	}
	
	public function exec( $type )
	{
		echo "\n";
		$links = $this->getLinks();
		foreach( $links as $l )
		{
			if( $type == 'lin')
				$command = $this->format( $l );
			else 
				$command = $this->formatWin( $l );
			echo $command."\n";
			
			system( $command );
		}
		
	}
}

if( empty( $argv[1] ) || empty( $argv[2] ))
{
$a = "
Sw pro vytvareni potrebnych linku aplikace.

	init.php [configuration.ini] [exec|show] [win|lin]
	init.php [configuration.ini] [del] [really|show]

";
echo $a;exit;
}


$c  = new MakeLinks;
$c->setDirname( __DIR__ );
$c->setConfigurationFile( $argv[1] );
if( $argv[2] == 'exec' )
{
	$c->exec( $argv[3] );
} elseif( $argv[2] == 'del' ) {
	$c->delete( $argv[3] );
} else {
	$c->show( $argv[3] );
}


?>